
Trusting Nonsense
===================================

Questa è una raccolta di cose scritte nel passare del tempo e parecchio in ordine sparso. Sono per la maggior parte cose vissute, alcune inventate ma sempre con un fondo di verità.
Il _vero_ che si mescola nella semantica delle parole con tutti i possibili significati che altri vogliano attribuirvi.

I testi sono ordinati alfabeticamente in base al nome del file e assemblati automaticamente da un software, OxEpubCompiler, scritto in Ruby tanto tempo fa. Non cercate quindi di intravedere in qualsiasi punto comprese le date dei file, a meno che non sia indicato esplicitamente, **una qualsivoglia cronologia**.  

Ci saranno sicuramente tanti errori, generalmente scrivo e non correggo. Alcuni saranno ridicolmente stupidi, altri forse più gravi e da provvedere al più presto. Sarà mia cura nel caso e su segnalazione intervenire più o meno prontamente.

I testi sono protetti da una licenza CC-BY-NC-ND:

![CC-BY-NC-ND](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png "CC-BY-NC-ND")

*Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Unported License*

Siete liberi di leggere, commentare e maledirmi ma non potete sfruttare queste parole commercialmente a meno che non vi autorizzi io dietro lauto compenso.  

[**Repository**](https://gitlab.com/minimalprocedure/trusting_nonsense)

+ La cartella _Texts_ contiene i file.  
+ La cartella _build_, il file _epub_.  
+ Il pdf è generato tramite _Calibre_ e dato il tempo di conversione non è da considerarsi aggiornato all'epub, controllate la data di versione nel titolo.

*Massimo Maria Ghisalberti*
Copyright (c) (1980) 2012 .. 2025

<mailto:anzu@ik.me>

ps: sono ben accette donazioni via PayPal a zairik@gmail.com o come preferite.
