[title: il viaggio]

Mi preparo bene per quel viaggio
è per un cammino inclinato, tortuoso.

Colmo valigie di immagini che ho
una sull'altra per esserne sicuro.

Canovaccio da seguire negl'incontri
per, nella recita, non perdere battute. [m]
