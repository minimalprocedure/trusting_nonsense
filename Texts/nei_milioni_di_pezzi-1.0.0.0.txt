[title: nei milioni di pezzi]

Nei milioni di pezzi ho lasciato  
la strada di briciole nel bosco.  

Camminando lungo il sentiero  
ora so che i corvi mangeranno.  

Lasciando la via senza indizi  
facendo che non possa trovarmi. [m]
