[title: il valzer dei fiori]

La incontrò passeggiando lungo la strada.  
Lei era lì, appoggiata e pensosa.  
 
Le vide una lacrima sfuggire  
asciugarla veloce, come per vergogna.  

Allora le disse:  
<< Piangi per oggi, lascialo andare, domani ti porterò lontano,  
avrai fiori da raccogliere e labbra da baciare. >>  

Lei lo guardò e rise, lo prese sottobraccio.  
Camminando insieme, verso _quel lontano._ [m]  
