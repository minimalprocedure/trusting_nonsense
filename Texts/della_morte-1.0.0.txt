[title: della morte]

Tempesta, enorme Tempesta,  
squarci di impetuosa violenza  
corrodono tristi il soffio del vento.  

Gridano le anime insulse  
brulicanti di solfurea vita.  

Sghignazza il mio demone folle  
quando chiuso in una stanza, penso,  
e uccido con violenza i miei ricordi,  
dissolti in aloni sepolcrali.  

Roteando gli occhi  
vedo l'insoluto intorno  
e riconosco la visione, l'incanto,  
le lacrime che il mio specchio sgorga.  

Il riflesso, l'angosciato riflesso   
del mio sangue tra le mani.  

I miei alberi, la paura,  
la folle corsa contro il tempo.  

No, non la vita  
ma splendidi glaciali gioielli.  
Cascate di rubini tintinnanti  
che tutto travolgono e distruggono.  

Solo l'ombra del corvo  
che assisterà alla mia rovina,  
solo un enorme fragore e spruzzi di cervello  
imbratteranno le vostre facce.  

Tempesta, dolce Tempesta  
non può essere che ciò, la fine. [m:1982]
