[title: Aspetto e non ho fretta, del tempo.]

Come per conservare le buone parole,  
quando attendi ogni cosa avverarsi.  
 
Sulle spiagge adagiato mi stendo,  
che non sentono e che non volgono.  
 
Il pulsare dei nervi pronti allo scatto  
la vita, scorre e va nel tempo. [m:engine]  
