[title: la parola sogni nei miei scritti]    
    
➜  Texts git:(master) ✗ ack-grep sogni
congelo_i_sogni-1.0.0.txt
1:[title: congelo i sogni]
3:Congelo i sogni a volte    
    
cameretta-1.0.0.0.txt
3:C'è una camera nei miei sogni, una camera non grande ma infinita, ha due porte e a volte tre, non sono chiuse a chiave. Al centro un letto quadrato, altare dei momenti, su cui poggiare i sacrifici dei sensi.      
    
digressioni-1.0.0.txt
9:dorme senza sogni,    
    
la_solitudine_dell_uomo_solo-1.0.0.0.txt
6:Nella mente che dei sogni vaghi    
    
errore_finale-1.0.0.0.txt
1:[title: l'errore finale - poemetto finale de 'Il tramonto dei sogni' (1986)]    
    
sollevano-1.0.0.txt
12:Muoiono i sogni della vita    
    
aspetto-sogni-rotti-nei-pezzi.md
1:[title: Aspetto i sogni rotti, nei pezzi.]    
    
il_tempo_per_i_sogni-1.0.0.0.txt
1:[title: il tempo per i sogni]
3:Un tempo per i sogni    
    
buongiorno-2.0.0.0.txt
13:È il buongiorno delle dieci e mezzo, di quelli che sembra aver dormito troppo, di quelli in cui è vero che ci si è svegliati presto, alle quattro, con i pensieri e i brutti sogni ma che poi si è ripreso sonno allo spuntar dell’alba. Sono quelle mattine dopo le sere prima che vengono dopo i pomeriggi prima, le mattine prima. Sono quelle ore che si susseguono, spesso sempre uguali, in cadenza ritmica. Colazione, pranzo e cena. Notte. Mattino.    
    
lasciare-1.0.0.0.txt
14:Lasciati entrare, divarica sogni da bambino    
    
io_sono-2.0.0.0.txt
10:nei sogni, nelle paure del buio. Io sono. [m]    
    
sogni-2.0.0.0.txt
1:[title: i sogni]
3:I sogni vanno seminati su terreni impervi, aridi.     
    
i_sogni_impossibili-1.0.0.0.txt
1:[title: i sogni impossibili]
6:Tutto un costruire di mondi, realtà che vivono nella proprio testa, loro sono vere, palpabili, sono sogni e lasciano quel piacere addosso che nessuno potrà mai portarti via.    
    
divenire-1.0.0.0.txt
6:Alla sera, tiro lontano quei sogni noti    
    
come_gargolla-1.0.0.0.txt
7:al lento sospiro dei sogni,    
    
giorni_dopo_giorni-1.0.0.0.txt
9:Nel ristoro dei radi sogni la notte    
    
artista_della_delusione-1.0.0.0.txt
36:Costruisco sogni vividi,    
    
dove_sei-1.0.0.txt
5:e dove i sogni terminano nel buio.
30:i sogni di nascere e volare.    
    
le_follie-1.0.0.0.txt
5:Nel circo arcano, gli animali dei sogni    
    
voi_che_siete-1.0.0.0.txt
10:raccolto e avvolto nelle spire dei grandi sogni.    
    
buongiorno_al_sogno-1.0.0.0.txt
11:Sogno? Forse, ma i miei sogni sono sempre molto complicati. Si dipanano in enormi cittadine, piene di vicoli, scale e cancelli.
13:Faccio sempre una fatica tremenda nei miei sogni.
16:Faccio sogni ripetitivi, ve ne racconto uno fatto tempo fa per molte volte di seguito.    
    
scrivere_il_drago-1.0.0.txt
10:per volare nei suoi sogni...    
    
claudio_ricordo-1.0.0.txt
67:Un saluto, e vieni a trovarmi ancora nei miei sogni. [m:2003]    
    
la_donna-1.0.0.0.txt
3:La donna dei miei sogni, danza    
    
metti_una_sera_a_cena-2.0.0.0.txt
35:Se ci penso mi piacerebbe farmi scopare qui nel bagno, appoggiata alla parete. Quasi quasi adesso vado di là e glielo dico, lo facciamo qui a metà pranzo e chi se ne frega se entra qualcuno e ci vede. Bagno dei maschi o quello delle femmine? bella domanda. Forse quello dei maschi sceglierei, uno dei miei sogni proibiti, farmi prendere mentre altri mi guardano e magari si fanno una sega. Non lo so se lo farei mai davvero, ma l'idea sarebbe eccitante.    
    
paura-1.0.0.0.txt
28:di sogni non comuni.    
    
gli_esseri_delle_montagne-1.0.0.txt
8:le creature dei sogni, morte e vive,    
    
nei_offuscati-1.0.0.0.txt
1:[title: nei sogni offuscati]
3:Nei sogni offuscati, alle notti    
    
il_tuo_sogno_bello-1.0.0.0.txt
3:Ognuno ha una valigia dei sogni appartenuta a un altro.
6:Le foglie dei sogni sono scritte in lettere piccole    
    
un_pezzo-1.0.0.txt
3:Un pezzo dei sogni    
    
porgi_la_mano-1.0.0.txt
6:ci racconteremo dei sogni e del vissuto    
    
buco_nero-1.0.0.0.txt
4:le materie primordiali, l'energia dei sogni.    
    
il_tramonto_dei_sogni-1.3.2.txt
1:[title: il tramonto dei sogni]
256:La camera spoglia con le pareti trasparente vetro e il mare sopraggiunge da lontano, il sole con il vento fresco e la sabbia scura, è un movimento veloce di sogni fugaci che restano irrimediabilmente scritti e non corretti, velocemente abbandonati.
307:Ci sono molti libri di molte pagine ingiallite e rosicchiate dai topi, che vivono nelle vecchie case di pietra. Stazioni di transito, case dalle finestre nere, dove si susseguono popoli misteriosi di occhi sottili e abiti lunghi. Le storie si susseguono, un film veloce accelerato, culla dei sogni, rapito dagli incubi e incatenato a morti visionarie, falciato da acque battenti.
315:Non sono dio, queste mille prove, le immagini sfuggono sacre al sangue, i corpi che si crede di pensare, le piccole mani, la pelle confusa con seta e pizzo, un occhio predatore che stampa i sogni e serra nei bauli di ottone roboanti e fastidiosi inviluppi.    
    
confessioni_ad_una_sorella-1.0.0.0.txt
5:i sogni di ieri e lo sguardo.    
    
una_commedia-1.0.0.txt
37:     Modella i sogni a tuo piacimento e tutto vivrà.    
    
nelle_parole_delle_cose-1.0.0.0.txt
17:dei sogni che si frangono sempre    
    
francesca-1.0.0.txt
7:i sogni di ieri e lo sguardo.    
    
nei_momenti-1.0.0.txt
7:Mancano i sogni di ogni volta    
    
sempre-un-poco.md
10:la donna dei miei sogni, danza. [m:engine]      
    
raccontare_il_sacco-1.0.0.0.txt
7:sogni andati senza niente in cambio.    
    
le_cose_mai_avvenute-1.0.0.0.txt
9:I sogni, veleno dell'anima, maledetti ingannatori    
    
metti_una_sera_a_cena_1_2-1.0.0.0.txt
64:Se ci penso mi piacerebbe farmi scopare qui nel bagno, appoggiata alla parete. Quasi quasi adesso vado di là e glielo dico, lo facciamo qui a metà pranzo e chi se ne frega se entra qualcuno e ci vede. Bagno dei maschi o quello delle femmine? bella domanda. Forse quello dei maschi sceglierei, uno dei miei sogni proibiti, farmi prendere mentre altri mi guardano e magari si fanno una sega. Non lo so se lo farei mai davvero, ma l'idea sarebbe eccitante.    
    
in_questa_mattina-1.0.0.txt
3:(anche solo nei sogni...)    
    
ti_leggo_la_sera_racconti_bellissimi-1.0.0.0.txt
7:per una notte di bei sogni. [m]    
    
mi_chiedo_dei_volti-1.0.0.txt
11:coi rami avvolti di sogni e speranze...    
    
la_parola_silenzio-1.0.0.0.txt
77:il_tramonto_dei_sogni-1.3.2.txt        
    
expo2015-1.0.0.0.txt
18:Il vibratore anatomico col cambio automatico e la donna dei sogni robotica.    
    
solitudine_compagna-1.0.0.txt
18:Le parole che raccontano dei sogni mai nati con gli uomini lasciati,    
    
la_valigia_dei_sogni-1.0.0.0.txt
1:[title: la valigia dei sogni]
4:aperta, abbandonata, piena dei sogni di qualcuno.    
    
il_volo_della_notte-1.0.0.txt
12:nei sogni bagnati    
    
guardo_un_orologio-1.0.0.0.txt
8:Una gira veloce, è quella dei sogni, è quella delle speranze,    
    
la_memoria_dei_sogni-1.0.0.0.txt
1:[title: La memoria dei sogni.]
13:Il sole mi esplose nelle orbite, il nero si impossessò della mia luce e il liquido verde bollì e si rapprese, un calore insopportabile, gridavo avvolto in quell'albume filante. I sogni si squarciarono e ricordai il nome.    
    
amore_mio_perso-1.0.0.txt
3:(Di quei momenti, quando vorresti uccidere i sogni,     
    
non_posso_che_giocare_con_le_mani-1.0.0.0.txt
19:Il demone dei sogni, che trascenda dal probabile per portarti nel possibile) [m]    
    
nei_pezzi-1.0.0.0.txt
6:Aspetto i sogni rotti, nei pezzi    
    
le_armi-1.0.0.0.txt
3:Dimmi i tuoi sogni, quelli di notte    
    
sogni-1.0.0.0.txt
1:[title: sogni]
3:Ho avuto in passato due sogni ricorrenti:    
    
