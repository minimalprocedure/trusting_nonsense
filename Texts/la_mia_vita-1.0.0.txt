[title: la mia vita]

La mia vita cerca la morte da sempre  
per amarla poi in tutti i suoi sensi.  

Ricerco l'arte in ogni anfratto  
per poter creare, domare e plasmare.  

Ho voglia di un amore enorme adesso.  
Di un amore che non avrò mai   
dovunque cerchi, dovunque vada.    

Un amore perduto e mai arrivato. [m:tempo fa]  
