[title: del vestire]

Nella vita mi sono vestito in modi terribili, terribili a detta di alcuni.

I miei, che mi hanno comprato mille giacche e cravatte, delle cravatte li ho sempre ringraziati, mi sono servite in molte occasioni anche se non le ho messe. So fare dei nodi alle cravatte molto interessanti.

Alcune delle mie donne, finché stremate, mi hanno sistemato con un bel vaffanculo ed hanno desistito.

Nel mio passato di Dandy, Dandy-Dark, Dark e oggi stile barbone…

Forse perché tutto sommato l’aspetto non mi è difettato molto nella vita (mi dicono) non ho mai badato al vestito, considerando l’intelligenza qualcosa di superiore all’aspetto e specialmente al __vestito__.

Mi vesto casual, decisamente casual: casualmente mi metto le cose pulite che sono buttate nell’armadio. [m]

