[title: gli dei che vanno]

Quando li guardi allontanarsi
le cose vecchie, gli dei che ti raccontano.

Brutti, bassi e troppo umani
lasciano il sapore del vivere libero

mentre se ne vanno da questa terra,
sorreggendosi tra loro. [m]
