[title: I need to surrender]

I need to surrender
================================================================================

Pam, I need to surrender and to give up, and quit.  
Maybe, I just need to share that pain that eats me inside, a pain of impotence to the outside world. Impotence that is difficult to explain to the world that grips me, my digging in the thick air that I struggle to breathe. I look out the window and find a wall, which is transparent and lets me see the outside but don’t make me go.  

Maybe my biggest failure was that I didn’t die when I should die, and now I’ve taken the stone and pushing forward, like a rock that grows ever more, without being able to feed myself or drink.  
As a modern Tantalus, everything always escapes from my hands.  

— 	John Dapper Jones, The Brave New World And Beyond
