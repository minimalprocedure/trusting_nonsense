[title: thinking]

thinking  
================================================================================

“What do you think of me, Pam?”    
“I think you’re wonderful.”    
“And why we’re not together then?”    
“Because you’re too much…”    

— 	John Dapper Jones, The Brave New World And Beyond
