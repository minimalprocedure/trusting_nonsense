[title: on this day]

on this day
================================================================================

Pam,  
on this day that draws to a close, in these shorter days of the year fall, I want to write.  
Shrunken fingers that I've are slow for written word and move in an uncertain use by so forgotten pen. Paper, seems so rough and the stylus digging on it.   

You know, once I wrote a lot and for a long time, but today everything is so fast too.  
Missing the wait and postman peek through the window. Its check in the maroon bag, his rise and fall from the bike. His push to get up here, up the hill.  
I thought a lot I must shrive, to our meeting. So fleeting, irish coffee for me... And you?  
I can't remember as I was lost in your eyes. a kid that I did in my few stuttered words.  
The handshake and the our: “Good morning, how are you?”     

Time was a flash, it was much or little, slow or fast? I know nothing to say, in my head are just confusing words.  
Who among us then chose the table? Perhaps, I think maid has sensed our situation and giving us the one in the corner. Perhaps we are privileged, perhaps...  
What embarrassment!, Only hands or hug and kiss immediately?    

Good manners, they are taught from an early age. Instilled at the sound of the scolding sound, those make us shaking hands instead of jump on.
We are adults and we should behave appropriately. You, however, would not want to run naked on the beach full of people, for one time? You would not want to have sex on a busy square? Lock the office lift in order to make love?
We're adults. Words that all people repeat and repeat.   

I would like living in a huge dream, where everyone sleeps and wakes and where everything is possible. Running with open arms with closed eyes and collide and fall laughing.  

In this world of darkness and black smoke, you shine.  

— 	John Dapper Jones, The Brave New World And Beyond
