[title: il sogno gravido]

il sogno gravido
================================================================================

Pamela,   
il nostro sogno è così gravido e le parole pregne di noi. Il tuo viso mentre seduta guardi nel vuoto per raggiungermi. I tuoi capelli neri, prima lunghi e ora corti, il viso dall'accenno triste e il sorriso che illumina la stanza.  

Mia Selvaggia.  

— 	John Dapper Jones, The Brave New World And Beyond (trad.)
