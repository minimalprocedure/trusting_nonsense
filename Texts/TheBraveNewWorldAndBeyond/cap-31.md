[title: dreaming]

dreaming
================================================================================

“This night, I had a dream. I dreamed a dream, that I should not have to do and that I try hardly to forget.  
A dream where everything was perfect and I’ve looked forward, with nothing more behind.  
Pam, I looked beyond the horizon with my hands no longer empty.”   

“My Lord, you know where are my hands.”    

— 	John Dapper Jones, The Brave New World And Beyond
