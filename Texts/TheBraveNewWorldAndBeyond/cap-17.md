[title: come here]

come here
================================================================================

“Pam, what is love?”    
“You are love, My Lord.”    
“Don’t kidding Pam, tell me really.”    
“Love is being in balance as a tightrope walker without pole, forget on the morning and remember on the evening. It’s dangers and fearful to lose and regain the strength. It’s pain at the slightest hint and immense joy, in the after. It’s losing and finding for a thousand and thousand times and a reach out blindly. Love is trust.
Love is dying all the time being reborn, every time.”    

“I love you, Pam.”    

“Even I, My Lord.”    

— 	John Dapper Jones, The Brave New World And Beyond
