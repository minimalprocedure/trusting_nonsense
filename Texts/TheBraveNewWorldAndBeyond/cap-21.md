[title: the last fallen]

the last fallen
================================================================================

“Pam, My Queen, I am the last fallen.
I will die in the time of void, with no more you in body and mind, with no more hope, with my little flame off. Pam, I would pray long and highly and far and wait and hope and plan for your return, but I could not make it.
Infinite palpable and carnals dreams…”    
 
“John, My Lord, how can I reason you? How best telling you that I’m yours?
I would open the chest and getting you in, join hearts like siamese twins, lungs to breathe together…”    

— 	John Dapper Jones, The Brave New World And Beyond
