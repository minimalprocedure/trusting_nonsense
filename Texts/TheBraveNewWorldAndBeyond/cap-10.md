[title: waiting]

waiting
================================================================================

“Pam, it’s so difficult at time.”  
“I know My Lord, I know that, it’s not easy.  
I wish could lose at least for only once in you and you into me.  

It ’s crazy amazing how after this so much time and distant place, after all, often pampered into the silence of our thoughts and the beating of our hearts, I still feel so and so close, and so feel myself so yours.  
This strong and intrinsic need to breathe you.  
I’m always waiting, at least in my dreams and often in my open eyes thoughts.”    

— 	John Dapper Jones, The Brave New World And Beyond
