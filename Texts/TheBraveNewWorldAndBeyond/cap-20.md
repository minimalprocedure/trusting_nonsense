[title: the last one fell]

the last one fell
================================================================================
Pam.

I am the last one fell.  
The one playing horn at rising of giants  
warning the Bridge of his come, near end.  

The heavy tanks from shod horses  
maces and axes and swords and bows.  

Gods will fall, one by one, tamed gods  
the wolf who kills _allfather_, all will die.  

The last fell, killing the foolish betrayer  
the death for mindless god, and then to die.  
 
— 	John Dapper Jones, The Brave New World And Beyond
