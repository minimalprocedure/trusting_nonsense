[title: thanks my lady]

thanks my lady
================================================================================

Thanks My Lady, you’re always so… so dear.  
I don’t know how much… how much I would forsake me in your arms, closing eyes and thinking nothing.  
Locked in a room, me and you for one day or two or three … or more.  
Talking and laughing, eating sandwiches on the bed filled by crumbs, or… a pizza in the box? Please, make me laugh and play strong with me.  
I touch you and then you will touch me and please, please please, help me to forget everything.  

— 	John Dapper Jones, The Brave New World And Beyond
