[title: asking me first]

asking me first
================================================================================

Many people feel duty to say something or much about me, without asking me first.  
Does that make me an open book? Or just a forgotten book that everyone says that have read?  

— 	John Dapper Jones, The Brave New World And Beyond
