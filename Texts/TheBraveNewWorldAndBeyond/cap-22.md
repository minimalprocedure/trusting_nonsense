[title: leaving]

leaving
================================================================================

“Pam, I would tell you that I’m leaving the light going off, I would tell you that all in me is lost, my gaze that goes a little further, as the blind that gropes in darkness.   
I would tell you how you can own me in my whole being.  
Trembling at the thought of your body while my hands grasping the air. My utter lack of a sense for life.  
Let me forget everything and hold me into, take me with you, kiss deeply and not let… let me go…”   

“My Lord I’m here, and I’ll take you into all myself.”    

— 	John Dapper Jones, The Brave New World And Beyond
