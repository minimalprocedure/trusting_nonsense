[title: a day of another day]

a day of another day
================================================================================

Pam, today is another day, another day of another day.   
They are the ones who make years as time passage.  
I get old faster than the carry of time, one step after step walking away and approaching the end.  
 
You sleep Pam, and I watch you breathing slowly.  

— 	John Dapper Jones, The Brave New World And Beyond
