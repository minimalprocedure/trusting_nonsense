[title: butterfly]

butterfly
================================================================================

“You are my butterfly, a butterfly on my hand that I view floating in my eyes, around the lines of a long distance tree.
You are my butterfly, closed in my hands. 
A prisoner who can escape when she wants.”    

“I won’t fly away never, My Lord.”    

— 	John Dapper Jones, The Brave New World And Beyond
