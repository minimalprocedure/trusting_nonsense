[title: without words]

without words
================================================================================

“My Lady,  
you are a forbidden game, this is what you are, so alive in my mind.  
Thinking of you pervades all the others as soon as I remember.  
An elusive catch, for a thousand faults and delays, I smile at closed eyes moving my head slightly.”    
 
“Without words, My Lord…  
My heart is turmoil as always when it happens, when I’m tangled to you. Bright eyes and "butterflied stomach", reading those few and deep lines that concern me.
Hear my name written by you and just for me.”    

— 	John Dapper Jones, The Brave New World And Beyond
