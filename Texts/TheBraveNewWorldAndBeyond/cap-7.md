[title: sometimes]

sometimes
================================================================================

“Sometimes Pam, I think that I’m for you just a hiccup.”    
“No, you’re not, you are mine and I am your… in part…”    
“You’re always in part… in part… in part… you… you, ask me to trust you but you don’t trust me!”    
“I trust you, in part…”    

— 	John Dapper Jones, The Brave New World And Beyond
