[title: life]

life
================================================================================

“Pam, life is the all times biggest exposure of lies.  
We can also face it right with the truth, at the risk of succumbing in adventure.  
Tonight, at the vernissage we go armed.”    
“My Lord, then we will go naked?”    
“Of course, naked and smelling of sex.”    

— 	John Dapper Jones, The Brave New World And Beyond
