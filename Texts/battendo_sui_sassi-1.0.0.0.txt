[title: battendo sui sassi]

I rumori acuti delle bacchette
che battono sui sassi.

Rimbalzano tra gli occhi
le immagini di un anziano ricordo. [m]
