[title: il demone]

Ti alzi, poi cammini, ti volti, quindi cauta guardi  
eccole, le tante paure sono affettuose compagne.  

Nella via che in avanti piano ti porterà laggiù  
è tutto il trascorrere delle tue malcelate passioni.  

Ora è qui che sei giunta, qui sei davanti alla porta  
col demone appoggiato di lato che porge la chiave. [m]  

