[title: Case e stanze dove poter essere.]

Aspetto che torni, così mi ha detto  
per non cadere da poco.  
 
Non posso che giocare con le mani,  
l'indomani mi trasferiscono a medicina.  
 
Quello che non so dire quando ti vedo  
a spirale avvolge il viso. [m:engine]  
