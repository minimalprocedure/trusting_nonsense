[title: Un po' triste del tempo perduto.]

Li portano fieri, da amanti lasciati  
rompo le barriere che dividono.  
 
L'albero anciente dei forti rami  
abbassa appena che possa sfiorarti.  
 
Sono io, mi fai salire? al citofono  
forse siete tutti solo proiezioni e niente esiste. [m:engine]  
