[title: Una mano stretta dove la vita nasce.]

Proiettando le tristezze su uno sfondo bianco  
attirato da un cappellino d'epoca con la veletta nera.  
 
Ci abbandoniamo attivi e passivi  
la luna calava ormai sotto l'orizzonte.  
 
Miseria, caduta di principi, ideali  
guardando un fazzoletto volteggiare nell'aria. [m:engine]  
