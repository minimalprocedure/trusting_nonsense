[title: Invoco dio a volte e mi vedo allo specchio.]

Che mi dona la sua vecchiaia,  
dove per un attimo posseduto.  
 
Ascolto il suono  
ci abbandoniamo attivi e passivi.  
 
Quello che da il significato alla fine  
sapore di vittime sulle labbra socchiuse. [m:engine]  
