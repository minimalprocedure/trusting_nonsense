[title: Sul mio fronte una scacchiera bianca e nera.]

La spada è legata ad un salice triste  
dentro quella vendetta da stregone.  
 
Ma forse era soltanto solo  
miseria, caduta di principi, ideali distorti.  
 
Guardando un fazzoletto volteggiare nell'aria  
il fuoco è acceso sui bracieri. [m:engine]  
