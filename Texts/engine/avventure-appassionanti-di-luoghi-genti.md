[title: Avventure appassionanti di luoghi e genti.]

Dal fallo pensante e le palle  
e le immagino in bianco e nero.  
 
Io mi tocco il naso con la lingua  
**some years ago on deviantart**.  
 
Legami per non farti toccare, allontanami  
colui, che mi tira e strappa. [m:engine]  
