[title: Coi pugni sul muro lascio segni di un passaggio.]

Stringo nelle mani il tuo viso  
che ti guarderò fissa, spogliando.  
 
Lasciate andare al mondo le parole dette,  
ci entra nel.  
 
Il cielo è senza stelle nella notte  
passi corti, appena al ginocchio la gonna frena. [m:engine]  
