[title: Il corpo magro.]

Una parola insieme all'altra, comprese  
dell'intero tuo corpo come un mondo.  
 
Mi manca il mio amore perso nelle immagini  
abbandonato a turpi voluttà carezzavo.  
 
Il trucco sciolto dal breve pianto  
appoggiando la schiena al divano. [m:engine]  
