[title: Un gesto semplice, gradito e facile.]

Lascio appoggiare le mani al tavolo  
numeri nel sacchetto agitati nella tombola.  
 
La vecchia città che accoglie  
dove ho voglia di te.  
 
Sensi smembrati e distrutti, persi  
lo zucchero sciolto di bava e il tavolo basso. [m:engine]  
