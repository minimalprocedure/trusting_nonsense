[title: Voglio il senso del delirio.]

L'ombroso tuo morbido diluisce in piacere  
carica di senso ogni segno.  
 
Ti prendo per mano, vieni  
per far crescere l'aria che manca.  
 
Nel giorno, che diventa notte e giorno  
solo allora nelle mie tasche non mancheranno mai. [m:engine]  
