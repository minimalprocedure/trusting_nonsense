[title: Dal corpo a goccia che hai.]

Il buio fuori con luce fioca dentro  
nella parte che ti tengo cara.  
 
Legherò anche le tue caviglie, vedi?  
L'occhio che la guarda è il mio.  
 
Adoro la lentezza orgiastica, del guardare i corpi con calma  
ci sono molte lanterne, piccole e grandi. [m:engine]  
