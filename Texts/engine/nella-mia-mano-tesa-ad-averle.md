[title: Nella mia mano tesa ad averle.]

Mangiare la sua voce che sussurra  
quasi nel sonno avesse paura.  
 
Giocarmi una vita intera a carte  
il gioco delle parti, dove.  
 
Il corpo esule dell'eterno perduto  
ti fai largo come a casa tua. [m:engine]  
