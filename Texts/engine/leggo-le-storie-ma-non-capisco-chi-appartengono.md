[title: Leggo le storie ma non capisco a chi appartengono.]

Mi alzo e mi lascio  
questo attimo fisso dove i molti, vicini.  
 
Con le mani nelle mani  
nelle radure delle foreste, i vecchi riti.  
 
Mi perdonerà l'ardire, ma non  
senza chiavi si aprirà per passare. [m:engine]  
