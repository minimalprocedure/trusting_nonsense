[title: Le sue labbra sono rosee.]

Immagino le mani accese per farmi dimenticare  
sotto acque battenti ci raggiungiamo.  
 
Nel mio **silenzio**, mi guardo intorno  
i capelli, gli odori prendono vita.  
 
Chiuditi in una stanza e togli la chiave,  
importanza risolve l'amicizia, ma di ognuno. [m:engine]  
