[title: Il fuori odora di notte.]

Si è alzata e lo ha agganciato al mio  
a viso ignaro, seduta là in mezzo.  
 
Lì improvviso tra le foglie secche  
la realtà, va presa per quella che.  
 
La mediocrità dell'amore, baci e baci  
buongiorno anche all'antennista che sta passando il cavo. [m:engine]  
