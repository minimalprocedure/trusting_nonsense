[title: C'era una volta un ragazzo, uno.]

Di quei sensi mai avuti  
poi chissà, forse ci saremmo ignorati in ogni caso.  
 
Le parole raccontate, fili di lana  
il nulla dei suoni che impazza nel cuore.  
 
Il calore secco che brucia  
lasciandoti stringere come tuo nemico. [m:engine]  
