[title: Non è vero, solo basta cercarle, provi.]

Vicino il volo silenzioso di una civetta  
in cerchi sempre più stretti e vicini.  
 
Mi fa venire voglia di prenderti per mano  
le novità arrivano quando hai sempre altro da fare.  
 
A camminare nell'acqua, eri per mano  
si può saltare e incontrarci a metà. [m:engine]  
