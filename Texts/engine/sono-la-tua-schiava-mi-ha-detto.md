[title: sono la tua schiava, mi ha detto.]

Uscire, fuori l'aria è fresca  
gli sguardi sono tutti fuggiti.  
 
La forza di strapparsi il cuore  
che il cuneo muove la sinapsi mentale.  
 
La mia barba, che le sfrega con forza  
a tratti nascosto e solo immaginato. [m:engine]  
