[title: Aspetto i sogni rotti, nei pezzi.]

E scendo il collo bianco  
guardo la collina degli olivi.  
 
E guarda come pioggia in una pozza  
poi nuda balla e vai, porta al mondo.  
 
Strato per strato ricreato nel tempo  
spicca il volo e salta. [m:engine]  
