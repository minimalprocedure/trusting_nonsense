[title: Ho voglia di vederti toccare.]

Quale incanto poter vedere i tuoi occhi  
- mi scusi.  
 
Negli occhi chiusi dal tormento  
nell'attesa il tempo immoto esposto all'aria calda.  
 
Non è semplice, non lo dimentico,  
ed afferro poco dopo una lacrima con la mano. [m:engine]  
