[title: Rimbalzando sulle pareti e collidendo.]

Si è voltata abbassandosi e dicendomi di inserirglielo nell'ano  
ho camminato distrattamente per un tratto.  
 
Da mano piccola e gentile  
si tiene la testa tra le mani.  
 
<< Godi di me >>,  
lo si ascolta con quell'ansia pacata. [m:engine]  
