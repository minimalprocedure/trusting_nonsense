[title: il dolcetto]

Essere gustato come il termine del pasto, il sapore che chiude la voluttà del mangiare, quello che persiste più a lungo…

Non può fare che piacere. [m]
