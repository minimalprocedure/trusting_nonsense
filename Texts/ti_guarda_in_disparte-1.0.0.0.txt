[title: ti guarda in disparte]

__primo movimento__

In balia di chi conosci e che osserva,  
ti mostri negli eccessi come ordinato.  

Arrivata pronta e per non perder tempo  
nella stanza dove il tavolo è in mezzo.  

Largo, nero e lucido, li per appoggiarti   
e già il tuo corpo è tenero e ospitale.  

__secondo movimento__

Stendi il seno curato, segnato di rosso  
con le braccia avanti, libere e immobili.  

Nessun laccio potrà servire, così libera  
di andare o di restare, come è da sempre.  

Aspetti ora, che quel brusio si plachi   
delle voci sospese e che il primo inizi!  

__terzo movimento__

I molti e più _mostri_, tutti erti di sesso  
ti sbattono al dentro, con bruta forza.  

Ridono così osceni e spingono veloci  
ma non muovi o piangi, dato che sai.  

Chi ti possiede ti guarda in disparte  
per ultimo poi, sarà a donarti un bacio. [m]  
