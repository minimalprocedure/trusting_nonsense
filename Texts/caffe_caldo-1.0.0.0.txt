[title: caffè caldo]

La senti? È una musica che arriva  
dammi le mani e appoggia la guancia.  

Ti solleverò piano, lasciati portare  
dormi mia bella, dormi e sogna  

ti sveglierai domani, con calma  
all'odore di un caffè caldo. [m]  
