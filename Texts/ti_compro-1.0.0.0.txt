[title: ti comprerò, dimmi quanto]

Ti comprerò, dimmi quanto tu costi  
dimmi quanto, tutto il tanto che vale.  

Tutto ciò che devo verserò alla cassa  
solo per averti soltanto mia e oggetto.  

Carne graziosa, morbida umida e vezzosa  
alla corda corta, poi in piacere e voluttà. [m]  
