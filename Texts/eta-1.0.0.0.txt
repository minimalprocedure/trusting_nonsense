[title: età]

[anonimo]
Quanti anni hai?

[m]
Mi si chiede l'età, oddio che domanda difficile.  
Sono ormai decrepito e in avanzato stato di decomposizione. La gamba di legno e l'occhio di vetro certo non aiuta, ma l'uncino smontabile, ne convengo, torna in certe occasioni parecchio utile.  
Quindi vede caro anonimo, sono un pezzo avanti ma come si dice: _la morte mi troverà vivo_. [m]
