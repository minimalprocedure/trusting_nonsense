[title: il perduto]

La voce che sopraggiunge da lontano
chiama il perduto nelle vie contorte.

Percorso scosceso dai tanti gradini
che dal basso lungo il crinale vanno.

Alto verso i cieli, cammino al freddo
a passo di fiera ed armato di niente. [m]
