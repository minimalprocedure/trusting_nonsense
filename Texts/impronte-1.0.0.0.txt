[title: impronte]

Nel mare non in tempesta, l'acqua
bagna lasciando sabbia liscia sulle orme.

Quando uomini e donne camminano
a due mani od una mano, unite.

Lasciano passati scomodi, impronte
di sé abbandonate indietro e perse. [m]
