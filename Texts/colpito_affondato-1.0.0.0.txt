[title: 5B, colpito e affondato]

**[anon]**  

Signore lei riesce sempre a colpire nel segno. Grazie per la splendida lettura 

**[m]**  

Caro _anon_ affezionato, la ringrazio per l'apprezzamento.  
Sono anni e anni di allenamenti al tiro con l'arco, da quando li facevo da piccolo con le stecche degli ombrelli.  

Sa, in questa battaglia navale che è la vita, talvolta basterebbe soltanto un foglio di carta a quadretti e una penna che scriva.
Poi è tutto un 3C, colpito; 5B affondato.  
Dovremmo allegramente spararci bombe col divisorio di cartone.

Almeno quello si dovrebbe sempre fare.  

Con affetto, m.
