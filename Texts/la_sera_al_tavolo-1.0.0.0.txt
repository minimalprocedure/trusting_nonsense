[title: la sera, al tavolo]

La sera d'estate, al tavolo, al bar
le ragazze che guardano i ragazzi.

Al mio posto accanto all'albero
distratto ti osservo seduta, lontana.

Il vestito scostato, quanto basta
al soffio della brezza, che sale sotto. [m]
