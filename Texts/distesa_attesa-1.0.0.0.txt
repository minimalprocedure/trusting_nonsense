[title: distesa e attesa]

Distesa e attesa alle mie labbra, appoggio
un umido bacio appena sotto i suoi capelli.

La comoda via da percorrere di tenue contatto
seguendo le curve che sono sue, ma a me mostrate.

Mi avvicino per la strada lunga, al basso solco
che morbido si scosta a offrire i suoi segreti. [m]
