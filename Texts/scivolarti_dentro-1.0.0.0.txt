[title: scivolarti dentro]

Ti circondo di semplici attenzioni
nodi complessi di intrecci e volute.

Le tue gambe flesse senza timore
nell'interno della pelle liscia.

Potrei guardarti per ore senza gesti
immaginando soltanto di scivolarti dentro. [m]
