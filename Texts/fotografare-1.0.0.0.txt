[title: fotografare]

Lasciati guardare,
fammi stare leggermente discosto,
ho bisogno di guardarti.

Ho bisogno delle tue forme
da poter fissare.

Per renderle almeno belle
più di quanto non lo siano già. [m]
