[title: andata e ritorno]

Se riuscirai ad aspettare, io tornerò  
all'improvviso, aprirò quella porta.  

Sarai lì e non più mia  
non più sola e triste.  

Ti farò ballare, ridere ancora  
per poi ripartire poco dopo. [m]
