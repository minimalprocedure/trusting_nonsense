[title: il senno perduto]

Sei con le braccia le gambe e il corpo  
cosa da usare e alla mercé del demonio.  

T'infondo quella forza dal doppio fallo  
per l'unione animale a singulto spinta.  

Tra le cosce bagnate tu mi afferri e tiri  
facendomi perdere il senno del mondo. [m]  
