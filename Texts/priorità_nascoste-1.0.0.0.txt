[title: priorità nascoste]

Mi bussano alla porta
ma non corro per vedere.

Le facce del come stai
gli occhi che scrutano nella casa.

Solo per vedere se hai lavato i piatti
o steso i panni. [m]
