[title: allungo una mano e so di trovarti]

Ti volti ogni volta che mi allontano
salvo poi chiamarti a gran voce.

Sei uno spiraglio della vita
qualcosa da lasciare per trovare. [m]
