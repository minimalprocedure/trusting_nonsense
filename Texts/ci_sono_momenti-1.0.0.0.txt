[title: ci sono momenti]

Ci sono momenti in cui mi manchi moltissimo, mi manca non vedere che sei su msn o che metti qualcosa su tumblr, a volte penso che mi iscriverei di nuovo su FF e lo userei questa volta.
Ma poi penso che non mi va di darti troppo fastidio.

Ci sono momenti in cui, ti penso così tanto quasi da materializzarti, mi sembra di poter allungare una mano e accarezzarti.
Non ho più le immagini che mi hai mandato, perché non voglio essere trascinato dal vedere come sei, voglio solo sentire come sei.

Vorrei poterti raggiungere ogni volta che ne ho voglia, vorrei poterti raccontare le cose a voce, parlarti e starti ad ascoltare.

Vorrei poterti coccolare e fare lo scemo per farti ridere.
Prepararti la colazione la mattina, che preferisci? faccio delle ottime crepes...

A volte penso che mi piacerebbe vedere giocare il mio bimbo con la tua bimba. stare lì, appoggiato un po' distante a curiosare. Vedere il mio che le parlerà di robot sicuramente, di pianeti e di batteri.
E' affascinato dai batteri, da quando è stato all'ospedale. Per cercare di farlo stare tranquillo gli ho spiegato tutti i meccanismi cellulari, come funzionano gli antibiotici... :D aveva tre anni, ma si ricorda tutto. Poi facevamo i disegni li ritagliavamo e li attaccavamo sulla finestra della camera dell'ospedale. Era diventata la camera più bella.
Poi le racconterà di qualche libro che gli abbiamo letto, lo fa spesso, o che nuota con maschera, boccaglio e pinne ed ha visto i pesci con babbo.
Chissà la tua che potrebbe dire, più grande...

Me ne voglio andare a casa, ma non ci voglio andare.
Ho bisogno di un viaggio, staccare la spina, ibernarmi per una decina di giorni.
Lasciarmi andare senza pensare, farmi portare dalla corrente.

Voglio un'isola deserta, voglio vivere in un faro, su uno scoglio battuto dal mare.
Affacciarmi alla stretta finestra e guardare le onde frangersi poco sotto.

Voglio sedermi e scrivere cose bellissime, da mettere nelle bottiglie per lanciarle in mare.

Voglio dipingerti di luce,
per renderti più bella di come lo sei gia.

E parlo, parlo, parlo per esorcizzare le angoscie che mi prendono, quel male delle cose che mi fa bivaccare nella vita.
Mi manca a chi raccontare delle cose che faccio fatica a raccontare, che spesso di concludono con dei "non lo so". E' la mente che è più veloce della parola, la lingua non riesce a correre dietro e alla fine si arrende: << non lo so >>.

Sono una palla al piede, come vedi. Uno che è meglio perdere che trovare, che va bene per un po', ma poi stufa. [m]
