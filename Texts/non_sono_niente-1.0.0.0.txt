[title: non sono niente]

Non sono niente, solo uno dei tanti.
Non ho soldi, non ho niente, non ho potere.
Non possiedo che me stesso.

Non ho altro da poter donare, solo un piccolo soffio
un alito d'amore che mi tenga in vita. [m]
