[title: il pensiero vagante]

Buona serata,
vado, nella stanchezza di questo giorno.
Lascio un pensiero, vagante, che aleggia nell’aria.
Lo prenda chi vuole e quello che vuole, ne faccia. [m]
