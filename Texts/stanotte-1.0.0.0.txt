[title: verrò da te stanotte]

Stamattina ti dico che verrò da te stanotte col buio, nella tua casa da oscurare in pieno. Entrerò dalla porta socchiusa e dovrai lasciare cero a fiammiferi in basso sulla soglia.  

Alla luce della candela, ti raggiungerò in camera e dovrai essere nuda e distesa sul letto. Poggerò al lato la borsa degli attrezzi e cercherò la corda grande per legarti le mani ai fianchi poi la corda media per unire le caviglie strette.  

Stanotte ti ricoprirò di cera rossa e sai, comincerò dalla gola. Scenderò goccia a goccia disegnando linee dalla bocca al seno, ammucchiandone un po' su quei capezzoli che si muovono. Seguirò le forme disegnando cerchi, lucidi e bollenti e poi opachi e freddi.  

Ti scriverò sulla pancia parole che urleranno di chi sei e alle gambe dirò di farmi vedere e sposterò, le labbra con le dita per placcare il tuo _grilletto_. Saranno gocce lente che colpiranno forte e bruceranno e coleranno insieme a te. [m]
