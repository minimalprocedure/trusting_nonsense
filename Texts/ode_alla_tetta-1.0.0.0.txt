[title: ode alla tetta, ma dato il numero di versi solo un’odina]

Oh! Perché ogni ode così comincia.  
Oh! Tetta, al di sopra erta arguta  

di ogni giudizio di colui il quale  
a righel munito ti squadra e misura.  

Innalza sì fiera il di carne puntale  
al tatto sfiorato, nonché pizzicato. [m]
