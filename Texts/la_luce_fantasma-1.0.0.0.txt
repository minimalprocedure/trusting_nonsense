[title: la luce fantasma]

Ecco vedi, arriva una luce fantasma
madre incorporea e fiato del vento.  

Tremula e luminescente appena fioca
è quasi scura agli occhi e leggera.  

Prende le forme, quelle del desiderio
le curve piene del sogno che ho perso. [m]  
