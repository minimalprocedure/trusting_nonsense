[title: ti ho sognato]

Ti ho sognato stanotte.
Eravamo in un casale molto grande, con un sacco di gente ma quasi tutte donne (forse anche tutte).

Eravamo a letto insieme, niente di ché, solo abbracciati.
Avevi un pigiama da uomo scuro, in tela leggermente lucida. Io se ricordo bene come al solito, maglietta scura soltanto.
Ad un certo punto ti svegli e mi fai: << mi va un panino >>.
Ti rispondo: << vengo con te >>.
Ti metti a sedere sul letto, apri la giacca del pigiama e stacchi una flebo che avevi attaccata al seno destro.
Ti alzi e cominci a vestirti.
Io mi alzo e vado in bagno.

In quel posto c'erano un sacco di bagni, piccoli e grandi. Erano tutti occupati, quindi ne giro parecchi.
Alla fine ne trovo uno libero, uno piccolissimo.
Mi do una lavata, poi torno in camera e mi vesto.
Ti raggiungo in cucina dove mi stavi aspettando già pronta per uscire.
Ti prendo per mano e usciamo.

Poi non ricordo altro.

Le immagini si alternavano tra me in cerca del bagno e te che ti stavi vestendo e preparando per uscire.
In giro tutte donne, molte conosciute, che parlavano tra loro. [m:2008]
