[title: quel qualcosa di sbagliato]

Quel qualcosa di sbagliato nelle perdite
l'allontanarsi del guardarsi da vicino.

Gli errori delle mani che scivolano via
nei cammini solitari che portano lontano.

Perduti in qualche posto, ognuno di noi
cerca non più quello che trova, ma altro. [m]
