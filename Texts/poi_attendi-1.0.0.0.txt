[title: ... e poi attendi]

Torna a casa, torna e perdi quello che fai
che importa, lascia pure a metà non fatto.

Il giorno va finendo, si accascia il sole
entra dalle tue porte, disperdi il vestito.

Lasciati del tuo odore dal fuori coperta
e distendi il ventre sulla pila di cuscini. [m]
