[title: respiri ravvicinati]

Sono il lento passare sfiorando
appena il tocco, in punta di mani.

Come un libro letto a voce alta
ascolto ogni frase detta, attento.

Nei respiri ravvicinati, dalle bocche
che socchiuse prima e ora aperte. [m]
