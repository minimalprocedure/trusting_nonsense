[title: non avete ancora chiamato Giacobbo?]  

Pare che dove è stato avvistato il __Ranzaponzolo del Sarmanacco__ e cioè nelle immediate vicinanze, ci siano testimonianze certe di un passaggio dei Cavalieri Templari. Si dice infatti che l'animale sia stato importato dal famoso maestro _Robert de Craon_ per le sue uova di grosso calibro. Uova che sembra fossero usate anticamente come palle __Ben Wa__ e comunemente conosciute come l’__uovo a sonaglio__. [m]  
