[title: il lento cadere]

Quando tutte le parole dette
restano segni sui fogli bianchi.

Tra le piccole macchie scure
di una penna che perde inchiostro.

Ho lasciato sempre la stessa idea
del volo, prima del lento cadere. [m]
