[title: equilibrio in terra]

Nessun favore, nel limite  
della linea che si percorre.  

Scritta in terra, mezza alla via  
di vernice bianca, scompare avanti.  

La seguo un piede dopo l'altro  
braccia aperte, occhi chiusi. [m]  
