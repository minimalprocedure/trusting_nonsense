[title: le righe sfuocate]

Il mondo che gira, gira e gira  
vortice di cose e genti fuse.  

Mescolati in righe di falsi colori  
sembrano ravvivare di poco la vita.  

Immagini sfuocate, disegnate e dipinte  
con pennelli e tele da poco prezzo. [m]  
