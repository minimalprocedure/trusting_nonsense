[title: come vino prezioso]

Con la tua coda posticcia
fissata dal perno lucido.

Penzolante, ancheggiando
ti allontani a gattoni.

Attendi come vino prezioso
da stappare e ritappare. [m]
