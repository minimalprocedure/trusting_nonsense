[title: immaginare scrivendo]

Una volta avevo delle cose che oggi non ho più, 
oggi ho cose che allora non avevo,

Oggi mi vesto come non mi vestivo,
oggi guardo come non guardavo.

In quegli anni che passano,
in quello che a volte sembra volare via, per mancanza di tempo.

Mi alzo dalla sedia, per guardare fuori,
solo un attimo, era un rumore, delle voci.

Torno a sedere, per continuare a immaginare scrivendo. [m]
