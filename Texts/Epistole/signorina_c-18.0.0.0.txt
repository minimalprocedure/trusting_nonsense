[title: la signorina C. (18)]

Mio M.  

leggervi è come lasciarsi andare ad un fiume in piena, ti lasci trasportare , ignara e felice. 

Che bella immagine vi siete fatto! Sorrido e aggiungo che i pipistrelli sorvegliano la mia finestra mentre i ragni riposano ovunque in questa immensa casa. 

Sapete comunque che tra le mie letture la stregoneria non manca, sono argomenti affascinanti quanto terribili e per questo li amo. 

Ma veniamo a noi mio caro, avete ragione sul confronto, a tal riguardo una vostra parola mi ha riportato alla mente uno dei tanti libri proibiti,

Le undicimila verghe,  vi confesso che alla fine ho lanciato il libro contro il muro. Non ero pronta a tali argomenti e mai lo sarò.  Salvo alcuni passi di quel piccolo libro il resto ai maiali. Perdonatemi, oggi passo da un argomento all'altro senza badare alle parole, al senso.

Vi desidero sapete? Si, forse è questo mio confessare che mi fa parlare senza seguire una via, le mani tremano e la voce...balbetto sapete? Ve ne siete mai accorto ?

Adoro ciò che dite, adoro ciò che riuscite a fare con la mia mente, con il mio corpo.

Non provo attrazione per la nostra signorina, ammiro le sue grazie, quel  fondoschiena ben tornito, il seno piccolo ma pieno ma, ma non riesco a desiderarla, c'è qualcosa che mi tiene a distanza. Sapete, questo un po' mi dispiace, sono curiosa e questo mi porta a domandarmi come sarebbe baciare un'altra donna, cosa potrei provare nel portarla al piacere. ..

Come vedete oggi ho idee molto confuse, potete aiutarmi a trovare una via?

State tranquillo, i miei gioielli sono lì che mi guardano in trepida attesa.

Vostra G
