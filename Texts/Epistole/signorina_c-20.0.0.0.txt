[title: la signorina C. (20)]

Mio caro M.

Provocate, vi prego non smettete mai di farlo, senza il vostro provocarmi diventerei muta, triste.

Come potrei adirarmi con voi, le vostre provocazioni raccontano, mi parlano e senza loro vorrebbe dire che nemmeno io esisto, sarei invisibile ai vostri occhi.

Che brutto pensiero, triste. 

La vostra descrizione del libercolo mi ha fatto sorridere, a tratti l'ho trovato divertente, descrizioni dettagliate , poemetti ricamati su misura in ogni momento ma, come detto, non è stata una lettura da ripetere. 

Il troppo descrivere ci priva dell'immaginario,  ci toglie quel brivido che fa sospirare, ecco perché alterno le mie letture e spesso rimango a fantasticare su storie delicate, sussurrate.

Mio caro provocatore, sapete però che mi accade la notte quando sono sola in quel letto caldo? Le storie si fondono , i delicati racconti si colorano di rosso, quel rosso dato da una scudisciata, quel rosso dato dal morso su un seno, quel rosso che tanto amo tra i capelli.

Voi migliore? Migliore di chi ditemi?

Migliore del marito della Duchessa? Colui che la adora davanti al mondo ma che nell'intimità la ignora dalla prima notte di nozze?

Migliore del marito di mia sorella che preferisce andare dalle baldracche lasciando lei in silenzio e senza soldi?

Ditemi mio caro amico, desiderato uomo, chi sarebbe migliore di voi? 

Io vi conosco perché voi vi siete mostrato senza veli, vi conosco perché so come amate, vi conosco perché non mi avete mai mentito.

Si, vi desidero.

Volete far della signorina il mio piacere? Ho provato a pensare a ciò che avete descritto e vi confesso che conosco il motivo per cui la signorina non mi attrae, ci ho riflettuto a lungo e sono giunta davanti al perché, temo di non avervi mai confidato questo mio segreto sapete?

Sono comunque combattuta perché alla fine siete voi il mio pensiero e per voi potrei andare oltre ...

Alla fine lei è solo uno strumento per...

Non voglio dirlo ora, non voglio che questo desiderio si perda.

Vostra G.

Vi prego, provocatemi ancora.

