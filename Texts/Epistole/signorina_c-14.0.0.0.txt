[title: la signorina C. (14)]

Mio Servo, Mio Domatore,  

Sorridete ora?  

Sapete stupirmi ogni volta con le vostre parole. Riuscite, con il vostro dire, a smuovere i miei pensieri, il mio corpo, il mio cuore.  

Mio domatore, così dovrei chiamarvi ora.  

Sorrido, ho davanti a me l'immagine che mi avete donato. Le scudisciate, il trotto, il galoppo.

Ho percepito la forza della nostra signorina, ho visto anche la luce nei suoi occhi ai vostri comandi, ho visto il suo desiderio, la sua voglia.

La signorina è affamata mio bel signore, ha fame e dubito arriverà mai a saziarsi con il futuro marito. Arriveranno giorni di magra per lei e sono certa che non tarderà a cercarvi.

Sapete, inizialmente sarei anche rimasta nuda vicino a lei con voi a guardarci, cercarci, ma ora voglio confessarvi una cosa, il solo pensiero mi infastidisce. 

Cerco di capirmi mio bel signore, aiutatemi a guardarmi dentro la confusione che alberga nella mia testa.

Sono curiosa, curiosa di sapere cosa avete in mente per i prossimi incontri,  voi ne sapete sempre una più del diavolo. Sorrido, sapreste tentare anche la più pia delle donne.  

Sul domarmi vi lascio nel dubbio e sorrido.  

Nell'attesa vi bacio  

Serva vostra G.  


 
