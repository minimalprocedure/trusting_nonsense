[title: la signorina C. (9)]

Mia Cara G.

Ieri quando finalmente siete arrivata, il mondo intorno si è fatto più luminoso. Vedervi poi accomodare comodamente sulla poltrona mi ha fatto molto piacere. Ho visto nei vostri occhi un lampo di curiosità ed eccitazione, ditemi erro o tal sensazione era nel giusto?  

Nell'accompagnare la signorina C. nella stanza ho provato una certa sadica soddisfazione, ma non per la nostra in sé e l'idea di addestrarla, quando nel pregustare certe vostre possibili reazioni all'evento.  

Quando per la prima volta ho dato l'ordine, col tocco del bastone sulla testa, di scendere in ginocchio, vi ho scorta accomodarvi sulla poltrona come a meglio sprofondare nel cuscino. Lo sguardo attento. Ho infatti atteso abbastanza a lungo prima di impartire il nuovo compito per permettervi di osservarla meglio.  
Vi siete accorta di come velocemente avete leccato vostre labbra quando con la punta del bastone ho sfiorato i capezzoli della signorina? Dovuto al tè, agli ottimi biscotti che la mia cuoca sa fare, oppure all'ergersi della carne? Notato come la pulzella reagisca subito, visto il piccolo brivido che l'ha scossa?  

Nell'altra vostra missiva mi accusavate di volerla punire apposta, ma penso conveniate come ella si stata diligentemente sottomessa e quasi perfetta. Ci sarebbe da pensare che già fosse stata addestrata o che abbia una predisposizione innata. Che ne dite?  

Sapete, questi vostri accenni mi hanno davvero incuriosito. Per tal motivo senza indugio l'ho fatta voltare e mettere carponi. Tal che potesse mostrarvi le sue bellezze che definirei, interiori. Con il tocco che le ha fatto allargare le gambe vi ho notato scivolare appena in avanti, vi siete accomodata meglio ancora? Quando sfiorando il sesso della bimba quella ha alzato leggermente la testa, lo avete fatto anche voi. Ve ne siete accorta?  

È stato il primo errore, ma per quale idea fremevate? Per la punizione prossima o per il solo gesto? Il mio col bastone o il suo di piacere?  

Ecco si, per cercare di capire meglio e lo confesso, l'ho fatta fallire per il gusto della punizione. Non per lei s'intende quanto per voi. Sono ragionevolmente sicuro che al colpo sui glutei voi abbiate sentito bruciare i vostri. Mi sbaglio?  

Avete notato poi di che glutei torniti, perfetti nelle sue forme, sia dotata la signorina C.? Quasi è un peccato che l'accordo preveda che rimanga illibata per tal cicisbeo promesso. Sapete però cosa vi dico? Forse la signorina è stata precipitosa o forse maliziosamente determinata perché in tal scritto controfirmato dalle parti, s'intende per illibatezza quella verginità da prima notte e non citando in alcun caso come inviolabili gli ulteriori anfratti.  
Lo sapevate questo? Non credo di avervelo detto, data l'intenzione di svelarvelo in seguito.

Poi ditemi, quando appena ho appoggiato la punta senza penetrarla potrei per caso aver notato il vostro accavallare le gambe? Cos'era? A mò di rifiuto? Chiusura della vostra apertura o sfregamento del piccolo gioiello?  

Mi perdonerete tutte queste domande, ma dovete sapere che forse la prova non era solo per la fanciulla. Mi assolverete per questo bieco gioco?

Vostro m.
