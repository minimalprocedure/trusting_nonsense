[title: allegoria vitale]  

Nel cammino saltellante  
da una casella all'altra.  

Con un sasso piatto in mano  
da lanciare alternando un piede a due piedi.  

Giocando a campana avanti e indietro. [m]  
