[title: Lentamente, mi lega.]

Ad occhi aperti e striati di lacrime  
la luce cala, per quello che manca.  
 
Quella pioggia, nel tepore del fruscio di piante  
dei desideri che forse abbiamo.  
 
Poi lasciarla per uscire con un'altra  
che parla con gli altri e ride, con gli altri. [m:engine]  
