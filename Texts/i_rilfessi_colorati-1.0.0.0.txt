[title: i rilfessi colorati]

[La fuga dopo, con le mani a coppa
raccolgono i pianti di quell'attimo.]

Vorrei conservare ogni lacrima
in belle bottigliette rigorosamente catalogate.

Saranno sopra una mensola nei loro mille colori
a riflettere lampi di tristezze. [m]