[title: E. ]

Quando sei andata per non tornare  
è rimasto un buco grande, tu piccola  

Non sorrisi, non più nei capelli  
neri e volanti sul viso bianco.  

Sei partita con i molti altri  
terra bruciata che mi è dietro. [m]  
