[title: disimpegno intellettuale]

bel post : la fine del giorno.

http://yesiamdrowning.tumblr.com/post/27423694791/la-fine-del-giorno

L' evoluzione informatica è pericolosa (e te lo dico da uno che si occupa progettare e sviluppare software) la 'massificazione' della informazione è pericolosa. Pericolosa come tutte le 'massificazioni'. Sarebbe molto bello che tutti fossero pronti e 'critici' di fronte alle informazioni, analitici e razionali, ma non è così.

Mi ricordo, quando facevo finta di fare l'artista, certe lunghe discussioni sull'arte, sulle arti. Su cosa sia 'arte' e cosa non lo sia. Ne uscivano come puoi facilmente immaginare una marea di cazzate.
La non concettualizzazione dell'arte era il problema. Pensare che uno schizzo su una tela bianca sia di per sé 'arte'. L'emulazione dell'arte, l'emulazione delle informazioni.

Il percorso è stato lungo e inizia secondo me dalla fine della seconda guerra, da una pianificazione 'complottistica' per una massificazione di menti non pensanti. La teoria del complotto universale :D

L'era moderna, l'era tecnologica. Il Nobel per chi ha 'inventato' il microprocessore. Fino ad allora avevo sempre pensato che il Nobel fosse dato per chi innova nella ricerca a favore dell'umanità. Il Nobel per un prodotto industriale, come si dice: - non c'è più religione -.

L'uomo è emulo, l'uomo è pecorinamente (in tutti i sensi) asservito. Più semplice leggere che scrivere no? Guarda i bambini a scuola, imparano prima a leggere che a scrivere.

La dottrina tecnologica, il verbo a buon mercato (a prima vista, tutto si paga prima o poi).
Pensare è una attività in disuso, siamo pigri dal culo pesante, se qualcuno ha già pensato per noi perché pensarci due volte, no?

E poi (non si inizia una frase con 'e' figuriamoci con 'e poi'), parliamo di queste cose su Tumblr? Il trionfo del disimpegno intellettuale.

buona giornata. [m]
