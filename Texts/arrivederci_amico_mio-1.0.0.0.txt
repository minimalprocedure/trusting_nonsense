[title: arrivederci Claudio, amico mio]

Amico mio,  
mi ricordo tante cose di te  
mi ricordo il tuo viso.  

Mi manchi a volte e mi manca poter parlare, di tante cose  
di donne, di computer, di metafisica e poesia.  

Mi manca ogni tanto venirti a prendere,  
ti ricordi, ti dissi _suicidati_, ma non lo hai fatto.  

Ho pensato io a trascinarti verso la morte,  
ti ho portato per mano per non vederti più.  
 
Mi manchi, io non so se manchi ad altri  
ma un pezzo di me, quello buono non esiste più.  

Ricordi gli appuntamenti, quelli che davi e non rispettavi mai.  
L'uomo "sola", appuntamento alle quattro, arrivavi alle quattro certo,  
di qualche giorno dopo.  

Quando ti vedo, vedo il tuo sorriso, la tua dolcezza,  
una canna in mano e sorrido, quasi posso parlarti ancora.  

Le cene insieme, i vari progetti.  

Mi manchi, a giorni più e a giorni meno  
ero tuo fratello ed eri mio fratello,  
a dispetto di quelli veri.  

Mi manchi, sai,  
mi mancano le tue parole. [m]  
