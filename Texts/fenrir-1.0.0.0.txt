[title: Fenrir]

Io sono il lupo dalla bocca enorme
che dalla terra al cielo ingoia.

Nella guerra della fine ultima
al galoppo sfrenato dagli inferi.

Fiero e dannato non potrò che morire
uccidendo il dio da un occhio solo. [m]
