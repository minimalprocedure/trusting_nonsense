[title: Il roseo organo vibrante]

Come un libro antico e rilegato  
sei piena di ogni tipo di lacci.  

Fatti accarezzare che con la mano sento 
quanto sei bagnata di baci e di sapori.  

Linea di partenza delle labbra scivolate  
come fossero le tue parole, da tempo mute. [m]  
