[title: il faccio nll vita]

[anonimo]

cosa fai nll vita?

[m]

Caro anonimo mattiniero o serale, buongiorno.  
Che domanda difficile a cui rispondere, sa che non saprei da dove partire?  
Inoltre vuole sapere quello che faccio in questo momento o quello che faccio di solito? Quello che ho fatto in totale o quello che ho fatto nei vari periodi?   
Il periodo giallo, quello rosso o quello nero che perdura da un po'?

Quello da studente liceale, universitario o quello delle scuole elementari? Quello che faccio con le donne? Quello che ci faccio qui?  

Vede, nella sua apparente semplice domanda si potrebbe nascondere un universo di contorte risposte.

Per l'intanto le dico che nella vita scrivo in italiano o almeno ci provo, si ingegni in tal senso anche lei che in tal caso ne godremmo tutti. 

Servo suo. [m]
