[title: l'indizio nascosto]

Facciamo un gioco, semplice  
nella noia di queste giornate.  

A venire, lascia indizi sparsi  
frammenti da collezione, unire.  

Da mettere nel sacco e mischiare  
tutti insieme poi, da attaccare. [m]  
