[title: ardono i fuochi]

Ardono i fuochi  
dei sacrifici immortali  

delle mani che muovono   
di carezze la pelle  

di oli e unguenti   
di lacci fissati e altari.  

Il sacerdote che passa   
delle vite degli altri  

per giocare, essere un dio   
senza troppa pena, volere.  

Dammi il perno, da unire al corpo  
a mani giunte levato in alto  
e rapidamente abbassato. [m]  
