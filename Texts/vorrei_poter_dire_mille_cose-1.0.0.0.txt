[title: vorrei poter dire mille cose]

Vorrei poter dire,
ma no ho che il silenzio con me.

Mi manca il suono della mia voce.

Ed aspetto un momento migliore
per poter dire.

(nulla va male se non in me stesso) [m]
