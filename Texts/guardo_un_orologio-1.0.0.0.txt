[title: guardo un orologio]

Guardo un orologio che non porto,
le sue lancette indicano il tempo.

La corta le ore, la lunga i minuti.

Una gira veloce, è quella dei sogni, è quella delle speranze,
quella che è troppo rapida da afferrare. [m]
