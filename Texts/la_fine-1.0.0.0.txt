[title: la fine]

Lo strumento che non so più suonare
buttato nell'angolo che ho lasciato.

Torpido e lento, manco della prontezza
e mi serve solo poter andare via.

Il tempo, però, non è ancora giunto, 
giorni sono da passare, prima della fine. [m]
