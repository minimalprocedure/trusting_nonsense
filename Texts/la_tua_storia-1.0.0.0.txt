[title: la tua storia]

Non voglio la tua storia
voglio te che la racconti.

Voglio ascoltare appoggiato
guardando in su, un cielo buio.

le mani alla nuca, nella notte
e sdraiato sulla schiena. [m]
