[title: la donna]

Una volta avevo scritto qualcosa da qualche parte, ma non me la ritrovo più e quindi mi toccherà riflettere a tentoni un'altra volta.

Vediamo un po'.

**Cominciamo dalla donna sicura.**

Lei è sicura e non c'è molto da dire, niente la turba e non si innamora mai; non si è mai innamorata. Il mondo è terreno di conquista e abbatte ogni muro che le si para davanti. Mediamente intelligente con mai una piega fuori posto.  
Lei non cammina come le altre persone ma scivola sul terreno a testa alta.

**La donna bomba del sesso.**

É bomba del sesso, te lo fa vedere dovunque e comunque. Truccata da bomba del sesso se ne va in giro provocante in qualunque occasione, sia essa la festa di compleanno dell'amichetto del figlio sia la riunione col capo. Sprizza sesso da tutti i pori e non suda come gli altri, trasuda. Amante incondizionata della fellatio normalmente te lo succhia per ore guardandoti di sottecchi con occhi volpini. 
Spesso nell'orgasmo tremano i muri della camera e i vicini si lamentano. Non ammette rifiuti, le bombe del sesso non ammettono rifiuti, ma nel caso il partner è solo un coglione che non capisce o un _finocchio_.

**La secchiona (a scuola).**

Chi non ha avuto una secchiona per compagna di classe? Chi non alza la mano è un bugiardo. 
Ci sono due tipi di secchione, quelle stupide e quelle intelligenti. La secchiona stupida è stupida e antipatica, quella intelligente è intelligente e quasi simpatica. Le differenze terminano lì.  
La secchiona, fa il cuoricino al posto del puntino sulle i. Scrive incurvata per proteggere la preziosa versione di greco o latino dagli sguardi indiscreti e avidi dei soliti nullafacenti, quelli che cercano disperatamente di copiare. 
Spesso scrive poesie che se disgraziatamente riesci a entrare nelle sue grazie leggerai dalla prima all'ultima. Attenzione al commento giusto, la secchiona porta rancore e lo porta per tutta la vita.  
La secchiona potrebbe essere una esperienza nel sesso, difficile definirla, ritrosa e aggressiva pare ami la masturbazione ai massimi livelli. Donna di mano insomma.

**Quella insulsa.**

Oddio è difficile da dire, lei è come dire... insulsa. Non si sa cosa farci, non è né carne né pesce ma fa delle crostate buonissime che regala a tutti. Anche a letto è... insulsa. La appoggi da una parte e lì sta, la sposti e lì rimane. Non si ha idea di cosa le piaccia a parte le crostate.  
È normalmente intelligente? Non si sa.   
Stupida? Impossibile da valutare.

**La bruttina stagionata.**

È un elemento interessante, spesso non è nemmeno bruttina anche se certo non è bellissima. È conciata, conciata male. Un po' trasandata. Si cura poco e si mette la prima cosa che trova nell'armadio.   
Alcune di loro però hanno spesso addosso un indumento particolare e curato, uno solo.   
La bruttina stagionata è spesso intelligente ed è un piacere parlarci anche se ti dice continuamente e convinta di quello che dice: come sei veramente. Lei lo sa, ha esperienza anche se non si capisce bene da dove provenga. Lei ti guarda e ti capisce al volo. Ti giudica non giudicandoti e sta sempre appena un po' in disparte.   
Nel sesso la bruttina stagionata è quasi sempre una sorpresa e abile in parecchie cose. Ne vale quasi sempre la pena.

**La più bella.**

Lei è fica, nel senso che lei è una fica.   
Una fica con le gambe che cammina e non guarda nessuno. Magra, alta e depilata... perfetta.   
La fica non si tocca, si rovina. Lei non si concede, non si concede a nessuno. Solo ogni tanto si fa battere a neve dal decerebrato ornamentale, col tatuaggio se va di moda il tatuaggio o la barba se va di moda la barba.   
La più bella ama il bello e il bello si compra con i soldi, ergo la più bella deve avere grosse disponibilità di denaro. Quindi a volte la più bella deve scendere a compromessi, farsi battere a neve dal decerebrato di prima e farsi sbavare dal tappo magnate della finanza o della televisione. Ignorate la più bella, non vi capiterà mai.

**La donna sirena.**  

Questa è come ben si può immaginare _melodiosa_. Lei _canta_, bella o brutta o _così-così_ lancia nel cosmo _melodiose melodie_ ed effluvi feromonici. Di lei se ne percepisce l'odore, quell'odore dolciastro da notte di bagordi. Astuta ingannatrice, oscilla tra il ritroso e lo _smaccato_ così che all'ignaro navigante a nulla valga il legarsi al palo. La sirena è interessante perché foriera di viaggi che si vorrebbe intraprendere richiamati dal quel canto sommesso. Appare improvvisamente al momento giusto quando nella tempesta non si fa che tirar cavi per non affondare, ti mostra la terra a mo' di faro e talvolta lo spenge quando si arriva agli scogli. La donna sirena è pericolosa, badate bene, meglio la cera calda nelle orecchie che il suo canto soave.

**La donna normale**  

Qui è molto complesso. Esisterà la _donna normale_? Dalla notte dei tempi, filosofi e scienziati si interrogano. Se _normale_ non è _strana_, esiste un limite identificabile di _stranezza_ accettabile da definire normale? Le _donne normali_ pare si lamentino che l'uomo medio non le cerchi, sembra temerle. L'uomo medio testosteronico preferirebbe le altre categorie. Si mormora che la _donna normale_ sia ingestibile perché indipendente, un concetto che instrada ulteriori quesiti su cosa possa mai essere l'indipendenza. La _donna normale_ si paga le bollette da sola, la _donna normale_ non ha bisogno dell'uomo. Certo di che uomo non ha bisogno? Dell'uomo normale? Del palestrato tatuato? La _donna normale_ non bada alle dimensioni ma se è lungo e grosso meglio, quindi di che uomo non ha bisogno? Sono davvero molte le domande. Si potrebbe anche pensare che la _donna normale_ sia talmente _normale_, ma così _normale_ che più _normale_ non si può. Totalmente prevedibile, sarebbe la certezza assoluta e chi desidera una vita di certezze assolute? C'è chi dice che l'uomo cerchi quella _strana_ per affermare la propria _mascolinità_, masochisticamente pare tenda a procurarsi una serie di _incomprensioni_, che qui per brevità chiameremo _pippe_, in maniera sistematica e continuativa per una propria _fame di possesso_. La _donna normale_ per cui, vive di una certa latente invidia verso quella _strana_, quella che gli limita buona parte del parco maschile disponibile.  
La _donna normale_ vuole l'uomo dolce e sensibile, disponibile e solerte al messaggio sulla chat di grido, che si ricordi il compleanno, l'anniversario, quello delle rose e delle margherite, delle cene nel ristorante di lusso, degli occhi negli occhi col calice di vino in mano, del bacio improvviso all'uscita del cinema, che sia ritroso nell'approccio ma forte nella presa, curato e profumato. La _donna normale_ anela il turbine della passione ma solo dalle 21:00 a mezzanotte e la colazione a letto la mattina. Quindi a meno che non siate disposti a tale _normalità_ per una qualche vostra tara genetica, evitate.

**La donna schiava.**

Qui bisogna esserci portati.   
Lei è devota al suo Signore. Un essere normalmente dotato di turbe ossessive e di intelligenza media.   
Si può spaziare nelle mille esperienze per comprendere perché uno debba avere una schiava (a parte che la schiavitù è ufficialmente abolita più o meno dappertutto).   
Lei ha un lessico molto vario (aggiungete sempre dopo Mio Signore o Padrone): si, no, ancora, grazie. La donna schiava non è facile da trovare, ha già un padrone: difficile trovarla da sola a meno che non sia stata ripudiata di fresco.   
Lei vive per soddisfarti, si innamora di quello che fai ma molto spesso non di te. Lei non ama soffrire o lo ama ma più spesso vuole soddisfare il Padrone che nella media è una emerita testa di cazzo che si diverte per la maggiore a picchiarla e umiliarla. 
La donna schiava è di sovente una donna intelligente invischiata in un gioco da cui non è in grado o non vuole uscire. Molte di queste sono donne fantastiche, sensibili e fragili di cui persone con pochi scrupoli si approfittano.  
Attenzione però, la donna schiava pare che sia sottomessa ma in genere è lei che subdolamente ti controlla.

**La donna anale.**

La mia preferita ma non solo per quello che pensate, malpensanti!   
La donna anale è sempre intelligente, sofisticata nella mente. Lei è vestita con cura, il che non significa quello che pensano più o meno tutti. È vestita con significato, a suo agio.   
La donna anale anche vestita di juta è anale.   
Ha la sensualità insita in se stessa, semplicemente naturale. Non è questione di bellezza, lei è magnetica.  
Alta, bassa, mora, bionda o rossa... la vedi per un attimo e non è più come prima. Il limite è solo l'orizzonte, ci cammini contro e non lo raggiungi mai.   
La donna anale è un gioco infinito di piccoli coinvolgimenti, sguardi di intesa, sorrisi velati e notti senza limite.   
Lei è libera di pensiero e si dona. Bisogna essere liberi di pensiero e donarsi.   
Lei va lasciata andare se vuole, non imprigionatela mai, sarebbe come una farfalla a cui si toccano le ali. [m]
