[title: dedica]

Rompo le barriere che dividono  
con la forza del mio essere.  

Il significato in quello che dico  
negli spazi lasciati volutamente aperti.  

In quelle righe che ti dedico sempre  
rotolando incalzanti, a una a una. [m]  
