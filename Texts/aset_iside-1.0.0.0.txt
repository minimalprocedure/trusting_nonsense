[title: Aset (Iside)]  

Per amore lei ritrovò i tredici pezzi  
frantumati dal fratello, sparsi nel fiume.  

Per amore a ricostruzione del suo amato  
a pezzo mancante e perduto nel rettile  
lo fece eretto e di terra madre plasmato.  

Per amore infinito e unione eterna. [m]  
