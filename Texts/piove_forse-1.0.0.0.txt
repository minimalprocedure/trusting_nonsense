[title: piove, forse]

Piove, forse
il sibilo delle tormente.

Le gocce invisibili
che bagnano solo dentro. [m]
