[title: il divano ripulito]

L'ospite conduce doni con sé  
per comprare l'essere accolto.  

Porta le stantie cose dette  
il: ciao come stai? Io bene e tu?  

Le facce sedute sul divano scuro  
appena ripulito per l'occasione. [m]
