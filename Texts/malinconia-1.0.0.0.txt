[title: malinconia]

Aspetterò l'incontro al momento opportuno,
intanto le riserverò la mia malinconia.

Non me la farò levare da nessuno che non sia Lei. [m]
