[title: chiedere per sapere]

Io a volte chiedo, a volte no.

Mi piace scoprire lentamente, supporre e pervenire piano piano alla verità.
Amo le persone che mi raccontano le cose e di loro spontanea volontà e mi piace anche rimare nel dubbio a volte, con quel sapore del potrebbe essere. [m]
