[title: il viso e il mare]

Raccolgo nei pensieri il viso
di quando solo a guardare

era una scossa alla schiena.

Nel liquido più denso e liscio
immersi in acqua di mare stretti

e le gambe tra le gambe avvolte. [m]
