[title: guardare le cose con occhi diversi]

Mi metto gli occhiali, mi tolgo gli occhiali
Ci vedo bene, ci vedo meno bene.

Mi piacciono quelle linee sfumate intorno ai bordi,
fanno vedere il mondo più poetico. [m]
