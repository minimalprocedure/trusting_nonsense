[title: mi soffermo al ricordo]

Mi soffermo un momento al ricordo
delle tue labbra, al sapore che mi davi.

Al momento giusto, sentirlo stringere
quasi non mi volesse far più uscire. [m]