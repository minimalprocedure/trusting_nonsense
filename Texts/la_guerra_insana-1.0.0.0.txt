[title: la guerra insana]

Non so e tu non sai, 
chi sceglie e chi subisce.

Nello sguardo che scruta
o nel logorio dei nervi.

È questa guerra insana
di promesse da mantenere. [m]
