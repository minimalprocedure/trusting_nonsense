[title: Tutto! Per soddisfarla!]

<< Guardi, tutto per soddisfarla! >>   
<< Ne è sicuro? >>   
<< ... ma certo! >>   
<< Uhm! Non lo so mica... >>   
<< Mi metta alla prova! >>   
<< Guardi che sono esigente, sa? >>   
<< Niente mi intimorisce, cosa crede? >>   
<< Non mi convince. >>   
<< Come mai? >>   
<< È che... >>   
<< Mi dica, mi dica... >>   
<< Non lo so, non oso chiedere. >>    
<< Perché? Guardi che non scherzo! >>   
<< Lei si prende gioco di me, caro signore! >>   
<< Lo giuro, sa come si dice... Giurin giurello... >>   
<< ... che le possa cader l'uccello? >>   
<< Signora! ... ma che dice!? >> [m]
