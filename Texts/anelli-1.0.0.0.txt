[title: anelli]

Ho regalato un solo anello nella vita e tanto tempo fa, perché __lei__ ci teneva da morire e io l'amavo da morire.
Poi ovviamente come tutte le cose che si amano da morire, vanno a puttane. [m]  
