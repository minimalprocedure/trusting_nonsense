[title: scivolare senza meta]

Quando il passo avanza di poco
è come scivolare senza meta.

All'aria coi vestiti alzati
opposti al vento ed esposti al mondo.

Udendo dita veloci che alternano premendo
quei tasti neri a quelli bianchi. [m]
