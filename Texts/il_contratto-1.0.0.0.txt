[title: il contratto]

Vieni, sediamoci al tavolo  
ho portato carta e penna.  

Scrivi con l'inchiostro  
cosa di te a me appartiene.  

Non dimenticare proprio nulla  
lasciando in calce poi, il nome. [m]  
