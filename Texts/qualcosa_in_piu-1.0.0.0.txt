[title: qualcosa in più]

Prendi una penna e disegna dei cerchi
uno dentro l'altro, uno attraverso l'altro.

Prendi un foglio grande, salici sopra
disegna il cammino della tua vita.

Piegalo bene, mandami il pacco
lo attaccherò al muro e lo guarderò la sera. [m]
