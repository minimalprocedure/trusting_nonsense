[title: la vita segreta]

La vita pacata, la spesa, il pranzo  
figli tornati da scuola, mariti, mogli.  

Nel cassetto vicino, della stanza  
il letto di ferro tornito a ricci.  

L'acciaio e canapa degli oggetti  
il legno, pieno di caldo bagnato. [m]  
