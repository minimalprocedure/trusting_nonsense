[title: con carta e penna]    
    
Con carta e penna, il lucido dell'inchiostro...    
    
Ho imparato a scrivere della materia per farlo, l'estetica del segno e del valore semantico, uniti nella loro forma e arte.
Nel piacere della carta, della fibra ruvida o liscia alle volte, delle penne e degli inchiostri e dei colori, delle sabbie scure, in un paese con il mare.
Nelle viste lontane, sulle montagne solo per vedere gli immensi.    
    
Oggi, vivo, nella mente dei miei occhi, che vedono quello che non c'è, quello che gli altri solo immaginano e mi perdo. Mi perdo, conscio, mi perdo per il fascino di ritrovare la strada, ascoltando i rumori, leggendo le tracce, toccando i tronchi della foresta che mi raccontano la via.    
    
Ho visto la realtà a mio piacere, giocando coi pezzi, con i corpi divelti dal capo e riempiti di solo senso, delle parti viste e interpretate e a volte distorte ma di essenza, vere.    
    
Ho modificato la materia, estraendola a schegge con forza eliminate, materia di calore e di forme, per un'amante donata nelle sembianze della testa ideale e modifica dei pensieri.    
    
Ho con colori e modelli, descritto il modo dell'uomo di capire, dei motivi delle parole, dell'azione del conoscere, del progresso e della fine. 
Ho costruito ponti verso squarci dello spazio, catene per impedire alle energie di disperdere. 
Maniache parti collegate, di piccoli elementi, sopra l'uno all'altro in un crescendo di spirali e dimensioni.    
    
Continuo a scrivere, come ogni volta, seguendo quelle parole usate insieme ai suoni creati, per poter riprendere quei momenti incontrollati che ti sfuggono davanti.    
    
Chiudo gli occhi per vedere quel momento che sarà e che sento vicino, come non mai, ma forse solo a volte non capito e ingannevole, nella fretta di possesso. [m]
