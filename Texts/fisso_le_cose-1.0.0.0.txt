[title: fisso le cose]

Fisso le cose finché lo sguardo si sfuoca, 
mi lacrimano gli occhi ma non li chiudo. 

Inchiodatemi le palpebre per non poterli più chiudere, 
lasciate che assapori la luce e il buio, 
lasciatemi dolere dallo sforzo di guardare. [m]
