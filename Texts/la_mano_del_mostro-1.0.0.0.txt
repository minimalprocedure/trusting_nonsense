[title: la mano del mostro]

Ci si aggira, la notte  
nella ricerca delle paure dei bimbi.

Quei mostri, nascosti  
che oggi ci violentano di piacere. [m]
