[title: sono un uomo e sono vivo]

"Sono un uomo" e sono vivo
sono la materia del comprendere,
sono l'intero delle cose possedute,
sono il groviglio dei ricordi nascosti.

"Sono vivo, sono un uomo"
assaporo quei profumi
e mi uccido da solo, per poi vagare. [m:1992]
