[title: il tuo sogno più bello]

Ognuno ha una valigia dei sogni appartenuta ad altri.  
Forse tu hai la mia, chi potrà mai saperlo.  

Le foglie dei sogni sono scritte in lettere piccole  
per lasciarsi trasportare oltre le cime degli alberi.  

In una giornata ventosa, ne vedrai scendere piano una  
tutte le altre voleranno, ma quella cadrà giusto vicino.  

Prendila e leggine le parole, sarà il tuo sogno più bello. [m]
