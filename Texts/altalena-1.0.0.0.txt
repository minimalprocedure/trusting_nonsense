[title: l'altalena]

Avanti e indietro, dal dietro all'avanti  
le catene cigolanti, seduta sul legno.  

La pelle nuda che preme, i polsi in alto  
le caviglie ferme sono poco in basso.  

Sempre più forte spinge  
oscillando verso il cielo. [m]  
