[title: nei sensi]

Nei sensi delle linee,
che sfuggono al tatto.

In quei baci leggeri,
nelle curve sensibili.

Si alza e si lascia
esausta nel sonno. [m]
