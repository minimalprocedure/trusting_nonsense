[title: ridi]

Ridi mia bimba, mia donna, ridi  
butta indietro quei tuoi capelli.  

Guardami e ridi, son tuo buffone  
ti faccio le smorfie, così tu ridi.  

Poi ti dirò delle cose all'orecchio  
per farti ridere e ridere ancora. [m]  
