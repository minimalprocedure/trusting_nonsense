[title: ... è ora conosciuto]

Avventure appassionanti di luoghi e genti
percorsi immoti di corpi volti allo sguardo.

Spalla a spalla nel nudo della pelle
stretti nelle mani tirando forte a sé.

Uno con uno, girando sempre più forte
fino allo schiocco del tremendo distacco. [m]
