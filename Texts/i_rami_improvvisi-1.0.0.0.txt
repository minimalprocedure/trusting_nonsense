[title: i rami improvvisi]

I rami improvvisi mossi dal vento
fermi quando mi giro per vedere.

Di una fine estate come tante
passata tra le sabbie di mare.

Sognando tempeste mai arrivate
e cavalli d'acqua che portino via. [m]
