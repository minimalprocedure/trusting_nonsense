[title: linee rette]

Il mio essere cordiale e disponibile, non presuppone che non abbia interessi precisi e spesso non particolarmente identificabili all'apparenza.
Il mio cervello segue strade convolute e contorte e quindi non è opportuno cercare di vedere linee rette in me. [m]

