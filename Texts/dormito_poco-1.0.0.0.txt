[title: dormito poco]

Dormito poco, sonno agitato.
ho voglia di passeggiare in una folla di maschere bianche, di visi informi, persone idolatre che dimenticano di guardarmi. Mi serve di spingerli via, con forza e con le mani.  
Correre nello spazio scostato da chi lascia passare e distratto sospira.  
Lasciatemi stritolare ogni parola d'amore, sputare sui baci dati.  
Lasciatemi saltare sul treno in corsa, lasciate polverizzare il mio rientro nella natura.  
Ho parlato, ho lasciato che la mia voce trasalisse verbi per raccontare quel nulla che circonda. Nella foga dell'amplesso degli sguardi.  

Lei fugge e chiama, mi afferra e tira, mi accoglie e mi entra dentro.  
Le sue corde sono ruvide e gentili, le mie catene riscaldate dal corpo. La vedo girarsi offrirsi aprirsi, perché le mie mani la cercano, esplorano i suoi interni. Perché devo afferrarla dal di dentro per sentire un suo respiro?
Lei mi dice, quello che mi si nasconde, che si mostra e scappa e rallenta per farsi raggiungere.  
Dammi un morso, donna, un morso tra le scapole, dimmi che non sei mia, dimmi che ami un altro, dimmi che nulla sono, dimmi per farmi tornare demone.  

Per possederti con furia. [m]
