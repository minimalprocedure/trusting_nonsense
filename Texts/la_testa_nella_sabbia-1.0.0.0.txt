[title: la testa nella sabbia]

Dagli uomini, i più dementi raccogli
il peggio delle cose e ti nascondi.

Nell'essere cosa per non pensare
materia morbida da battere e pestare.

Ad ogni colpo sei più nascosta
ad ogni colpo dimentichi di più. [m]
