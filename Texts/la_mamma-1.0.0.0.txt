[title: la mamma è sempre la mamma]

Hanno sognato che mia madre era morta, poi che si era risvegliata.
È morta di nuovo, ma di nuovo si è risvegliata.

Mia madre non l'ammazza nessuno o né il diavolo né dio la vuole. [m]
