[title: possesso]

Alzando i capelli sciolti
ti parlo all'orecchio di sensi.

Il significato dei suoni alle mia labbra
che chiedono di muoverti,

nel momento del possesso. [m]
