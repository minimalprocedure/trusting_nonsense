[title: a volte sono un idiota]

A volte sono davvero un idiota, ma un idiota serio, mi perdo nelle cose. Non si può pretendere e neanche sperare o desiderare che le persone siano quello che non sono.
Se ne esce solo un po' delusi, quando si raccontano cose che si ritengono importanti senza ricevere cenno alcuno. Non importa, imparerò prima o poi, vorrei solo farlo senza perdermi nella freddezza dell'indifferenza. [m]