[title: la partita iva]

La partita IVA si può considerare a tutti gli effetti una pratica sado-masochistica ad ampio spettro. Scatena sensazioni tra le più disparate.

Si va dal prostituirsi a comando per chi ti paga gli F24 quando i soldi non li hai alla smodata voglia di schiavizzare la tua commercialista che te li invia sempre a ridosso scadenza. È foriera quindi delle più malsane e oscure pulsioni. 
Poi basti pensare ai nomi delle pratiche da affrontare: IRPEF, CCIAA, ILOR, INPS, IRAP, IRES, IVA e chi più ne ha ne metta o ne prenda. 

BDSM? Mi fa una pippa... Qui ti frustano col trabucco, mica lo scudiscio. [m]
