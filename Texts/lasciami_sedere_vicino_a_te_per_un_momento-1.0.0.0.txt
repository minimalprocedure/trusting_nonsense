[title: lasciami sedere vicino a te per un momento]

Se mi sedessi vicino, mi racconteresti una storia?
Se immaginassi di poter vivere, mi faresti un sorriso?
Se un giorno non fossi più qui, mi vorresti dimenticare?

Quando mi alzerò da te, lo farò per non tornare
Lascia che nessun ricordo rimanga, come mai vissuto.

Il tuo viso si perderà tra i molti, confuso. [m]
