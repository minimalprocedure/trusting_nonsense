[title: analisi del modo]

Nell'analisi del modo e del mondo  
mi perdo a volte troppo incauto  
ma e ogni volta io poi mi chiedo.  

Semplice sento il calore che mi arriva  
nel viso immaginato, negl'occhi chiusi  
che amerei veder dormire, poi riaprire.  

Nelle mani strette in quei momenti  
con le voci un po' tremule che sfuggono  
sono le cose pensate tanto e così temute.  

Dai fondi di ognuno  
le figure si muovono rotonde e vacue  
da lontano forse, loro arrivano.  

Dai tutti quei paesi vissuti e da soli  
vengono senza pretese e si tengono per mano  
e i loro visi per un giorno, s'illuminano. [m:2000]  
