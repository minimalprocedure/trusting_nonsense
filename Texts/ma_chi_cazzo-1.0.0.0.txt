[title: ma chi cazzo è?]

**[anon]**  

ma chi cazzo è sto thomas che gli scrivi ogni tanto? che si frocio e lui l'amante?

**[m]**  
Caro anonimo sboccato, mi sto scervellando su chi tu sia. Donna, uomo o chicchessia.  
Come vede sono riuscito anche a far di rima.  
Oddio! Lei mi crede omosessuale? No guardi, benché non mi manchino nel pregresso amici di tale inclinazione, persone oltremodo brillanti e intelligenti, ahimé no.  
Ha qualcosa per caso contro di loro? No mi dica che nel caso sa, un bel _viaggio oltre i confini del mondo_ glielo auguro volentieri.  

Prima di diventare però indisponente, le rispondo.

Chi è Thomas.   

_August Müller_, protagonista de _Lo scrutatore d’anime_ di _Georg Groddeck_, si trasforma a causa di una serie di accadimenti in _Thomas Weltlein._
_Thomas Weltlein_ è in una costante ricerca del _godimento assoluto_ e della _liberazione totale_ da ogni convenzione e costrizione sociale. È in qualche modo è la personificazione dell'Es di _August._  

Preso a prestito, _Thomas_ diventa in queste brevi _missive_ un interlocutore non giudicante proprio perché al di fuori di ogni convenzione. Un _Es_ che racconta all'altro.  
Ecco, Thomas è sia me che altro da me in questo discorso interiore.  

Per cui vede oltre che sembrar _frocio_ come dice lei, paio pure matto con l'amico immaginario. 

Servo suo, ma non troppo e si pari il culo che come si dice: _hai visto mai!_ 

[m]
