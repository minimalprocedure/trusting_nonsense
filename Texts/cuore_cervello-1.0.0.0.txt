[title: cuore e cervello]

Nell'eterna lotta tra cuore e cervello, non vorrei deludervi ma il cuore è una mera pompa che continua a battere anche dopo che il cervello è morto. Poche gocce di adrenalina da un contagocce fanno il miracolo o il giusto impulso elettrico.

Siamo rimasti dopo millenni agli antichi egizi, che mettevano il cuore nel vaso e buttavano il cervello dopo averlo tirato fuori dal naso come moccio con degli speculi. [m]  
