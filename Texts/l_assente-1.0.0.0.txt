[title: l'assente]

L'assente che è in noi
ci lascia un sapore amaro.

Quel vuoto da colmare
con le poche speranze raccolte.

L'uomo solo con la gente
che si gira intorno, guardando. [m]
