[title: stasera vorrei lasciarmi]

Stasera vorrei lasciarmi,
puoi giocare come più lo desideri.

Mi dono come cosa a te donata.

Mia Signora, oggi ti lascerò fare,
oggi non sarò niente, ma tutto nelle tue mani. [m]
