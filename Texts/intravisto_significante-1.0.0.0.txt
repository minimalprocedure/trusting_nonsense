[title: l'intravisto significate, ovvero gli spazi tra le parole che vanno molto di moda]

Ci vuol ben altro signora mia  
per chiamar tale, alta poesia.  

Lo verbo messo a riga, buttato là  
par che sia emozione che fa parla'.  

Però le confesso con cotanto ardire  
ch'è lo spazio perso che fa perire. [m]  
