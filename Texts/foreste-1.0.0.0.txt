[title: foreste]

Mi siedo in mezzo alla stanza  
dove il pavimento è più freddo  

unisco e apro le mani, l'intorno sparisce.  

Davanti a me solo magnifiche foreste  
dove ascoltare quel cantar dei lupi. [m]  
