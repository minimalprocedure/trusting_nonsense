[title: bisogna pensarci bene prima, dopo ormai è fatta]

Prima di chiudere un blog bisogna pensarci bene, perché poi come si fa con i seguaci?  

Mettete che _pincopallo163_ non lo ritrovate più o _cippetta67_ sparisce, addirittura se _comeuncetriolo_ o _studiavopocomasonpassatalostessograziealmioculo_ svaniscono?
Pensate all' _uomocheleggevalibrimanoncapiva_ o a _ranzapicchio21sonoicentimetri_...

Dio mio e _chiappatondaladavia_?
