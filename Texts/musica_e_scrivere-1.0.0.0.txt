[title: la musica e lo scrivere]

Adoro la musica minimalista, mi piace la carica ripetitiva e la sua ricerca della armonia complessa nella semplicità. Adoro rimanere fisicamente senza suoni ma interiormente pieno di melodia, il continuare a sentire il suono quando esso è ormai finito.  

Nel mio passato di musicista mancato ho sempre ricercato di sviluppare questo sistema, suonare era come è il mio scrivere o come cerco che sia: un crescendo di sensazioni che si troncano improvvisamente lasciando libera la mente di vagare. [m]  
