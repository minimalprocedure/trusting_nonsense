[title: cammino nel vento]

Cammino nel vento con le mani in tasca,
vedo intorno cose che volano,

alzo lo sguardo al cielo un po' distratto
e mi fisso a lungo a pensare. [m]
