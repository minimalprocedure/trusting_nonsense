[title: sai che ti dico?]

Sai che ti dico? 
Io adoro chi nella testa ha un pozzo profondo.
Chi si perde in pensieri convoluti.
Chi ama incondizionatamente.
Chi si concede senza ritegno.
Chi si fa possedere perché possiede con forza.
Chi ha l'anima oscura dalle mille sfumature.
Chi mi fa perdere la testa, cosa non facile. [m]

