[title: Ti preparavi a un funeral party di luna piena.]

A ricoprire quello che ne rimane  
le immagini mi frullano davanti, molte.  
 
Il vacuo senso di un viso che si allontana  
elfa cadente, passi nel cielo lasciando scie di luce.  
 
Le narici vibranti, la criniera scomposta  
tu sei l'amore che accarezza. [m]  
