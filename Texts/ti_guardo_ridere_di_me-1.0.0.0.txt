[title: ti guardo ridere di me]

Mi guardi e ridi delle cose che dico  
i tuoi occhi lucidi, ti asciughi di lato.  

Ascolti il far da buffo, le smorfie  
il goffo muoversi, gesticolando le mani.  

Racconto per te commedie colorate  
di giovani amanti persi e ritrovati. [m]  
