[title: amore]

Amore è una cosa che mi porto dietro e non mi riempie mai, 
quella persona spesso lontana con cui parlo, 
a cui racconto e che ogni tanto sbadiglia.

Amore che mi avvolge e mi solleva
che mi fa camminare da ubriaco.

Amore che manca, amore dato e ricevuto
alle persone sbagliate così tanto spesso.

Ma Amore è la mia vita, come posso non darlo. [m]
