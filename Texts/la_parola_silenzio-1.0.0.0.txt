[title: la parola 'silenzio']

Texts git:(master) ✗ ack-grep **silenzio**    
come_posso_spiegarti-1.0.0.txt    
22:Il **silenzio** cupo delle nevi profonde    
    
errore_finale-1.0.0.0.txt    
77:ascolto il suono del **silenzio** dentro di me    
    
i_voli_ancora-1.0.0.txt    
14:Lontano e **silenzio**    
    
l_avidita-1.0.0.0.txt    
4:aspetto senza l'ansia di un possibile **silenzio**.    
    
movimento_introduttivo-1.0.0.0.txt    
10:da quel piccolo momento di **silenzio**. [m]    
    
a_volte_le_cose_basta_dirle-1.0.0.0.txt    
8:Quando racconto, mi piace unire le parole per suscitare reazioni, per creare quelle atmosfere che ognuno può a suo modo trovare avvolgenti, senza ritorno, a scopo del **silenzio**. I miei contatti sono speculativi nei nuovi modi di pensare che incontro, monotoni e ripetitivi.    
    
la_palla_che_batte_sul_muro-1.0.0.0.txt    
6:Nel **silenzio** ammiro il tutto    
    
alla_fiera-1.0.0.0.txt    
6:Mangiatori di fuoco, nel **silenzio** attutito    
    
ansito_della_pressione-1.0.0.txt    
7:Nel passato quel **silenzio** provocato    
    
una_donna-1.0.0.0.txt    
10:spogliandosi al suono del **silenzio**,    
    
vorrei_poter_dire_mille_cose-1.0.0.0.txt    
4:ma no ho che il **silenzio** con me.    
    
il_vuoto_nella_testa-1.0.0.0.txt    
10:**silenzio**so oggi, mi faccio solo vuoto nella testa. [m]    
    
e_sei_un_viso-1.0.0.txt    
11:nel vento terso, nella sera parlare in **silenzio** con un viso vicino,    
    
buongiorno_al_**silenzio**_ed_alle_volpi-1.0.0.0.md    
1:[title: Buongiorno al **silenzio** e buongiorno alle volpi.]    
3:Buongiorno al **silenzio** e buongiorno alle volpi.    
6:La volpe sta in **silenzio**, pensa e scruta.    
8:Il **silenzio**, che vive spesso nell'assordante e caotico mondo che lo circonda. Il mio **silenzio** e il **silenzio** dell'altro.    
13:Un disegno grande su un foglio grande e attaccato al muro, a larghe bracciate lo farei. Calcando nello scuro dei contorni, cercando il **silenzio** del suo sguardo.    
15:La gente che non racconta è nel **silenzio**, quelli che lo fanno sono nel **silenzio**. Ci parliamo non sentendoci mai, nel **silenzio**.    
17:Nel **silenzio** ti guardo come sei, nel **silenzio** ti immagino. Nel **silenzio** sei come solo io so come sei.    
19:Apri bocca e chiudi bocca, rumore e **silenzio**. Allo specchio chi si guarda e a volte si piace e a volte no, rumore e **silenzio**.    
21:Siamo rumore e **silenzio**.    
23:Gridiamo amore e odio, rumore e **silenzio** e **silenzio** e rumore. Ci guardiamo nelle scorrere di tutti i giorni: il **silenzio**.    
25:Le persone vicine nel **silenzio** scorrono in fila, una dopo l'altra: << il prossimo? >>. Nel **silenzio** si scambiano per chi viene e chi va.    
27:Mi tappo le orecchie per non sentire, mi metto le cuffie per sentire. Musica e **silenzio**, è la misura dell'armonia e del progressivo divenire del suono.    
29:Sono 4.33 minuti di **silenzio**, il numero perfetto, il suono perfetto. L'armonia transiente della semantica del suono.    
31:Nel mio **silenzio**, mi guardo intorno    
37:Solo nel **silenzio**, mi guardo intorno    
42:Attende in **silenzio**. [m]    
    
claudio_ricordo-1.0.0.txt    
48:Il nulla, **silenzio**, cliente non collegato.    
    
donne_che_amo-1.0.0.0.txt    
5:Non mi piace la donna smaccata. Preferisco le allusioni, i giri di parole o il **silenzio**. La donna che per farsi emancipata si comporta come l'uomo stereotipato. Questo credo che sia un problema di tutte le parti tenute in inferiorità dalla parte dominante, nella fretta della emancipazione ci si comporta esattamente come il peggio del proprio antagonista senza sviluppare caratteristiche proprie. Sviluppare la propria particolarità invece che massificarsi al peggio.    
    
non_tutte_le_giornate_vengono_col_buco-1.0.0.0.txt    
16:Quel giorno non è scesa una luce dall'alto e né una voce possente, calando una mano dal cielo, ha parlato, si è svolto tutto in un completo **silenzio** e nessuno se n'è accorto, hanno cominciato tutti di nuovo a camminare e parlare tra di loro.    
    
nel_**silenzio**_attendimi-1.0.0.0.txt    
1:[title: Nel **silenzio**, attendimi.]    
7:sciolti nel **silenzio** assordante di qui.    
    
camminare_la_sera-1.0.0.txt    
5:nel pensare il **silenzio**.    
    
il_tramonto_dei_sogni-1.3.2.txt    
43:Attonito **silenzio**.    
68:vicino il volo **silenzio**so di una civetta    
132:L'alito dei cadaveri incessante spezza il **silenzio** di nausea che avvolge a sé.    
172:cade in **silenzio** e si distende nel buio.    
451:ascolto il suono del **silenzio** dentro di me    
    
buongiorno-6.0.0.0.txt    
20:Pare che il trattore abbia finito per ora e c'è un **silenzio** mortale. [m]    
    
illusionista_lanciatore_donna-1.0.0.0.txt    
16:Mi fisso sulle labbra che si muovono, nel **silenzio** delle cose che dice. Parla di cose che non si vedono, cose che non posso toccare. So che non esistono ma le posso vedere, sono quei suoni che emette. Sono una lettera dopo l'altra, sussurrate e gridate; parole in catena come visioni senza occhi.    
63:Ogni cosa è dunque come prima, ogni cosa al suo posto nel niente mai cambiato. Nella Donna a Metà, nei suoi occhi di bottone e la bocca in **silenzio**. Nel Lanciatore di Coltelli dal petto nudo e villoso.    
    
del_conoscere_ovvero-1.0.0.0.txt    
19:Giusto due giorni fa. La donna ci sopporta, in **silenzio** quasi sempre, avrebbe qualcosa da dire ma non lo fa. Senza entrare in chi le donne le 'tonfa' dalla mattina alla sera, ma qui si rientra in altri campi su cui credo che la maggior parte delle persone senzienti siano daccordo.    
    
buongiorno_a_rava-1.0.0.0.md    
7:Qui c'è la nebbia e non si capisce cosa ci sia sopra, sole? Nubi? Tempeste che si scatenano in **silenzio**?    
    
riempita_di_caldo-1.0.0.0.txt    
13:Lui era sempre lì, appoggiato e mi guardava. Il **silenzio** sembrava infinito.    
    
torna_da_me-1.0.0.0.txt    
5:nel suo **silenzio**, nella sua piena realtà.    
    
il_pezzo_di_vetro-1.0.0.0.txt    
3:I pochi colori del **silenzio**    
    
terno_o_ambo-1.0.0.0.md    
11:Mi prende per mano e mi porta in sala. Il divano appoggiato leggermente obliquo, il tavolino da tè in legno scuro è scostato. Due bicchieri usati. Li guardo e sto per chiedere, mi fa un cenno di **silenzio**.    
    
il_**silenzio**_inevitabile-1.0.0.0.txt    
1:[title: il **silenzio** inevitabile]    
7:scivolando gradualmente nel **silenzio** [m]    
    
indietro_nessuno-1.0.0.0.txt    
9:Cammino, guardo, muovo, in **silenzio** parlo    
➜  Texts git:(master) ✗     

