[title: le mani piene e la bocca colma]

Hai le mani piene e la bocca colma
il corpo bagnato, viscido di uomo.

L'uomo raccattato, quello qualunque
dal fallo pensante e le palle gonfie.

Speri che il bruciore tra le gambe
ti aiuti a perdere, quel tuo dolore. [m]
