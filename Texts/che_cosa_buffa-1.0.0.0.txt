[title: che cosa buffa]  

Che cosa _buffa_. Rileggo le vecchie cose scritte e ora mi paiono così inadeguate nelle persone dei ricordi. Come fossero scritte da sempre solo per qualcuno, come se i nomi, i posti, le sensazioni, le situazioni fossero da sempre dedicate a quell'unica persona. Sconosciuta nel tempo che è scorso ma aspettata da così tanto. 

Rileggo e ogni corpo toccato, guardato e usato in quelle parole è il suo. Immagino e vedo lei che mi occupa tutti i ricordi.  

È una sensazione _potente_, materica. [m]
