[title: sarai mia]

... ma sai che ti dico?

È in quel giorno di incontri
che ti guarderò fissa, spogliando.

Nuda di pelle, dovrai lasciarmi fare
nelle miriadi di cose possibili.

Non ti lascerò scegliere né cosa né come
dovrai solo aspettare e poi goderne. [m]
