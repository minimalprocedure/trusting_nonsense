[title: l'amore negli incavi]

Dove vivo io ci sono alte mura  
massi enormi uno sopra l'altro.  

Stretti passaggi con la luce in fondo  
fiamme a lato che fioche illuminano.  

Lunghe passeggiate in vicoli torvi   
con incavi di pietra, scura e nera. [m]  
