[title: le parole materiali]

Poggio ogni strumento al suo posto  
leggermente a portata di braccia.  

Quella distanza non troppo lontana  
giusto lì vicino alla mano spostata.  

Gli oggetti dal piacevole uso  
per chi agisce e ne subisce. [m]  
