[title: la luce che non trovi]

Alla notte nel buio,
quando ti svegli e cerchi con la mano una luce che è vicina e a tastoni cerchi, finché al buio ti alzi e inciampi nello spigolo del letto. Saltelli verso la porta che è irraggiungibile. 
Lei non sente o fa finta, apre un attimo gli occhi e ti sente uscire, ma tu non ti accorgi che forse piange e che sa che non sei più con lei. Lei sa, anche se non ti dice niente. 
Lei e il suo altro,  tu e la tua altra, in quella vita che si trascina. 

Come una persona che si alza la notte e cerca la luce, che non trova. [m]
