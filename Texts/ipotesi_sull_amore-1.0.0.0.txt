[title: ipotesi sull'amore]

È quell'attimo che desta dal torpore  
quel suono che ti squilla metallico.  

Sempre davvero un poco più in là  
corsa a staffetta allungando la mano.  

Segugio che annusa, che perde la strada  
ritrovandosi poi solo, al punto di partenza.  

Una ipoteca sul futuro che ci attende  
un bieco contratto firmato al demonio.  

Mi alzo, dopo apro la porta  
ma è solo un falso allarme. [m]  
