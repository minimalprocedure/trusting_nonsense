[title: con i corpi nudi]

Ci saranno i vestiti, le scarpe
qualcosa buttato lungo la via.

La cena, fredda ormai pronta
da riscaldare per il domani.

Oggi non c'è stato tempo per mangiare. [m]
