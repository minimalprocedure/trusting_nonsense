[title: le navi che partono]

Ascolto da lontano una nave partire 
e navi arrivare, quel lavoro frenetico.  
 
Gridano i marinai, gettando le corde  
altri le raccolgono e insieme legano.  

È d'amore quando si appoggiano i fianchi  
allargando aperture per entrare e uscire.  

Con le mani rudi che passano  
dall'una all'altra carezzando. [m]  
