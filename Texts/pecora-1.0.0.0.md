[title: pecora]

Mi è tornato in mente un fatto del passato, istigato subdolamente da un bel viso.

La pecora.

In una giornata di sole di tanti anni fa: Tresnuraghes.  
Andai a pesca subacquea col capo officina della concessionaria IVECO e Ferrari dove mio padre era direttore commerciale, a Tresnuraghes appunto.  
In quella battuta di pesca, presi una spigola di cinque chili.  

Andò così.  

Con la coda dell'occhio vidi questo pesce nuotare e nascondersi dietro una serie di scogli, quindi velocemente mi immersi per aggirarla. Mi avvicinai piano piano e una volta a tiro, sparai.

Dovete sapere che sinceramente in quel posto non ci andai aspettandomi chissà che, ci andai perché mi convinse mio padre, quindi il mio _medisten competizione_ aveva montata una fiocina a cinque punte.

<< Ci ha invitato tante volte, almeno vai te >>.  
Insomma da buon padre premuroso, scaricò l'incombenza.  

Tornando al pesce, lo colpii bene e nelle branchie. Il colpo lo immobilizzò immediatamente e quindi con calma iniziai a tirarlo su per la sagola. Non appena presi l'asta in mano cominciò a vorticare. Dovete sapere che un pesce di quelle dimensioni è molto forte e con lo strappo del movimento iniziò a svitarsi la fiocina.

Per farla corta, fortunatamente avevo dietro il mio fido _miniministen_ carico e con l'arpione, quindi lo presi al volo dalla coscia e gli sparai ancora.

Presa nell'attimo in cui la fiocina si svitò completamente.

Ora però arriviamo alla pecora.

Uscimmo dall'acqua, i soliti complimenti di rito per il pesce e poi l'annuncio fatidico: - Ho degli amici qui, andiamo a pranzo da loro -  
Il terrore corre sul filo. No, vi assicuro che il terrore corre lungo la strada portato dal puzzo.  
Ho ancora questa visione apocalittica: una strada in salita con in cima una casa gialla. L'odore che aumentava ad ogni passo, la speranza che non venisse dalla casa.  

La pecora bollita.  

Mi sentii svenire all'entrata dalla porta, in cucina un pentolone di alluminio degno dei peggiori film fantasy di serie B o C o D o zeta.  
Una vecchia in nero cupo, bassa e inquartata, che con un forchettone tirava su pezzi di pecora bollita piena di grasso tremolante giallo da un liquame lattiginoso.  
L'aria si tagliava col coltello e si spalmava sul pane.  

Taniche di cannonau, impicciavano dentro la sala da pranzo. Vino che a diluirlo in 1/3 parti di acqua fai del vino rosso pesante. Era cremoso, mica liquido.  

<< vuole del brodo? Ci vuole la carne? >>  
<< Il brodo va bene grazie, senza carne per favore >>.  

Ci sbriciolai un po' di spianata di Ozieri secca e tentai l'avventura.  
Allora, il brodo di pecora puzza come voi umani non potete immaginare, ma è buono e quindi basta tapparsi il naso.  

La carne? Boh! Divento vegano piuttosto. [m]

