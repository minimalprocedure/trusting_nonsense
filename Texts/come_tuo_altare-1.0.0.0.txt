[title: come tuo altare]

Ti sei aperta a me sedendoti sopra
nell'accesso stretto, mi hai catturato.

Spingendo dall'alto in sacrificio
come tuo altare di sorreggo da sotto.

Tenendomi ai polsi, ondeggi il fianco
mi tiri e mi spingi a che possiamo venire. [m]
