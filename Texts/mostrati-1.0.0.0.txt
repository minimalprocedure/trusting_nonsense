[title: ti mostri]

Negli sguardi degl'altri tu, ti mostri  
nuda di carne ma curata in ogni dove.  

Come infante, chiara di pelle ti muovi  
istigando così nei vecchi torbide bave.  

Con le mani sfreghi e passi, stringi  
strizzi e spingi, allarghi e chiudi. [m]  
