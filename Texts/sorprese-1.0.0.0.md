[title: sorprese]

**[anonimo]**  

**Titolo**: sorprese al risveglio.   
**Sottotitolo**: quando improvvisamente arrivano tutte tranne quella che aspetti.   
**Sequel**: che cazzo di sorpresa sarebbe?  

**[m]**  

Le sorprese al risveglio, ci sono quelle che ci si aspettava ma si sarebbe preferito evitare, quelle che improvvisamente ci impastano la bocca da digestione cipolla e peperone, quelle che oh! Quelle che ah!  

Le sorprese dovrebbero per definizione essere improvvise altrimenti perché definirle sorprese. Sorpresa, una parola dalla etimologia oscura, forse viene da _sor e presa_, _presa_ nel senso di afferrata ma _sor_?  
Analizziamo attentamente. _Sor_ potrebbe stare per _sora_, insomma _suora_ o _sorella_ con il tutto un po' alla romanesca: 'a sora Lella. Quindi: _suora presa_, nel senso di _afferrata_, _chiappata_ e forse anche _inchiappata_.  
Certo che detta così suona tutto un po' blasfemo però o perlomeno incestuoso, no ci deve essere senz'altro un'altra spiegazione.  

Sorpresa dovrebbe derivare, se la memoria non mi inganna, dal participio futuro passivo di _superprehendō_ (mi perdonino i latinisti) che al nominativo femminile singolare dovrebbe fare _superprehensa_. Scavando ancora a fondo lo stesso _superprehendō_ si potrebbe scomporre in: _super_ e _prehendō_ e _prehendō_ in _pre_ con _hendō_.  

Santa Paletta da PiccolPasso!  

Ricapitoliamo, se _hendō_ equivale nella lingua proto europea a _cogliere_ e _prae_ a _prima_, _super_ come _sopra_ o _che sta in alto_ allora avremmo: _dall'alto prima cogliere_. Se poi vediamo il _prima_ come un qualcosa che arriva prima e quindi inaspettato potrebbero andare il: _raccogliere dall'alto l'inaspettato_. La Sorpresa.  

Visto poi che è il tutto _passivo_ la sorpresa la si subisce e non se ne può discutere.   
La vuoi? Non la vuoi? Niente da fare te la _ciucci_ e basta. A volte son gelati, a volte caramelle e a volte pure cazzi (metaforici non pensiamo subito male).    
La sorpresa non si può aspettare quindi, non lo sarebbe altrimenti. La si prende e festa finita.  

Oddio e citando la Santa Di Prima da Cavallina Storna, la sorpresa del cazzo o al cazzo potrebbe a volte esser pure gradita. [m] 
