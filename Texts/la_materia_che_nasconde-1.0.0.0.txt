[title: la materia che nasconde]

Nella casa, negli angoli, il pavimento  
le mura, finestre, porte e spigoli.  

La materia che nascondo agli altri  
il sudore e i rumori, voci e sospiri.  

L'ansito al colpo, il vibrare della carne  
l'esser posti, impossibili al muoversi. [m]
