[title: in disparte vi guardo ballare]

Il _valzer dei mostri_ suona accanto  
mentre l'orchestra soffia nei fiati.  

Vi vedo ballare, mentre voi ruotate  
e i capelli tuoi, sono sul viso di lui  

e come sorride, lui che sa di averti  
tu lo guardi e poi abbassi gli occhi,  

stringi le mani e te lo porti via. [m]
