[title: amore mio perso]

__Di quei momenti, quando vorresti uccidere i sogni, scrollare la patina di vecchiezza, quella polvere sulle mani che risuona al tremito.__    

Le immagini che poi fai e che desideri    
che non mancano, che vivono e muoiono  
che sono tutte, tanto avvolte in un corpo.  

Ritorna il passato sì pressante e potente  
nel sospiro contorto, l'afferrare di labbra.  

Nell'umido e dentro la curva che segui  
che poi socchiude e che quindi ti ammira.  

Volgi altrove l'emblema per donare il resto  
e delle mani strette e soffocate dal calore.  

Amore mio perso, amore mio perso. [m:1991]
