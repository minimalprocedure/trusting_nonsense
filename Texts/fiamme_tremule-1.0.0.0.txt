[title: fiamme tremule]

Nella distanza immensa dei corpi
nella misura di tutte le voglie.

Nella carne erta, turgida di sangue
nel morbido gonfio, bagnato e buio. 

La luce si spenge in notte ancora
in fiamme tremule, lambenti le menti. [m]
