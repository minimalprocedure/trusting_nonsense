[title: ricostruzione]

Pezzo a pezzo, la casa da ricostruire
per avere ancora cose da vivere.

Poso la mano sui tuoi mattoni
tra le macerie delle tue cose.

Pulisco dalla vecchia calce
per rimetterne di nuova, forse migliore. [m]
