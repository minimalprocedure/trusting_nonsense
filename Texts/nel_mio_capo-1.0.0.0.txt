[title: nel mio capo]

Mi ritrovo spesso nei mondi del mio capo
paesi dove tutto va male o tutto va bene.

Montagne sferzate, boschi di vento
foglie vaganti di pensieri scritti.

L'immagine di poter toccare la felicità
o cadere nel'oblio informe. [m]
