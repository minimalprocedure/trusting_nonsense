[title: alzare una mano]

Mi basta certe volte alzare una mano
un cenno di saluto insieme alla testa

mentre cammino ondeggiando appena
a volte anche, tenendole in tasca.

Sono i piedi quelli che avanzano
comandati o no non fa differenza. [m]
