[title: una carezza]

Una carezza per asciugare 
una lacrima di pioggia
sul tuo viso.

Ed un bacio
per vederti sorridere.
