[title: il mare lontano]

L'odore del tuo mare nello scricchiolio del pontile,
attendo quanto basta e mi lancio al passaggio.

Lo sento cedere, come facevi tu. [m]