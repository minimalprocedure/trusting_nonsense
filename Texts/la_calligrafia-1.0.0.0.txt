[title: la calligrafia, che sarebbe un buongiorno ma a metà]

**[anonimo]**  

Calligrafia sua?  

**[m]**  

Buongiorno a tutto il mondo, a qualcuno di più e ad altri meno. La vita va così non me ne fate una colpa.  

Caro anonimo affezionato, sa che la sua domanda mi ha fatto tornare alla mente i lontani anni '80. Quando _sbrindellato_ _dieciassettenne_ avevo la delega dei miei per firmarmi le assenze?  

_Calligrafia_ mi dice, qui si potrebbe opinare il _calli_ che starebbe meglio nel _callipigia_ e forse un poco meno nel _callipigio_, anche se... Ma lasciamo perdere 'sti discorsi.  

Iniziai a scrivere in quella che chiama esagerando _calligrafia_ nel lontano 1980. Deve sapere che allora frequentavo una quarta liceo nella mia cittadina natale, dopo la fuga più o meno obbligata da quell'isola in mezzo al mar dove ci sono (non ci sono?) camin che fumano. Frequentavo quindi a giorni alterni questa classe di _disadattati_ seriali, dove avevo per prof di filosofia e storia una ex suora, che ricordo con affetto, poi una prof di matematica decisamente gnocca e anche questa la ricordo con parecchio e oserei dire _smodato_ affetto. A parte gli altri di insulsa presenza, spiccava la prof di italiano e latino: la _tucia_.  

Dovete sapere che la _tucia_ era la moglie del _tucio_, ovvero il preside del liceo in questione.  
Forse qualche lettore potrà capire se della mia generazione e del luogo e del tempo di chi stia parlando.  

La _tucia_ faceva l'appello e firmava il registro, anzi _sfiorava_ il registro. Con la penna leggera partiva dal fondo della pagina per arrivare in cima a mo' di movimento tellurico di nove Richter. La _tucia_ era un _donnone_ inquartato, almeno così lo ricordo ma pronto alla smentita, dalla parlantina _noiosa_ ma talmente _noiosa_ da azzardare un _oltremodo noiosa_.  
Poi c'era il _tucio_.   
Il marito della _tucia_ era pure lui assai inquartato ma di quella _inquartatezza_ evidente: basso e quadrato. Tutto era quadrato nel _tucio_, dalla testa ai piedi. Chi conosce il noto giochino a _voxel_ capirà immediatamente, agli altri è richiesto uno sforzo immaginativo. Il _tucio_ da _squadrato_ aveva anche la voce _squadrata_. Si ipotizzava infatti che fosse in realtà un automa segretamente assemblato dalla _tucia_.  
Qualunque fosse la spiegazione erano una coppia perfetta, i Tuci, ovvero il _tucio_ e la _tucia_.  

La mia _grafia_ corsiva è possiamo dire _particolare_, non ci si capisce una mazza per essere chiari. La _tucia_ probablmente stizzita dalla fatica del leggere, dubito però che ci abbia mai provato, mi forniva del famigerato sei, che non era quello politico ma sulla fiducia. Sulla fiducia? Si, sulla fiducia.  

Dovreste conoscere il mio cognome e le lontane parentele per capire, ma fidatevi che per lei era impossibile che non sapessi scrivere in italiano e tradurre le versioni di latino. Sei sulla fiducia.  

Allora ero un _giovine_ impertinente appena appena nullafacente, quindi elaborai una strategia per ovviare al sei. Iniziai a scrivere in _stampatello_ ma pure li svolazzavo il giusto.  

Alla prima prova di codesto cimento, versione di latino o tema di italiano che adesso il ricordo sfugge, mi procurai una penna in verde e produssi un ottimo testo.  

Ahimé però, il verde fu una scelta poco saggia e finii dritto filato dal _tucio_ per due giorni di sospensione.  

Da allora scrivo davvero di rado in corsivo e nemmeno per me che l'abitudine come si sà, prende sempre il sopravvento.

Fine della storia.  

_lunga e la strada e stretta è la via che portava a casa mia, a casa mia c'è una fontina e mi ci lavo le mani e il viso e poi di corsa in paradiso_.  
Questo lo diceva sempre la mi' nonna quando finiva la favola della buona notte, specialmente quella _del mondo di sotto_ che ogni volta era piacevolmente diversa.

Servo vostro, a tratti però. [m]
