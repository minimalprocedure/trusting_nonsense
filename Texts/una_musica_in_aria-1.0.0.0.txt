[title: una musica è nell'aria]

Fondo le note che sento con quello che leggo
e nel mondo mi risplendono gioie infinite e oscuri tremendi. [m]
