[title: alzo le carte]

Alzo le carte  
con davanti a me il nulla  
per ritrovare la XII casa.  

_Il gioco nella dodicesima notte._  

Mi troverai nel momento del fuoco  
bruciando lento come libro smesso.  

In una città dove tre si amano. [m]  
