[title: il santone arcano]

[la via della seta]

Lui è seduto sull'angolo della strada,
appoggiato sulle mani.

Dal basso, fin verso la curva
alcuni salgono per trovarlo.

Toccano i suoi occhi chiusi,
le sue braccia nude, mentre guarda l'eterno. [m]
