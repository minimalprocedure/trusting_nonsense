[title: il sospiro dell attesa]

Il sospiro dell'attesa.  

Mi siedo e penso,  
la sabbia è fredda quest'inverno  
e ci gioco con le mani,  
attendo.

Sulla sera,  
lei arriva, si siede e mi parla.  
Mi stendo esausto continuando a guardarla. [m]
