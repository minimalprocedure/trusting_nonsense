[title: il quadro delle vite degli altri]

Cerco furioso certi giorni, strumenti pesanti  
con cui poter battere le cose che si rompono.  

Sbriciolare il senso del momento che verrà  
renderlo polvere e da mischiare con l'olio.  

Dipingere con le speranze di molti  
una tela immensa e che prima del colore  
era proprio davvero quasi bianca. [m]
