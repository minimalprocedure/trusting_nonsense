[title: oblio]

Nei giorni che non vedi,
che ti sfuggono e afferri
non sai quelle che svolge.

La mente, gli occhi
il tutto con niente intorno,
le mie mani sono calme da troppo.

Nell'attimo che ti volgi di scatto
e sparisce veloce,
la rabbia della strada che fai.

Di quello che hai bisogno
di quello che vuoi
di quello a volte non si trova mai. [m]
