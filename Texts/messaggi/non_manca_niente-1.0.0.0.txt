[title: non manca niente]

Buongiorno,
hai ragione e si dovrebbe reagire sempre.
Quello però succede e succede sempre, non appena ti 'spogli' c'è subito l'agguato. Di questo sono stanco.
Ad un certo punto poi inevitabilmente si fanno i computi della propria vita, sono calcoli quasi puri e freddi, ti accorgi di cosa hai fatto.
Non parlo di rimpianti che non ho o di errori o cose buone, solo di mero calcolo. Ci si accorge che in fondo non si è creato quasi nulla, se non una routine continua. Un ciclo che si ripete e si ripete.
Ci sono poi continue delusioni che mi rendo conto sia per la maggior parte colpa mia, io idealizzo le persone e cerco di fargli tirare fuori il meglio ma loro non lo vogliono fare molto spesso. Si accontentano forse, non lo so.

Non so se sono un libro con una bella copertina che quasi nessuno legge mai, sono solo un libro come tutti. La copertina è anche consumata e sporca, sporca di polvere perché è tanto che sta li riposto e sporca delle ditate dei molti che lo hanno preso in mano ma abbandonato dopo poche pagine. 

Ho le mie colpe, le mie colpe per non essermi fatto leggere da quelli che forse mi avrebbero letto e con piacere, questo mi rattrista perché nella mia inconsulta brama di possedere chi era oggetto delle mie attenzioni mi sono perso tanto intorno.

A volte penso che io possa essere di tutti, nessuno escluso, perché quando sono di uno soltanto lo sono per poco, se ne va. Non credo di essere brutto non tanto almeno, però se ne va. Ho una vita di abbandoni, dove quasi tutte le persone che ho amato di più sono partite. Una vita di treni che partono mentre rimango seduto sulla panchina.
La vita è qualcosa che mi passa accanto, mi sfiora e mi lascia una carezza, poi continua mentre io rimango seduto a guardare nel vuoto.

È tutto qui e non c'è nient'altro. [m]


