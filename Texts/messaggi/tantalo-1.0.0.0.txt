[title: Tantalo]

Si, mi devo arrendere, arrendere e smettere.
Rimettermi la corazza, una corazza migliore, per non far entrare più niente dentro.
Non ho più il tempo di soffrire in questo modo, non ne ho più la forza.
Mio figlio mi impedisce di uccidermi perché è ancora troppo piccolo.

Vorrei poter non vivere più e non è solo per questa storia, questa è solo un caso fortuito.
Il mio malessere è più ancorato dentro, più mio. 
Forse, avrei solo bisogno di condividere quel dolore che mi mangia dentro, il dolore dell'impotenza verso il mondo esterno.

È difficile spiegare quell'estraneità al mondo che mi attanaglia, il mio scavare nell'aria densa che fatico a respirare. Guardo fuori dalla finestra e trovo un muro, trasparente mi lascia vedere il fuori ma non mi fa passare.

Il mio fallimento più grande è di non essere morto quando avrei dovuto e oggi lo porto avanti sorreggendolo, come un macigno che si ingrandisce sempre di più e pesa, non potendo nutrirmi o bere.

Come un moderno Tantalo, ogni cosa sfugge sempre dalle mie mani. [m]
