[title: rimanere]

Vede non è questione di rimanere o non rimanere, vado e vengo da tanto tempo. Sono un veterano e so quale siano le dinamiche di questo posto. Sa ho creato anche un bot (se sa cosa sia) che era in grado di tenere un tumblelog da solo.
Ho passato qualche mese analizzando accuratamente le dinamiche di questo posto.
Mi creda so di cosa parlo.

Quindi non è questione di rimanere o no e se vado non è per rabbia.
Lo so bene che le cose che scrivo piacciono, che le persone ci si immedesimano, ci si masturbano. Lo so bene. Molto bene.

Conosco il potere delle parole e le so usare, ma sa perché? Perché sono mie. Perché le mie parole sono vissute, vissute nel passato e vissute nel presente. Sono pregne di gioie e dolori. Le persone mi leggono e si immedesimano, fanno proprie le mie parole.

Il modo di scrivere, fa pensare a volte al non vero. Persone che si illudono che io scriva per loro, persone che mi accusano di non farlo più per loro.
Ci sono persone che si illudono di possedermi, anche se a loro non l'ho mai detto.
Vede io amo lo scambio di parole e se trovo qualcosa posso rispondere in rima o per le rime se preferisce. Questo in menti semplici instilla la convinzione che io possa essere posseduto.
Non sono di nessuno e sono soltanto mio, mi dono a chi scelgo e glielo dico apertamente.
Gli dico: sono tuo. È tutto qui, divento suo.

Il resto è rumore, rumore puro. Un rutilante insieme di suoni sfalsati.
Io gioco, in una ironia che può essere mal compresa in certi casi.
Questo è un mio cruccio, un difetto e lo ammetto. Credo però che ci siano difetti bene peggiori di questo.

Non sono perfetto e meno male, sono forte e fragile allo stesso tempo. Il mio mondo è pieno di pieni immensi e vuoti enormi. 
Io salgo quella montagna di cui parlavo stamattina, la salgo ancora e la salirò sempre.

...
Salgo per sedermi in cima ad ammirare l'infinito.
Salgo per soffermarmi in un pensiero, materializzarlo sulla mano aperta e guardarlo crescere, formarsi e diventare carne.
 
Poggiarlo a terra accanto e con un sorriso prenderlo per mano.
...

Vede, alla fine del viaggio dopo essermi seduto e aver preso per mano questo sogno mi lancerò nel vuoto e forse riuscirò a volare. [m]
