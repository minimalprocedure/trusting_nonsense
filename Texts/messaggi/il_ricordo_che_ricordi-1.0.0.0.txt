[title: il ricordo che non ricordi]

Vedi, la mia colpa è forse quella dell'intravedere le potenzialità nelle
persone e allora le spingo a scavare dentro se stesse.
Questo non amare la mia vita, ricercando di morire in molti modi, mi fa adorare quella degli altri.
Buffo, non credi?

Io voglio morire e voglio far vivere gli altri.
Voglio che vivano con tutto me stesso, che si trovino. Come padre premuroso li accompagno nei primi passi verso una prole che se ne va.

Vorrei sai, una volta morto, non essere ricordato ma solo rimanere come una sensazione vaga: il ricordo che non ricordi.
Perché non sono importante, sono solo un piccolo incidente di percorso, uno di quelli che improvvisi ti trovi davanti. Cosa che probabilmente siamo tutti, tutti per ognuno. Si inciampa nell'altro e si cade e dipende solo dalla caduta.
A volte sono risate e a volte pianti, a volte si muore. [m]

