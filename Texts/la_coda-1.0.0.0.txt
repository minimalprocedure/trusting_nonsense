[title: la coda]

Ti lasci porre al tuo interno  
la punta smussata di una coda.  

Con calma dopo fissata nell'incavo  
ti apri per far passare, poi chiudi.  

Gatta, muovi e mi cammini intorno   
sinuosa mostrando il tuo trofeo. [m]   
