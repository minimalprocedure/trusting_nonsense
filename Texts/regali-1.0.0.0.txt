[title: i regali]

Ci sono regali che amerei fare   
ci sono regali che gradirei ricevere.   

Ci sono quelli da tener nascosti     
quelli da indossare per l'ignoto vicino.    

Ce ne sono poi altri, dai tanti usi  
che impongono dopo, i molti piaceri. [m]
