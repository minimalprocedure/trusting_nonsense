[title: buongiorno al giramento e al caffè]

Fuori piove oggi, non tanto ma solo quello che basta a inumidire il tutto.  
Buongiorno al mondo intero e che se lo portasse via un _canchero_.  

Si diceva, buongiorno ai _giramenti di palle_, poi c'è chi dice _coglioni_ o _cojoni_ alla romana, chi invece ormai col termine sdoganato dal successo in tv: _gabbasisi_ o meglio _cabasisi_. Non sono siciliano, anche se avendo avuto alcuni contatti con bionde di quella terra qualcosa lo dovrei sapere, quindi cari isolani perdonatemi.  

Quel vorticare che qualche volta ci prende anche all'improvviso. Qualcuno potrà ricordare le _Clic Clac_,  quelle palline attaccate a un filo che si fanno rimbalzare. Non sono quelle palline attaccate a un filo che si infilano nell'ano della vostra bella, ragazzi; sono solo quelle che si fanno rimbalzare. Poi dell'uso improprio non prendo responsabilità.  

Quando ero piccolo c'erano, loro e un sacco di lividi sui polsi. Si iniziava con dei piccoli clic, clic, clic e poi clic clac, clic clac, clic clac, clic clac, clic clac furiosi.  
Chi durava di più aveva vinto. Semplice no?

Traslitterando, immaginate di prendere idealmente il batacchio in mano e poi, clic, clic, clic... Ecco un vortice di palle in piena regola.  
Questo stato parossistico della coscienza può essere molto utile nell'allontanare il cosiddetto _schiacciapalle_ o _sfrangipalle_ che dato il veloce movimento non riuscirà nella sua attitudine naturale.  

Bisogna però, prestare attenzione. Come ben si evince, questa è una reazione a catena e come Fukushima e Chernobyl insegnano o si esplode o si rompe il nocciolo.  

L'esplosione in genere ha risvolti drammatici, ma è una rapida evoluzione. Pappetta sanguinolenta sparsa dappertutto ma basta chiamare il signor Wolf. La rottura del nocciolo invece è più sottile, insidiosa.  
Con la rottura del nocciolo ci sono fughe radioattive che colpiscono un po' tutti, la mamma vi chiama per la cena? Giù raggi gamma alla mamma. Il vostro ragazzo non vi fa la coccola giusta al momento giusto? Raggi gamma... Dopo un po', siete circondati di persone con le pustole.

Attenzione, cercate di non arrivare mai alla rottura del nocciolo. Se il caffè vi rende nervosi non lo prendete, a me non fa niente, quindi ne bevo a litri.  
Siete di quelli che: - No grazie la sera no, altrimenti non dormo -. Non lo prendete.  

Ho sempre avuto la fase catabolica molto potente. Mi ricordo di quando mi dilettavo di anfetamine, qualcuno ricorderà le Plegine o le Lipopil. Insomma non c'era versi, le prendevo e la notte dormivo come un sasso. Allora, grazie a delle amiche svedesi di una mia amica, ci arrivavano delle compresse di _caffeina_, come farsi trenta _moka_ tutte insieme. Niente anche lì.  
La notte io dormo, se devo dormire, cascasse il mondo.  

Lo volete sapere cosa è davvero fico? Ho degli studi di medicina alle spalle, quella di serie B mentre la mia donna di allora di serie A, quindi ci siamo dati al _triptofano idrossilato_ .  
Dovete saper che il triptofano è un aminoacido e come tutti gli aminoacidi che si inseriscono con la dieta deve essere metabolizzato immediatamente. viene quindi trasformato in vari neurotrasmettitori tra cui: serotonina o adrenalina.

Quando volete essere grilli siete dei grilli, quando volete dormire dormite. Stupendo.  

Strano? Non direi, comprai nell'84 (mi pare) una macchina per l'aerosol per usare l'hashish, non fumando. La facevamo girare.  

Forse le sigarette elettroniche lo ho inventate io.  
A saperlo allora, oggi sarei ricco. [m]  

