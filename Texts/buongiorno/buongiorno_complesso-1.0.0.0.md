[title: buongiorno complesso: Zeppelin e Torte]

Ci sono giorni in cui dare il buongiorno è leggermente complesso, comunque:

Buongiorno ai Led Zeppelin e alle torte di compleanno.

Buongiorno alla musica, la mia musica. Vi confesso che non sono proprio tipo da rock, pop-rock, hard-rock, qualcosa-rock.
La mia musica è elettronica e minimalista.

Caro Robertino Plant e Jimmy Pagina, non me ne vogliate di roba vostra ho Led Zeppelin IV, Houses of the Holy e Physical Graffiti e basta. Mi siete piaciuti ma come altri e senza impressionarmi più di tanto.
Insomma allora ascoltavo anche Pink Floyd, Genesis, Jethro Tull e Black Sabbath.

Qualcuno si chiederà, ma le torte di compleanno cosa c'entrano? Abbiate fede che ora arrivano.

La mia musica è fatta di circuiti elettronici ed è ripetitiva, vi suona familiare? Le leggete le cose che scrivo? Io sono _r-i-p-e-t-i-t-i-v-o_. Leggete bene quello che sembra _barocchismo_ alla prima occhiata, non lo è. È invece ripetitivo e minimale, ipnotico.

Si vabbè e la torta?

Festa di compleanno a casa di un amico, ecco la torta. La torta, i Led Zeppelin.
La festa era in una casetta di campagna, in mezzo a un castagneto e l'amico in questione ascoltava rock in tutte le salse e blues.

La sorella è stata la mia amante di sempre.

Mi ricordo quando iniziò, un pomeriggio verso un'altra festa dentro una auto con tante persone. In sette se ricordo bene. Mi siedo davanti, lei sopra le mia gambe. La festa era a tema: anni sessanta, i favolosi per qualcuno anni sessanta. Insomma io parevo Dan Aykroyd, nei Blues Brothers (ma più bello), lei una bella quattordicenne in gonnellino scuro.

Senza pensare ai miei ventuno anni, mi ritrovo con una mano sotto il gonnellino a pieghe a fare un buco nelle calze e ad entrarle dentro con un dito.

Un sussulto suo e si accomoda meglio. Era molto stretta, ma tutto scivolava a meraviglia. La torta era con la panna ma io non amo la panna.
Lei era la mia amante di sempre e da allora. Dopo più di dieci anni così, andammo a vivere insieme ed è durata sette anni poi finita male.

Tornando ai Led Zeppelin, chiusi nella stanza di sopra cornificando i nostri rispettivi compagni con Robertino a squarciagola.

Tanto la torta era con la panna e a noi la panna, non piaceva tanto. [m]



