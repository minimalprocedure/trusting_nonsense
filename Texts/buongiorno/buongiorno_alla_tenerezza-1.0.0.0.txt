[title: buongiorno alla tenerezza]

Ci sono giorni dove ci si sveglia come sempre, ci si alza e si fa colazione, è lunedì e la settimana avanza. Ci sono giorni dove improvvisamente senti un calore grande che si sprigiona dentro, un aggrovigliato mischiume di tenerezza, protezione e carica sessuale.  

La voglia repentina di colmare di carezze e abbracci, di condividere disagi accumulati di sublimarli anche solo per poco. Sprofondare nell'oblio di un sogno racchiuso in quattro pareti. Prendere un viso tra le mani e potergli dire: - Urla, urla più che mai. Voglio che ti svuoti -.  

Come una spugna assorbire quel dolore percepito e intuito e vegliare poi sul sonno tranquillo.  

Oggi è quella giornata.  

Buongiorno. [m]  

