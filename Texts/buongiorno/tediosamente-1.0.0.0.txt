[title: tediosamente sempre uguale]


**[m]**  

Care Muse e Musi ispiratrici/ori,

fornendo adeguato spunto favorireste buongiorni frizzanti e alta poesia sconnessa. Basta un suggerimento verbale o una foto adatta e richiesta.

Son qui per servirvi tosto o quasi, ma puntuale.

Quindi approfittate degli sconti.

**[AdG]**  

Forza gente, suggerite buoni buongiorno  

**[m]**  

AdG, che panorama asfittico di questi tempi. Ne conviene?

**[AdG]** 

Ahi che tristezza. Ahi che dolor...messere. Un buon pomeriggio ai bei tempi andati?

**[m]** 

Ahimé! AdG,

qual buon pomeriggio si prospetta in questi tempi di acredine e sospetto?  
Che forse noi, piccoli e insignificanti esseri dotati di braccia gambe piselli e buchi, possiamo qualcosa?  No Signora Mia, la vedo oltremodo grigia se non nebbiosa.  

Chi, pur dotato di arguta speme, possa affermar con noncuranza un _buon pomeriggio_ senza tema? Son momenti in cui anche se il sole splende, il male sta li in agguato. Tremori o tristezze, non bistecche, sono li a cuocer sulla griglia; porcelli a fuoco lento siam noi, niente di più. Sia con l'aglio oppure no o l'olio buono, il pane è sempre troppo secco e a taluni smove la dentiera.

Tempo che passa, che corre e che si sfila, vigliacco fuggiasco col latte alle ginocchia. Ci lascia solitari a sperare in bene, quando poi ci si spenge il fuoco.  
Così al bivacco fa troppo freddo, mancando pure il calore umano.

Tra un po' inoltre s'accorcian anche le giornate, l'ora legale se n'andrà via e tutti a letto presto.

Tempi duri, tempi duri.

Servo suo.

m.
