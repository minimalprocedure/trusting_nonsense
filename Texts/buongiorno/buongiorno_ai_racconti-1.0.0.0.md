[title: buongiorno ai racconti]

Buongiorno ai racconti.

Amo i racconti, amo chi racconta.  
Ci sono tanti modi di farlo, chi lo fa con le parole e le scrive e chi lo fa con le immagini. Chi lo fa suonando e dipingendo, chi semplicemente parla.

Molti raccontano.

Ci sono anche quelli che raccontano i racconti di altri, che smembrano i racconti e li ricompongono incastrandone i pezzi. Ci sono tante persone diverse e tante tutte uguali.

Io vivo di racconti, vivo di chi mi racconta e di chi mi ascolta.

Giuro di averci provato a non farlo, di aver provato nella normalità e nello scorrere di non raccontare e di non aspettare che mi venisse raccontato. Sono stanco, stanco di aspettare.

Sono una persona _sbagliata_, _sbagliata_.
Nella mia famiglia i miei genitori sono _giusti_, loro non sbagliavano mai e mi hanno insegnato a non sbagliare, _mai_. Mi hanno insegnato a essere _forte_, sempre _forte_, sempre _giusto_, sempre _sicuro_, sempre _perfetto_.

Non sono _giusto_, né _sicuro_, né _perfetto_. Sono spesso _insicuro_, estremamente _imperfetto_, tanto _sbagliato_. 

Avevo il _terrore di sbagliare_, l'ho avuto per anni sempre fino da grande. L'ho combattuto con tutte le mie forze e ho sbagliato ancora.

La mia _finta_ sicurezza, è frutto della _conoscenza_. Per combatterla ho _imparato_, _imparato a imparare_, ho _conosciuto_ e ricercato la _conoscenza_. Oggi posso dire di sapere _tante cose_, _cose_ imparate da zero, sperimentando e sbagliando da solo. Sono una persona non _sola_, sono una persona _isolata_.

Oggi, vedo in mio figlio le mie paure. Le stesse, quel _blocco da paura di sbagliare_, quel _terrore_ che vela gli occhi e ottenebra il cervello. Non so come fare, sinceramente. Non so come spiegargli, ogni parola che esce fa l'effetto _sbagliato_. Mio figlio ha preso da me la _paura_ e dalla madre la _testardaggine_, un bel mischione di _difetti_.

Voglio intorno persone _imperfette_, voglio persone che siano _difettose_. Esseri combattuti dalle _paure_, uomini e donne _perduti negli abissi_.

Il mio viaggio è da barcaiolo, sopra una barca di legno scuro, che naviga in un lago nero e nebbioso, calmo e dall'acqua come olio. Ci scivolo sopra nella penombra dei crepuscoli, nel buio delle notti con una lanterna in mano.

È il rumore del remo che colpisce piano l'acqua, il ritmo lento e a tratti fermo.

Manca vicino il _racconto_.

I miei continui _non lo so_, alla domanda: _cos'hai?_.   
Non lo so davvero, non ho le parole per spiegare a chi non capirebbe mai; a chi ha sempre la _sicurezza_ e la risposta _giusta_. Non ci riesco, non so cosa dire, non so da dove partire.

Cosa posso dirle?   
Che l'abisso è dentro di me? Che se non ci fosse mio figlio, forse sarei già morto? Cosa posso dire? Come posso raccontare?

Non ci sono risposte, non ci sono parole. Ci sarebbero racconti, da narrare e ascoltare. Racconti che sono sommersi sotto montagne di macerie di fuochi e terremoti.

Il mondo, ci dicono, è un bel posto da vivere. La vita va vissuta, la vita _è bella_.

La vita, questo _nulla di nulla_ nell'attesa di morire.  
Un rudimentale bivacco, un accampamento dove ci si siede col fuoco acceso per non rischiare il buio e riscaldarsi dal gelo.

Qualcuno a volte ci si siede accanto, a volte parla e a volte no.   
Raramente _racconta_ e forse _ascolta_, ma più spesso _giudica_ perché lui è _giusto_ e tu no. [m] 





