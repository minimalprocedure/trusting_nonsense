[title: Tu sei per me la più bella del mondo]

Buongiorno al profumo, quello di Donna.  

Pare che lo sentano tutti, come pare che tutti facciano un po' tutto. Ormai ci _professiamo professionisti_ del _qualunque cosa_. Tutti lettori accaniti e tutti amanti appassionati e tutti super esperti di pratiche erotiche esoteriche. Insomma ormai c'è wikipedia che pesa meno di una enciclopedia.  
Il _Profumo di Donna_ quello vero però, sono convinto che lo sentano in pochi.  

Bisogna essere _bestiali_ per sentirlo, ma non come quello giù in fondo alla sala che alza la mano (lui è un Master, un Dom. Qualunque cosa voglia dire, ma ha letto lo scibile sul tema e sa anche tutti termini giusti a memoria).  
Ignoriamo la mano alzata e andiamo avanti.  

Occorre essere _bestiali_ dicevo, vuol dire lasciare la briglia al nostro Es ma quello di _Groddeck_ e non quello di _Freud_. L'Es arcaico, _la forza ignota e incontrollabile da cui veniamo vissuti_. In lui vivono i ricordi ancestrali comprese le capacità rimosse. Non ci deve essere razionalità conscia ma una _analitica razionalità irrazionale_. Contraddizione in termini? Forse.

Facciamola corta che poi mi dilungo sempre troppo su questioni che annoiano la gente.  

Oggi è così: buongiorno al profumo di Donna.

Quello che ci fa allontanare storcendo il naso (ci sono donne che non si lavano) o quello che ci inebria, quello di cui vedi il colore sospeso nell'aria. Il profumo della Donna che ami e di quella che ameresti volentieri. Quella che ti respinge, di cosa saprà? Quella che invece lo fa sentire a un altro?  
Tutti aromi che si mescolano e scatenano reazioni.

Il profumo che sale dal Tuo collo, quel misto di giornata vissuta e profumo. Il profumo denso nella camera dopo il sesso. Il profumo mentre cucinavo insieme a Lei e l'odore d'olio della polenta, fritta in pigiama la mattina della notte insonne. Lei, che profumava di sesso anche appena uscita dalla doccia. Gli oli e le essenze che ci strofinavamo insieme, fusi con quelli estratti da dentro.

Nel ballo dove non si riesce a essere seri, passo, passo, perno e girata. Occhi che si guardano, fissano e abbassano. La guancia appoggiata al petto. Io più alto di te. Gira, muoviti e balla, tacco e punta. A braccio teso lontana come a scappare e lo strattone per riportarti a me. Mia, mia e mia, quella donna un po' mia e un po' di tutti. Mia nel corpo e nella mente e di tutti alla vista.  
La gonna libera e stretta, indossata sui fianchi protesi dal tacco e senza segni; leggera buccia sui tuoi frutti turgidi e sugosi.  
Quel seno costretto che vibra appena.  
Nel piede posto e portato, nel contrappunto del nudo che strofina.  

Vieni Donna, balliamo ancora come non avessimo mai smesso, chiudiamo gli occhi come a ciechi e giriamo ancora.  
Raccontami intanto la tua storia e con le labbra dimmi, quello che non osi scrivere.  
Vieni, mentre ti afferro.  

La musica intanto è finita, ma noi non ce ne siamo accorti. [m]
