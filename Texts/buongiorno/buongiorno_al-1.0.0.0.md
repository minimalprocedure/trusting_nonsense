[title: buongiorno al lattice]

Buongiorno al...  

Buongiorno al latex?  

Certo che poi uno pensa, ma che razza di Buongiorno è?  

Latex, gente è latino. Come parecchie cose che erroneamente pronunciamo all'inglese: media, plus. Insomma quando mi pronunciano _plus_ come _plas_ mi sento male.  
_Media_ come _midia_.  
Ci mancherebbe mi dicessero _latex_ come _leitix_.

LaTeX, è però un'altra cosa e si pronuncia _latek_. Gli informatici qui dovrebbero sapere cosa è, magari anche gli ingegneri e i matematici.  
Curiosi? No vero? Ve lo dico uguale: è un sistema di produzione documentale, particolarmente adatto alle formule matematiche. Punto.

Il lattice.

Che materiale fico, ci si fanno un sacco di cose e parecchie porno.

Via su, vediamo cosa ci si fa? Vedo già l'ultimo della fila, lo _scaldabanco_ che lo sa e alza la mano: lui li usa in quantità e della misura _maxi_.

Solo quello? Poi? La tipa con gli occhiali, la secchiona. Anche lei lo sa, la sera mentre le _belle_ escono coi ragazzi lo usa sempre per un paio d'ore.  
Ancora? Altra mano alzata. Quella con il caschetto nero e l'occhio pesto, lei lo indossa tutti i venerdì sera, compreso di cappuccio solo col buco per i buchi.  

C'è anche il lattice dei fichi, che non è che sono i _belli_. I fichi fichi, quelli che si mangiano. Quello si mette sui _porri_ e le _verruche_, te li seccano in men che non si dica. Il lattice del papavero, quello da oppio ovviamente, come lo vedete?

Insomma, questo lattice ha molteplici usi e si appiccica alle mani.

Bianco latte o nero lucido, il latex fa tendenza. Fa tendere, sia che lo si usi tendendolo come un elastico che per far tendere oggetti organici spesso appartenti al soggetto anteposto. Si, quello con la mimetica e gli anfibi, la canotta nera e il bicipite esposto. Il maschio verace, quello master, mica cazzi.  

Ci sono anche genti allergiche però; si, allergiche al lattice. Fortunatamente la scienza ci ha fornito anche quello sintetico e anallergico: un copolimero dello stirene-butadiene.

Pensate e riflettete un attimo.

Lui, nel pieno della eccitazione.  
Lei: << caro hai un preservativo? >>  
Lei con fare esperto lo infila e reazione allergica, lui si gonfia a dismisura.  
Lei: << Caro, come ce l'hai grosso... Sfondami! >>.  
Lui poi va in shock anafilattico, ma lei è contenta.  

Chi invece si mette la tutina lucida nera bordata di rosso? Lo immaginate già diventare un pallone?.  
Immaginate soltanto la visione apocalittica del dildo in lattice.  
Immaginate per un attimo la secchiona di cui sopra, in un impeto erotico estremo con due: uno per buco.  
Ci manca anche che gli si inceppi la vibrazione, orgasmo multiplo? Bazzecole.  

Dimenticavo, il lattice ammazza gli acari.

Poi c'è lo sperma a volontà nelle foto di tumblr, ma quello non è lattice è latte condensato. [m]

ps: mi dovete scusare per lo smodato uso dell'ellisse.
