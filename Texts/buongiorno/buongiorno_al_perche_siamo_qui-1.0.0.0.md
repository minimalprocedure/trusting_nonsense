[title: Buongiorno al perché siamo qui o su facebook o su twitter o...]

Oh!, buongiorno...

Buongiorno al perché siamo qui o su facebook o su twitter ora X, sul fediverso e compagni di merende.

Già, perché ci siamo? C'è chi si autoproclama dittatore del mondo e il morto di fica che si fa le seghe davanti al monitor.
Ci sono quelli, che hanno il _reblog_ compulsivo, ci sono quelli invece che leggono quello che _rebloggano_. C'è chi si sofferma a osservare un'immagine e quelli che passano oltre.  
Ci sono quelli disadattati e quelli molto _adattati_. Gli sfigati e i bellocci, chi se la tira e chi la da al primo che passa.
Chi si fotografa il cazzo e lo sparge ai quattro venti, sempre di _tre quarti_ perché vien meglio.  
Chi si fotografa allo specchio, in bagno o in camera. Chi si fotografa solo le tette, chi solo le chiappe, chi i piedi o le mani.
C'è chi scrive, cose belle e molte brutte. Chi scrive perché lo sente, chi lo fa perché deve. Chi vive le sue parole, chi vive quelle degli altri. Chi copia spudoratamente e chi ti chiede il permesso. Chi butta là, nel mucchio aspettando che qualcuno abbocchi.  

Ci sono quelli che non leggono. I post pullulano di inesattezze: errori ortografici, citazioni di autori latini in inglese, traduzione errate da traduzioni errate, autori scambiati, autori che mai si sarebbero sognati di dire cose simili. Anonimi famosi.

Alcuni social sono come una enorme scatola di _Baci Perugina_, scarti il post e leggi la stronzata. Altri sono invece come scatole di _Biscotti della Fortuna_, sapete quelli cinesi? Quelli che sono già un falso in partenza perché cinesi non sono. I _Social Network_.  

Perché siamo sui _Social Network_? La risposta non è mai così ovvia ma certo io non ci sto per far sentire _bello e intelligente_ qualcun altro.  
Sono _egocentrico_ e abbastanza _pieno di me_ da _far bello_ me stesso e non gli altri.  

Nonostante tutto giro poco per _Social Network_, anche se ho account in molti lasciati a vegetare nel tempo, quindi non mi accorgo di chi si appropria indebitamente delle mie cose. Ogni tanto me lo fanno notare e ci guardo, poi se mi va lo sputtano e se ho altro da fare no.

La prendo quasi sempre con ironia, ma non sempre. Non sempre quando mi ribattono. Allora diventa una questione di principio e divento noioso, stronzo e pedante.  

Voglio insistere sul fatto che _Internet_ non è sinonimo di _libero dominio_. Non confondete _Internet_ con _internet_ che sono due cose diverse con la stessa parola.  
_Internet_ non è _libero dominio_, mettetevelo in testa, fatevelo trapanare nel cervello o se avete aria fritta nel cranio usate un compressore per alzare la pressione.

_Internet_ non è sinonimo di _pubblico dominio_.  

La mia storia su internet risale ai modem a 2400 Baud e le BBS, sono vecchio ebbene sì. Esisteva una volta la _netiquette_, il modo di comportarsi in rete. Ma eravamo pochi.  
Oggi siamo molti.  

Sono un _anarchico_, non nel senso _famo casino_ ma in quello di _Bakunin_ e _Kropotkin_ o oggi si potrebbe dire, di _Chomsky_; tutto nella mia visione utopica di un mondo migliore. Sono anche _pragmatico_ però e so che l'utopia _anarchica_ è e rimarrà almeno per un po' ancora un'utopia.

Molti hanno dei concetti di libertà molto vaghi e soprattutto guardano alla _propria libertà_ come assoluta ma ignorano quella degli altri. La _libertà_, per la libertà sono morti uomini migliori di quanto potrò mai io essere, sono state fatte rivoluzioni per la _libertà_, esperimenti miseramente falliti molto spesso.

L'uomo non capisce la _libertà_, non ne capisce il _limite_. Il _limite_ che ha, perché c'è un limite: la _libertà_ degli altri.

Ognuno di noi come una antica _città stato_ si arrocca e difende dalle altre. Ci sono però quelle che invadono, perché alcune hanno mura fragili o non le hanno affatto.

È facile dire: << E proprio perché esistono leggi perché non si denuncia? >>, le leggi ci sono certo, ma non tutti hanno la disponibilità per intentare una causa, non tutti hanno anche solo la voglia o la forza di vedere in faccia certe persone.

Le persone sono fragili, molte di quelle che scrivono lo sono. Non parlo per me che sono una _emerita testa di cazzo_, ma ci sono persone _fragili_. Sono mascherate con la rabbia spesso, ostentando una forza che faticano a mantenere. Riflettete gente. Pensate prima di smembrare o buttare la quella frase a effetto sulla vostra immagine di un cazzo in bocca o in culo alla fica perfetta. Riflettete prima di sbandierare pregi che spesso non ci sono.

Sono una persona che istiga a scrivere, cerco di far scrivere tutti. Non c'è bello o brutto per me, c'è l'atto dello scrivere, dello _esternare_ emozioni, pensieri e rabbia.

Lo so che ci sono persone che vorrebbero, ma nel timore non lo fanno. Loro usano parole di altri, loro usano immagini di altri, ma loro chiedono. Dichiarano che il pensiero che le fa vibrare è stato scritto da un altro. Non si vergognano di farlo. Ci parlo con queste persone e qualche volta vedo due timidi versi, un microscopico racconto. Qualcosa.

È bello, leggere le emozioni degli altri. Sentire che c'è gente viva in giro.  

Non si va d'accordo con tutti, sarebbe strano e io odio l'_amico di tutti_; ma una persona _vera_ la puoi _odiare_ con tutto te stesso apprezzandone la sua _verità_.

Il _centesimo_ e il _millesimo_ che farà cause interminabili e costose riuscirà in qualcosa? No, non riuscirà in niente. Manca la _cultura_ del rispetto, la _cultura_ della _legalità del rispetto_. Non servirà a niente.

Quindi? È una battaglia contro i mulini a vento, la carica contro l'impossibile.

Non sono le leggi che obbligano le persone, le persone si dovrebbero _obbligare da sole_. _Anarchia_ e cioè _assenza di governo_, l'uomo che consapevole degli altri conosce i propri limiti vantaggio degli altri e di sé.

Allora perché siamo qui?

Forse perché abbiamo bisogno di scrivere, perché la nostra pulsione è quella. Per fissare momenti, per far vivere momenti agli altri. Per raccontarci o per far vedere quanto siamo _bravi e fichi_. Ogni motivo può essere valido.

Il rapporto deve essere _etico e morale_ con gli altri, prima della legge che ce lo impone. Le leggi ci sono ma si _uccide_ ancora, si _tortura_ e si _violenta_, si _ruba_.
Queste persone non saranno mai fermate dalla _legge_, la _legge_ non è un deterrente se non si ha il senso della _legalità_. Senso che va insegnato ai bambini da piccoli, nelle scuole e nelle famiglie. Parole come _onestà_ e _rispetto_ sono fondamentali nel loro valore semantico e non come semplice _orpello_ di cui fregiarsi.

Si sbaglia, si sbaglia continuamente, si travalica il limite continuamente. Lo faccio, lo fanno tutti. Si fa spesso inconsapevoli o a _fin di bene_, sbagliando.

Si deve imparare a _conoscere_ l'altro e non _consumarlo_ per i propri scopi.

I _Social Network_ sono _bugiardi_ e sono _infidi_, ma i _Social Network_ sono niente senza le persone che ci sono dentro. Sono meri mezzi, loro non hanno colpa. La colpa è di chi li usa.

Quindi è da vigliacchi andarsene? È da vigliacchi allontanarsi da mille nerboruti che si picchiano? È da vigliacchi lasciare che chi vuole bolla nel suo brodo?

Qualunque cosa sia, io amo gli stimoli e il confronto, la discussione costruttiva. Le persone interessanti che ci sono, sparse nel mondo e sono dovunque, quelle mi interessano.

Il resto è noia e quando mi annoio lascio perdere. [m:tempo fa]


