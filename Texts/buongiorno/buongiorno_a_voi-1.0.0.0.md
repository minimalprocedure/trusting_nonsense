[title: buongiorno a voi]

Buongiorno a voi.

Buongiorno a te, per prima. Buongiorno Bionda, buongiorno a te che sei bella, buongiorno intelligenza testarda e fallace.
La tua stanza è sempre lì, con vista mare. La pulisco e la spolvero ogni tanto, casomai tu volessi passarci le ferie.

Buongiorno a te Donna, testarda e scontrosa, fuggitiva. Pronta alla fuga e a ritrarsi un po' piccata.
Hai la rabbia col mondo ma non ne vale la pena, il mondo non ascolta e il mondo semplicemente ci ignora.
Il mondo sai, è come un dio che ci fa e ci guarda poi morire.
 
Buongiorno Impiegata del Governo, mi metto il trench e arrivo. Passami sottobanco i dati sensibili nel tuo ufficio fumoso e fammi saltare la fila.
Staremo attenti però a non smagliare le calze e mi raccomando quel libro... finiscilo.

Buongiorno TP o PP, nella mia lingua madre. Indecisa nell'essere una cosa oppure un altra, o una cosa con chi e l'altra con altro. Tiriamo i dadi o giochiamo al Lotto, lasciamo la sorte avversa e bariamo al gioco. Io l'asso l'ho già nella manica, è quello di picche perché il _malo destino_ mi perseguita ma io lo giro e sembra un cuore.
Ho un posto nella barca e possiamo remare a turno.

Buongiorno all'Opera, dal nome sul libretto. Tu con piacere mi ricordi il passato, le tante cose che ho lasciato in un baule. Ho sempre la chiave, da qualche parte è appoggiata, dovrò pulirla dalla ruggine prima o poi.

Buongiorno Piccolo Giglio, anche se ci parliamo poco ultimamente, ti penso con affetto. Nella tua gentilezza e il tuo _saper stare_ nascondi più cose che non mostri a tutti. È l'esperienza del tempo che passa con dentro l'ardore del tempo che fu. Sappiamo cosa significa.

Buongiorno Signora, quanto è lontana, lontana, lontana... Perché è sta laggiù in fondo? ... e porca zozza!

Buongiorno a Quella, che è strana. Lei mette sempre immagini diverse, ma mica tette e culi... no 'fotografie'. Quella che il _sentore_ mi dice _isolana_ in qualche maniera.
Grazie, nel marasma tumblero di gatti, caffè, cappuccini, muffin, tette, culi, cazzi e fiche in ordine casuale... qualche bella fotografia _rinfranca_.

Buongiorno al Grafico, sei grafico. Direi di aver detto tutto a questo punto e invece no. Sono sicuro che il grafico non grafichi e basta. Sotto il Grafico c'è di più.

Buongiorno a chi è un nome solo o solo un nome, lei scrive. Bene. È un piacere leggerla e a me non piace leggere gli altri a parte me stesso. Sapete una cosa... sono sicuro che a letto ne valga davvero la pena. Come dissero una volta dei ceffi passando in auto a una mia ex mentre aspettavamo il verde al semaforo pedonale: << Bionda, beato chi ti tromba! >>. Non è bionda ma rende il concetto.

Buongiorno poi ai Fidanzatini, solo per lei e solo per lui. Sono inconcepibilmente teneri. Ragazzi... me li scoperei tutti e due. Siete bellissimi, se vi lasciate vi trovo e vi picchio.

Un buongiorno speciale a chi è Semplice, semplicemente lei. Vagamente timida e vagamente offerta. Lei risveglia pensieri sopiti, attenti.

Buongiorno a chi mi ha chiesto un giorno: << ma segui solo me? >>. Per beffa del destino non era vero, ma la chiacchierata è stata piacevole e la curiosità stuzzicata.

Buongiorno Bella Pupa, irraggiungibile Bella Pupa. Niente da fare, battaglia persa a meno di estreme decisioni. Vabbè, facciamocene una ragione.

Buongiorno Irriverenza in ordine sparso, guarda che le leggo le cose... a quando un altro _verso per uno_?

Buongiorno a tutti gli altri, oggi non vi ho citato e sarà per domani. 

Buongiorno a questa comunità schizofrenica di gente con tanti segreti e tante speranze.

Buongiorno anche agli stronzi e stronze che come dappertutto ci sono anche qui, in abbondanza. [m]
