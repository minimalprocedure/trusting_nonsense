[title: Andare oltre, altrove]

Dalla regia mi forniscono un tema del buongiorno, però siamo alle 17:43.  
Cara regia, fossi in lei mi sveglierei prima o scapperei dal lavoro almeno verso mezzogiorno.

_Andare oltre, altrove_  

Lo sa che di questi tempi andrei volentieri _oltre_, senza sapere nemmeno dove e men che mai passare dal via? 

<< Dove va di bello? >>  
<< Di bello non saprei. Lei invece? >>  
<< Che le dico... >>  
<< Che mi dice? >>  

Non è nemmeno molto chiaro _oltre_ a cosa. Si va _oltre_ il precipizio? _Oltre_ la curva, morte certa?  

<< Dio santo! Sei macabro stasera! >>  
<< Sarà che il sole non c'è più e vedo parecchio nero? >>  

Sarà quel che sarà, l'andare _oltre_ presuppone un salto di qualità. Dalla _missionaria_ alla _pecorina_ per esempio, l'atto dell'oltre potrebbe diventare quel passo ulteriore che fa la differenza. Certo se si presuppone una _missionaria_ che altrimenti pure quella sarebbe un _oltre_, dal nulla a qualcosa. Oltre la _pecorina_ inoltre c'è un _oltre_ ulteriore. 

L'oltre il crinale per esempio è quasi preoccupante, specialmente se sei quasi in vetta a dieci minuti dal tramonto. Scendere di notte una montagna non lo auguro a nessuno, specie se la donna del momento se la fa tutta con le chiappe. Quando si arriva in fondo dopo ore di _raspa-sasso_ per i canaloni pietrosi, il tocco sull'asfalto pare mare poco mosso.  

L'oltre la siepe ha invece qualche cosa di intrigante, perlomeno se siete dei _guardoni_.  Oltre la siepe c'è anche spesso il buio.

<< Scusa ma ti sei perso l'altrove? >>  
<< No, attenda che ci arrivo. >>  

Al Trove, non _trovate_ che faccia tanto _noir_ anni quaranta? Il risoluto detective privato, vagamente innamorato della donna del boss? Bionda, statuaria, occhi grigi come si conviene in un film in B/W?

Entrò dalla porta senza nemmeno bussare. Posso? Mi disse con quella sua voce squillante ma bassa, suadente come miele appena sciolto e graffiante da gatta in calore.  
Mi infilai la camicia nei pantaloni in fretta e furia e mi tenni a debita distanza, ancora mi avvolgevano i fumi dell'alcool dove avevo affogato i baci di Jane. Mi mancano i baci di Jane. Non li avrò più i baci di Jane, Jane che due giorni prima mi aveva lasciato con un _Al tu bevi troppo_ e _io mi sento sola._   

<< Prego si accomodi. >>  
<< Al, posso chiamarla Al? >>  
<< Si certo, può darmi anche del tu se vuole. >>  
<< Fammi accendere Al. >>  

Le dissi che il fumo faceva male, ma lei sbattendo le ciglia mi gelò con un _si deve morire prima o poi_. 

<< Mio marito è scomparso. >>  
<< Ne è sicura? >>  

Fu una brutta uscita, me ne resi conto subito. Era _gnocca_, cazzo se lo era. Nessun _marito_ sarebbe uscito per le sigarette con una gnocca così per casa.  

<< Non mi ha visto bene Al? >>  

Lo disse sporgendo l'ampia scollatura a v del maglioncino striminzito.  

<< Ha ragione, mi dica... >>  

Accettai il caso e la guardai uscire ancheggiando. Fu il peggior caso della mia vita. [m]

