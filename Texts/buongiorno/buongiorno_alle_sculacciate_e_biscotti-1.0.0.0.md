[title: buongiorno alle sculacciate e ai biscotti]

Buongiorno alle sculacciate e ai biscotti.

Intanto premetto una cosa, ma solo sempre due indizi per il buongiorno? A voi ragazzi manca davvero la fantasia. Non è che _rebloggare_ tutto questo porno ve la _attufa_? Se è così, fatelo di più questo porno e _rebloggatelo_ di meno.

Fine della _premessa_.

La mattina ultimamente faccio colazione con tre biscotti e non sculaccio nessuno. I biscotti a volte sono anche quattro.
Che vi importerà a voi proprio non lo so ma vedetelo come il _tedio_ mattiniero quotidiano.

Tre biscotti, quelli con i cereali così si ha l'illusione che siano _dietetici_, che poi _dietetico_ è qualunque cosa è il _come_ l'importante.

Cosa c'entrano le sculacciate? Ah! Io le chiamo sculacciate perché tendo a non usare termini stranieri se ho una possibile traduzione in italiano. _Spanking_, lo so che fa più Master ma io sono alla buona e non un professionista, quindi _sculacciate_.

Oltretutto per noi italiani, _s-cul-acciate_, ci riporta alla mente culi torniti e chiappe esposte. Il tremore del colpo e la deformazione della chiappa al rallentatore.

Pensateci. È un po' come _Il trattamento Ridarelli_ di _Roddy Doyle_, si svolge tutto nel lasso di tempo compreso tra l'alzata e l'abbassata (chi non ha letto il libercolo in questione lo faccia leggere al figlio).

La mano che accenna il movimento allontanandosi indietro per prendere la rincorsa. La cintura di cuoio da vero duro, larga e di cuoio, temporaneamente trattenuta dalla sua elasticità in posizione che inizia a spostarsi all'indietro. Il braccio nel pieno della sua estensione all'indietro, dopo una tensione muscolare repentina in cui ogni fibra muscolare a causa del rilascio di molecole di calcio si accorcia e tirando il tendine attaccato all'osso ne causa il piegamento, parte di potenza. Le dita della mano si contraggono sulla cintura di vero cuoio da veri duri che vibra nella inversione di direzione e per un attimo continua il suo percorso inerziale. L'arco percorso e studiato per raggiungere velocità angolari di tutto rispetto.

Il tempo che si ferma a metà strada, rallenta in una progressione frattale. In un _bullet time_ alla _Matrix_, la camera gira intorno al movimento.

La parte _cul_ dell'azione che avverte lo spostamento d'aria e reagisce dipendente dal cervello attaccato al _cul_, chi indurisce e chi rilascia. Il cervello sa perché non indurire, ma non tutti lo fanno.

In pochi attimi, solo un battito di ciglia, l'azione riprende. La cintura, di vero cuoio da veri duri, riparte. L'attrito con l'aria la schiaccia, la velocità sta raggiungendo il suo massimo.

Il braccio intelligente, sa che deve allentare la forza appena prima e lasciare che la cintura, di vero cuoio da veri duri, faccia il suo dovere. L'attimo prima è importante. Il colpo non deve arrivare brutale, il colpo deve scaricare l'energia accumulata dalla cintura, di vero cuoio... blah, blah.

Il braccio si ferma, l'inerzia accumulata protende la cintura, blah blah, la cintura si avvicina e sfiora la pelle. Affonda nella chiappa morbida. Bordi di chiappa intorno che avvolgono bordi di cintura, blah blah. La cintura, blah blah, scarica tutta l'energia sul _cul_.

La chiappa trema assorbendo energia.

Qui si spiega perché il cervello di prima, deve ammorbidire la chiappa. La chiappa deve vibrare. Si spiega anche perché le sculacciate piacciono di più ai _cul_ generosi. La chiappa troppo secca o troppo allenata, vibra poco. La chiappa secca o troppo allenata sente più dolore che piacere. È un problema _fisico_ (non in senso del corpo, ma scientifico). Un corpo duro e rigido assorbe meno energia.

L'energia assorbita si propaga come onde sismiche, risalendo dalla zona urto, verso l'alto. Si considera sempre verso l'alto il verso dal _cul_ alla testa, indipendentemente dalla posizione assoluta. Se girate una cartina geografica il nord sempre lì è.

Queste onde sismiche propagandosi arrivano al clitoride e dentro la vagina e trema il punto G. Mentre signore, il vostro punto G assorbe l'energia residua il braccio ricomincia a pendolo il percorso inverso. Questo oscillare scandisce il tempo come un metronomo la musica.

Ritorniamo ai biscotti, ai biscotti?

Signore, ricordate che se vi fate sculacciare il _cul_ lo dovete tenere morbido e il vostro punto G vi ringrazierà.
Se lo tenete duro, vi farà solo male.

Oddio, piace anche quello a volte. [m]

ps: ogni ripetizione è volutamente voluta  
