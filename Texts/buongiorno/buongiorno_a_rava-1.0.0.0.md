[title: buongiorno a rava]

Buongiorno moscio e poco ispirato.

Mi dicono che oggi è venerdì, certo e domani è sabato. Ah! dopo viene domenica e poi lunedì.

Qui c'è la nebbia e non si capisce cosa ci sia sopra, sole? Nubi? Tempeste che si scatenano in silenzio?

Qualcuno fuori brucia gli sterpi, le potature degli olivi. Mi hanno detto che quest'anno ci sono state poche olive, la _mosca_ le ha ingravidate tutte e avremo _olio moscato_.
Insomma quest'anno scarseggerà l'olio.

Anche l'uva dice non sia messa meglio e avremo poco vino che ovviamente sarà molto caro, sbronza solo per ricchi quest'anno.
I poveri si accontentino, in fondo che vorranno mai? Già li lasciano vivere.

Sto sorseggiando del tè verde, nella tazza bianca quella con la ricetta del petto di tacchino con gli asparagi. Bianca bianca non è, è di quel bianco detto _sporco_, poi detto _ghiaccio_ e poi detto in altre maniere. Sono quelle definizioni di colore che la gente si inventa, giusto per confondere le idee.
È color _porcellana bianca, ma sporca_ visto che comunque non è di porcellana, anche se del caolino ce lo deve avere dato che è sbeccata da un lato e il materiale è bianco (_sporco_).

Il computer è acceso, per gli esperti è un _i7 quad core di terza generazione_, ha 8 Gb di ram, un disco da 5400 RPM da 1 Tb e uno ibrido meccanico/solido da 750 Gb e 7200 RPM.
Ho _Emacs_ aperto e ci sto scrivendo questo. Ho aperto anche _Firefox_ e _Chromium_ mentre chatto col socio via _Empathy_ e _opam_ (il _library manager_ per _OCaml_) mi aggiorna il tutto.
Il sistema operativo è _Ubuntu 14.04_ ma uso come desktop _Gnome_. _Windows_ lo uso solo per giocare e a proposito devo ancora finire _Skyrim_, la mia _maga lesbica di razza elfa nera_ mi reclama.

La giornata scorre più o meno lenta. Qualcuno mi fa girare le scatole, ma ci sono abituato ormai e faccio finta di niente; ogni tanto scoppio e rispondo male e così mi viene detto che sono _uno stronzo_; quindi smetto di parlare e me ne vado.

La nebbia si sta alzando ma è sempre lì, il sole non si vede ancora. [m]
