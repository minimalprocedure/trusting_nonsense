[title: buongiorno ai pellerossa e al gusto]

Buongiorno, oggi la sfida è perigliosa.
Pellerossa e gusto.

I _Pellerossa_, così al volo, mi fanno venire in mente _Koyaanisqatsi_. Famoso film che tutti conoscerete: 1982, registi Godfrey Reggio e Ron Fricke, musica di Philip Glass.

_Vita in divenire_, divenire in senso _tumultuoso_ e non ordinato. Senza regola e disallineata. Fuori dalla natura.

Il film fa parte di una trilogia:  _Koyaanisqatsi_, _Powaqqatsi_ (1988, musica Philip Glass), _Naqoyqatsi_ (2002, musica di Yo-Yo Ma).

I titoli dei film sono parole _Hopi_, e sono usate in questa trilogia per indicare sostanzialmente come l'uomo distrugga la sua vita e quella dei altri.

Forse i _pellerossa_ la sapevano lunga sull'uomo in sé, forse sono stati sterminati per quello.

L'occidentale, il _cristiano_ nella sua predica dell'_amore_ e in nome di esso ha ucciso milioni e forse più persone.
Interessante no?

L'_amore_ sempre intrecciato con la morte. Quelli erano cattivi _cristiani_ però. Cristo, che _dio lo abbia in gloria_ e ammesso che sia esistito sul serio, fondò una delle correnti religiose più moderne e più difficili da seguire della nostra storia. Nella sua tremenda semplicità è così complessa da seguire che la rende _impossibile_.

Il _perdono_, questa cosa è fenomenale. L'idea che puoi fare di tutto, ma, col solo atto del pentimento sei _perdonato_. Sei lindo, niente _karma_. Niente strascichi. Niente reincarnazioni in insetti, sassi, merde pestate. Puliti e col culo a _pelle di pesca_ come da piccoli.

Permettetemi di dirlo: _m-i-c-i-d-i-a-l-e_.

La religione migliore per gli occidentali, nella nostra cultura sempre più anglosassone. Il _pragmatismo_ occidentale. È una svolta epica.

A noi ci basta pentirci in punto di morte, un sano pentimento all'estrema unzione e ci timbrano il biglietto.

Biglietti! Tutti si affannano a cercarlo nella borsa, nel portafoglio.

<< Non li avevi fatti tu, i biglietti? >>.

<< Ma si, dovrei averli qui! >>.

Un povero cristo nero, si alza alla chetichella e si sposta nel vagone avanti.

La vita, un treno malconcio che ci porta da qualche parte: _il binario morto_. Pentiamoci, nella luce del _Signore_ verso il paradiso.

Odio il _Paradiso_, ma voi pensate che palle... Stare lì in adorazione di una luce splendente senza vedere un tubo. Non vi può venire nemmeno la curiosità di che lampadina usi il nostro _dio_. Sarà a incandescenza? A risparmio energetico? A led?.

Forse le ha usate tutte nel tempo, prima a incandescenza ma poi gli hanno detto che inquinava. In vetro con ottone e tungsteno, la lampadina a incandescenza è stata messa in pensione; consumava e inquinava di metalli pesanti. Tungsteno, che ha un certo sapore indiano, indiano dell'India e non pellerossa. Insomma un metallo pesante si, ma praticamente inerte.

Siamo passati alle lampadine a risparmio energetico, dette anche elettroniche. Quelle per cui _venti watt_ corrispondono a una lampadina tradizionale da _cento watt_. Quelle che consumano poco, ma non le puoi buttare nel secchio del misto. No, devono essere riciclate apposta. Ci vuole più energia a riciclare una di quelle lampadine di quella consumata da una lampada a incandescenza per tre mesi almeno. Plastica, vetro trattato, materiale elettronico, metalli pesanti, gas pesanti, vapori di mercurio.
Vapori di mercurio? ... Ma se mi hanno fatto ricomprare pure il termometro per la febbre perché conteneva il mercurio?

Ora però abbiamo i led. Diodi a emissione di luce, sono di plastica e poco metallo. Plastica normale? no...

Insomma, abbiamo lasciato la benzina rossa perché conteneva _piombo tetraetile_ sospetto tuttora di cancerogenicità per la benzina verde _come la speranza_, che contiene _benzene_ e ovvero la prima molecola _aromatica_ dichiarata ufficialmente cancerogena ai primi del '900. Chi ha studiato un po' di chimica lo avrà studiato perché è _fico_, ma non da respirare.

Mica basta però il benzene, no, nella benzina verde c'è pure l'MTBE (_metil-t-butil etere_) che serve anche lui per aumentare la comprimibilità della benzina. La fa _scoppiare_ meglio (quello che faceva il _piombo tetraetile_). Anche l'MTBE è cancerogeno e neurotossico e se si mischia allo ZDTP (_zinco ditiofosfato_) che viene dall'olio (non si può fare a meno dello ZDTP nell'olio altrimenti non dura) si formano delle cose interessanti come alcune famiglie di _esteri fosforici_.

Io lo so cosa è un _estere fosforico_ avendo tentato di suicidarmici, ma forse voi no... Avete presente anche solo per sentito dire il _Sarin_?

Insomma, si è sostituito un _normale_ sospetto cancerogeno come il _piombo tetraetile_ con un _sicuro_ cancerogeno come il _benzene_ e una neurotossina come l'MTBE.
Fortuna però, che abbiamo le marmitte catalitiche che sono di platino e funzionano solo per 20.000 Km.

I _pellerossa_ la sapevano lunga e secondo me, come molti dei popoli che vivevano in armonia con la natura, una volta capito l'andazzo si sono fatti sterminare apposta.
Giusto per non respirare questa merda. [m]

ps: Il gusto? quello dove lo hai lasciato? Il _gusto_, quello, me lo serbo per quando l'_assaggerò_.



