[title: snac e affini, varie ed eventuali]
---

Buondì.  

Questa istanza _ActivityPub_ è gestita da [snac2](https://codeberg.org/grunfink/snac2). Eviterò di entrare nella mia solita affermazione _controversa_ di __activitypub è un colabrodo_ e invece mi manterrò nelle _varie ed eventuali_.  

Amo la gente che scrive, che scrive roba sua. Brutta, bella, scritta bene o scritta male, condivisibile o meno. Mi piacciono le persone che parlano di sé soprattutto e meno quando lo fanno di altri. Salto quasi sempre a piedi pari gli _annunci_ dell'ultima uscita di _questo_ o _quell'altro_ che per carità nevvero, sono anche interessanti talvolta. Guardo con sospetto chi si appoggia sempre ad affermazioni di altri, quasi cercasse conferma assoluta dal _pincopallo_ di turno come se questo avesse la _verità infusa_. Mi sta più simpatico quello che _reinventa la ruota_ nella propria ignoranza o distribuisce _castronerie_; denota perlomeno l'attivazione di qualche neurone. Decisamente apprezzo quindi chi _produce_ del suo.  

Perché _snac_ e non bivaccare un una istanza più _affollata_? Gravitando nell'attuale _fediverso_ ufficialmente dal lontano 2017, passando per varie grosse istanze e polemizzando in alcuni casi con gli amministratori delle stesse, sono giunto alla conclusione che di fatto esista nella media (mastodon in particolare) un utilizzo alla _twitter_. _Link_ pubblicati, _micro-minchiate_ più o meno schierate, _bacchettonismo_, _benpensantismo_ e molti altri _-ismo_. Tutto questo con la derivante difficoltà (pregio per quello che mi riguarda) di _connessioni sociali_ di seguiti e seguenti. Difficoltà nello _scovare_ l'anima gemella o gemellata (l'ultimo della fila non pensi subito al sesso) con cui condividere l'esternazione del momento. 

> Sai che...  
> Si?  
> Vero, giuro!  
> Bravo!  
> Bene!  
> Bis!  

Uno dei limiti maggiori per me è la quantità di caratteri a disposizione, nella scrittura non voglio limiti. Passare dal versetto ermetico di due righe, per il _falso_ (gli haiku scritti non in giapponese sono solo falsi haiku) haiku fino al _papiro_ di dieci pagine è fondamentale. Certo si potrebbe usare l'hack di pubblicare un'immagine, ma poi i _pirati_ frequentanti il loco lo leggeranno? No, meglio la parola scritta non credete? Serve almeno a _discriminare_ chi e/o cosa.  

Una piattaforma (commerciale) che ho sempre trovato interessante è _tumblr_. Vi fermo subito che non è per il porno. Fortunatamente non mi è mancato di poter vedere e toccare _femmine_ in abiti succinti o agghindate in vari modi. Penso di essere _entrato_ in _tumblr_ dal suo debutto; uscito e rientrato con pause di qualche mese una miriade di volte. Oggi (penso) definitamente lasciato. Dalla _libertà_ di espressione _estrema_, alla _censura_ Mayer, fino ad Autommattic (che è tutto dire). Sinceramente mi immagino già lo _sbarco_ col passaggio a _wordpress_ e di conseguenza _activitypub_ la moria di _tumbleri_ nel combattere con uno degli _-ismi_ del _fediverso_.  

Molti _tumbleri_ (ma non solo) non sanno poi nemmeno perché la loro piattaforma si chiami _tumblr_. Qui il ricordo dovrebbe andare al mitico _guru_ di Ruby, _why the lucky stiff_ (in arte Jonathan Gillette) e alla sua definizione di _Anarchaia_ di _Leah Neukirchen_.  
In fondo ogni piattaforma di micro-blogghing odierna viene da quell'idea di _tumblelog_.  

Dentro _tumblr_ ho incontrato _imbecilli_, _disadattati_, _pornografi_, _artisti_ e chi più ne ha e più ne metta di gente interessante o meno in un rutilante intreccio di _connessioni umane_.  

> ... ma snac?  

Vero, mi stavo perdendo nei ricordi. Avendo deciso di lasciare _tumblr_ definitivamente nell'attesa sto comprando i pop-corn e le birre per la sua discesa in queste lande.  
Sono poi venuto in contatto con _snac2_, grazie al Sig. Giacomo. Un piccolo software programmato in C.  
Lasciamo il merito della _qualità_ del codice, a mio avviso poco strutturato (su cui ho applicato a mio uso degli hack _quick'n'dirty_), devo dire che è il mio ideale di software per la presenza in rete.  

Banale da compilare, banale da far girare (con qualche hack sperimentato pure in termux), permette di avere un _tumblelog_ alla portata di tutti con un veloce ritorno a quell'Anarchaia citata sopra. 

Insomma posso scrivere quello che voglio, mettere le immagini che voglio con i limiti solo decisi da me. Cosa si potrebbe volere di più? Ci sono ovviamente dei limiti di _prestazione_, l'assenza di database e l'appoggio alla velocità del _file system_ alla lunga si può sentire ma, mi importa? No.  
Come non mi interessa se e quando chiunque possa leggere o vedere, così non mi interessa se debba aspettare un secondo in più per farlo.  

Così dovrebbe essere il _fediverso_ nella sua forma ideale (non valutando i limiti e le problematiche di altro tipo) una miriade di istanze personali che si intrecciano dove ognuno è _responsabile_ di ciò che _pubblica_; dove la _moderazione_ siamo soltanto noi a farla.

> Sai che mi stai sul culo?  
> Sei una stronzo!  
> Vaffanculo!  
> ... ma vaffanculo te!  

Niente _moderatori_ che vi dicono cosa e cosa non dire.  

Per concludere, da buon Sig. Fottesega, se mi leggete buon per voi ma altrimenti amici come prima.

Servo vostro.


