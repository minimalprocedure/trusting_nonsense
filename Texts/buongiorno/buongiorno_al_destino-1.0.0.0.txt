[title: buongiorno al _destino_]

Buongiorno al _destino_? Sarebbe?

Se fosse _destino_ ci sarebbe bisogno di dargli il buongiorno? Inoltre _dargli_ o _darle_ il buongiorno? Il _destino_ è maschio o femmina? Tutti e due? Nessuno? Inoltre si da del _lei_, del _tu_ o del _voi_?  

_Destino_ beffardo, talvolta burlone ma non troppo o gaiamente allegro mentre altre orribilmente maligno e oscuro. Si dice che il _destino_ sia nelle carte: torre, bagatto e morte.  
Il cerchio antiorario delle previsioni arcane.

<< Mi dica c'è speranza? >>  
<< Figlia mia, che vuoi sapere? >>  
<< Tutto, la risposta alla domanda fondamentale sulla vita, l'universo e tutto quanto... >>  
<< Quarantadue, figlia mia... quarantadue. >>  

Lo sguardo del _destino_, quello al primo sguardo. L'amore al primo sguardo, l'antipatia al primo sguardo detta anche _a pelle_. Pelle d'oca e peli rizzati a causa o complemento del _destino_?  

Calvinisti predestinati del ricco è buono e povero cattivo? Semplificazioni spinte giù dalle rupi impervie e filosofia spicciola da _comari di paese_.  

<< C'è un bel sole oggi, non trova? >>  
<< Me lo sentivo, sarà _destino_... >>  

A che serve, mi chiedo, scaldare l'acqua per il tè se tanto poi lo si berrà freddo?  
Il caldo che volge al freddo e la luce che volge al buio, è tutto questo il _destino_? Il cadere nel cui _destino_ sta il cadere di nuovo, ancora e ancora come se _non ci fosse un domani_.  

Il _destino_ ci dice che il gallo canterà ancora domani e il giorno dopo, almeno fino a quando non canterà troppo presto e gli tireremo il collo e sarà _destino_ anche quello, almeno per il gallo.  

Quel _destino_ che fa rima con il _quando_, poi con il _come_ e infine con il _quello_ o _quella_; il lui o lei, uomo o donna del _destino_. È spirale contorta di accadimenti, farfalla Lorenziana, l'attrattore che tutto spinge e tira.  

Il _destino_ c'è e non c'è, esiste ma manca, ha forma ma non si trova e sostanza ma non si tocca.  
L'amico di tutti e di nessuno, l'ultima parola alla fine del tutto:  

<< Era _destino_... >>  

_Claire cambiò direzione, cambiando per sempre la sua vita, cambiando le vite di tutti noi._ (Wim Wenders - Fino alla fine del mondo)

[m]


 
