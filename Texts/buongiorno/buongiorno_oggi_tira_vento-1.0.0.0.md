[title: buongiorno, oggi tira vento]

Buongiorno, oggi tira vento.

Lo vedo, fuori della finestra verso il campo degli olivi. Lo sento sbattere le porte e le finestre, nell'aria che si incunea nelle stanze.  

Oggi tira vento.

Io adoro il vento, quello forte che porta via le cose perché mi immagino a braccia aperte a tentare di volare. Immagino la folata immensa che con un soffio mi porta via.  
Sono quei giorni in cui anelo all'oblio, all'essere dimenticato completamente come non fossi mai stato.  
Adoro in particolare il vento d'autunno, quello notturno. Le foglie morte e vive di colori che si innalzano e cadono e poi rialzano ancora, trasportate e senza meta. L'oblio della direzione, la dimenticanza del dove andare e perché. Le vedi che si scontrano e cozzano, ballano insieme per un attimo insieme e poi chissà.  
Adoro il vento perché mi ricorda che la vita è fatta di fugaci incontri, di opposizioni per non essere spazzati via; ma io voglio essere spazzato via.  

Questo malessere interno, nato con me e vissuto con me. Morirà con me.  

Come fossimo un due o più di due, discorriamo tra noi e talvolta con altri. Questi altri indefiniti nel fuori da sé, nell'intorno materiale. Altri, forse come me e forse, talmente diversi da sembrare uguali in questa illusione del reale; la grande beffa a cui dobbiamo sottostare nostro malgrado.  
Tu, che sei nell'ombra, a te vorrei dare un nome. Un nome dimenticato, un nome che solo noi possiamo ricordare.  

Perché possa essere scritto su delle foglie morte e affidato al vento. [m]
