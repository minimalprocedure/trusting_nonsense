[title: Buongiorno al pigiama]

Buongiorno al pigiama.  
Che poi si fa presto a dire pigiama, ci sono quelli di _flanella_ per esempio; gli odiosi _pigiami di flanella_ che si appiccicano alla pelle e non scorrono, altamente sconsigliati nelle notti agitate. Si dovrebbe anche aprire una parentesi sulle lenzuola di flanella ma si potrebbe rischiare il _soffocamento in culla_. Ho visioni funeste dell'accoppiata, il _pendant_ perfetto per onorare almeno uno dei _figliuoli_ di _Hypnos_ e _Nix_ ma più che sogno, direi incubo.  

Tralasciando quindi il lenzuolo e tornando al pigiama affronterei quello di maglina.  
Che dire... È insopportabile pure quello: perfetto all'acquisto e slabbrato nell'uso. Non regge il lavaggio a 60 e diventa sempre più corto ma lungo di maniche. I pantaloni poi? Con l'elastico allentato? Cadono sempre nel tragitto _letto-bagno_ o _camera-cucina_ per il caffè. Si sta sempre li a tirarli su di un lato con la mano libera.  

... ma poi... Pigiama da uomo o pigiama da donna?  

Lo stesso _pigiamino_ ha le trine per lei e più spartano per lui, vari cuori rossi e fiorellini per lei e più spartano per lui, scritte varie che incitano l'ormone di lui o di lei per lei e più spartano per lui. Cuccioli, gattini e fumetti per lei e più spartano per lui.  
L'_uomo non deve chiedere mai_, quindi perché _ornargli_ il pigiama?   

Il pigiama da uomo inoltre, è quasi sempre blu nelle sue miriadi di sfumature o grigio. È altero.    

Quello di seta è liscio, che a trovare un paragone si dovrebbe fare sforzo di memoria per ricordarsi di quanto lo era l'_intimo bagnato_ di quella diciassettenne di tanti e tanti e tanti anni fa. Uno _sguitto_ ed eri dentro senza nemmeno accorgertene.  
Il pigiama di seta è un atto sessuale senza orgasmo, ci si deve muovere piano per non venirne espulsi come una saponetta. Ha quindi un innegabile vantaggio, la si può denudare solo con un gesto. Lo svantaggio? Ci si ritrova sempre le cuciture in posti dove non dovrebbero essere.  

Vestiti di giorno e vestiti di notte?    
No grazie.    

Giusto una t-shirt, che data l'età si vorrebbe evitare quel _torcicollo da spiffero notturno_, si leva al volo e lascia l'uccello libero di respirare nonché all'occorrenza _volare_.  

Va bene pure la _canotta_ altresì, che non impiccia alla puppa e non copre la _gnocca_.   

Dimenticavo, la _mutanda_ non è contemplata.  

Servo vostro m.

 


