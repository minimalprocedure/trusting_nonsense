[title: buon oggi alle bretelle]

Buongiorno, buon pomeriggio o buonasera, dipende da come la si vede.  
Però oggi è un buon qualcosa alle _bretelle_.

In questi gironi infernali di cinghiate a tutta chiappa, nessuno prende in considerazione le buone vecchie _bretelle_. Ricordate quelle da bambino a righe?  

Le _bretelle_, signori miei sono tra gli accessori indispensabili del buon _magister_ del sesso.  
Ci sono quelle doppie o singole al posteriore, ambedue hanno certi loro usi specifici, specialmente in abbinamento alla cintura in cuoio così immancabile.  

Cintura e bretelle. Come il più classico dei _precisini_ che teme la calata del calzone.  

Applicata la cintura in vita, sempre ben stretta mi raccomando, la bretella a tre che chiameremo _zoppa_ per distinguerla dall'altra, si presterà a un doppio uso.

+ agganciare la singola sul davanti, poi tirando adeguatamente posizionando al passaggio anteriore si procederà all'allargamento chiappe tramite le due altre estremità. Agganciando le clip divaricate a debita distanza, l'antro proibito sarà così scoperto alla vista e pronto all'esplorazione.
+ Invertendo gli addendi il risultano cambia poco. Pur sempre gli agognati anfratti saranno disponibili.

Le bretelle a croce, con la toppa al perineo, permetteranno invece l'azione combinata della doppia lama, quando l'una lo alza e l'altra lo prende.  

L'azione sadomaso della sculacciata può invece essere svolta sfruttando la naturale elasticità della bretella. Sia che sia ante-posizionata o retro-topica.  
Tirare dolcemente e lasciare con lo _schianto_, far contare oppure no è facoltativo. Ringraziare poi il _padrone_ a piacere.  

La loro versatilità le porta però anche a un utilizzo meno di _bassa_ lega, essendo utili anche alla tetta.  

La stessa cintura bene stretta può dar sostegno sia al basso che all'alto, con l'elastico ben mirato lo _crepitio_ al capezzolo è assicurato. Qui poi si può lavorar di _coppiola_ nel gergo del _tirator a piattello_.  

Che dire poi delle mollette che oltre alla cintura di prima applicabili sono, in tal e tanti loci, da far volar la fantasia. A guisa di scherzo all'elastico potreste rincorrer la bella per tutta casa o porla a bersaglio per cinque alla chiappa, 10 alla tetta e 20 alla gnocca.  

Insomma, che un buon _insomma_ alla fine ci sta sempre bene, Lasciate per un momento la cintura come _masculo attrezzo_ punitivo per dedicarvi almeno in prova, alle più _sofisticate_ bretelle.  
Qualcuno vi _maledirà_ e qualcuno vi _supplicherà_, chi di smettere e chi di continuare, chi poi dopo parteciperà al gioco.

Insomma, perché un altro _insomma_ ci sta bene, siate per una volta alternativi e con lo _schiocco_!  

Servo vostro, ma non troppo, m.  

 
