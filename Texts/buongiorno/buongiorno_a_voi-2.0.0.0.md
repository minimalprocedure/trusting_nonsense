[title: buongiorno cari voi]

Cari voi e care voi, Lei, io tu egli noi voi essi: buongiorno con una leggera nebbia..  

Mi guardo a volte il silenzio delle cose intorno, l'immobile silenzio degli oggetti posati o il rutilante silenzio delle persone tristi.  
La tristezza delle persone che è rumorosa e rombante, assordante e anestetizzante. Il silenzio di chi è triste, rumore rosa continuo e oscillante.  

La gente triste, che parla senza muovere labbro e che vede a occhi chiusi. Quelli che basta poco, quelli dalle ferite che non guariscono mai e che colano a goccia dopo goccia sangue rosso. L'uomo che è triste, solo e silente.  
Quello che si svela agli occhi di un bimbo al suo: - Babbo, tu non ridi mai. -  

Sempre è nel destino il camminare soli, nel pastrano scuro e nelle mani in tasca: è certe volte speranza di calore e altre solo fuga senza meta.  
Nel silenzio del rumore del mare, nei piedi scalzi al freddo e sulla sabbia dove le tracce di noi mai permangono.  

A niente vale il coprirsi pesante  
cercare quel fondo al buco nella fodera.  

Niente quel fermarsi in mezzo al traffico  
dove tutti passano urtandoti un poco.  

Nemmeno quel voler urlare: - Sono vivo! -  
per tutto il rumore che fai, è nulla.  

A niente vale, l'attacco o la difesa o ritirarsi, quando la tristezza arriva. È tutto il peso del mondo che cala piano da un alto così lontano, il peso nostro, unito e pieno dei milioni di altri.  
Annaspando si vorrebbe trovare l'ago nel pagliaio dei dolori contorti, mischiati, annodati, cuciti a filo spesso gli uni agli altri. Si tenta in tutti i modi e le mani si macchiano, le unghie si spezzano, le ginocchia si sbucciano, gli occhi si gonfiano. Sono pianti e cadute, cose scritte e quaderni poi bruciati, lanciati oltre i cancelli chiusi.  

Pieni di parole, quelle che non servono mai a nessuno. [m]

