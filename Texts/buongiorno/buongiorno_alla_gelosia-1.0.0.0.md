[title: buongiorno alla gelosia e grazie per tutto il pesce]

Ragazzi come siete irruenti stamani, fioccano _suggerimenti_ a palettate.  
Va bene, è una balla e ne sono arrivati solo un paio.  

Allora oggi: _gelosia_.  

Sapete quel sentimento che vi fa affilare i coltelli e recuperare la vecchia attrezzatura per fare le cartucce?  
Non sapete fare le cartucce?  

Allora, ci vuole per prima cosa una bilancina di precisione...  

<< Quella dei pusher? >>  
<< Si proprio quella. >>  

Poi vi serve la polvere da sparo e lì la scelta è difficoltosa, ne volete una lenta o una veloce? Volete impallinare il malcapitato in che modo?  
Le polveri lente tendono a sviluppare un impatto più profondo, ma quelle veloci porteranno il piombo più lontano. Dovete pensare alla opportunità concessavi.  
Tra le polveri lente ci sono quelle molto lente che hanno bisogno di una piccola quantità di una veloce da usare come innesco.  

<< Tu gli tiri a pallettoni o pallini? >>  
<< Guarda che i pallettoni sono vietati, quindi pallini o palla >>  

Dovete poi procurarvi delle borre, anche qui c'è la classica in feltro o quella tecnologica in plastica a contenitore. La borra in feltro è _vintage_ e ha il suo fascino, a volte nel tiro riesci anche a vederla annaspare dietro ai pallini che se la tirano perché più veloci. A volte la vedi persino colpire come _colpo di grazia_ che fa cader l'_uccello_.  
Quella di plastica invece è la massima espressione della tecnologia balistica. In basso è molleggiata, per ritardare il più possibile caricandosi e assorbendo energia. La capsula invece ha il compito di lubrificare la canna per una fuoriuscita del piombo ottimale.  

Siamo quindi arrivati al pallino o la palla. La cartuccia, ebbene si, può essere sia pluriorchide che monorchide, multi palla o mono palla.  
Ci sarebbero anche i pallettoni e questi a seconda del calibro si mettono _tre a tre_ o _cinque a cinque_. I pallettoni però, a meno che non siate camorristi, mafiosi, della 'ndrangheta o Sacra Corona Unita, non li potrete usare. Io vi ho avvertito.

Il pallino dicevo. Il pallino può essere nichelato o no, cioè ricoperto di nichel o così come mamma fonderia l'ha fatto. Vantaggi e svantaggi? Quello nichelato è più duro e si deformerà meno ma tenderà a trapassare, quello normale si deformerà di più ma l'impatto sarà migliore. Stesso discorso della polvere: vicino o lontano, grosso o piccolo?  

La palla invece è quel pezzo di piombo grosso un dito che sconquassa. Avete presente il calcio nelle palle con l'anfibio dalla punta d'acciaio che vi ha dato il _metallaro_ quando facevate il _paninaro_? Non avendo mai fatto il _paninaro_ e avendo avuto dei Dr. Martens (quelli che ci voleva una giornata solo per allacciarli avendo le stringhe fino al ginocchio), quei calci nelle palle non li ho mai presi. Però posso immaginare.  
Ecco, la palla è questo grosso cilindro di piombo con le scanalature di lato e la punta incisa. Incisa? Si perché nell'impatto il cilindro sboccia a fiore, è l'orgasmo della palla.

Finita? No perché vi serve il cartoncino, il bossolo e la macchinetta per chiudere la cartuccia.

Coding
---
Pesare la polvere -> mettere nel bossolo -> mettete la borra -> pigiate la borra né troppo né poco -> pesare il piombo -> mettete il piombo -> mettete il cartoncino -> caricate la macchinetta -> stringete -> girate -> estraete.

_Una serie di passi unici e non ambigui_ (cit)

Il finale
---
Ora avete l'emozione della gelosia e lo strumento, _fate vobis_.

Il finale alternativo
---

Non sarebbe stato meglio un vaffanculo?

Firma
---

Servo vostro.
