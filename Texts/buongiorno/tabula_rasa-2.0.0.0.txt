[title: buongiorno tabula rasa] 

Buongiorno.  

Ieri mi si chiedeva perché della _tabula rasa_, ho risposto e lasciata qui la risposta per adesso.  

Volevo aggiungere che per me non ha molto senso lasciare per alcuni motivi fondamentali:  

+ non sono in cerca di consenso

Sinceramente e in tutta amicizia, non mi può importare di meno il consenso altrui. Certo non disdegno chi apprezza, specialmente se sono delle persone particolari che ora mi pare inopportuno elencare. Per il resto grazie ma non è che siete fondamentali.  
Come penso si sia notato sono perlopiù cose scritte e a volte lunghette, _wall of words_ tanto disdegnato in questi tempi di messaggi fugaci di _centosessanta caratteri_ (l'ultimo della fila mi farà notare che oggi sono di più, ma comunque pur limitanti).  
Le _genti_ di quest'era così _veloce_ già strabuzzano l'occhio alla quinta riga e grasso che cola se arrivano alla decima.  

Le foto, in mezzo alle poche riproposte dell'altrui, sono mie. L'altrui è più o meno sempre lo stesso, tolto anche qui poco.

Detto questo, a che pro mantenere centinaia e centinaia di testi e immagini?  

Mantenerli poi in un sistema _demenziale_ come questo? Dove è praticamente impossibile ritrovare proprio quella cosa che era stata pubblicata nel 2016?  

Talvolta leggo, robe del tipo: sono dieci anni qui!   
_Miei coglioni_, mi viene di rispondere. _Non ti sei ancora stufato?_, stufato di questo _nulla_ compulsivo?  

Stessa domanda potrebbero rivolgerla a me, che sono più di dieci anni che a fasi alterne bazzico in queste lande desolate.  

Una cosa però va detta e questo fa il ritornare: in nessun'altra piattaforma sociale ho trovato persone così interessanti. In mezzo davvero alla banalità di sentimenti, alle citazioni sgrammaticate, a quelle di Dante in inglese o di Foscolo in giapponese, a fiche incaprettate in modi prevedibili, in mezzo a tutto questo c'è il _lumino_ acceso.  
La mia solita descrizione del lago oscuro e la barchetta con la fioca candela.  

In mezzo a questo buio, ho incontrato persone davvero interessanti. Disadattati dal mondo esterno, che in questo _segreto di Pinocchio_ si espongono.  

... e io **adoro**, per dirla proprio tutta... **adoro** chi si espone.  

Badate non quello che sbandiera il _cazzo eretto_ allungato col filtro fluido o la fica depilata a gambe larghe. La tetta prosperosa dal capezzolo più o meno erto che pare tutti i maschi anelino dal profondo.  

Questa esposizione sa davvero di _hard discount_ dove la miriade di marche misconosciute sono poi della stessa banda.  

Ci sono invece, quelli che si espongono a pezzetti. Una parola qui e una là, nascoste bene e talvolta in un sacco di spazzatura. Ci sono e io voglio trovare quelle.  

Ecco perché sono in qualche maniera ancora qui. [m]
