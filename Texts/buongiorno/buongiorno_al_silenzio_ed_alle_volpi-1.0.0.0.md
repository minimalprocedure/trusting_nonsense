[title: Buongiorno al silenzio e buongiorno alle volpi.]

Buongiorno al silenzio e buongiorno alle volpi.

La volpe, nell'immaginario collettivo animale furbo e solitario.
La volpe sta in silenzio, pensa e scruta.

Il silenzio, che vive spesso nell'assordante e caotico mondo che lo circonda. Il mio silenzio e il silenzio dell'altro.

La volpe è un disegno abbozzato, fatto come da bambino, incerto, col lapis. Lo disegnano e sembra un gatto, nella mano incerta che delinea. Un disegno.  
Un disegno grande su un foglio grande e attaccato al muro, a larghe bracciate lo farei. Calcando nello scuro dei contorni, cercando il silenzio del suo sguardo.

La gente che non racconta è nel silenzio, quelli che lo fanno sono nel silenzio. Ci parliamo non sentendoci mai, nel silenzio.

Nel silenzio ti guardo come sei, nel silenzio ti immagino. Nel silenzio sei come solo io so come sei. Apri bocca e chiudi bocca, rumore e silenzio. Allo specchio chi si guarda e a volte si piace e a volte no, rumore e silenzio.

Siamo rumore e silenzio.

Gridiamo amore e odio, rumore e silenzio e silenzio e rumore. Ci guardiamo nelle scorrere di tutti i giorni: il silenzio.  
Le persone vicine nel silenzio scorrono in fila, una dopo l'altra: 

<< il prossimo? >>. 

Nel silenzio si scambiano per chi viene e chi va.

Mi tappo le orecchie per non sentire, mi metto le cuffie per sentire. Musica e silenzio, è la misura dell'armonia e del progressivo divenire del suono.

Sono 4.33 minuti di silenzio, il numero perfetto, il suono perfetto. L'armonia transiente della semantica del rumore.

Nel mio silenzio, mi guardo intorno  
c'è gente avanti e dietro e a lato.  

Sono tutti fissi e mobili, sfumati  
mentre il rumore cala e sale piano.  

Solo nel silenzio, mi guardo intorno  
e scopro visi ignoti e già perduti.  

Sul muro la volpe disegnata, quella che sembra un gatto, mi guarda con gli occhi indecisa. Lei, non sa che fare. Morde o si lascia mordere?

Attende in silenzio. [m]







