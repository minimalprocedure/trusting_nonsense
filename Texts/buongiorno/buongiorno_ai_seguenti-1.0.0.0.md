[title: buongiorno ai seguenti]

Buongiorno, oggi piove.  
Lo vedo dalla finestra, nel campo degli olivi.  

Buongiorno a quelli che ti seguono perché tu li segui, a quelli che ricambiano la _seguitudine_. Quelli che _nonglienepuòfregaredimeno_ di quello che hai in un blog. Loro _ricambiano_. Li segui e ti seguono, non li segui e magicamente spariscono.  
Non che sia una grande perdita, se non li segui un motivo ci sarà. Un po' come su **facebook**, quando: _non ci sentivamo da dieci anni_; a cui rispondi: _appunto. un motivo ci sarà_.  

Insomma, pigi il **segui** e sarai seguito, lo _depigi_ e sarai miseramente e inesorabilmente _deseguito_.  

Che poi, il _deseguire_ è diverso dal _non aver mai seguito_, il _deseguire_ è un _demerito_, un approccio _desensibilizzato_, _demenzialmente_ _deleterio_.  

È la colpa che ti rimane sul groppone, il _pussa via_ _demente_ che ti macchia il **karma**.  

Il fatto che tu _desegua_ qualcuno è accolto come una offesa personale da lavare col sangue, il _defollow_ diventa una delle offese più gravi. Passi che ti ho rigato la macchina con la chiave, ti ho violentato la figliuola od ho dato della _troia_ a tua moglie. No, il _defollow_ no.  
Se la legano al dito, il mignolo o il medio o l'anulare, l'alluce per ricordarlo _proprio bene_.  

Certo, tu li hai **defollowati** e come mai? Ci sarebbero molte spiegazioni: una che magari, ma solo un po', ti sei rotto delle solite chiappe e di tutta quella passione riciclata.  
 
Ma come? Il riciclo va di moda oggi. Ricicliamo anche il tetrapack, perché non le passioni.  

Con tutta questa passione in giro, mi viene quasi voglia ma spassionata però (e ma e però insieme non si dice), di _a-passionarmi_ o _depassionarmi_, diventare freddo e insensibile, _desensibilizzarmi_.  

Amore di qua e amore di là, strusciamenti vari, particolari che nemmeno un anatomopatologo ha mai visto nella sua carriera, posizioni convulse in estasi da strozzamento, falli di dimensioni bibliche e orifizi in dilatazione da parto (a quel punto è come il cane che si morde la coda, per forza il fallo è di dimensioni bibliche).  

Tutta questa roba che passa e ripassa, prima a colori e poi in bianco e nero. Virata in ciano o seppiata con quel suo gusto _retro_. Lo stesso fotogramma del film porno di moda, un conto è a colori e un conto in BW. Si sa che il BW rende tutto poetico. La luce che _dipinge_ i corpi, il chiaroscuro che enfatizza l'estasi di lei alla presa _en passant_.  

Pare tutto più vero. Più vissuto e più: _Lo vorrei fare anche io, ma non ho il coraggio_.  
Oddio, anche: _Io lo faccio e me ne vanto_, o _Io, altro che quello faccio._  

Questa passione senza peli e non solo senza veli.  
Tutto questo pelo tagliato, rasato e, oddio (l'ho già detto), che fugace visione la ceretta al buco del culo.  
Lo strappo secco della eviscerazione del pelo.   

Domanda per i master in giro, le avete mai depilate laggiù le vostre schiavette con la ceretta? Potrebbe essere una pratica interessante al posto della sculacciata. A me, il solo pensiero che qualcuno mi depili con la ceretta l'ano mi prende male.  

Pensate.  
Prendete il pentolino e scaldate la cera, nel frattempo avete legato la ragazza carponi sul cavallo (quello ginnico).
Polsi e caviglie strette alle gambe dell'attrezzo. Lei deve essere ben a cavalcioni per poter esporre e protrudere l'areola pelosa all'estetista improvvisato in anfibi e calzoni makò.  

Consiglio di far passare una corda tra le natiche in obliquo per tenerle il più possibile divaricate, cercate di estendere l'area in oggetto, lavorerete meglio.  

Il pentolino sarà caldo, lo avrete scaldato in base alla _schiavitudine_ di lei, ma senza ustionarla però, state molto attenti. Facciamo sui 44.5°C, direi temperatura ottimale.  

Con la spatolina, passate la cera calda su tutta l'area, insistendo sull'areola più bruna (sempre che non abbia l'ano sbiancato, visto che va di moda). Non fatevi prendere dalle smanie eh? La cera non è lubrificante e nel caso, le cose sono due: le brucerete l'intestino e certi giochetti poi ve li scordate oppure non è detto che a depilarsi alla fine sia lei, potreste _pilarla_ con i vostri di peli, anziché _depilarla_.  

Passata la cera, sbrigatevi ad applicare le strisce di stoffa.     
Consiglio: le strisce che trovate nella confezione non funzionano mai, meglio delle strisce di stoffa e di lino sono l'ottimo.  

Potreste aver usato anche la camicetta di lei, quella di Prada, tagliata a strisce. Le tagliate la camicetta davanti con aria sadica e lei dal dolore potrebbe già venire.  

Attendete che la cera raffreddi.  
 
Siamo al momento, afferrate un lembo e strappate, prima piano per un pelo alla volta e poi uno strappo deciso.  

La vostra _schiava_, sospirando, ringrazierà. [m]  
