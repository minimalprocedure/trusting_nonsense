[title: buongiorno alla paura e alla confusione]

«_Marcia    del    cannoneggiamento    futurista colosso-leitmotif-maglio-genio-novatore-ottimismo fame-ambizione     ( TERRIFICO  ASSOLUTO  SOLENNE EROICO      PESANTE   IMPLACABILE    FECONDANTE ) zang-tuumb tumb tumb_» [Filippo Tommaso Marinetti]


Buongiorno senzienti e non, innamorati e non ma solo per chi lo fa che quelli che non fanno oggi non sono presi in considerazione. Salve a voi.  

Sono pervenuti oggi, o meglio uno ieri sera e l'altro stamattina, suggerimenti: paura e confusione.  
Devo ammettere che è quasi una sfida il coniugarli insieme.  

Alla via così allora: orziamo la poppa e strambiamo la chiappa.  

Che poi si fa presto a dire _paura_, _paura_ di che? Di chi? Paura o semplice timore? Se la _paura_ fa novanta, il _timore_ fa quanto? Cinquanta? Come si dice... timor di dio o paura di dio?  
Quando ero piccolo sotto il mio letto viveva di notte una colonia di esseri neri, una specie di granchi o scorpioni. Questi animali terribili avevano al posto di una chela una lama luccicante e tagliente come un rasoio; questi simpatici bastardelli scorrevano il bordo del letto continuamente con queste lame e quindi ero costretto a rimanere nei confini del materasso pena il taglio netto di qualunque cosa sporgesse. Paura di bambino.  

A domanda mi rispondono sulle paure normali o irrazionali e al che mi sono soffermato a pensare alla distinzione. Quali sarebbero le paure _normali_? Quella di perdere il treno? La _paura_ non è per forza irrazionale? Altrimenti che paura è, _paura_ ragionevole?   

«Cara sono ragionevolmente impaurito oggi.»  
«Caro, vuoi del té?»  

Paura Razionale? Siamo pieni di domande, nevvero?.  

C'è pure chi ha paura della _confusione_, magari della _confusione_ degli altri in particolare. Della _confusione_ mentale, del disordine _mentale_ e di quello del cassetto dei calzini. Il _cassetto del buio e della luce_ dove tutto lo _sporco dei pensieri_ vien gettato alla rinfusa nella _paura_ di perderlo.  
Chi ha _paura_ di perdere quei pensieri più _sporchi_ che a volte ci affiorano inesorabili? Tu, tu e tu, voi non temete questo? Io un po' si e per questo tento spesso a materializzarli, infonderli nella materia vivente o inanimata. Foglietto arrotolato sotto la lingua del golem. 

Paura e confusione come una provoca l'altra o al contrario, biunivoca relazione di _causa/effetto/effetto/causa_. 

Interessante poi come la _paura_ sia sempre umida, associata alla perdita di fluidi:  

«S'è cacato addosso dalla paura!»  
«Mi sono pisciato sotto dalla paura!»

Pure la _servetta_ o il _servetto_ si bagnano nella _paura_ mentre attendono la _punizione_. In quello stato _confusionale_ in cui i pensieri vorticano tra il conosciuto, l'improbabile e l'ignoto. In questa calma irrazionale di un'attesa che non termina mai o che vorremmo, non terminasse mai. L'ordine non favorisce l'endorfina come lo fa la confusione.  

Insomma, non vi sentireste bene anche voi dopo aver impaurito vostro figlio con urli e minacce affinché metta a posto il casino che ha in camera?  

«Oh! ...e che cazzo!»  

Siete _sbolliti_ e vi sentite in pace col mondo: _zang-tuumb tumb tumb_. [m]
