[title: il lato oscuro]

Buongiorno al lato oscuro.

Quella parte che pare abbiano tutti, specialmente sui social.  
Il _lato oscuro_, quel mistico recesso della mente che si espone giusto per fare un po' _il fico_.  

È come dire: 

<< Guarda che io... mica cazzi! >>.  

Talvolta questo lato è talmente a _latere_ che se cerci di scorgerlo lui ti gira intorno, _a lato_ appunto. Immaginate, lei o lui con il suo _lato oscuro_, voi alla sua sinistra e il _lato_ alla destra. A niente vale voltarsi velocemente o girare intorno, _lui_ è più veloce di voi.  
Gioco di bambini intorno all'albero.  

Poi mi chiedo sempre, ma questo _lato oscuro_ corrisponde all'ES? Quello di Freud o quello di Groddeck? No, perché c'è differenza a parte che quello di Freud è una copia di quello di Groddeck.  
Quello più famoso una copia? Santa padella! Non ci sono più le mezze stagioni e si stava meglio quando si stava peggio.  

Il _lato_ pare che sia davvero in fondo, ma talmente in fondo che più in fondo non si può e oltre.  
Lo trovi citato in miriadi di citazioni, quelle da Bacio della nota fabbrica di cioccolato (che non è quella di Dahl) o di Tumblr (che poi è lo stesso).  Citazioni a volte rigorosamente anonime (mi chiedo il perché), a volte orgogliosamente col nome sotto (mi chiedo il perché anche qui).  

Penso ogni tanto, di non avercelo questo _lato oscuro_ o almeno di averlo chiarificato da tempo. Insomma, se mi chiedono quale sia il mio _lato oscuro_ glielo dico senza tirarla per le lunghe, perdendoci indubbiamente di mistero.  

Che poi, sarà solo uno? Se ce ne fossero più di uno? Altro che _lato oscuro_, i _lati oscuri_ come miriade di sfaccettature _oscure_.  Un momento... Qui si entra nel _diamante grezzo_ che piace tanto alle donne.  
Aladino ci insegna che se sei un _diamante grezzo_ ti puoi _ribattere_ la principessa Jasmine in tutte le maniere, pure il buco segreto ti concede (anche se molto _principescamente_).  

(Basta, altrimenti il mio _lato oscuro_ mi obbliga a una capatina in bagno data la momentanea mancanza di principesse usabili.)  

Se ci fate caso questo _lato oscuro_ poi, è per la maggior parte relativo al sesso. Sono quelle cose inconfessabili che si sputtanano su Tumblr, insieme ai gatti e i libri.  
Non trovate che sia interessante? _Lati oscuri_ con gatti e libri, quale il nesso?  
Certo leggere è un lato oscuro, ma i gatti? Se poi ci mettete il caffè, il cappuccino e i dolci...  
Tutti _lati oscuri_ ovviamente, diversi o manifestazioni dello stesso?  

Quante domande e dubbi e misteri, _lati oscuri_ anch'essi.  

Stamattina, nel cazzeggio ho letto una bellissima frase che dice più o meno così (la memoria da vecchio ormai decrepito e disilluso mi tradisce nel più bello):

<< ... vorrei che atterrassi sul mio lato oscuro ... >>   

Diciamole chiare le cose, basta coi sotterfugi:

<< ... vorrei che mi atterrassi sul cazzo e possibilmente di schiena ... >>

Buongiorno. [m]




