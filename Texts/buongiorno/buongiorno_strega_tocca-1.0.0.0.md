[title: strega tocca...]

Buongiorno alla strega che tocca.  
Che tocca? Cosa si toccherà mai la Strega?  

Che poi è _strega tocca_ o _strega comanda_? Non sarebbe mica la stessa cosa, ma se fosse invece _strega si tocca_ le illazioni sarebbero molteplici ma è certo che _strega si comanda_ sarebbe strano.  
Appurata la non riflessività e non reciprocità delle azioni ci si dovrà concentrare sul cosa tocchi o comandi.

<< Strega comanda color... color... Giallo paglierino chiaro! >>  
<< ... ma non esiste! >>  
<< Come no! Pisciati in mano! >>  

Certo che se si è sani va anche bene, ma se malaticci no: può andare dal rosato chiaro al marrone scuro, passando per il giallo più o meno acceso.  
Da piccolo lo chiamavo semplicemente _color... color..._ togliendo _Strega comanda_, sono sempre stato insofferente all'autorità probabilmente. Visto che siamo in rimembranze ricordo anche quando giocavo a _nascondino_ nel paesello _della mi' nonna Francesca_, in giro per il paese con quella masnada di ragazzini urlanti.

<< Tana per Giovanni! >>  
<< Uffa! >>  
<< Marco! Corri... corri! >>  
<< Tana libera tutti ! >>  
<< Uffa però, conto sempre io! >>  
<< 1, 2, 3, 4... 32. Chi è dentro è dentro e chi è fuori è fuori! >>  

Facevamo anche le bande e _guardie e ladri_.  
L'edicola di sotto vendeva dei coltelli giocattolo in gomma morbida e adatti a non far male. Erano però troppo morbidi e quindi, nelle lunghe sedute di preparazione, venivano sprecati interi rotoli di carta igienica da pressare nel manico per renderlo più duro. Il risultato erano parecchi bernoccoli ma si era tutti più contenti.  
La fase super eroi era però unica e tutti quanti invidiavamo il figlio del falegname. Ognuno di noi faceva un super eroe e io ero per gli X-Men (quando ancora Wolverine non c'era) e facevo _Bestia_ saltellando facendo del _parkour_ quando ancora non esisteva. Il figlio del falegname faceva Thor.  
Il _su' babbo_ un giorno gli fece un martello di legno... Santa Patonza da Inquarto di Sotto, era un spettacolo.  
L'adorazione del martello.  

Le ragazzine invece giocavano a _campana_ saltellando qua e là. Giocando a campana ho perso il primo _dente da latte_: già dondolante mi fu strappato da una di quelle ragazzine con uno spago. 

Noi giocavamo e la _mi' nonna_, una signora anziana alla _Marlene Dietrich_, passava le giornate sotto la torre dell'orologio a leggere e fumare. La _Francesca_ era conosciuta e soprattutto _temuta_ in tutto il paese. Donna dal cambio repentino d'umore, oggi si era ospiti per poi essere buttati fuori di casa all'improvviso. Donna di polso penserete, no solo _donna oltremodo stronza_.  

I bei ricordi dell'infanzia.  

Oggi che alla _lippa_ non ci si gioca più e nemmeno ai _quattro cantoni_, la _strega comanda_ ci riporta brutalmente alla realtà anche se sovente sempre di giochi si tratta.  

<< Strega comanda... Lecca lo stivale! >>  
<< Si, Mia Signora. >>

Carponi poi si accinge all'azione labiale, linguo-palatale, velofaringea, laringea e cricofaringea dello stivale alla coscia, rigorosamente nero e condito di escremento di vacca. 

<< Ti piace? >>  
<< Ottimo Mia Signora! >>  

Anche _strega tocca_ ha un suo perché in tal guisa. La strega che tocca lo scroto a mo' di antistress per esempio, certo ti rotea un po' troppo le palle ma potrebbe pure piacere. La strega che ti tocca l'uccello piace sempre su! Oltretutto mica te lo tocca solo con le mani no? Lo fa pure con la lingua.  
Dubbio... Quando si scopa, chi è che tocca chi?  
È la chiappa che viene a toccar l'uccello o viceversa?

Inoltre... Chi tocca muore? 

_Invece questa visione dell'uccello dell'orologio a cucù? La porticina che si apre e chiude... Cucù, cucù!_  
_... non andiamo fuori tema per oggi._ 

Servo Vostro.

