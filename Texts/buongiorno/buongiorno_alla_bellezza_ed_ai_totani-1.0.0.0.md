[title: buongiorno alla bellezza e ai totani]

Buongiorno alla bellezza e ai totani.

La bellezza, questa emerita sconosciuta e spesso relegata alla chiappa o alla tetta.  
Per carità, chi sono io per disdegnare il gluteo sodo e ben piantato tra i piedi e la testa o la tetta marmorea che sfida ogni legge naturale?  

Ovviamente apprezzo.  

La bellezza però trovo sia più sottile, più evanescente, aleatoria e stocastica. La bellezza è imprevedibile.  
Appare, all'improvviso e quasi non l'aspetti. Non c'era e ora c'è.  

Ci sono volte in cui la si intuisce, bivacca nascosta tra le pieghe, è quasi sfuggente. Non vuole farsi quasi trovare, forse per timidezza.  
Difficile che si metta in mostra, che stia lì agitando le mani per farsi notare.  
Insospettabilmente, a un certo punto appare.  

Ci prende allora quello stupore infantile e la sottile voglia di averla tutta per sé, come un gioco da tempo desiderato ma che Babbo Natale ha sempre ignorato.  

<< Si, ci siamo e abbiamo capito ma non cogliamo il _totano_ >>  

Già, il totano.  

Quando praticavo la pesca subacquea, cosa che mi sono premunito di dirvi tempo fa, ho visto parecchi occhi di pesce. Dall'occhio di cernia a quello di triglia, passando per l'occhio di polpo e di seppia.  Dimenticavo l'occhio di dentice, quel pesce bastardissimo che sta sempre un po' più in la e mai un po' più in qua.  

L'occhio di cernia è quello che ti scruta e valuta continuamente se sei buono da mangiare, controlla le dimensioni e applica algoritmi sofisticati per valutare come poterti inserire in bocca. Le cernie giganti dei mari tropicali ammazzano più subacquei degli squali.  
L'occhio di triglia, lo sappiamo tutti, appartiene a più specie animali e non solo quella ittica. L'uomo in particolare ha sviluppato un occhio di triglia estremamente evoluto.  
Quando il maschio in cerca della femmina in piena ovulazione cerca, lo usa a mezzo di rimorchio. Sfodera l'occhietto e a volte tutti e due, con quel fare leggermente ammiccante.  

<< Ha una drosophila melanogaster nell'occhio? >>  
<< No >>  
<< un pappatacio? >>  

La femmina umana ha sviluppato una particolare difesa contro l'occhio di triglia e sa ormai come identificarlo. Questo sarà un problema, perché andrà estinguendosi non potendosi più accoppiare e trasmettere i propri geni alla prole.  

L'occhio di polpo, che si dice polpo perché il polipo è quello del corallo o del culo, è quello triste.  
Lo vedi e abbarbicato allo scoglio, ti vede. Cambia colore e ti guarda. Tu lo vedi, lo sai che è lì e basterebbe appoggiargli il fucile sulla testa e sparare.  
L'occhio di polpo ti fa pena, perché poverino... Cambia ancora colore e ci riprova. La natura, ingrata maestra, gli ha insegnato la mimesi e soprattutto a crederci. Ti guarda e tu lo guardi, in questo perfido gioco di sguardi. 
L'occhio di polpo è intelligente ma ingenuo, si fida. Poi fa all'improvviso una nuvoletta nera e si infila nel buco dieci centimetri più in là.  
Come si fa ad ammazzarli? I polpi non li ho pescati mai.  

L'occhio di seppia invece è un bel tiro. Improvvisamente vi scontrate quasi. Lei ti vede e tu la vedi, _sfida all'Ok corral_. Vi girate intorno pronti all'azione.  
L'occhio di seppia punta, controlla e misura le distanze. Lo vedi o meglio li vedi, perché l'occhio di seppia sono due occhi di seppia, si muovono e guizzano. Loro ti sfidano.  

<< grande e grosso coglione vestito di gomma col fucilone ora ti faccio vedere io come sono svelta >>  

È un attimo e ora c'è ora non c'è. Dopo quel minuto di interminabile attesa cogli un fremito, uno sgargiante cambio di colore dal marroncino al rosso mattone. Diventa una _Mord Sith_ senza _Agiel_ in mano.  
Scatta.  
Parte a una velocità che noi umani possiamo solo immaginare, in quel quarto di battito di ciglia sparisce, si volatilizza lasciando solo un po' di inchiostro.  

Come a dirci: << questa è la mia firma, pollo >>  

Come ho detto l'occhio di seppia è un bel tiro e si deve essere veloci.  

Il totano non l'ho mai preso, mi toccherà provarci un giorno, magari sarà un bel tiro anche quello. [m]


