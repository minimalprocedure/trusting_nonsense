[title: la mancanza]

Buongiorno alla _mancanza_. 

Buongiorno alle cose che non ci sono. Buongiorno a voi.

Una volta ho scritto: << mi manca ogni cosa e ogni cosa è mia >>.

Mancano le cose, mancano le persone, mancano gli amori, mancano i soldi. Ci mancano occhi da guardare e da sognare, ci manca il sesso, ci manca accarezzare, ci manca lo _scopare_.
All'uomo manca se stesso, spesso. Si manca.

L'ho mancata una volta alla stazione, sono arrivato tardi. Il treno è scivolato sulle ruote di ferro davanti a me e lei era sopra, aldilà del vetro.
Ho mancato i tempi, gli amici. Ho mancato e sono mancato.

Ci sono momenti in cui ci siamo ma non c'è l'altro e poi arriva e non ci siamo più.

Manchiamo. La mancanza.

Dico sempre di non voler mancare più, ma come posso farlo? Come non perdere continuamente tutto? È la mia natura. Io perdo, io manco.

Ci manca, la persona che si ama, ci manca perché la si vuole con tutti noi stessi. La bramiamo, la pensiamo, la vogliamo, la desideriamo. Ci manca e ci manca anche quando è con noi.

La _mancanza_ è uno stato della mente, una insoddisfazione latente che è scritta nel DNA, nei geni, alleli, spirali lunghissime di codici amminoacidici. Uno stato della mente.

Persone ammalate di mancanza, in modo tale da annientarsi e mancarsi. Ci si vede sfilare sul quel treno che va da qualche parte e non importa dove. Dove, dove, dove?

I _Mancanti_ si salutano con la mano, con il fazzoletto bianco sventolato al finestrino. Passano sui marciapiedi, scendono predelle. Nascosti da cappelli e velette nere si scorgono e passano, sorridono e salutano.

Sono negli orologi del tempo con le lancette che girano: quella lunga e quella corta, quella veloce dei secondi. Aggrappati alle lancette ruotano nel mondo scorgendosi appena.

Il tempo del passato, dell'oggi e del futuro è scorto nelle mani di carte.
La zingara vestita di pezze, di tutti i colori del tempo che passa, mi legge la mano nelle sue molte righe.
È confusa e mi dice: _lei è morto due volte e rinato, morirà ancora e ancora fino all'ultima._

Una maga una volta mi disse: _Non so leggerla la sua mano e non capisco le sue carte, mi dispiace_. Non ho nemmeno dovuto pagarla.

Noi siamo i _Mancanti_, le _mancanze_ sono l'alito della nostra vita.

Ci manca tutto e tutto è nostro. [m]







