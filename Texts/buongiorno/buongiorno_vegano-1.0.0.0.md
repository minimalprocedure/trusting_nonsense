[title: Vegani, Marziani, Pincopalliani]

Buongiorno e per dopo buon pomeriggio, agli extraterrestri, _Vegani_, _Marziani_, _Vesuniani_ e _Pincopalliani_ (o _Pincopalliti_).

Tutta gente con usi e costumi diversi, tradizioni e cucina. Si va dal _seme liscio_ a quello _integrato a pillole_, a quelli solo _ciccia_ e a quelli _no ciccia, ma latte sì_, da quelli _anche uova_ a quelli _che nemmeno il maglione di lana_.  
Le idee sono tante, spesso confuse, con alcune verità ma così _naïf_ e di tendenza.  

Del _marziano_ possiamo dire che a parte le antenne verdi e le pratiche riproduttive complesse e articolate che potremmo definire _parecchio complesse_, di loro si sa poco o nulla. Sono esseri schivi fortemente allergici alle sonde spaziali che gli abbiamo inviato, c'è però anche da chiedersi chi non sia particolarmente allergico alla sonda spaziale o no. A nessuno piace _la sonda colonscopica_ (spaziale nel senso che fa spazio) per esempio, anche se alcuni apprezzano l'acciaio chirurgico nell'ano purché abbia il diamante sul pomello. Un po' come l'anello di fidanzamento che è meglio allegato alla pietra dura che _scusso scusso_.  
Nella nostra storia solo _John Carter_, intraprendente uomo della _Virginia_ riusci a _sondare_ la bella _Dejah Thoris_ in maniera approfondita, ma quelli erano bei tempi che oggi tristemente non ci sono più.

Il _venusiano_, invece è sicuramente la razza più bella dell'universo. La femmina venusiana è di indole che qui sulla terra definiremmo _mignotta_, _parecchio mignotta_ o _assai mignotta_. Il maschio invece in gioventù di cranio liscio, inizia a sviluppare con l'età varie protuberanze che lo portano in vecchiaia ad assomigliare ai nostri cervi cosiddetti _cornuti_. La _venusiana_ ha un rapporto simbiontico con l'acqua dove si dice nasca attraverso uno speciale rituale. Le _venusiane_ sarebbero in grado di partorire solo _venusiani maschi_, la femmina invece parrebbe _procreata_ tramite _castrazione rituale_ di un maschio appositamente scelto i cui testicoli, passati in un apposito _shaker_, vengono poi lanciati con un trabucco adatto in mare. La _venusiana_ è alla nascita dalla spuma del mare _bionda tinta con ricrescita_.

Il _picopallita_, nella accezione moderna _pincopalliano_, è di statura bassa e tozza. L'arto inferiore è corto e presenta un accentuato _sfericismo_ al ginocchio, l'anca bassa e pronunciata in quello che noi volgarmente definiremmo _culo basso_. Non esiste praticamente _dimorfismo_ sessuale, ambedue i sessi sono virtualmente indistinguibili a una prima occhiata. Il _pincopallita_ è ermafrodita, infatti l'apparato riproduttore, che è situato sotto la pianta del piede, è presente nelle due forme anatomiche. Quello che chiameremo _maschio_ ha l'apparato femminile situato nella pianta del piede destro e quello maschile nel sinistro; quella che invece definiamo come _femmina_ al contrario. Recenti studi hanno concluso che questa alternanza si sia affermata come varianza genetica per motivi di pura _comodità posturale_.
La gestazione del cucciolo _pincopallita_ dura per dodici anni in un'apposita sacca fetale posizionata sul lato posteriore del collo, dando al _pincopallita incinto_ un aspetto ingobbito. Al termine della lunga gestazione il nascituro, ancora morfologicamente in _bolo_, verrà _vomitato_ nel _secchio puerperale_ dove terminerà lo sviluppo in soli due anni. Sembrerebbe che a causa di questo particolare ermafroditismo, alcune posizioni sedute a piante del piede unite siano molto apprezzate e facilmente individuabili tramite osservazione delle espressioni facciali.
 
il _vegano_ o _veganiano_ è l'aristocratico degli extraterrestri qui presi in considerazione. Per il _vegano_ l'altro da sé è prevalentemente un _subvegano_ (noi lo definiremmo un _subumano_) in piena _convulsione parossistica da demenza proteico-degenerativa_ causata dall'utilizzo smodato di proteine animali. Su _Vegan_, i _veganiani_ sono l'unico organismo animale presente; pare provengano dal pianeta _Vegetarian_, dove dopo lunghe discussioni sul tema: _ma il latte causa la cacarella o no?_, _le uova già pensano?_ e _meglio il tuorlo o la chiara per gli strangozzi?_, si attuò il _grande scisma_ con conseguente esodo verso _Vegan_.  
Per evitare ogni tentazione gli animali furono proibiti e si accettarono solo barboncini, chihuahua, gatti (siamesi, persiani, devon rex, sphynx e manx), gerbilli e criceti più qualche altro esemplare la cui taglia non eccedesse la _fendi_ presa come unità di misura. 
La civiltà _veganiana_ si presume sia rimasta a uno stadio _protometallurgico_ considerando il fatto che per la _tempratura_ in antichità si era soliti usare _orine_ notoriamente di origine animale, ma anche superando tale fase non sarebbero arrivati all'_acciaio_ dato che come affermato da _Vegan Outreach_: 

_However, if one accepts a process-based definition of vegan, then many other familiar products would also not be considered vegan. For instance, steel and vulcanized rubber are produced using animal fats_

La civiltà _veganiana_ ha comunque fatto passi da gigante nelle produzione di integratori alimentari ad alto _tasso di costo_, distribuiti in comodi _blister_ da dodici o boccette da duecentocinquantadue _capsule ingoiabili con facilità dovunque ci si trovi_.  

Il _veganiano_ è concettualmente _pragmatico_ e fa un po' quello che gli conviene come ancora dice _Vegan Outreach_:

_In general, we recommend that vegans concentrate their attention on the most obvious animal ingredients, instead of getting bogged down by reading lists for every possible animal-derived ingredient. Our experience has been that many vegans burn out because they are worn down by the details, missing the true meaning of veganism._

Come si direbbe qui tra gli _umani_: << non sta' a guardà 'r capello!'>>.

Tra serio e faceto
---

Non sono _vegano_, mangio pochissima carne, ma non potrei prescindere da ogni derivato del latte (a me la cacarella non la fa venire). Pur condividendo alcune idee che sono alla base del _veganismo_ mi mantengo alla larga da loro. Mi basta solo che _non mi rompano il cazzo_ (cosa successa e che talvolta succede) quando mi voglio mangiare una bella _rostinciana_ o qualche salsiccia alla brace.
L'uomo dal mio punto di vista è _onnivoro_ e non _erbivoro stretto_, ma sicuramente questo sistema di sfruttamento animale non solo non va bene ma non è più sostenibile; compreso quello vegetale (la soia naturale praticamente non esiste più). Come tutte le _sette_ o confessioni più o meno religiose quasi sono pronto ad ammirare chi vive la _regola_ in maniera perfetta o perlomeno ci prova seriamente, ma non chi _pragmaticamente_ quando gli serve si fa i cavoli suoi. Quindi quando un _vegano_ si fa le pippe mentali se l'_E920_ (L-Cisteina) è ammissibile o no come derivato animale negli alimenti lo posso pure capire, ma quello che mi dice: _they are worn down by the details, missing the true meaning of veganism_; mi chiedo:

<< ... ma allora qual'è il vero significato del veganismo? >>  

Servo vostro.


