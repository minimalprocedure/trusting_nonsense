[title: Buongiorno a me, a voi e alle definizioni]

Buongiorno a me, a voi e alle definizioni.

Odio le _definizioni_.

Non sopporto quando mi chiamano: _il tecnico_. Dio! Che odio!

Le definizioni etichettano la gente, capisco che in certe menti deboli il _ridurre il concetto_ sia indispensabile ma ciò non toglie che non lo sopporti.
Mi fa scattare quella fase da: _ti spezzo le gambe sotto il ginocchio, appena sotto il ginocchio_.

Ogni tanto mi chiamano anche _il programmatore_, che per talune persone è pure peggio, una sorta di _mago arcano_ in grado di domare la _macchina irrequieta_ meglio del _tecnico_.
Ancora però, _tecnico programmatore_ non mi ci hanno chiamato e per questo li ringrazio.

Quando mi chiedono che razza di lavoro possa io fare, mi limito a dire _analista programmatore_ giusto per tagliare corto. Non capiscono cosa sia, ma capiscono che c'entra la _magia_ e quindi mi rispondono sempre: _ah! Io non ci capisco niente!_. Tra me penso che sia meglio e mi limito a sorridere.

La gente ha bisogno di identificare, non riesce a comprendere la _poliedricità_: il _molto_ rispetto al _poco_.

Pare che essere _definiti_ faccia più _professionale_ e altrimenti sei solo una _schiappa_.

Bene... io sono una _schiappa_ (il che è già una definizione). Sono un _dilettante professionista_, uno che _improvvisa_ e la mia vita è sempre una _jam session_.

Il mio processo mentale è semplice e si riassume in: _tabula rasa_ e _reinvenzione della ruota_.

Quando mi cimento in qualcosa di nuovo parto da zero, dallo zero assoluto. Non voglio condizionamenti e il processo mentale è il _mio processo mentale_.

Ogni tanto mi accorgo di essere arrivato alle medesime conclusioni di altri e me ne rallegro, è per me una delle tante dimostrazioni che sono _intelligente_.

Già immagino qualcuno che a denti stretti dice: << Non sei intelligente, sei un montato! >>. Ebbene sì, anche quello.

Quando ho iniziato a fotografare, mi sono costruito delle macchine fotografiche partendo dalle camere stenopeiche. Ho fatto le pellicole e la carta fotosensibile, ho fatto liquidi di sviluppo e fissativi, viraggi. Ho una miriade di ricette cambiate, variate e tentativi andati bene o male.
Ho iniziato a suonare smontando e rimontando giocattoli elettronici che facevano semplici _bip_, sono passato ai synth e ho programmato synth analogici e digitali.
Ho dipinto, facendomi i colori e costruendomi le tele.
Ho scolpito infimi pezzi di legno con legno e pietre, prima.

Ho scritto, reimparando a scrivere e sperimentando _maniacalmente_ sul segno calligrafico.
La mia scrittura è _visiva_, vedo le parole davanti agli occhi che passano tutte insieme.

**Io vedo le cose.**

Quando mi chiedevano: << Lei è un fotografo? >>, rispondevo: << No >>.
Quando mi dicevano: << Lei è un artista. >>, rispondevo: << No >>.

Ogni tanto mi chiedono anche: << Ma sei un Master? >>, rispondo: << La risposta semplice o quella complicata? >>.
Sono più _complicato_ di così e al limite sono un _master_, con la emme minuscola. Come sono un _uomo_ con la emme minuscola.
La minuscola è la mia caratteristica perchè io _imparo_ sempre e continuamente. _Conosco_.

**La conoscenza è la mia vita.**

Io sono quello che scrivo e che qualcuno di voi legge, sono tutto quello e sono questo che sto scrivendo. Le mie parole sono vissute.
Mi hanno detto spesso: << Quello che scrivi è _vero_ >>. Quello che scrivo sembra vero a _Voi_ perché lo _é_, è _vissuto_ nel tempo e nello spazio.

So di quello che scrivo e conosco quello che scrivo, conosco profondamente _me_.

Conosco _me_ tra gli _altri_. [m]


