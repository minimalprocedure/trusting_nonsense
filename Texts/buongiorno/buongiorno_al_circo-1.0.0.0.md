[title: Buongiorno al circo degli anni '20 e alla punizione. ]

Buongiorno al circo degli anni '20 e alla punizione.

Letti questi indizi, non so perché mi è venuto subito in mente un film statunitense del 1932: _Freaks_ di _Tod Browning_. Chi non l'ha visto lo reperisca in qualche modo e se lo guardi.
Avevo, credo, diciotto anni quando l'ho visto per la prima volta a una rassegna di cinema prebellico statunitense.

Il film è interpretato da reali _uomini deformi_ (_freaks_) ed è una storia cruda di amore, di soprusi, di avvelenamenti e punizioni.

Il circo o _Circo_, con la lettera maiuscola. Cosa c'entra con la _punizione_.

_Punizióne_ s. f. [dal lat. punitio -onis, der. di punire «punire»].

_Circo_ s. m. [dal lat. circus «circonferenza, orbita; circo»].

Il _circo_ è _qualcosa_ di circolare in senso reale, un edificio o un luogo. In senso figurato: _il circo della vita_. Si intente comunemente un luogo dove si realizzano e presentano spettacoli di vario genere. Noi tutti (o voi tutti, a me il Circo non piace molto) amiamo il Circo, la perfezione degli acrobati e degli animali che corrono felici a raggio o diametro del _circolo_; del _Circo_.

Il _Circo_ però è in stretta relazione con la _punizione_. La punizione (_Pena, castigo inflitto a chi ha commesso una trasgressione o dimostrato cattiva condotta, allo scopo di correggerlo_) è alla base del circo, la punizione è alla base della _perfezione circense_.

Il principio del _bastone e la carota_.

Spesso si dimentica che la _punizione_ debba avere un principio di _correttezza e onestà_, il rapporto _colpa_ e _punizione_ deve essere chiaro, principio fondamentale.

L'_addestramento_ quello che segue come _lusinga e danno_ (_la carota e il bastone_) deve essere _coerente_. La _gratuità_ della punizione non è ammessa.

Come più volte ho detto alla domanda che ogni tanto mi fanno: << ma tu sei un Master >>. Rispondo che le risposte sono due: semplice e complessa.

Semplicemente non lo sono, _complessamente_ (che non si dice)... nemmeno, ma cosa significa? 

_Master_ ‹màastë› s. ingl. [ant. meister, maister, dal fr. ant. maistre (mod. maître), che è il lat. magister «maestro»] (pl. masters ‹màastë∫›), usato in ital. al masch. (e comunem. pronunciato ‹màster›).

_Magister_, maestro ma anche comandante, educatore o capo.

Oggi i _Master_ si sprecano. Sono tutti _educatori_ nell'arte del _bastone e la carota_. Via su, ammettetelo è facile proclamarsi _Educatori_, che ci vuole. Siamo bombardati di arnesi preconfezionati _made in china_ o _made in thailand_ o _made in qualche paese asiatico dove la gente lavora per pochi spiccioli o a bastonate_. Il _bastone e la carota_.

L'arnese che si usa è già carico di _punizione_ molto spesso. È il _mercato_, la civiltà che avanza.

Il _Master_ educa, a cosa? A servire, mi si dice. Il _Master_ educa la _slave_ (maiuscola e minuscola è significativa). La cosa interessante è che il percorso dovrebbe essere _finito_. L'educazione è _finita_, inizia e progredisce ma poi finisce. Si è _educati_ a un certo punto. Lo si è _per forza_ perché l'educazione con _il bastone e la carota_ piega la mente.

Credo che questa esplosione di _Master_ sia solo una conseguenza del cambio del ruolo della donna. La doppia natura oggi della donna, aggressiva e dominante ma comunque in qualche modo ancora attaccata alla vecchia cultura maschilista. L'uomo per conto suo, spodestato dal controllo _pubblico_ se ne riappropria nel _privato_.

Nel _privato_ l'uomo domina la donna sottomessa. Lo vediamo ovunque. Non entro nel discorso Mistress che sarebbe da vedere a parte e magari un'altra volta.

La _punizione_ quindi, per il _Master_ odierno non è mai _gratuita_ e la donna viene punita perché _donna_, semplice. Non gli importa se _sbaglia_ o no, si _punisce_ col _bastone_ e la si lusinga con l'_orgasmo_. L'_orgasmo_ le si concede, non è un suo diritto. Deve _venire a comando_.

È controllo, pensateci.

La donna vuole che il maschio abbia un orgasmo, non è completa se non succede. La femmina serve il piacere del maschio, ma senza entrare nel _bsdm_. Al maschio piace contenere la femmina. Lo fa in molti modi, sia che non si curi del suo orgasmo, sia che glielo ordini. È _dominanza_, è cercare di _possedere fisicamente_. Per il maschio la femmina è _cosa_.

L'interessante è che alla femmina spesso e volentieri sta bene. Lo si etichetta come _gioco_, ma molte volte _non è un gioco_.

La donna si _punisce_ a letto, perché il giorno si è permessa di avere iniziativa. Quante donne vorrebbero lo _zerbino_ di giorno e il _carnefice_ di notte? Su dai... ditelo.

La donna casta alla luce e puttana al buio. Ci veniamo allevati così, da quanto? Da millenni. Come possiamo in poco tempo liberarcene.

È troppo difficile o forse troppo comodo.

Per questo, sostengo che ci debba essere condivisione e scambio. Nel _pubblico_ e nel _privato_.

Cerco di educare mio figlio che esistono _persone_ e non _donne_ e _uomini_, _maschi_ e _femmine_. La forma fisica è un _incidente di percorso_ solamente. Sono le _persone_ l'importante.

Io _cucino_ e tu _cucini_, io _stiro_ e tu _stiri_, io _cucio_ e tu _cuci_. Io _lavoro_, tu _lavori_. Io _educo_ e tu _educhi_.
Io _ti scopo_ e _tu mi scopi_, io _ti lego_ e _tu mi leghi_, io _ti sodomizzo_ e tu _mi sodomizzi_. Io _ti limito_ e tu _mi limiti_.

Scambio, condivisione, errori e punizioni.

_Uomini_ cari miei... ma quando scavate nel culo delle vostre _donne_ il problema ve lo siete mai posto?
Fatevici scavare da Lei nel vostro di _culo_ ogni tanto, giusto per capire. No? [m] 


