[title: buongiorno al tempo]

Buongiorno al tempo, oggi grigio qui fuori nel campo degli olivi.  
Lui è arrivato, stamattina come appena alzato senza caffè, cornetto e pizzetta.  

Si dice che ci sono più tempi nei verbi, che forse oggi pioverà ma nonostante tutto sarà una delle città più calde. Si dice che non è più il _tempo delle mele_ e che è anche un po' tiranno.   
Dice il coniglio «È tardi, è tardi, è tardi!».  

Si va dal _è tempo che me la dai_ a _è tempo che me lo dai_, passando per _è tempo che me lo rendi_. Sono i libri e i dischi, quelli che non si sa come mai una volta prestati non tornano mai a casa. Ci deve essere una regola non scritta ma impiantata nel DNA primate: libri e dischi se ricevuti in prestito passano di mano.  

Il _tempo_ non si può fermare e infatti invecchio ma mi consolo: invecchiate pure voi e quindi siamo pari. Quando invece siamo in fila alle poste non passa mai e lì va guardato il lato positivo: alle poste non si invecchia, non si ringiovanisce certo ma si entra in stasi temporale. Una _iperguida iperveloce_ che ci permette di arrivare alla fatidica linea gialla in una sola mattinata.  

Se però mi permettete il tempo è anche un po' fascista, _tiranno_ diciamo. Comanda a bacchetta anche quando non siamo in camera da letto o nell'antro oscuro delle _pratiche sadomaso_. Di fronte a lui, siamo tutti a chiappe nude non c'è niente da fare: culi rossi in giro che nemmeno i _Theropithecus gelada_. Da _master_ universale con lo scudiscio di salice, schiaffeggia natiche a destra e a manca: _'ndo coglie coglie_.  

Al _tempo galantuomo_ non si può però dire niente. Aiuta le signore a scendere dalle carrozze, cammina un gradino avanti sulle scale ad afferrar tosto la fanciulla _inciampatrice_. Quello, pregno di buone maniere, è sempre politicamente corretto e un po' _radical chic_.  

Newton lo definì _assoluto_ insieme al suo amico di vecchia data: lo _spazio_. Nel _Sensorium Dei_ tanto per capirci al volo.  
Certo che però dalla scoperta delle _violazioni dell'invarianza_, tutto si stravolge: il signor tempo non è _simmetrico_. Ostia! Sarebbe?  
Pare che questo tempo birichino non scorra in tutte le direzioni in maniera uguale, ha delle preferenze. Insomma è più umano, più _cazzone_ anche lui.  
Pare scorra più veloce nell'avanti che nel dietro, oddio a dire il vero questo era già palese: si sa che per _affrontare il dietro_ ci vuole più _tempo_ che per l'_affrontare il davanti_.  
Questa storia dell'_invarianza_ però non colpisce mica solo il _tempo_ ma anche le direzioni: destra/sinistra. Forse è per quello che al mondo ci sono tanti _rincoglioniti_ di _destra_ (che se se si è di destra un po’ rincoglioniti lo si è è per forza è nella propria natura). Son però divagazioni sul tema e non mi soffermerò.  

Il _tempo_ è anche decisamente _mignotto_, pare sia di tutti: _il mio tempo_, il _tuo_. Per qualcuno è anche _prezioso_ e infatti lo incastona sulla fedina d'oro per la fidanzata. Può essere anche _tempo di scarica_ se lo applicate a una batteria ma potrebbe anche adattarsi alla diarrea da dissenteria amebica.  

Il _tempo di cottura_, il _tempo di marcia_, _chi ha tempo non aspetti tempo_, la _scadenza del tempo_ e il _tempo di scadenza_ che è scritto sul barattolo o in grigio chiaro sull'etichetta bianca nel punto tipografico 5.  
_Tempo massimo_ e sei fuori, _tempo minimo_ e ti tocca aspettare. _Tempo di eiaculazione_ che affligge alcuni se _veloce_ e altri se _lento_, _ma quanto tempo di ci vuole?_ quando cominci ad averne le palle piene e lei non viene.  

_Tempo morto_, Amen!  

A proposito di _tempo_ ma quando rimettono l'ora solare? [m]


