[title: buongiorno all'accoglienza e all'indifferenza, gioia, varie ed eventuali]  

Buongiorno all'accoglienza e all'indifferenza, gioia, varie ed eventuali.  

Cara AdG,
la ringrazio innanzi tutto per il tema datomi nel fuggevole messaggio nel giorno che è passato ormai.  
Lei mi dice accoglienza e indifferenza, che posso dire? Sono temi profondi e difficili.  

In questi tempi di flussi migratori dove le persone arrivano come sciami di rondini e, come loro in molte cadono in mare di fatica, quelli altrettanto cadono.   
L'uomo ha una grande forza interiore, è l'indifferenza. Quando il disagio diventa troppo grande, il problema insormontabile arriva e si stabilisce, si fa padrona del nostro pensare. L'indifferenza ci salva, come la paura dal pericolo immediato, dal disagio costante.  

Sapere di chi muore e sapere di bambini che muoiono; sapere di persone il cui sangue è rosso come in tutti noi, è disagio.  
Dal reale del corpo gonfio e pallido all'astratto dell'idea della morte. La morte in sé ci è indifferente, altrimenti non riusciremmo a vivere nella sua pressante idea.  
Dell'accogliere poi, c'è chi ne fa anche un alibi grottesco, un mettersi a posto con la coscienza da bravi cristiani e cattolici. Una ipocrita falsità che ci asfalta la via del paradiso.  
_Accogliere_, chi naviga e chi no; dal vicino di casa e quello accanto, quello o quella o quelli.  
_Accogliere un mucchio_ perché si ha il cuore grande o così piccolo e secco da dovercene per forza creare uno che gli altri possano vedere.  

In questo egoismo di maniera, noi _accogliamo il tutto che fa brodo_, ma senza interrogarci su quesgli _accolti_. Sulla loro esistenza e sui motivi non ci piace sapere e in fondo perché? Offuscherebbe forse il troppo _esser buoni_?

Ci da gioia questo?  
Ecco che si alza il coro dei sì.  
_Alleluja_! grida qualcuno dal fondo.  

Nella sfiducia nell'uomo che mi contraddistingue qual cosa potrei rispondere?  
 
L'uomo è questo animale che domina il nostro pianeta, nostro notate? Lo definiamo nostro a tutti gli effetti. Che animale presuntuoso che siamo. Presuntuoso e arrogante.  
È tutto nostro, l'aria e l'acqua e la terra. Il fuoco, lo spazio e l'universo. Quante chiacchiere, ne inondiamo il tutto di parole. Parole spesso vuote e inutili.  

Bivacchiamo su questo pianeta solo in attesa di morire e lasciare il passo ad altri che faranno altrettanto.  
Si nasce e si cresce, ci si riproduce e si alleva chi nasce. Si muore e nemmeno torniamo alla natura, questa madre indegna e indifferente. Ci inscatoliamo dentro zinco e legno. Mi dica, qual'è la differenza tra noi e un tonno? L'olio?  

Speriamo nella vita che ritorni prima o poi, da un dio ingrato che resuscita la carne o così per punto preso in un flusso metempsicosico. La vita dopo la morte, nella pragmaticità odierna di mettere il tutto nella cassa invece di fare tanti vasetti.  

Poi a pensarci, quei coglioni il cervello lo buttavano via dopo averlo tirato fuori dal naso. La cosa migliore che ci ritroviamo è la cosa più deperibile, più marcescente, più molle.  

Più inutile, spesso e volentieri.  

Gia mi vedo il lettore che dentro di sé e anche fuori si indigna, perché lui il cervello lo usa. Uomo o donna che sia è intelligente.  
Tutti lo sono e tutti lo credono, l'intelligenza è come quell'accoglienza e non si può non avere.  
Se qualcuno è idiota è diversamente intelligente, è 'politically correct' intelligente.  

Qui in questo luogo di perdizione poi, sono tutti intelligenti. Tutti nelle loro citazioni di cui non controllano la veridicità o la correttezza, almeno sintattica. Citazioni di autori in una lingua tradotti, mi si permetta, alla pene di veltro in un'altra. Autori nella propria lingua ma tradotti che fa più interessante.  

Citazioni, immagini, video, chiacchiere fugaci e senza senso.  
Dovrei io accogliere questa massa informe, mostrare indifferenza? Accoglierli nella indifferenza? 

Cari  
  compagni posteri!  
Rimestando  
          nella merda impietrita  
                              di oggi,   
scrutando le tenebre dei nostri giorni,   
voi,   
  forse,   
      domanderete anche di me.   
E forse affermerà   
                il vostro dotto,   
coprendo coll’erudizione   
                      lo sciame delle domande,   
che, pare, ci sia un certo   
                        cantore dell’acqua bollita,   
nemico inveterato dell’acqua naturale.   
Professore,   
          si tolga gli occhiali-biciclo!   
Io stesso racconterò   
                    del tempo   
                            e di me. [... Vladimir Majakovskij]  
                            

Mia Cara e miei cari,  
questa è la vita, brulicante e informe, affannata ricerca di un senso ottuso in una improbabile affermazione di sé.  
Nel 1980 scrivevo questo:

Molto spesso ci si chiede quale sia il vero valore della nostra vita o del nostro continuo affanno. Probabilmente consiste solo nel continuo movimento, una corsa sfrenata per rientrare in noi stessi, nei nostri corpi: una ossessiva auto penetrazione.  
  
Sodomiti di noi  
ci abbandoniamo attivi e passivi  
verso i sensi del demonio.  [~1980 m]


Credo a questo punto di essere rimasto bambino.  

Suo devoto Saltafile. [m]
                            
                            
