[title: buongiorno alla punizione]

Buongiorno! Buongiorno alla punizione.  
Urca! La punizione? Stamattina siamo contrariati?  

<< Metti a posto la camera! >>  
<< No! >>  
<< Niente telefono per un mese! >>  
<< Uffa! >>  

Le porte sbattono e i mugugni si sovrappongono.  

Mi chiedo, ma il rapporto _mettere la camera a posto_ e la _privazione del telefono_ in che relazione sono? Si dice che la pena debba essere proporzionata alla colpa, ma chi stabilisce che la colpa sia colpa e la pena sia proporzionata?  
Il _mettere a posto la camera_ è più proporzionata al _niente telefono per un mese_ o al _niente playstation per una settimana_? Se invece fosse il _studiati tutto il libro di storia in tre giorni_?. La pena guarda caso prevede sempre del tempo da dedicarglisi, quindi la vera punizione è il tempo che occorre per espiarla o la durezza in sé? Insomma pena _molto dura_ ma breve o _leggera_ e lunga?  

Cosa poi è colpa per uno e non per un altro? Se la barista mi portasse un caffè lungo potrei aver anche voglia di baciarla, ma per altri sarebbe onta da sculacciata col _gatto a nove code_. Certo, per la barista un mio bacio potrebbe pure essere una punizione e questo alimenta riflessioni ulteriori.  
La pena è punizione per chi, quando, come?  

Inoltre la pena è solo privazione? Non potrebbe essere un'aggiunta? Se la punizione fosse: portare quaranta chili sulle spalle in cima a una montagna; sarebbe _privazione_ o _dotazione_? La punizione ha un concetto di _privazione_ che sta alla base di se stessa, ovvero l'eliminazione del _libero arbitrio_ e della _libera autodeterminazione_; ma questo non succede anche in alcune religioni o regimi dittatoriali? C'è anche in molte democrazie moderne, ma li fanno finta di fare i tuoi interessi. Se così fosse, molti di noi sarebbero _puniti_ anche senza saperlo.

<< Sei punito! >>  
<< ... ma non ho fatto niente! >>  
<< Punito uguale! >>  
<< ... ma...  >>  
<< Tre giorni! >>  
<< ... io... >>  
<< Cinque giorni! >>  
<< Uffa! >>  
<< Una settimana! >>  

Brutti ricordi di militare, quando si passava il tempo a evitare il sergente semianalfabeta di diciassette anni.

Questa punizione al dunque, dovrebbe essere una cosa che per chi la subisce lo sia, una _punizione_. Cari miei, se sculacciate la vostra protetta di masochistiche tendenze, non la state punendo perché _a lei piace_ (già vedo le facce sorprese e quello che se la ride pensando: con le mie nerbate no. L'illuso...). Cari dominatori dominanti, una _punizione punitiva_ davvero forse sarebbe che la costringeste a sculacciare voi.  

Se invece le scudisciate proprio non le volete, potreste fargli leggere _Le 120 giornate di Sodoma_ in tre giorni, con annesso riassunto, analisi logico-grammaticale e comprensione del testo.  
Consiglio spassionato: con la punizione corporale la o lo farete solo contenta o contento, quindi passate alla punizione intellettuale. 

Certo, se ne siete in grado.  

Servo vostro e a tratti molto meno.

m.

