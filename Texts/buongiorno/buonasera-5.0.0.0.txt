[title: buonasera]

Care donzelle e cari donzelli, qui quo qua e tip e tap,
la sera avanza inesorabile e ho comprato quattro mozzarelle.

La cena quindi per oggi è preparata e domani è un altro giorno e si vedrà. Vi auguro sogni per i non desti e veglie per gli svegli ma ricordate che niente vale un buon lungo e agognato riposo.  
Per chi di fortun fortunato nel letto si infila accompagnato, auguro salti e balzelli al fronte e al retro e per gli altri solitari ma di man forniti, che si ingegnino senza tema che né si diventa ciechi e né si accorciano le gambe.

Chi irato dalle circostanze si quieti tosto che non ne val la pena e per nessuno lo passar la notte a digrignare i denti, quelli che poi nulla capiscono non si impegnino che tanto è tempo perso. Chi non ricorda non cerchi di ricordare e chi poi ricorda ma fa finta, continui pure nell'impegno.

Cari dolci piccoli compagni d'avventura, fuori non fa molto caldo qui da me anche se da voi non so e detto con il cuore in mano poco mi interessa.
Al divorar la mozzarella mi appresto al prima, immaginando fosse tetta turgida e sugosa e a morso affonderò nel microscopico capezzolo.

Al giorno che verrà se ci saremo ancora tutti, ma altrimenti che possa dipendere da voi.

Vostro affezionato e a tratti ingarbugliato m. [m]
