[title: buongiorno alle divagazioni sul tema]

Buongiorno alle divagazioni sul tema.

Oggi il cielo è variabile, stamani nuvoloso ora solicchio tiepido.
Mi sono fatto la barba, sono quindi rientrato nella civiltà. Tanto ricresce.

Divaghiamo adesso, visto che il tema del buongiorno è la divagazione sul tema. Qualcuno attento ma poco accorto potrebbe anche chiedersi: che tema? Il mio ovviamente.
In pieno egocentrismo da dementia senilis troppe volte mi straparlo addosso, quindi perché smettere.  
Adesso mi metto un po’ di musica, vediamo… È che alla fine ascolto sempre le stesse cose… boh!. Lancio Clementine.

__The Draughtsman’s Contract - Michael Nyman__

Il Mistero del giardino di Compton House, il primo film di Peter Greenaway che ho visto, dopo di che è diventato il mio regista preferito. Uno dopo l’altro dovrei aver visto tutto quello che ha prodotto.  
Questa musica, ha grandi ricordi in me. Ricordi di sesso anche, molti, in questo passato decadente che ho.
Amo il minimalismo oltre ogni ragione, la musica rinascimentale ma soprattutto quella barocca.

Il problema sono sempre le persone giuste, ma questa è un’altra storia.

Adoro la __latenza__, quello stato intermedio della sperimentazione, del movimento, del gioco. Il poco tempo che divide l’inizio dalla repentina fine.  

Il tocco dal sospiro.  
Il bacio dalla parola.  

L’estranianza di sé, la capacità di vedersi dal di fuori.   
Come un quadro posato o l’attimo a teatro. Guardarsi.  
Spettatore del disegno estatico e fermo. Immobile nel tempo.  

La mano posata, al ventre esposto.  
Visi che si guardano con gli occhi.  

Nelle bocche socchiuse al sussurro  
ogni cosa è erta e indurita.  

L’obelisco di pietra, conficcato  
avvolto dal senso degli universi.  

Vedersi dall’esterno. Artefici di se stessi e complici nell’Arte.  

Regina e Re, dal fango e dall’oro  
ogni cosa è preziosa e viva.  

Avvolti e in parti scoperte  
nei drappi rossi e pesanti.  

Nei polsi fermati da mano, fermati.  
Nei colli stretti, morte e vita.  

Tutta l’aria che manca, soffoca.  
L''aspiriamo con forza, nascendo ancora.  

Distruggiamo dio in ogni estasi  
polverizziamolo nel dolce orgasmo. [m]  




