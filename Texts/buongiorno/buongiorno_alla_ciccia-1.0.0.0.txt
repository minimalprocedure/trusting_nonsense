[title: buongiorno alla ciccia]

Buongiorno alla ciccia.

<< Alla ciccia? Sei sicuro? >>  
<< Che ti dico, così dicono... >>  
<< ... ma ciccia nel senso di ciccia... ciccia? >>  
<< Uhm! >>  

Buongiorno _ciccia_, che voi siate bistecca, capocollo, filetto o rollé. Dal taglio pregiato a quello scadente, frattaglia invece che zampetto.  
La _ciccia_ quella abbondante della chiappa o della maniglia dell'amore, quella sulla pancia che tremola al passo o quella tesa salata e pepata? Dalle mie parti quella si chiama _rigatino_, tagliata fine è bontà assoluta nel pane scuro di Montescudaio, magari con qualche fetta di cacio.  
Cacio e ciccia, paiono far parte di una cosa sola.  

Sarà la ciccia che fa la pancia morbida? La pancia dal muscolo introvabile nascosto bene sotto ben diversa dalla pancia da birra, dove la dilatazione gassosa non fa il soffice.

Mentre invece la _ciccia vegana_? Quella finta da: << Pare ciccia vera! >>. Non so come mai poi alla fine queste correnti di pensiero così _vegetabili_ abbiano bisogno di ingannarsi. La ciccia di soia, quella che la si deve prima bagnare come una spugna secca. No grazie, perché se _ciccia_ deve essere che _ciccia_ sia.

<< Mi fa due fette di carne di manzo? >>  
<< Va bene, solo due? >>  
<< No, guardi le faccia più alte >>  
<< Più erte? >>  
<< No, non vanno ancora bene. Più alte >>  
<< Più erte di così? >>  
<< Si. >> 

La lunga battaglia della _mi mamma_, appena trasferitasi nella bassa Umbria e ancora memore della vera _bistecca_. Anni di variazione macellaio in do minore, forse fa maggiore? In pieno delirio atonale e dodecafonico di insoddisfazione _ciccica_, un giorno gettò la spugna e passò al maiale. Andò meglio, mi direte? No, cari i miei piccoli lettori perché il legno di pioppo non è quello di quercia e non si può chiedere a chi non comprende appieno.  
Quindi ciccia! Niente ciccia.  

<< Ciccione! >>  
<< Cicciona! >>  
<< a'... secca! >>  

La _secca_ e la _ciccia_, coppia perfetta. A braccetto oppure a striscette stese al sole, da lunga masticata nel passeggio montano. Chiappa abbondante col giusto grasso nella rosetta alla romana che fa tanto pranzo al sacco o rosolata in porchetta da piazzola di sosta?  

Alla zuppetta di miso che fa tanto _yin_, volete mettere tutto quello _yang_ sugoso e sfrigolante da brace di faggio? Farà pure diventar nervosi, ma con una buona birra o qualche bicchier di vino tutto si riequilibrerà.

Quindi?  

<< Del filetto, alto per favore >>  
<< Così va bene? >>  
<< No, grazie comunque. Buongiorno. >>

[m]
