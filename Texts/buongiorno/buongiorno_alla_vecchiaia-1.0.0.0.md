[title: buongiorno alla vecchiaia e le donne]

Buongiorno.

Stamattina di ritorno in auto, un po' prima di arrivare al passaggio a livello, ho visto passare una madre e una figlia. L'occhio volpino è cascato prima sulla madre e poi distrattamente sulla figlia.  
Bella donna la madre, molto carina la figlia.  
Per carità, do per scontato che fossero madre e figlia, una certa somiglianza, l'incedere; quelle cose che ti fanno pensare che le persone siano parenti. Forse erano solo zia e nipote.  

Comunque, questo mi ha fatto pensare a come l'età condizioni l'apprezzamento estetico.  
Lasciando perdere le manie patologiche: il ragazzino di diciassette anni con la tardona di sessanta o viceversa; l'età ci porta ad apprezzare, forse, una estetica più incartapecorita.  

Quando ero giovane e bello... Oddio, meglio di ora diciamo, mi piacevano le ragazze della mia età, quelle più piccole non mi hanno mai attirato tanto. Crescendo ho cercato di mantenere la linea principale ma poi un giorno nel pieno dei miei venticinque anni, mi perdo per una diciassettenne.  
Allora, questa diciassettenne era proprio bella e soprattutto mi teneva testa. Mi ricordo il primo bacio che ci siamo dati, Firenze al Plegine. Era così bella che avevo paura di romperla.  
Uscendo da un rapporto di quelli _ci siamo capiti_, quasi non sapevo cosa fare.  

Comunque, torniamo al tema e non divaghiamo.  

Penso che con l'età che avanza, non si sia più disposti a perdere troppo tempo. Non dico che: - Scopiamo? -. No, solo che è finita l'era dei mezzi si e dei mezzi no, delle finzioni varie. Almeno dovrebbe.  
Dovremmo essere nell'età della consapevolezza e di quello che siamo o non siamo disposti a fare, condito da quella solita _vita complicata_ di rapporti, possibili figli o coniugi, parenti serpenti e cose che a una certa età non si dovrebbero fare più.  
A me affascina delle persone il pregresso, non perché voglia sapere il loro passato, quanto perché le ha portate a essere quelle che sono oggi. Forse le penso in riferimento a me, al mio passato e penso così a come l'intimo possa essere sfaccettato e più o meno complesso.  
Il mio vivere da solo dai favolosi anni '80, mi ha portato a numerose esperienze in parecchi campi. Il non avere il _parental control_ ma solo la _parental pecunia_, mi ha permesso di vagare nell'aria portato da un filo di vento.  

In questo oggi di non ripianti per delle cose che ho fatto o potuto, non ho bisogno della ragazzina di ventanni. È maturità questa? Solo fortuna?  
Guardo le Donne e immagino il loro passato, le immagino nei momenti e nelle cedevolezze. Le tristezze o le gioie della vita passata. Sguardi pensosi, compresi nel lavoro o nei figli o nei mariti.  

Insomma oggi, sotto i trenta e anche trentacinque c'è _poca trippa per gatte_, magari tra un po' quando sarò completamente rincoglionito e mi ricomprerò una motocicletta, uno sguardo approfondito alle ventenni lo darò. [m]
