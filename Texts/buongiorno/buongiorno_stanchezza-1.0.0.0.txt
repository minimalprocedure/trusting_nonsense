[title: buongiorno alla stanchezza]

Buongiorno alla _stanchezza_.

È quella cosa che a una certa età, ti fa guardare indietro perché l'avanti sembra non possibile. Il _fotogramma chiave_ del film che continuamente ti scorre davanti. Inesorabile, lungo, complesso e tristemente drammatico da regista russo post-rivoluzione. 	
Non c'è _tristezza_ in questo, non ce n'è più e magari ci fosse. Sarebbe probabilmente una via d'uscita, comunque una porta almeno socchiusa verso una strada possibile.  
La _stanchezza_ anche dei ricordi, che sono sempre gli stessi, per quanto li si voglia modificare o dimenticare. Tutto porta sempre a quel gelido pensare al passato.  
Così mi torna in mente quando, di primizia, ho tentato bruscamente di _dimenticare di pensare_. La brutale realtà del _fallimento_ che da spinta _vitale_ si è tramutata nella prima _stanchezza_. La certezza di questo arrivata con un padre che piangeva e mai visto prima, in quei giorni passati come in una delle peggiori pellicole di fantascienza.  
Poi però tutto si accantona nella frenesia di porvi rimedio.  
Tutto vortica congelando quelle immagini che avanzano e si susseguono, di amore e di violenze, di costrizioni e ricerche di libertà negate. Si cerca di muovere velocemente cercando la prossima immagine fissa, da catalogare e riporre a futura memoria.  
La _stanchezza_ non si evita, non si può, perché lei è viva come noi. Più viva di noi. Improvvisamente ti saluta come quella faccia che non vedevi da tempo, il compagno di scuola con cui _non ti potevi vedere_ ma che quasi quasi ti fa piacere reincontrare. Allora lo saluti con un _come va_, gli stringi la mano, gli offri un caffè. Lui ti dice _vediamoci_, tu gli dici di _sì_.  
Ti diranno poi che hai avuto fortuna e ti diranno che l'amica che ti ha trovato passava per caso, vicino al bar di quel caffè.  
Ti rendi anche conto che non c'è _fuga_, non esiste _antidoto_ ma solo _anestetico_ a quel veleno. Il pozzo va riempito di sabbia, l'acqua asciugata, la fonte arsa. Dimenticare, dimenticarsi nella nullità dell'atto  che sia lungo minuti od ore interminabili. Attese _forzate_ che interrotte da schiocchi fanno così piangere di dolore. Lo provochi, lo accogli, lo supplichi e lo elargisci. Come il più veloce degli uragani, il più devastante, poi ti fai paura e allo specchio l'immagine fluttua, ha i contorni sfumati, tremula sparisce e riappare e ecco che ancora ti saluta.  
Come si fa a non incontrare più chi non vuoi vedere, come, ci si chiede.  
L'abisso guardato in viso, è baciato, abusato e scopato nei suoi mille buchi. Non dimenticare l'impossibile ma ignorare, è la nuova _strategia_. 

Perdi. 
 
Perdi. Perdi. Perdi.  

Ci si accorge di _perdere_ comunque, di _perdere_ nell'abisso e di _perdere_ nella luce. Di parlare e _perdere_, di spiegare e _perdere_, di proteggere e _perdere_.  
È la _stanchezza_ dunque eterna compagna, l'unica che ci si siede vicino. Il vecchio _compagno di scuola_ non visto da tempo e puntuale nel capolino.  

Buongiorno _stanchezza_, buongiorno a te, che _rallegri_ l'atmosfera.  
Che bevi il caffè amaro in quel vecchio bar.  

Buongiorno. Sai, oggi tira vento.                             
