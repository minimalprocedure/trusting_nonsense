[title: buongiorno alla gente che batte le mani]

Buongiorno.

Fuori dalla finestra il sole è debole, nel campo degli olivi c'è una tenera brezza.
Le gazze fanno la posta al gatto che passa.
Il tè verde, accuratamente preparato nella giusta temperatura senza ossidazione, è nella tazza qui accanto.
Ascolto musica, i suoni di un tempo, quello in cui ero molto più giovane. Quando vestito di sbuffi e trine, in bianco e nero, vagavo per il mondo.

Oggi è un giorno, come tutti quelli che vengono, nelle solite cose da fare e da dover fare.

Buongiorno gente, gente esposta e gente nascosta, gente vera e gente falsa. Tutti con i loro atteggiamenti, inseriti dentro un sistema a compartimenti dove si può parlare solo attraverso fili e onde, vedersi dentro schermi di plasma o cristalli.

Il nostro mondo liquido che sciaborda dentro bottiglie di plastica indistruttibile. [m]
