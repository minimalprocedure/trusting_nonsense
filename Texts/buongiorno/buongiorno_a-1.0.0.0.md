[title: buongiorno al come viene]

Buongiorno al come viene, errori compresi.  
Errori ortografici e non, quelli fatti e quelli pensati o quelli involontari.  

Il _come viene_ invece è quando non si hanno idee precise sul cosa dire, scrivere e fare. Il vuoto mentale che nemmeno la _macchinetta vista in tv_ a soli trentanove euro e novantanove centesimi potrebbe arrivare: il _super vuoto spinto_.  
Questa misera posizione mentale ogni tanto mi attanaglia, ebbene sì; parrebbe che passi il tempo a vomitar parole e invece no.  

<< Si o no, quindi? >>  
<< Forse, dice lei? >>  
<< Sarà... >>  

Potrei partire da lontano, quando ancora non mi facevano sentire Mozart ma sguazzavo beato nel liquido paglierino dove senza tema ogni escremento eiettavo.  
Però meglio cominciare da più vicino, non credete? In fondo a voi che vi _frega_ di cosa aveva nella pancia _la mi' mamma_?  
Dall'età della ragione, senza indagare se mai ci possa essere arrivato, ho sviluppato delle caratteristiche che chiameremo _superpoteri_:  

+ la _stronzaggine_
+ la _freddezza_
+ il _distacco_

A questi per completezza d'informazione vanno aggiunti dei difetti compensativi:  

+ la _disponibilità_
+ il _non voler niente in cambio_
+ la _gentilezza_

Ci sarebbero poi alcune attitudini attribuitemi come: la _orsitudine_, la _pigrizia_, la _stronzaggine plus_ (anche se questa è più un attributo modificatore che una attitudine), l'_intolleranza_ e così via.  

_Occiòddunque_ dicevo, dall'età della ragione con le dovute cautele di cui sopra, ho materializzato questa _poliedrica_ personalità che non mi fa essere con tutti uguale. Sono perciò _stronzo_ con gli _stronzi_, _gentile_ coi _gentili_ (ma anche con gli _stronzi_ talvolta), in piena _schizofrenia_ comportamentale mi adatto alla situazione. Difetto o pregio? _Chi sa chi lo sa?_ (citazione di programma televisivo che chi ha la mia età potrebbe ricordare).  

Oltre ai _superpoteri_ e alle _attitudini_ ho anche delle _specializzazioni_ nel:  

+ non volere _rotture di palle_
+ non ammettere _ingerenze esterne nella mia vita_
+ non sopportare _chi pretende di conoscermi_
+ non tollerare _chi mi incalza_
+ odiare profondamente _chi cerca di farmi fare cose che non voglio_

Alla luce di questi _superpoteri_, _attitudini_ e _specializzazioni_ penso sia chiaro di quanto possa essere ingestibile.  

Dunque cari _sociali_ del borgo, il mio _caldo consiglio_ è: _chi tocca muore_.  
Anche se so che _è buona norma e regola_ non dare consigli questo lo lascio un po' _buttato là_ a memento di chi _mezzo avvisato_ è già mezzo morto.  

<< Sei solo un _cazzone_! >>  
<< Hai ragione. >>  

Riguardo alla mia vita privata, ebbene sì c'è, direi che vada lasciata abbondantemente fuori da ogni speculazione demenziale da bieco sobborgo metropolitano.  

<< ... e le donne? >>  
<< Ti ringrazio per aver toccato l'argomento... >>  

Confesso senza coprirmi il capo di cenere che sono affascinato dalla _femmina_, che volete sarà perché sono _maschio_? Non lo so e nemmeno lo voglio sapere, ma la _donna_ mi piace. Oltretutto mi piace _bella_, certo mi direte _a chi piacciono brutte_? _Non ne sso e non ne vvoglio sappere!_ (citazione isolana).  
Care donne sopporto la _chiappa cellulitica_ ma il viso deve essere _bello_, che poi cosa significhi _bello_ non lo so di sicuro.  

Il discorso è che se il viso non mi piace _non ce la posso fa_ ed è più forte di me.  

<< Tu ci provi con tutte! Bastardo! >>  
<< No, lei erra. Provoco tutti semmai. >>

Ringrazio l'ultima della fila per darmi l'occasione di precisare il _provoco_ non _ci provo_. Il concetto è molto diverso.  
Adoro scoprire l'intimo delle persone e cosa migliore che _provocarle_ a tirarlo fuori? La gente _vera_ o il _vero_ nella gente è quello che mi interessa. Certo a volte quel _vero_ non è piacevole ma il _rischio vale sempre la candela_. Quasi sempre.  
La cosa _buffa_ è che alcune pur _mettendo le mani avanti_ si _appolpano_, mi dispiace ma il _tentacolo appiccicoso_ tendenzialmente lo taglio.  

<< Sei uno stronzo! >>  
<< È uno dei miei _superpoteri_. >>  

Dimenticavo, dato che con le _donne_ mi piace parlarci, gradirei almeno un livello intellettuale appena superiore alla media.  

Che mi sia dimenticato di citare un altro _superpotere_?

+ non sono _umile_

<< Si e basta? Il fatto che sei un _egocentrico bastardo_? >>  
<< Quello non è un superpotere, ma un'_attitudine_. >>  

Servo vostro, nel bene e nel male e in tristezza e allegria.

m.


