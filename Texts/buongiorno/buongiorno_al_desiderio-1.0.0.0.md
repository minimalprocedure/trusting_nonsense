[title: buongiorno al desiderio]

Buongiorno al desiderio,  

a quell'ambizione di possesso assoluta che fa pensare in termini contraddittori.    
Il _ti desidero_ che equivale al non procrastinabile _ti voglio_ o all'ordine: _Vieni qui!_.   

_Ti desidero adesso_, indifesa, per dirti di cadere in ginocchio; di alzare e poi abbassare lo sguardo e di porgere le spalle e il collo.    
_Ti desidero adesso_, per mostrarmi il seno, accucciata sui piedi e con le mani dietro.    
_Ti desidero adesso_, per allargarti le gambe a contenere la mano.    
_Ti desidero adesso_, per imparare i tuoi anfratti e le curve e per studiare e poi capire quei tempi e quei modi.    
_Ti desidero adesso_, per accedere ai tuoi pensieri tutti, quelli dei gemiti e dei sospiri e al rumore, dei movimenti e dei tocchi.  
_Ti desidero adesso_, per guardare la tua bocca aperta e i tuoi occhi velati.  

**Ti desidero** perché **ti voglio** e **ti ordino** di _essere mia_... **Se lo desideri**. [m] 


