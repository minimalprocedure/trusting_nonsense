[title: buongiorno alla rinascita e a quello che viene]

Buongiorno alla rinascita e a quello che viene.

Siamo nella parte dell'anno in cui la vita muore, la vita conosciuta e vissuta. Il giorno che muore presto nella notte fredda, il senso di solitudine che avvolge in spire strette.
Tutto muore in questi giorni mentre le foglie si staccano e cadono.

La terra si sopisce, come inutile alla vita nel sonno del riposo. I colori che cadono portati dal vento mentre li guardo girando intorno.
Sulla montagna alta dei prati secchi, dell'erba strinata di freddo. Il vento, freddo alla nuca scoperta e avvolta, dai capelli legati.
Cammino verso l'alto oltre le vette, i valichi di neve. Cammino senza piedi freddi orma dopo orma disciolta nella neve.

L'aria che taglia e spinge, che parla e racconta e dice che non devo andare oltre la vetta, mi dice di ignorare l'ignoto, mi dice suadente che non ho speranze.
Un piede all'altro salgo al sole che sorge, scrollando chi tira verso il basso. 
Che tirano e si oppongono, spingono e intralciano.

Voci insistenti, quelle della ragion veduta e del buon fare. Voci di chi si oppone e crede di sapere. Scienziati dell'inesistenza nei loro loschi esperimenti della prova e della riprova.

Il sentiero perso, sepolto e nascosto del salire in diagonale. Verso destra poi verso sinistra, le ginocchia che spingono a terra lo scarpone pesante che vogliono farti trascinare. L'immensa fatica di opporsi a un mondo contro.
Salgo senza via, verso il sole che nasce oltre le vette.

Salgo per sedermi in cima ad ammirare l'infinito.
Salgo per soffermarmi in un pensiero, materializzarlo sulla mano aperta e guardarlo crescere, formarsi e diventare carne.
 
Poggiarlo a terra accanto e con un sorriso prenderlo per mano.

Buongiorno a tutti quelli che vedo scarpinare in salita. [m]

