[title: Pompe Funebri e buongiorno]

Oggi di ritorno dal viaggio mattutino ho visto di sfuggita un cartello pubblicitario:  

**La Pace - Pompe Funebri**  

Guidando negli ultimi chilometri ho riflettuto: ma come mai le pompe funebri hanno sempre dei nomi così?.  

Dovendo aprire una agenzia funebre, cercherei un nome migliore.  

**La Salma**, per esempio, non sarebbe rassicurante? Oppure **La Fornace** se mi dovessi occupare di cremazione.  
Credo che i soliti nomi ricadano nel _politicamente corretto_, nel: _era tanto un bravo ragazzo_; salvo poi sapere benissimo che si _inquartava le ragazzine_.  

Anche **Il Trapasso** non sarebbe male, esprime un passaggio o un cammino; l'aldilà che si raggiunge dall'aldiqua. Nell'agenzia **Il Trapasso** potrebbero esserci tre dipartimenti: _Inferno_, _Purgatorio_ e _Paradiso_; rispettivamente con i loro numeri di telefono e segretaria _in tinta_.
È che quando uno muore diventa automaticamente _bravo_, questa indulgenza verso la morte che abbiamo come fosse per tenerla buona. Parliamone bene altrimenti poi, magari, forse ci viene a cercare. La Morte, quella vestita di nero con la _frullana_ (un toscano lo sa cosa è, gli altri non so).  

Anche **L'Antro** lo vedrei bene che poi si presterebbe a molteplici interpretazioni, **L'Antro** del demonio, **L'antro** delle streghe, **L'Antro** nel senso romanesco di **L'Altro**. Il _fuori da sé_; l'anima, quella che fugge appena le cose si mettono al brutto. Brutta evanescente traditrice, appena se la vede brutta si dà e ci lascia alla mercé del primo falciatore vestito di nero che passa.  

Insomma, Ghost lo avete visto tutti no? Io ho rimosso il numero delle volte; avevo una donna a cui piaceva Quello, proprio Lui, il monoespressivo Patrick... Poi, Ghost era un film tanto romantico, _smosciapallamente romantico_.  
 
**Il Cadavere**, renderebbe l'idea ma anzi perché non: **Il Cadavere Danzante**? (La Sposa Cadavere non si può usare è un marchio protetto, comunque restringerebbe troppo il campo d'azione).  

Poi non potrebbero esserci le agenzie tematiche? **L'Ammazzato**, per chi è morto da poco o **Il Bigattino** per chi è morto da molto, **La Mummia** per le suocere.   

Se poi uno riflette... Le Pompe Funebri... Ha un non so che di necrofilia a cui non voglio nemmeno pensare, perché ho già visioni apocalittiche nella mente. Pensate... e se al dunque, quel dunque... cosa esce? Orrore! Basta, basta... pensiamo alle margherite.  

Torniamo ai nomi... Suggerimenti? [m]

ps: Buongiorno.
