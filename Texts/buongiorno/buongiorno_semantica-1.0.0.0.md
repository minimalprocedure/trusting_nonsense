[title: Buongiorno alla semantica del letto disfatto e a quella delle palettate]

Buongiorno.

Stamattina mi ha toccato la _parola_, il significato della _parola_.  
_Semantica_, ovvero lo studio del _significato_ delle parole e di queste nei testi.  
In questi giorni oscuri, di demenziale intellettualità sociale, è al pari del congiuntivo e condizionale una battaglia persa. Dimenticavo scusate, sta sparendo anche il futuro e ormai si vive solo nel presente. Una volta il futuro era speranza, si sa _era l'ultima a morire_, visto che di speranza non ce n'è più per nessuno: _carpe diem_ e a quel paese il _futuro_.

Qui la _semantica_ scarseggia, l'uso e la comprensione del significato delle parole manca, si sparano _minchiate_ più o meno a caso.  
Non capisco se _ci si fa_ o _ci si è_.  

Presi nella _frase a effetto_, tendiamo a dimenticarci che cosa possa realmente significare. Infatti ogni tanto resto interdetto e indugio sul _ma che vordì?_. Sia chiaro non sono un _campione di chiarezza_ a me gli _ermetici_ mi fanno un baffo, anzi _una pippa_.  
Almeno io sono _dichiaratamente evocativo_ e ogni tanto forzo la _semantica_ in una _semantica olistica_, come si potrebbe chiamare.  

Il significato della parola e della frase, se solo scritta; insieme alla gestualità e materialità di sé e dell'altro da sé, se parlata; forma un _aere_ spesso a volte pieno da far respirare e altre vuoto da soffocare. Questo manca nelle frasi _alla pene di veltro_, facili e scontate senza significato ma tanto adatte e veloci da _social network_.

Pensate alla differenza tra _concedere_ e _ordinare_, perché se do un ordine devo essere ringraziato? L'ordine non ammette repliche e non prevede ringraziamenti, mi ringraziano di cosa? Il _concedere_ invece lo prevede, quando permetto di fare qualcosa allora il _significato_, la sua _semantica_, prevede il _grazie_. Essere ringraziati per un ordine dato, dovrebbe suscitare fastidio, me lo suscita, visto che non c'è senso in ciò.  
La _concessione_ inoltre implica una volontarietà, si può fare o non fare. Non è obbligatoria, ma semmai suscitata dal desiderio di poter fare poi altro.  

Il _posso?_. _Puoi_. _Grazie_ (con qualche altro orpello semantico).   
il _fai!_. È diverso. 

Non sarà che stia sparendo insieme al congiuntivo, il condizionale e il futuro anche l'imperativo? Sarebbe una contraddizione no? Se l'imperativo diventasse interpretabile che senso avrebbe più? Niente più ordini e solo possibilità, lo sgretolamento di _certe pratiche_.  
Mi pare che qui si vada parecchio a capre e cavoli e insomma se ti do una palettata sulle chiappe tu non mi ringrazi a meno che non ti ordini anche quello ma in assenza la prendi e mi mandi anche a _fanculo_ tra te e te.
Certo se ti _concedo_ di prenderti le palettate allora puoi ringraziarmi a profusione.

Ops! Dimenticavo il disfacimento del letto la mattina.  
Quello lo si fa in privato e mica si racconta qui giusto per farsi belli col prossimo tuo e sia santificato il signore... Che poi, mi chiedo, capirebbero la _semantica_ o si prosciugherebbero a colpi di sega?

Servo vostro e per qualcuno un po' di più. [m]
