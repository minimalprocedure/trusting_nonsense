[title: sesso e fotografia]

Il sesso è come la fotografia.

Ho fatto molte fotografie, amo la fotografia.

Fotografare è un processo lento, riflessivo e non si ferma allo scatto.
Nasce dalla analità di un foro stenopeico, una scatola nera all'interno, un piccolo foro circolare che si rilassa alla luce per farla penetrare al suo interno.
La fotografia è ricerca, è bondage fatto in casa, andare dal ferramenta a cercare la corda giusta, la giusta catena dal giusto peso. Imbrigliare la luce che colpisce un corpo, lo accarezza, lo tiene immobile.

Un pezzo, uno scorcio di carne, un seno mezzo sfumato, spostare luci auto costruite per plasmarlo, idolatrarlo. La fotografia è accarezzare.

Scegliere la giusta pellicola, l'obiettivo per filtrare bellezze intuite.

La macchina fotografica, meccanica applicata al senso della bellezza, quanti segreti passati lì dentro.

La camera oscura, è puro godimento, le ore passate in una orgia di sensualità, la scelta della carta, l'apertura della confezione, la posa sull'ingranditore, come spogliare la più bella donna mai vista, un bottone dopo l'altro, scostare fino a rivelare un seno pieno, sfiorando un capezzolo subito turgido. Inondarla di luce ancora nascosta.

Lo sviluppo, con tutti i liquidi, lubrificanti del senso dell'immagine. 

Primo, prende forma, cresce, la senti pulsare, le passi le dita per scaldare leggermente quel punto, per tirare. Inclini un po' qua e un po' la, lei appare e sta per venire, la vedi, la senti...

Secondo, la fermi, la fermi nel momento giusto, che si rilassi, un poco, aspettando il resto.

Terzo, la senti esplodere, piena nei contorni e fissa, immortale, bellissima.

La doccia insieme, la lavi dolcemente, per sciacquare i resti dell'amplesso, la guardi ed è proprio bella.

Non la dimenticherai mai.

Odio abbastanza la fotografia digitale, un po' mi leva il meglio, è come guardare e non toccare e masturbarsi dal buco della serratura.

Oggi tutti fotografano, in questa masturbazione collettiva d'immagine.
Poi le filtrano e grazie a photoshop, si scopa più o meno tutti uguale. [m]
