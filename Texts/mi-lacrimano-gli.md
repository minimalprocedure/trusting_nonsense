[title: Mi lacrimano gli occhi.]

Se non è così importante  
scrivile, vomitale su un foglio, poi lo bruci.  
 
Il viso, le mani, il corpo  
e ti senti.  
 
Della morte che non arriva mai  
se riuscirai ad aspettare, io tornerò. [m:engine]  
