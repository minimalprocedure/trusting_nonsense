[title: l'intrigo]

La sfida è una grossa molla d'intesa, il grilletto che spara l'interesse. La provocazione nel farla e od oppure subirla è il fulcro dell'incontro. Provoco continuamente e sono affascinato quanto lo fanno con me.  
Non è questione del tener testa o no, lo si può fare oppure lasciar trasparire un pizzico di passività. L'intrigo è nel girarsi intorno quasi senza toccarsi ma senza perdere gli occhi dell'altro.

Posso sentire quel sesso da lontano   
averne le mani colme e tracimanti.  

Posso avverire le frasi dette piano  
nel sudore della guancia appoggiata.  

Lo svuotarsi del petto prima pieno  
dell'aria che fugge di colpo fuori. [m]
