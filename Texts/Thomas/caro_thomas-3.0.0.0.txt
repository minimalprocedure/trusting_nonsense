[title: caro Thomas]

Caro Thomas, che si dice?

Qui oggi il sole splende nel campo degli olivi, la finestra è aperta. L'aria fresca entra e una musica mi accompagna.  

Che posso dirti di nuovo?  

Cammino come sospeso con la testa leggera. Ho un peso sul petto come di una mano che spinge, spinge per spezzarmi le costole.
Confesso, sono al limite di un pianto liberatorio. Quello che l'uomo vero non dovrebbe fare mai, ma in fondo non lo sono e quindi di che mi preoccupo?
L'esterno intorno oggi è estraneo e fuori posto, questa è la percezione.

Sono così. Facile alla depressione, che involontariamente a volte tendo ad accentuare, spingere sempre più a fondo. Come se mi servisse immergermi fino a che l'aria non mi manchi. Sai quante volte ho cercato di arrivare alla sincope durante le immersioni? Sai quante volte sono rimasto quasi volontariamente incastrato giù in fondo per poi correre verso la superficie per l'indispensabile boccata d'aria? 

Perché questa continua prova? Cosa spinge a volersi distruggere?  

Il disseminare di trappole nei percorsi conosciuti facendo finta di dimenticarle. La beffa dell'inciamparci sopra, la tagliola che si chiude alla caviglia. Il dolore che prima breve poi si accomoda e non ti lascia.

Quel peso che preme, spinge e strizza lacrime dagli occhi.  

Sai a volte ci gioco, come in un orgasmo negato, a ricacciarle tra le palpebre. Farle spuntare e poi ritrarre caricando quel pianto a dirotto che tante volte desidero. Affogare nelle lacrime, il mare salato dei dolori repressi, delle speranze abbandonate e importanti ricordi. 

Quando rimango con me l'ambiente torna estraneo. Lo stesso di sempre ma che non da solo obbligo alla consuetudine, alla presenza. Cammino al margine guardando me stesso camminare, me stesso che muove, si alza, si siede, mangia e dorme, si sveglia. Parla senza suoni muovendo solo la bocca.  
Questo silenzio triste eppure piacevole, il compagno delle dormite senza sogni che ti abbraccia tanto stretto.

Questo è quanto mio Thomas e sono cose che già conosci.

Scusami davvero per il tedio.

Tuo m.



