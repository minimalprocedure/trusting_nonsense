[title: Chi è Thomas]

Chi è Thomas.  

_August Müller_, protagonista de _Lo scrutatore d’anime_ di _Georg Groddeck_, si trasforma a causa di una serie di accadimenti in _Thomas Weltlein._
_Thomas Weltlein_ è in una costante ricerca del _godimento assoluto_ e della _liberazione totale_ da ogni convenzione e costrizione sociale. È in qualche modo la personificazione dell'Es di _August._  

Preso a mio prestito, _Thomas_ diventa in queste brevi _scritti_ un interlocutore non giudicante proprio perché al di fuori di ogni convenzione. Un _Es_ che racconta all'altro.  [m] 
