[title: Caro Thomas]

Caro Thomas,

da quanto non ci si sente, da molto che tanto non ricordo.
Sono passati i giorni, non mi bastano le dita per contarli,
quasi è vacuo ormai quel tuo viso intravisto e mai incontrato.

Mi chiedo come stai, se quel tuo vagare irrequieto e calmo
ma solo per gli altri, ti abbia abbandonato almeno un po'.

Qui sai, è tutto più o meno uguale, la vita scorre piano
con gli occhi che calano di vista invecchiando sempre.
Questo continuo andare avanti senza mai poter tornare indietro
nella strana maledizione e gioia della vita umana.

Si cammina sempre e la polvere della strada accidentata
c'entra nel naso e nelle orecchie, ci chiude gli occhi.
Così da permetterci di morire senza sentir niente.

Con affetto. [m] 
