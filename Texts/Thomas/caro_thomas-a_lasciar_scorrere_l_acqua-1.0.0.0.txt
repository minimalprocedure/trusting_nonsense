[title: a lasciar scorrere l'acqua]

Caro Thomas,  

oggi ho deciso di ribaltare il cervello, rivoltarlo come un calzino sporco per lavarlo a rovescio.
Lo metterò in lavatrice al programma per sintetici, trenta gradi.  
Centrifuga leggera, per non turbarlo troppo.  

Per il cuore serve l'impostazione lana, anche se ormai troppo infeltrito.
Che cosa buffa, il cuore infeltrito, tiene più caldo. Le fibre strette le une alle altre, non passano spifferi, il caldo rimane dentro e non esce facilmente.

Tutto è quasi impermeabile, repellente all'acqua.  

L'acqua che tutto dilava.  

Vorrei lasciarmi nudo a braccia aperte disteso nel fiume gelato, lasciar scorrere l'acqua.
Portare a valle molte delle cose, lasciando solo il profondo, rintanarmi in un Es da guardare con occhi velati.

Nello specchio delle meraviglie, ho visto cose meravigliose e ho appoggiato la mano ed essa entrava, fluendo nel vetro come vagina umida.  
Sono passato al di là, nel mondo perfetto dove è tutto come vuoi.  
Nella paura di tutto ciò che va bene, ho preferito l'incertezza del nulla.  

Mi lascio andare nudo a braccia aperte e disteso nel fiume gelato, a lasciar scorrere l'acqua. [m]
