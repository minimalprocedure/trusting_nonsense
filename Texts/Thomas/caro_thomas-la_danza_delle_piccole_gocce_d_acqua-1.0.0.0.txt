[title: la danza delle piccole gocce d'acqua]

Sai Thomas,  

forse è l'autunno che viene, ricorrente come ogni anno. Saranno le prime piogge, quelle tristi non dell'estate.  
Il sentore latente, delle foglie che muoiono, la vita che si assopisce.  
Guardo fuori dalla finestra e quella nebbia poco lontano dal campo d'olivi, mi assorbe lo sguardo.
La danza delle piccola gocce d'acqua sul filo dei panni che si unisce alla musica un po' nostalgica che sento, non riesce a portarmi lontano dove vorrei essere.  
Mi hanno invitato al banchetto dei fantasmi, gli spettri dei visi del passato quelli morti e quelli vivi. Il passato Thomas, come liberarci del passato e poter continuare a vivere?  
Potrei scendere di sotto e stringere la testa nella morsa, col trapano arieggiare il cervello, ma sono cose che non servirebbero a niente; il passato è abbarbicato tra le pieghe craniche, agganciato alla scatola e inamovibile.  

Ho una terribile voglia di parlare, ma coi silenzi pregressi la lingua è rattrappita e il cervello lento. Non è più quando riuscivo a scrivere e parlare al tempo della musica. Una poesia al tempo di una canzone ti ricordi?  

Si invecchia Thomas, il peso degli anni che non si vede sul corpo, spreme la mente degli ultimi succhi di saggezza.  

Per me che sono nato vecchio, il futuro non potrà essere che antico. [m]


