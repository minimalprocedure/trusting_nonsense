[title: i gesti ripetuti]

Ci sono le non parole tra noi
i gesti calmi, ripetuti e saputi.

Ci muoviamo lenti, in un tempo
e nel secondo fermi, al sentire. [m]

 
