[title: tutto cambia, niente cambia]

<< Successo qualcosa in assenza? >>  
<< No, niente da riferire. >>  
<< Tutto come al solito quindi? >>  
<< Tutto. >>  
<< Meglio no? >>  
<< Decisamente. >>  
<< Almeno è una garanzia. >>  
<< Senza dubbio. Puoi lasciare e riprendere. >> [m]
