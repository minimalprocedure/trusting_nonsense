[title: dimensionalismo regole]

Il Dimensionalismo  l'arte del progresso della mente.
Il Dimensionalismo esprime meta-matematicamente l'evoluzione della conoscenza.
Il Dimensionalismo descrive l'essenza della ricerca.
Il contrasto degli opposti come pulsazione e distribuzione statistica dell'atto del conoscere.
Il processo della evoluzione cognitiva si muove per distanza.
Il dispiegamento grafico sinusoidale esprime una singola pulsazione.
Il processo non  cronologico, ma si espande e contrae in un tempo assoluto.
ogni sinusoide  divisibile in elementi componenti, frattalmente, la conoscenza  parte del conoscere. [m]
