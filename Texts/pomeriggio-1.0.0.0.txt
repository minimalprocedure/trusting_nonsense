[title: pomeriggio]

4 o'clock

L'ora giusta per una passeggiata invernale,
in una città sferzata dai venti.

Lungo fossi e canali, ponti e strade,
parlando di cose, giochi, sorrisi e occhi abbassati.

Afferro il tuo corpo
spingendoti al muro in un angolo buio. [m]
