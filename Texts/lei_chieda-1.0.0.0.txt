[title: lei chieda]

Signorina, Lei chieda che a domandar rispondo.  
Sarà alle volte chiaro e altre meno, ma sempre di verità le sarà fornita.  
In fondo sa, son di animo semplice anche se di uno poco orso.  
Lo intimo suo voler, sono sicuro, Lei saprà giocar d'astuzia nell'allungar la mano e poi ritrarla. Sbattendo gli occhi poi, allegri e tristi, mi rapirà d'incanto.  

Quale gioco appassionante, qui si propone, anche se sol di parole e di pensieri!

La mia scala che è del mio passeggio, si volge a spirale scendendo al basso. 
La luce c'è ma non è tanta, cosicché nello scender dei gradini potrò porgerle appoggio all'occorrenza. [m]  
