[title: Sempre un poco.]

Ma non i miei dopo  
per volare sul mondo a braccia aperte.  
 
Senza rumore sono gia morto due volte  
perché la morte sia eterna e viva.  
 
Delle stelle e delle notti  
la donna dei miei sogni, danza. [m:engine]  
