[title: donne e dentici]

Ci sono donne che sono come i dentici.  
Ricordo di quando praticavo pesca subacquea, maledetti dentici.  
In apnea all'aspetto a venti metri, dietro lo scoglio. Il fucile rigorosamente nascosto, perché loro lo sanno cos'è.  

Il dentice, arriva in branco. I piccoli prima, poi quelli grossi.  
Li vedi, grigi che nuotano lenti.  
Arrivano, eccoli arrivano. Poi si fermano. 
Si fermano sempre un palmo fuori tiro, non importa quanto hai il fucile lungo o potente. Quanti elastici hai incoccato o a che pressione è la tua camera di espansione del fucile oleopneumatico.  
Loro sono sempre un palmo fuori tiro.  

Si aspetta e comincia a mancare l'aria. Si fanno i conti: _mi devo fare venti metri di salita, con nove chili di piombo addosso._  
Dovete sapere che un uomo in assetto naturale affonda dopo i quindici metri e un uomo con una muta da cinque millimetri e nove chili di piombo? Affonda già a dieci.  

_dovrò pedalare parecchio per risalire, al limite sgancio la cintura..._ 
Si aspetta. Maledetto. Si sente già il diaframma che spinge, manca l'aria. Ossigeno finito.  
Poi, il più grosso si stacca dal branco e si avvicina.  
Adesso. Con l'ultimo ansito d'aria si scatta in avanti e si spara.  

Poi via verso un nuovo respiro. [m]
