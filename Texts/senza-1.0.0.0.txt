[title: senza]

Senza di te, ho le mani tagliate  
gli occhi ciechi, le labbra secche.  

Senza di te, non scorre sangue  
quando manchi è sete riarsa.  

Il calore secco che brucia  
la vita che tu mi infondi. [m]  
