[title: le donne del mondo]

Tu hai i capelli biondi, neri o rossi  
gli occhi verdi, blu e marroni.  

La pelle chiara o scura.  

Sei alta, bassa  
hai quel piccolo difetto,  
quello che ti fa unica ai miei occhi.  

Tu sei l'amore che accarezza le palpebre. [m]  
