[title: con la mia voce più bella]

Puoi chiudere gli occhi
ti scosto una ciocca di capelli,

ma non ti bacio sul collo,
ti racconto una storia all'orecchio. [m]
