[title: seduto in disparte]

Mi immagino a certe musiche  
di star lì, seduto in disparte.  

Osservare appena poco lontano  
mentre ti muovi e parli con altri.  

Vedere come poi ti guardano  
sapendo di quanto tu sia mia. [m]   
