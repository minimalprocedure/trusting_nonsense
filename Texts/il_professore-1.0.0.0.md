[title: il professore di buone maniere all'istituto Dominique Aury]

__Buone maniere e portamento, 08:30, 27 settembre 2017__

Mi presento, sono _il professore di Buone maniere e Portamento_ all'istituto Dominique Aury.  
Lavoro in una scuola particolare, forse nemmeno tanto, una di quelle dove si insegnano _le buone maniere_. Nella nostra scuola si insegna a servire al meglio i vari datori di lavoro che esigono un servizio e comportamento perfetto.  
Si insegna a cucinare con i migliori _chef_ del momento, si insegna a lavare e rassettare la casa, stirare ma anche a far di conto. Si può imparare da noi come sostenere una conversazione banale e una complessa, ad essere politicamente corretti e flessibili secondo le inclinazioni di chi assume.  

Abbiamo corsi di buon gusto, dove i nostri allievi imparano a vestirsi e a consigliare gli abbigliamenti più adatti per le varie occasioni, come abbinare cravatta e calze. Corsi di guida sia prudente che sportiva, per chi avesse paura o fosse di fretta.  

Lezioni di difesa personale e arte: pittura, scultura o fotografia; lezioni di dizione per un perfetto tono di voce e accento neutro, ma anche lezioni di dialetto per non eventualmente mortificare il destinatario del servizio.

Le materie nello specifico sono:

- Lingua italiana e Letteratura mondiale
- Lingue (inglese, francese, tedesco, spagnolo, il cinese sarà attivato l'anno prossimo)
- Storia
- Geografia
- Cucina
- Mansioni casalinghe
- Ragioneria
- Buone maniere e portamento
- Abbigliamento
- Conversazione  
- Arte e storia dell'arte.

Forniamo una educazione a trecentosessanta gradi e niente è lasciato al caso.  

Io mi occupo della educazione più particolare visto che i nostri allievi devono imparare a servire ma soprattutto a soddisfare. Nei miei corsi gli allievi, imparano a soddisfare i loro _padroni_ in ogni capriccio dal più semplice al più elaborato. Nelle lezioni qui, vige una prima regola: non si dovrà mai parlare.  
Tutti gli allievi hanno a disposizione un taccuino dove possono, in casi eccezionali e dopo alzata di una sola mano, scrivere ciò che avessero intenzione di chiedere. Il foglietto dopo dovrà essere inserito nell'urna delle domande, che poi il giorno dopo leggerò per, se domanda pertinente, rispondere a voce.  

La regola di non parlare è ferrea, non sono ammesse trasgressioni e queste saranno punite con severità. Le punizioni fanno parte del percorso istruttivo in ogni caso, qui arrivano anche gli allievi che per un qualche motivo insindacabile abbiano trasgredito le regole o i compiti degli altri corsi.  
Ogni punizione in questo senso sarà relativa al tipo di corso in cui hanno fallito. Se cucina, sarà in relazione al cibo o agli oggetti di cucina e così via. Fallire nel mio corso comporterà fino a tre richiami complessivi e poi all'espulsione.  

Oggi arriveranno le nuove dieci matricole che saranno come sempre cinque maschi e cinque femmine. La nostra scuola fornisce personale preparato per le varie esigenze nella massima soddisfazione del cliente.  

La mia è la prima lezione, quella di accoglienza, in cui gli studenti potranno scegliere il loro nome che sia quello natale o scelto. Qui, entrati in questo edificio, si lascia indietro tutto il passato e si rinasce per una nuova vita. Nuova vita e nuovo nome, se lo si desidera.  

I loro nomi detti a voce alta saranno anche le sole parole che potranno pronunciare di fronte me nell'anno di studio a meno che non sia richiesto.  

«Salve studenti. Conoscete già le regole e gli obblighi a cui sottosterete in questo anno di studio, un anno che sarà molto intenso e faticoso. Adesso mi dovreste dire i vostri nomi per prima cosa e poi inizieremo. Cominciamo dai maschi? Dal primo alla mia destra per favore?»  

Un po' imbarazzati iniziano:

«Marco»  
«Filippo»  
«Paolo»  
«Angelo»  
«Carlo»  
«Teresa»  
«Caterina»  
«Paola»  
«Lucia»  
«Rita»  

«Bene, i vostri nomi saranno trascritti nel registro e saranno preparati al più presto i collari con il vostro nome.»

I collari sono in cuoio nero alti circa tre centimetri e mezzo con una placca in cui è inciso il nome dello studente e un anello in ferro. Sono oggetti che dovranno indossare sempre, giorno e notte, senza fibbie: vengono applicati con due borchie e non potranno essere tolti a meno di tagliarli.  
Il taglio del collare è un momento importante o drammatico per gli studenti e può avvenire solo in due casi: l'espulsione o l'assunzione.  
Alla fine del corso in caso di assunzione il datore di lavoro avrà la facoltà di sostituirlo con uno di suo gradimento.  

«Studenti! ora avvicinatevi al tavolo giù in fondo e spogliatevi.»

Questa è una prima fase di controllo e valutazione, anche delle eventuali attitudini da evidenziare o esaltare per ottenere il miglior risultato.  
Una volta nudi e in fila, spesso qualcuno cerca istintivamente di coprirsi ma ciò non è possibile.  

«Le braccia giù! Non potete coprirvi a meno che non vi sia ordinato. Oggi è una giornata speciale, la prima, quindi lascerò perdere.»  

Sono davvero una bella classe quest'anno, i maschi sono sono sul metro e ottanta a parte Filippo che è un po' più basso; le femmine tra il metro e sessantacinque e il metro e settanta circa. Bene, perché sono le altezze più ricercate. Sono più o meno di pelle chiara tutti, forse Lucia è un poco più olivastra, gli occhi marroni o verdi per tutti. Rita ha gli occhi grigi.  
Le femmine sono ben formate, non grasse certo, ma con una curva dei fianchi visibile e con un seno perlopiù medio: seconde o terze, ma è possibile che Lucia sia una quarta scarsa. Anche questi particolari sono un bene, dato che i nostri clienti non amano per esperienza i seni troppo grossi o i _tronchetti con le gambe secche_, come mi disse una volta uno di loro.  
Caterina ha il pube rasato, mentre le altre più o meno curato. Dovranno imparare a curare anche quella parte. Qui non vogliamo pubi rasati, ma esigiamo una cura maniacale.

«Caterina, dovrai far ricrescere i peli, nel mentre indosserai la _maschera_.»  

La _maschera_ è una sorta di slip in cuoio morbido atta a coprire il pube nudo, ma confezionata in modo tale da lasciare libero l'accesso all'apertura femminile e all'ano. Come il collare, la _maschera_ dovrà essere indossata sempre e in ogni occasione, fino alla ricrescita.  

Mi accorgo improvvisamente che Carlo alla vista delle nudità sta avendo una erezione e questo non è ammesso a meno di un ordine specifico.  
Con lo scudiscio gli batto due volte sul pene in movimento.  

«Carlo, non puoi, controllati o sarò costretto a punirti.»

«Adesso giratevi, mani poggiate sul tavolo, gambe divaricate e abbassatevi.»  

Per saggiare la soglia del dolore, occorre dare un colpo di scudiscio bene assestato, questo è molto importante per capire quanto e a quale livello ci si possa spingere individualmente. È una questione di agilità di polso per un colpo rapido e intenso.  

I maschi come al solito hanno una soglia più bassa e tendono a ritrarsi di più, ma questi non sono male. Le femmine accolgono invece con più _classe_ se posso esprimermi così.  

«Bene studenti, avete una buona _soglia_, sarà interessante vedere fino dove si potrà arrivare.»

In ultimo, l'ispezione interna.  

Margherita, la mia assistente, è già arrivata col flacone di gel.  

«Margherita, saresti così gentile da applicare il lubrificante?»    

Annuendo, si avvicina agli studenti e a uno a uno inizia ad applicare il lubrificante sulle parti intime degli studenti: vagina e ano per le femmine, ano soltanto per i maschi.  

«Margherita, il divaricatore grazie.»  

Uno per volta, inserisco lo strumento e allargo per controllare e: «Bene molto bene, siete molto elastici, questa è una dote.»  
«Per oggi la lezione è finita, andate nelle vostre camere e riposate. A domani.»  

[m]









