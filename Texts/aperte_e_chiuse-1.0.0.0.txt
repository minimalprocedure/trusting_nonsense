[title: aperte e chiuse]

Scrivimi delle tue voglie, quelle intime
quelle nascoste, che se chiudi gli occhi 

l'ombroso tuo morbido diluisce in piacere.
Slaccia i pensieri, quelli giù, verso il fondo

dove il brivido che ti dissolve la mente
tocca le gambe, aperte, chiuse e aperte ancora. [m]

 
