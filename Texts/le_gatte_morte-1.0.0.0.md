[title: le gatte morte]

Insomma non ci sono più le _gatte morte_, che sia un bene non si sa e che sia un male forse.  

Una volta c'era la gatta che tanto andava al lardo ci lasciava lo zampino, ma oggi? Oggi viviamo in tempi bui, signori miei.  
Mi ricordo, giovinetto, quando _lei_ e parlo di _quella_ a prima occhiata un po' _racchietta_, se ne stava in disparte lanciando quei timidi sguardi _felini_ ma pur sempre smorti. Quell'anelante sospiro rivolto al _maschio alfa di successo_ ma con l'occhio da triglia giustamente calato.  
Il _maschio_ che tutte, ma proprio tutte, agognavano in notturna.

Leggermente in disparte, come sempre, mi chiedevo insistentemente senza certezze e risposte: _... ma la gatta morta è attirata dall'occhio di triglia perché il gatto mangia il pesce?_. Domanda rimasta ancora senza risposta.

Questo è il vero inequivocabile punto, quanto è _gatta_ la _gatta morta_? Soprattutto, ma quanto è morta la _gatta morta_?

Se fosse _morta ma non troppo_? In quello stadio notturno dove lei ha i piedi e le mani congelate e nonostante calzini e guanti, cerca calore senza ritegno in uno stadio intermedio tra paradiso e inferno dove alberga fuoco tra le cosce e gelo all'appendice?

Si potrebbe anche dire che non ci sono più le _gatte morte di una volta_, le amiche dimesse delle amiche _gnocche_ che ti lasciano intravedere il _vedo non vedo_ camminando sempre un passo indietro. In quest'andazzo sociale di _selfie_ a basso consumo e in un trionfo di _culi di gallina_, la _gatta morta_ ha perduto il suo fascino e ha smarrito il suo senso di esistere.

Viviamo in un mondo davvero triste.

Servo, mi si dice però che non va bene, vostro. [m]

