[title: perdoni letà]

Signor mio lei fraintende.  
Di generazione mangiapreti nacqui e perseverai.  

Suora poi non direi proprio anche se un avo mio una ne sposò ed era gnocca assai.  
Prete nemmeno, ma ho vagheggiato di far frate da rimorchio che non so se sà ma il francescano voto di castità non pare faccia e sarebbe comunque uguale. Già mi immaginavo col saio e sandalo in motocicletta a scarrozzare bionde e more.

Cardinale?  
Ma che dio ci scampi e liberi...  
Ops, ho detto dio? Mi pareva di averlo già bruciato, ma forse lo confondo con un altro. Perdoni è l'età. [m]



