[title: la parola secca]

Nel gioco degli ordini, della parola secca
come Padrone e serva e servo e Padrona.

Quella voce altera che bassa o stridula
contiene l'azione che si ha solo da fare.

Sotto si poggia, con gli occhi abbassati
a cuccia in terra il suo essere bestia. [m]


