[title: la memoria del mondo]

Ricordiamo insieme, tutti voi insieme  
chi mette una parola e chi ne mette altre.  

Teniamoci per mano nel discorso più lungo  
che non basti carta al mondo per tenerlo.  

Né inchiostro per scriverlo o diavolerie moderne. [m]  
