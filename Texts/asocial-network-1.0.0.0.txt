[title: asocial]

Voglio costruire un asocial-network, alla fine sono un analista informatico potrei farlo. In che lo scrivo? PHP (dio ci scampi)? Ruby On Rails? Uno degli altri tanti framework per ruby? In scala, ma si lo faccio in Lift. OCaml? Certo potrei farlo in Groovy/Grails, Mi do alle servlet magari, e sfodero Tomcat o JBoss. In python no vi prego, il python mi piace e non mi piace, ancora devo decidere.

Voglio fare un network asociale, un posto dove puoi solo segnare la roba che non ti piace rigorosamente con nome e cognome, dove non puoi avere amici ma solo nemici. Un posto dove se mandi affanculo qualcuno lo sanno tutti e possono così taggarti. Le foto non si possono mettere a meno che non riesca a sviluppare un sottosistema che riconosca la stronzaggine dalle foto. Ma anche tutto questo è sociale però.

No il portale non permetterà nessun contatto di nessun tipo, che agisca come una sorta di fattura che ti fa ignorare dal mondo.

Il mondo, quello che ci circonda, che ci avvolge nella "vita è bella". Per anni ho pensato che la vita fosse una merda, e convinto di ciò ho pensato bene di lasciarla per un paio di volte o tre. Nonostante tutto era sempre lì per dissotterrarmi dalle tenebre.

Perché si è arrogata il diritto di farlo.

Mi piace immaginarmi a volte la vita senza di me, le persone che conosco gli errori fatti e causati, le belle cose capitate dopo. Allora è stato giusto così, forse.

Il fatto è che non è stato né giusto né sbagliato, la vita non è né giusta né sbagliata. La vita è uno stato temporaneo in cui un aggregato di cellule persiste fino alla loro morte. Un affanno deludente per rimandare una cosa inevitabile.

Da quei tempi ho compiuto qualche anno, ho passato i trentatré, l'eta di cristo, un mio amico è morto a trentatré anni. poi sono arrivato a trentacinque, "trentanove"?, quaranta, oddio quaranta, si dice che si faccia il conto della vita passata e si pianifichi il futuro a quaranta. Il futuro ti casca addosso, il futuro ti contatta per email. Il futuro è bel futuro. Poi quaranta/1/2/3/4/5/6/7/8 e la mattina si ha sempre sonno, al lavoro ti incazzi e ti passa l'ispirazione.

Sono un tipo superbo, un poco forse sì, sono autoreferenziale, ma il mio orgoglio è sotto i piedi, mia madre me lo ha ripetuto fino alla nausea di avere un po' più di orgoglio. Forse avrei vissuto meglio e non potrò mai saperlo.
A volte voglio vivere in solitudine e sono anche riuscito a farlo in un paio di anni particolari, ma a volte voglio essere solo con gli altri. Così chiudo la corazza e osservo da invisibile semi-demente la natura delle cose che mi scivola addosso. Con solo indosso la maschera di me stesso irradio il mio essere per svuotarmi dei contenuti. Racconto di me per rimanere solo.

Ci sono dei momenti in cui, il mio 'racconto' mi travolge e travalico i "quindici minuti di concentrazione" che ho sempre riservati a me come massima attenzione e mi accorgo dei visi persi, dei visi fuggiti, dei pensieri vaghi e dei corpi assenti. Mi fermo immobile, ricordando i tempi che furono e le lunghe sedute con i pochi a parlare dei destini dei mondi.

Mi mancano le facce antiche quando la vita era un gioco che potevi perdere da un momento all'altro e dove le parole avevano significati pesanti. Oggi posso solo insegnare a mio figlio a diventare un disadattato come lo era suo padre per donargli il valore dei pensieri. [m]
