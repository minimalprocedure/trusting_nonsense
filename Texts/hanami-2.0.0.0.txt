[title: hanami]

Voglio a primavera,
seguire il percorso dei ciliegi in fiore.

Ad ammirare la vita che esplode
appoggiato al tronco per pensare ai tuoi occhi. [m]
