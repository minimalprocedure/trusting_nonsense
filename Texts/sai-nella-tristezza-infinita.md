[title: Sai, è nella tristezza infinita.]

Sono le luci lontane, punti colorati 
a meritarsi il premio da mordere.  
 
Che le parole mi morissero in gola  
ti farò la proposta, quella indecente.  
 
Una corta e una lunga  
perché per oggi, tu possiedi. [m]  
