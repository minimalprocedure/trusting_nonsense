[title: sulla barca di legno]

Ti racconto delle tante volte
di quando io remo all'indietro.

È In mezzo al grande lago calmo
dove il freddo, è profondo e cupo.

Cauto infine, poggio i remi sull'acqua
nel timore però, di incresparla troppo. [m]
