[title: quando la sera]

Quando la sera, talvolta guardo fuori   
il lampione fioco è sotto la finestra.  

Porta lo sguardo a vedere le piccole luci  
alcune sono bianche, altre gialle o rosse.  

Appoggio la fronte sul vetro freddo  
afferro dopo, una lacrima con la mano.
  
Prima che poi, muoia in terra. [m] 
