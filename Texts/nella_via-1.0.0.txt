[title: nella via]

Nella via delle vecchie idee
ritorna ogni volta quel sapore,
nella voglia di continuare.

Nei rumori sempre uguali
che circondano i nomi
tornati e perduti ancora...

Ogni nuovo segno
che si riempie dei vecchi sensi
dell'insieme dei ricordi.

Non ho bisogno di vedere
quanto di portare con me
l'anima degli esseri...