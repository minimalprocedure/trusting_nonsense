[title: su letti diversi]

Distesi su letti diversi è la sera  
girati di spalle alle consuetudini  

senza occhi aperti, mentendo di dormire  
soppesiamo i molti pensieri e le immagini  

ci incontriamo dentro quelle stanze  
che la mente per ora solo ci fornisce. [m]  
