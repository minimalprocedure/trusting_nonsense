[title: nel battere il ferro]

__(Pronte a incudine accette, lo spiano donzelle)__

Nel battere il ferro,
l'uomo con le mani pesanti e le spalle lucide.

Nudo nel bagliore dei colpi
la pelle rovente di sudore.

Cala il martello divino a preparar l'acciaio. [m]
