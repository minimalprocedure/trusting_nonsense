[title: Sulla pietra incontri (1986)]

Ci si aggira.

_(La notte, lentamente e quasi sospetti, immersi nella nebbia)_  

È in una città che si confonde coi ricordi.  

È il selciato umido, fondo ai nostri piedi  
che risuona di passi sconosciuti e invisibili.  

Con le spalle al muro, con occhi vacui  
ci sentiamo quindi subito, un po' assassini.  

Sulla pietra ci sono incontri.  

_(l'odore è così vicino, galleggia nell'aria.)_  

Sapore di vittime sulle labbra socchiuse  
si ha le mani fredde, il bastone è scuro.  

Parvenze di sé e respinti anni addietro  
su una carrozza sferrante, usciti da teatro appena.  

In tasca una calza, dell'ultima amante dopo la tragedia. [m:1986]
