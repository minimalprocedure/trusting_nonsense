[title: un bacio bagnato]

Un bacio bagnato, grondante di umori  
di labbra gonfie per il grande uso.  

Sono turgide e tese, un poco socchiuse  
pronte al guadagno di un altro momento.  

Sfidano sfrontate colui che possiede  
che ancora un tocco le possa arrivare. [m]  
 
