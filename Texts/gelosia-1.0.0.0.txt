[title: forse]  

Forse, leggendo il _buongiorno_, qualcuno potrebbe pensare che sia _geloso_.  

No, miei cari.  
Per nulla.  

Quante volte si confonde la gelosia come espressione del _possedere_?  
Pare sia la dimostrazione di _interesse_ che molti cercano. Molte volte me lo sono sentito dire, alcune poi hanno anche provato senza successo a scatenarmi questa bieca attitudine.  

Non sono geloso, non riesco proprio. Indifferenza? No, certo non amo la mia donna nelle braccia di un altro. Cosa però molto diversa.  
Possiedo quando qualcuno si dona, come può possedermi lei quando lo faccio. In questo scambio è la magia, dove non si "vive" senza l'altro.  
Che gelosia si potrebbe mai provare se da una parte questo manca? Il paradosso in cui forse si è _gelosi_ perché si è nel possesso, ma non lo si è più se ciò manca. Come si può eventualmente essere gelosi di una _fiducia_ tradita, malriposta, inesistente? Gelosi perché qualcuno ammira la tua donna? Perché tenti di insidiarla, rubarla?   

Mai compreso poi come ce la si possa prendere del malcapitato che ha osato posare l'occhio sulla chiappa della donna. Poveraccio, colpa sua? Se l'occhio volpino ha agognato l'esplorazione dell'interno coscia?  

Che gelosia provare? Dovrebbe semmai essere solo orgoglio, orgoglio della propria donna. Non credete? Che inettitudine e insicurezza si nasconde nella gelosia?  

La fiducia nell'altro che manca. Come è possibile essere gelosi nella fiducia? Come è possibile _possedere_ qualcuno che non è libero? Libero di andare e venire?  

Come si può _possedere_ chi non ha scelto di donarsi?  

Mia, Tua, Tuo, Mio. 

È in un mondo di _mercificazione edonistica_ che può vivere la gelosia, nel _possesso di maniera_ sbandierato oggi per ogni dove.

No grazie, mi tengo la mia _malcelata indifferenza_. [m]
