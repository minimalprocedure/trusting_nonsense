[title: una mattina come sempre]

# Una mattina come sempre


#### Atto primo

**9:30**

Mi siedo ultimamente sulla panca di legno sporco come tutte le mattine. Mi siedo, chiudo la lampo del giaccone e allungo le gambe intorpidite. Dormito poco stanotte, letto un po'. Che dire, spero di non andare per insonnia. Mi perdo per qualche minuto oltre le rotaie in un oblio temporaneo, gli alberi, quelle case lontane e un po' di nebbia leggera, quella specie di fosso lercio che so che c'è.  
Apro le lampo dello zaino, sono due, prendo il libro da viaggio. Ne ho due, uno da casa per specialmente la notte e uno da viaggio, una copertina scura e una chiara. Sono a metà circa, interessante, mi piace e non succede nulla, solo una squillo di alto bordo morta, sono a metà. Calmo, fremo nello scoprire il colpevole tra qualche viaggio.

Un gruppo di tunisini parla di imbianchini e di come li pagano poco o tanto e li accompagnano anche a casa, uno dice che gli piace ballare e va a Perugia. Li ascolto con la coda dell'occhio, con l'altro continuo il mio libro.

**9:37**

Il solito tipo che non so dove va sale prima di me come _al solito_, lungo e alto e dinoccolato, biondo mi pare, mi ha guardato un giorno e ho ricambiato distrattamente.  
Il primo posto libero nel senso di marcia, il mio posto comune, quello che prendo sempre, le ultime carrozze.  
Continuo il libro bianco, nipponico e ballerino, mi accorgo di uno dei protagonisti. Non so ancora se è il _protagonista_ però, se fosse solo una fugace apparizione, una comparsata con poche battute, un nome scandito più volte di nome e cognome? Cosa fa di un personaggio un _protagonista_? il numero delle battute forse, la psicologia intensa, la nullità assoluta o solo il suo nome?  
Un anagramma, uno scrittore fallito è l'anagramma di uno di successo, un nome anagramma di un altro. La cosa mi fa sorridere. Scrittori, reali o presunti, protagonisti o personaggi, scritti sulla copertina e sulle pagine.

Rifletto sul protagonista, il cui nome non ha a che fare con gli scrittori nelle copertine, come mi assomiglia, realtà o sospetto? coinvolgimento? Continuo a leggere il libro bianco.

**9:55**

Tiro su la lampo del giaccone marrone, ha le maniche un po' lunghe come sempre, non so mai se aspettarmi se si scorcino da sole o mi si allunghino le braccia, mi piace è caldo e accogliente, quasi umido come una donna. Mi ci infilo con calma.  
Ho il terrore di inciampare sugli scalini di metallo e cauto, quasi volando, mi incammino.  
Il sottopassaggio è sempre in fondo, d'altra parte monto sulle ultime carrozze, sempre. I passeggeri sfrecciano intorno, salgono e spingono quelle cose enormi che si portano dietro, scendono, camminano, corrono, urlano, baciano e salutano, perdono fazzoletti. Il sottopassaggio mi traghetta in via Mentana, la affronto incauto a larghi passi, il traffico e il semaforo. La sfida: oggi passo col rosso, oggi passano col rosso. La solita sensazione del disegno col gesso vagamente umano sulle strisce pedonali, dopo.

Compro della trementina, la boccetta che costa meno e facciamo i conti col commesso già morto, sul costo di due boccette di olio di lino, 75ml contro 125ml, quale costa meno? Uguale, prendo quella da 75ml e della resina _dammar_ che lui non capisce. _Abbiamo la vernice_ mi dice con accento napoletano, penso alle montagne colorate di Napoli, ma non glielo dico. _Va bene, è quella pronta_ rispondo unicamente.  
Esco e do un'occhiata al bar Mentana, Nicola mi avrà portato un caffè oggi? L'ultimo tratto è nell'oblio.

Do fuoco alle polveri, il ronzio si accende, lo dimentico in un minuto. Mi siedo e scrivo.
  
  
#### Atto secondo


**9:25**

_buongiorno tristezza_.  

Arriva una ragazza con due borse, una viola e una marrone e le tiene con la stessa mano, cammina verso di me rapida. La osservo mentre oltrepasso il cancellino. Il timore, eccola, mi segue e mi giro ma non ho dietro nessuno. La dimentico immediatamente, perché dovrei ricordarla? Non conosco lei, una nessuno. Stamani scelgo la panca della predella con cura, quella con le macchie nere o quella con le macchie rosse? Ovvio penso, quelle rosse. La solita lampo con i soliti piedi allungati.

Il signore in fondo tuona nel telefono, lo sento e non vorrei ma l'orecchio si sposta laggiù: _Mi hai mandato un messaggio per San Valentino..._, afferro la risposta al di là delle onde: _Ma veramente..._. Oddio, la telefonata è lunga mi distrae dal mio oblio. Mi forzo per guardare le rotaie, l'acciaio lucido, le traversine ormai di cemento. Il campanello suona, il passaggio a livello chiude, si interrompe il flusso della vita. Si freme di impazienza, arriva e non arriva, ma quanto ci mette. Le poche persone arruffate sulla predella si incamminano in avanti.  
Tutti si avvicinano alla destinazione a piedi nelle stazioni, si avvantaggiano verso i primi vagoni per arrivare prima, per mangiare quei secondi preziosi, una gara, primo io e quindi tu sei ultimo.

Come sempre tento la telecinesi per fermare una porta dritta a me. Il vagone a metà _come sempre_, prima classe. Vado avanti o indietro? Mi chiedo chi mai prenda la prima classe su un treno regionale.

Lo zaino è già sulla poltrona accanto, il giaccone sopra, lo apro con il mio libro bianco del mattino.

Non succede mai niente, spero che alla fine il protagonista ci provi con la tredicenne, partono per le Hawaii, ci proverà con la madre, con la figlia o con tutte e due? Al volo rimpiango un racconto di de Sade, almeno c'era del movimento.  
Aspetta un attimo, ma è morta una prostituta!

Ancora al telefono. In portoghese una signora informa tutti su qualche cosa e insieme l'ascoltiamo rapiti non capendo una parola. Davanti a me qualcuno si alza, alta mora, stivali allacciati, occhiali scuri. Nervosa va avanti e sparisce, poi torna oltrepassa e si risiede, ma si alza e si sposta in fondo. La guardo, donna inquieta.

Le donne della mia vita, belle e a volte meno, sensuali sempre.

**7:45**

Mi osservo nella nebbia della doccia. No, certo non sono più come prima, quaranta anni suonati e passa e quelli che rimangono sulla pancia. Gonfio i muscoli, potrebbe andare molto peggio, un sospiro e mi asciugo i capelli.  
Sono un tipo casual adesso con l'età, nel senso che _casualmente_ mi metto quello che trovo tastando nell'armadio. Non lo avrei mai fatto prima, strano magari ma sempre ricercato.  
Mi metto una t-shirt viola sotto un maglione grigio, pantaloni larghi e scarpe da trekking.

**9:55**

Cerco di non pestare i bambini per le scale, gli sorrido, mi sento la mora con gli stivali dietro, il suo vapore. Giro a sinistra e la sento ancora, allungo il passo mentre leggo distrattamente un volantino su un muro giallo: _koreana fa massaggi_. Mi volto ed è lei ed è lì, tanto evanescente quanto tremula; ma poi sento i suoi passi che vanno dall'altra parte. Paranoia, è come quando torno indietro più volte per non aver chiuso casa.  
Il solito traffico, il solito semaforo, premo il bottone giallo con gusto e attraverso col giallo. Auto che si bloccano.  
Il solito oblio dell'arrivo e travolgo la vecchia già morta e che non lo sa. No anzi, la evito. Sono ancora agile come un tempo, guizzo e divincolo, lei per la sua strada mentre io per la mia.

La prostituta morta del libro bianco. Do una occhiata di sfuggita al bar Mentana, pieno di vecchietti e le bariste. Ci penserò domani.

_buongiorno tristezza_
  

#### Atto terzo


**9:30**

_è tardi è tardi è tardi_ disse il coniglio.  

È tardi.

**2:15**

Bevo, sete e fame. Mi sono alzato, mi manca qualcosa, sento uno spazio vuoto irregolare come un momento non colmato. Ho fame.  
La tentazione di affogare nelle immagini pessime di una televisione pessima, il vuoto del nulla informativo. _Viviamo in una società capitalistica avanzata_ cito il libro bianco e tento per far qualcosa, ma poi prendo il libro scuro, quello della notte e lo finisco di un fiato, poi ne inizio il seguito.  
Sto scomodo sul divano, mi giro, non ho sonno, manca qualcosa. Mancano messaggi, i sensi che sono mandati, manca chi non c'è o cosa non c'è. Possedere delle parole e sapere che sono le tue.  
Non ho sonno, ma prendo sonno. Con calma.  

_è tardi è tardi è tardi_ disse il coniglio.  

**9:32**

Oggi cambio.  
Non la solita panchina sudicia, quella con la macchia rossa, oggi mi avvantaggio anch'io, cammino e cammino, vado avanti oltre i binari mentre il treno arriva, voglio arrivare prima oggi. Mi manca qualcosa e corro a cercarla.   
La predella è scomoda. Fisso una bionda affogata nel cappotto, lei e il suo trolley. No davvero, non mi manca lei, non mi può mancare, non sono parole e non sono sensi.  
È carne.

Ascolto i _Minimal Compact_, _Lowlands Flight_.  

L'ultimo posto della prima carrozza è quello che fa per me: _gli ultimi saranno i primi_ e ho fretta, mi manca qualcosa. Mi rassicuro col libro bianco, con un poeta senza un braccio, con una fotografa bellissima e diffusa, con il mio solito protagonista. Parlano e si chiedono, ma mi passa il tempo.  
Affogo nei ricordi, nelle rubrica vecchi numeri di telefono, dove saranno adesso? Mando un messaggio a tutti, mi espongo, mi valuto, mi scopro, mi manca qualcosa e solo una risponde: _Non ricordo, chi sei?_.  

Mi manca qualcosa.  

Potrei telefonare e fornirle la mia voce, allora saprà, saprà? Ricorderà?  
Ci scambiamo una serie di messaggi, non sa e io non so, che fosse lei? Che non fosse lei? I numeri cambiano, i visi si perdono. Chi vorrei rispondesse non lo fa, perduta nell'oblio delle lettere mandate e ritrovate da poco. Io so che scrive ancora, dovrò comprare un suo libro alla fine. Speriamo sia bianco.  

Non leggo, penso al nulla, casco distrattamente nei lampi di sole al finestrino e ripongo il libro. La stazione, ma oggi sono avvantaggiato e sono già quasi arrivato. Sono alle scale, le scendo veloce evitando il flusso in salita che vuole allontanarmi, perché non ti lasciano uscire? Perché le forze ignote devono fluirti in gregge?  
Mi faccio largo. Oggi che sono in anticipo, che sono già quasi arrivato e che pochi metri mi separano.

**9:55**

Rumori di tacchi, voci, sudori e colori, tutto oscilla e muove. Devo nuotare vigorosamente per non rilassarmi e perdere. Ho voglia di bere, del sale sulle labbra, dell'acqua nei polmoni.  
Ho voglia e ho fame e ho sete. Cammino e non mi accorgo, un passo dopo l'altro e non mi accorgo. Mi manca qualcosa e ogni cosa è mia.  
Il bar Mentana è aperto, di lunedì?  
Ho perso il segno, ho perso il giorno. Cosa è cambiato?  
Non vedo Nicola che mi supera, è in piedi appena lì: _Un caffè_? Lo guardo.  
_Posiamo gli zaini prima_ mi dice e lo guardo. Apro, chiudo. 
 
_Due caffè, lunghi_, il bar Mentana è aperto.  

_è tardi è tardi è tardi_ disse il coniglio.
  

#### Atto quarto


**9:30**

Parto in auto perché niente treno oggi. L'ebbrezza di guidare, di muovere il carro meccanico. Odio guidare da un po'. Portatemi voi, passivo e seduto, gradisco il movimento involontario.  

**1.20**

Non dormo, non dormo, non so che succede. Il tempo passa e non dormo, giro e mi avvolgo nel letto estraneo. Non so che succede e non dormo.  
Prenderò il secondo libro scuro. Lo leggo.  
Ho ripreso il raffreddore, maledizione! Chiamerò un esorcista. Mi faccio una tazza di tè e brandy, mezzo e mezzo.  
Leggo, leggo dei ghiacci, leggo del freddo, ma che succede? Non dormo. Il tè caldo e i suoi vapori non servono.   
Mi manca qualcosa, qualcosa succede, qualcosa cambia, lo sento. Sento la serie di eventi che si dipana, si chiama, che sussurra legami. Si organizzano tutti a mia insaputa, lasciandosi solo intravedere. Mi sento sfruttato, mi sento impotente, ma voglio che cambi. Mi giro e rigiro sul divano e alla luce un po' fioca leggo.  
Che cosa succede? Cosa cambia? Non capisco, mi sforzo di non controllare, non capisco. Mi mancano parole e voci, le ricerche spasmodiche, le lacrime e la voluttà. Mi manca di morire per poi rinascere.   
Fermate il tempo, esseri ancestrali.  
Fermate tutto e lasciatemi vagare, senza mente, senza occhi, senza bocca. Non odo, non assaporo. Riempite le mie mani della sabbia del nulla.

Fluisce come clessidra e non posso girare.  
La musica _danza e danza e danza_, chi vedo lontano steso nella polvere?  
Le mani che trascinano il corpo. _Cieco_.  

**10:15**

Sono arrivato in auto oggi, odio il traffico, odio la gente. Datemi il _solo_ che abbaglia.

Cosa cambia, cosa cambia. Cosa cambia?
  

#### Atto quinto


**9:30**

Perché devo guardare sempre oltre le rotaie. Per cercare cosa? Ci sono sempre le solite cose, la solita casa in costruzione, i soliti alberi, il solito fosso sporco che non si vede ma che so che c'è. Ci guardo e poi avanti e indietro. Il passaggio a livello e le auto che scavalcano.  

Apatia, la vita.  

Mi suonano i _tuxedomoon_ nella testa, _cabin in the sky_.

Apatia, le cose.  

La mia panchina in fondo! È libera!  
Allungo il passo mentre una vecchia mi scruta. Calcolo mentalmente la distanza (_a volte è meglio amaro o meglio_?). Lei anche, Lo scatto. La guardo male, le premo gli occhi addosso, mi siedo, sorrido sardonico. Vittoria e lei rinuncia, si allontana.  

_Bitter than better_?  

Apro tutte le tasche dello zaino, indifferente. Il mio libro bianco e leggo.

Apatia, l'intorno.  

**9:37**

La predella, alzo lo sguardo. 
_Mio dio parla da solo_! Scorro lungo il nero fino ai capelli rossi, poi riscendo. Parla da solo e racconta, se la tira, _lui_ immerso nei suoi pantaloni neri. Ipnotico coi miei occhi lo seguiamo. Tenta di aprire la porta ma non ce la fa all'inizio. Sono congelato, rapito dalla forma. Un attimo, la vedo sorridere, la testa sa chi le comanda. Lo seguo per mezzo treno, poi abbandono esausto e mi siedo.

Apatia, un mondo di pratici glutei.  

Leggo, il mio libro apatico.  

**9:58**

Sulle scale, opposta la rossa, ci sorridiamo e ci guardiamo da dietro. Glutei trovati e glutei persi. come visi estranei.

_Un uomo come io mascolino e carino parzialmente macchiato..._ (tuxedomoon)

Apatia, le auto che mi passano accanto, senza colore.  

Apatia. Oggi guarderò solo culi. Niente facce.  
  

#### Atto sesto


**9:32**

Piove. Piove. Piove.  

Niente mi disturba da lontano, la ragazza che arriva sulla strada, che guardo per niente. Verso il cancello e poi giro, cammino. La sensazione è nulla, il giorno è nulla. Due ragazzi seduti sono una lei, sono un lui. Oltrepasso verso la meta e siedo, apro, cerco il libro bianco.   
Lui rutta.  
Il vento generato senza odori, ridono. Vorrei rompergli le gambe sotto il ginocchio. Maledetto ragazzino. Forse ha ruttato lei? Peccato era carina.   
Muoiono tutti in questo libro. Il mio protagonista si scava la terra sotto i piedi e _danza, danza, danza_.

La musica sale e corre, corre e va lontano, la mente, il mago, il santo. Nelle bacchette cariche di magia, degli uccelli che volano, dell'oltre che mi sforzo di vedere. Batte, batte, picchia la mano, le dita, il piano scandisce tasti e vita.  
Giro, giro, giro, roteo e muovo contorto alle mani e avvolto. Mi chiedo su in alto del sotto che vedo.

Cado. Leggo il mio libro mentre intorno brulica passeggeri.

**9:37**

La predella, il treno mi porterà come al solito.  
Vuoto o quasi il vagone e scelgo il secondo sedile. Leggo qualche pagina ancora.

**10:00**

Piove, piove, piove. Rasento i muri per nascondermi, mi confondo col cemento, schivo l'esercito aggressivo. Mi muovo a scatti, imprevedibile, giro, avanti e un po' indietro, scarto a sinistra, corro. La battaglia è persa, sono bagnato.

Piove. Piove. Piove.  
Maledizione. Il ricordo.  

_La mia spiaggia è lontana_  
_in quello che ricordo_  

_dei piedi bagnati e della sabbia calda._  

_Nel suo corpo che chiama_  
_nei mari in tempesta_  
_nei mari calmi_  
_nelle acque tiepide._  

_Seduto a guardare il mare_  
_udire quello del richiamo_  
_mi bagno di pioggia_  
_ma non mi muovo._  

 **10:51**

 Piove fuori e non posso farci niente.
  

#### Atto settimo


**9:32**

L'auto si allontana, lei blu oltre la strada mentre la guardo. La bionda la incrocia, la incrocio con gli occhi. Il viale del cancello, in ferro aperto e senza pudore come una cintura di castità slacciata. Entro e cammino col mio solito colore della terra.

Il mio libro bianco è finito, è finito nel ritorno di ieri coi bambini urlanti e coi visi sconosciuti. Finito in un amplesso desiderato e agognato per pagine e pagine. Il mio protagonista desto dal sogno e non solo come sembrava. Simile, come già detto, danzante e in ricerca. Anche io mi muovo e aspetto, l'amplesso finale.

L'auto si muove, manovra per uscire, per entrare, nel suo amplesso col parcheggio.

Sono solo stamani, solo col campanello che mi tuona nel cervello. Segnale di arrivo che non ne favorisce l'arrivo, tintinna e rimbalza caotico, accelera e rallenta in sincro col suo treno.

Fischia, arriva, balzellando.

**9:37**

Un attimo, la folla.  
Pigia pigia per salire. Cerco di spostarmi ma non posso.  
Spingo la porta e spingo la porta, non si apre e mi preoccupo, mi guardano male.  

L'idiota non sa aprire.  
L'apro al contrario, in avanti poi siedo accanto alla signora grassa, nella mia nostalgia.
Oggi il mio libro blu, pedissequamente intriso di personalità. Due personaggi e molte facce, lui e lei e gli altri, la casa e il giardino, il lago da cui nascono le anime.  
Mi spingo sul lago e aspetto, ascolto e aspetto, è l'amplesso della nascita. L'umido del principio nelle mani e nella lingua, nel sogno delle curve ellittiche, tra fianchi e sudori tersi.

Il lago calmo, mai in tempesta, le acque limacciose. Senza scarpe bagno i piedi e cammino verso il fondo, accolto e amplesso.

**9:55**

Desto, alzo e accomodo, scendo e cammino. Il vento si alza e sposta, i capelli sugli occhi, soffia e tremola.

Le automobili corrono, corrono verso il loro pontili, veloci o lente per gettarsi in mare coi loro piloti, suicidi di massa.  
Così mi ricordo il mio ultimo giorno, il cancello e il suo viso, l'amplesso del finale. I suoi occhi con le sue lacrime, impassibili le mie parole perdute nel passato, con la mia vita che se ne va. Amplesso e ricordo della vita passata, del lago.

Nel presente, cammino oggi lento, non ho fretta e passo il negozio di belle arti e passo l'agenzia interinale e passo il bar Mentana, oggi aperto. Passo come ogni giorno, coi ricordi annodati ai molti fazzoletti. Li vorrei gettare a volte e ritornare a quel giorno, col finale diverso.

Amplesso e poi più niente.

(un bacio e una carezza per il tuo ricordo) [m:2002/2003]
