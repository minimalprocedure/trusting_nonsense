[title: lo sbaglio]

Io sbaglio, a volte non colgo il momento giusto e la mia voglia di proteggere sottovaluta me stesso e faccio del male. Faccio del male a chi voglio di più.
Questo mio nascondermi in un gioco allegro ed esterno ai mille dolori che mi porto dietro.

Il terrore di perdere chi amo, perché io amo. Amo da stringere fino a far fisicamente male, amo da poter pensare continuamente al mio amore.
Il mio amore, visto continuamente mentre cammino o accanto, dietro mentre lavoro.
Sento le sue mani sulle mie spalle, il bacio che a volte mi da sulle guance.

Posso sentire le sue mani tra i capelli, posso trovarla nel letto la mattina.
Il suo nome detto appena sveglio.

Io sbaglio e con un trapano mi arieggerei il cervello, togliere il marcio. Pulire e levarmi questa maledetta corazza.
Levami questa corazza, mettimi a nudo. Spogliamoci davanti un pezzo per volta e guardiamoci, nudi uno di fronte all'altro. Guardiamoci, occhi a occhi. Guardiamoci fissi per un tempo senza fine.

Io sono innamorato di te e ho una gran paura. [m]

