[title: come provare quello]

Come provare quello che viene
indossandolo come un maglione amato

quello vecchio e liso, senza ormai colore
che lo indossi ed è largo,
e ti avvolgi
e lo ami del passato
e con lui ti stendi.

A braccia aperte su un pavimento freddo
guardi il soffitto bianco e piangi. [m]