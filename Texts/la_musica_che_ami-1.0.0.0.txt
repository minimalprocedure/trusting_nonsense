[title: la musica che ami]

Con un dito batto il tasto,
un suono all'altro come da bianco a nero.

Il gomito appoggiato, la testa sorretta
indolente guardo, nuda distesa.

Poi ti sfioro fra le gambe
una musica che ami. [m]
