[title: mi divori]

Mi divori, mi assaggi, mi lecchi
mi morda e mastichi, mi stringa.

Mi beva e assapori piano il prima
poi, faccia sentire i suoi denti.

Mi mangi e non si sazi troppo, ancora
dovrà finirmi, perché sono cotto per lei. [m]

