[title: nel nome dell'acciaio]

Il ferro, bianco di fuoco diventa acciaio  
con martello e carbone, con sudore e forza.  

Batte l'incudine e il muscolo è terso  
colpi e colpi sulla materia che piega.  

Il corpo è bruciato, arso, lucido e rosso  
rilucente di luce e striato di fiamma. [m]  
