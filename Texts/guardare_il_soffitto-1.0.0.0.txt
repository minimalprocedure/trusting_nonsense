[title: per mano a guardare il soffitto]

Voglio afferrare i tuo polsi
scivolandoti dentro con calma.

Muoversi piano nella lentezza del tempo
spingere e ritrarsi nel sincronismo acasuale.

Al tempo dove ci si parla col corpo
per cadere a lato poi, per mano. [m]
