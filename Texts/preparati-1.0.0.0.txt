[title: preparati]

Al volgere dei fianchi
a lui donati ad aprirti con forza

poggia le labbra al di mio bere
e mordi il fallo che ti inonda la bocca.

Voglio guardarti, 
mentre con le mani ti prepari allargando,
il tuo per il mio, nel secondo venire. [m]
