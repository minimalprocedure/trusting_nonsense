[title: dio pifferaio]

Come topi, uno dopo l'altro  
mossi da inevitabile parvenza  

camminiamo, seguendo gli umori  
l'odore del prossimo e seguente  

guardando chi di noi avanti  
non muove che i piccoli piedi.  

Rapiti dalle musiche del dio pifferaio. [m]  
