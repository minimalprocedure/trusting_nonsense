[title: ti legherò per bene]  

Sai Bimba? Ti legherò per bene.  

Tanto stretta con un filo di lana    
da impedire così, ogni tuo movimento  

però è così fragile allo strappo  
da poterti poi, se vuoi, liberare. [m]

