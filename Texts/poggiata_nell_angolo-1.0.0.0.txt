[title: poggiata nell'angolo]

È nel buio senza poter vedere
aspetti, poggiata nell'angolo.

Tocchi la fronte al muro, il seno
il calcare trasuda, l'umido appiccica.

Al veloce rumore una mano spinge
alla nuca tiene e l'altra entra. [m]
