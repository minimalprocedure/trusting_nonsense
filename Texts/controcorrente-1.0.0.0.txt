[title: controcorrente]

La vita, scorre e va nel tempo
impotenti di questo, poco è concesso.

La vita va combattuta e controcorrente.
Con l'Arte va combattuta.

Scrivete, dipingete, scolpite, suonate.
Fotografia, musica, sesso e il ballo. [m]
