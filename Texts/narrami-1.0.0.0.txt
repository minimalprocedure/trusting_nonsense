[title: narrami]

Voglio ogni giorno chiederti di un pezzo  
da te che sia ben guardato e poi narrato.  

Come non fosse di tuo proprio ma di altra  
osservato non per uomo quanto per donna.  
 
Quel tuo corpo, che sia tanto tuo amante  
da poter bramare, soggiogare e dominare. [m]
