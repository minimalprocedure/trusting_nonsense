[title: ho gli occhi verdi]

Ho gli occhi verdi e i piedi piccoli...

Ho i piedi piccoli, vabbè non tanto grandi quarantuno e mezzo o quarantadue dipende dalle scarpe, ma ho il collo del piede alto, quindi sempre scarpe un po' lunghe altrimenti soffro :).
Anche il mio babbo ha i piedi piccoli, lui quaranta e la mamma da giovane trentasette, ma non significa niente se non si sa quanto alti sono: 1,75 il babbo, 1,71 la mamma.

Io sono alto 1,81 e ho una sorella (parecchio bella) di 1,80 (meno male, sono sempre il più alto della cricca), mio fratello non lo so mica quanto è alto, ci parliamo a malapena e solo quando lui è in buona, o meglio quando gli serve qualcosa e io non sono poi tanto incazzato con lui.

Ho gli occhi verdi, però verdi cangianti, grigio-verdi, a volte marroni verdi, come la mamma.

Stamani ho messo la maglietta del "bastardo", come la mamma l'ha chiamata appena vista. Mi è stata regalata da un mio amico carissimo, detto "Lo psicopatico", anche se non è psicopatico davvero... solo un po'.

Gioco a scacchi, non benissimo, faccio poche cose benissimo, alcune bene, altre male, ma sopravvivo lo stesso anche ascoltando musica pallosissima, come adesso: Dead Can Dance. Mi piace spesso stare da solo.

Divoro conoscenza e programmo computer (macchine idiote) in cinque o sei linguaggi, ma in teoria ne conosco una decina, non contento mi alzo la mattina intorno alle 7:00 mi faccio la doccia (tutte le mattine meno la domenica, quando in genere mi adoro puzzare fino al pomeriggio almeno, salvo diverse indicazioni).

Nella mia vita ho amato i profumi e ne ho usati tanti da donna e da uomo, dolci e secchi, e mi hanno dato spesso e volentieri del "finocchio", ma mi sono portato a letto le loro donne. Sono andato vestito nei peggio modi ed ho scritto lettere intere su rotoli di carta igienica, rigorosamente inviati al destinatario.

Adoravo portare almeno trenta o-ring di gomma nera per braccio, e mi piacevano i Soft Cell. Mi piacevano le donne vestite di nero, poi di rosso. Mi piacciono nude. Mi piacciono le calze di seta con la riga, ma non le metto, le tolgo a volte se capita.

Mi piacciono le bionde, mi piacciono le rosse e mi ritrovo sempre con le more.

Sono "burbero ma bonario", odio ipocrisie e falsità. Se qualcuno mi vuole come nemico gli consiglio di lasciar perdere.

Mi piace il mare e mi piace la montagna, sotto il mare e sopra il cielo, ma riesco a mantenere una conversazione seria per più di quindici minuti, anche se il mio interlocutore sbadiglia, non ho pietà di lui.

Non ho mai _picchiato_ nessuno a cui non piacesse, anche se potrei in taluni casi, reputo la guerra da estirpare con qualcuno dietro. Ho fatto il militare ma ho fatto anche quattro mesi di convalescenza. Odio i militari.

Mi piacciono tante cose, tutte non le posso avere ma mi impegno per averle. Sono contento se chi mi sta vicino è contento e mi mantengo i capelli un po' lunghi.

Scrivo per parecchi motivi che ora raccontarli sarebbe troppo lungo, e poi lo racconto a una persona per volta, magari dentro un lettone con la stanza fredda, un sacco di coperte, un caffè una spremuta, crepes nutella e mascarpone, insieme a cornetti alla crema e marmellata.

Sono a piedi adesso e fuori piove, ma se elimino un sacco di se e avessi la macchina partirei per un viaggetto un po' lungo, non tanto... abbastanza.

Adoro le donne, ma questa è un'altra storia. [m:~2009]
