[title: terno o ambo con giocattolo]

**Terno o ambo con giocattolo**  
===

Leggo il biglietto.  
È arrivato stamattina, portato a mano.  

_Metti il tailleur scuro, niente sotto, niente calze. Ti aspetto stasera._

Sono davanti alla porta, il vestito scuro e niente sotto. Suono.  
Alcuni minuti e mi apre, Lui.  
Mi prende per mano e mi porta nella sala. Ha spostato il divano che è ora messo leggermente obliquo, il tavolino da tè in legno scuro è scostato. Ci sono due bicchieri usati e una bottiglia di vino a metà. Li guardo e sto per chiedere, mi fa un cenno di silenzio.  

_Siediti._   

Lo faccio, sul divano, sulla destra.   

_Aspettami._  

Lo vedo andare verso il mobile basso, quello con i quattro cassetti grandi. Ne apre uno accucciandosi, quello a destra in basso. Torna verso di me con in mano un lungo foulard scuro.   

_Alzati._  

Io sono più bassa, ho la testa al suo petto, mi sovrasta e questo mi piace. Mi sento piccola con Lui, in balia di Lui. Mi gira con una mano e comincia ad avvolgermi la testa con il foulard. Mi avvolge come fosse una cuffia spessa, lasciando libero il naso e la bocca. Stringe il nodo forte.  

_Devi dirmi se ci vedi, la verità. Me ne accorgerò comunque._  
_No Mio Signore, non vedo nulla._  

Ero nel buio più pesto e davvero non vedevo nulla. Mi piace stare così con Lui, mi fa sentire inerme più di quando mi lega.  

_Spogliati adesso._  

Slaccio i bottoni della giacca del tailleur e me la tolgo. Il seno nudo struscia sulla fodera liscia, la lascio cadere a terra. Apro la gonna, la lascio scendere lungo le gambe.  
Mi sorregge mentre la passo oltre i piedi. Sono nuda adesso, nuda e cieca.  

_Togli anche le scarpe._ Lo faccio.  

Mi sfiora il seno con una mano, mentre l'altra è già tra le gambe.  
All'orecchio mi dice piano:  _Aprile._  
Le divarico e lo sento che rovista tra le labbra. Lo sento che armeggia e insinua un dito, entra facilmente.
Lo sente che comincio a bagnarmi.   

_Brava, sei brava._   

Sono brava si, sono brava. Sono brava.    

Lo sento staccarsi da me, si allontana. Il rumore di una una porta che si apre, entra qualcuno.  
Il cuore mi comincia a battere all'impazzata. Ho paura.  
Sento una mano sul mio sesso di nuovo, una mano... è la sua, la riconosco.  

_Mio Signore, chi c'è con lei?_  
_Sono io qui e adesso giochiamo. Tu sei il mio giocattolo?_  
_Si, Mio Signore, lo sono._    
_Brava, hai paura?_  
_Si, Mio Signore, ho paura._  
 
Ho paura e mi bagno, Lui lo sente, mi struscia con la mano aperta, forte, mi stringe, mi stringe... e mi fa male, ma lo voglio.  

All'improvviso una mano mi prende al collo spingendomi giù e cado a sedere sul divano.  
Non capisco e cerco di rialzarmi.  

_Stai giù!_  Mi dice il Mio Signore.
_Rimani dove sei_  
_Appoggia i piedi sul cuscino e tieniti le gambe aperte._    

Lo faccio, appoggiando un piede dopo l'altro sul cuscino e tenendomi per le caviglie.  Allargo le gambe quanto posso.   
Il mio sesso è esposto, lo sento, proteso... Sono fradicia e immagino la pelle del divano bagnarsi di me. Rimarrà la macchia a ricordo, ne sono sicura.      

_Scivola un po' più in avanti._   
_Fatti vedere bene._  

Mi accomodo meglio, così da espormi di più.    

_Sei brava. Zitta adesso e offrimi la tua fica..._  

Si sono brava e la mia fica è a sua disposizione.  

_Ti piace la sua fica?_ Lo sento dire.  

Nessuno risponde però. Non so che immaginare, chi immaginare. A chi mi vuol far vedere? Un altro Signore?  
Non era mai capitato che mi facesse vedere a un altro.  

Lo ascolto mentre cammina girandomi intorno. Riconosco i passi del Mio Signore. Mi prende per i capelli, tirandomi la testa indietro e accarezzandomi il seno. I miei capezzoli duri, li prende e stringe. La sua mano che scende sulla pancia, sul ventre.  
Passando sul clitoride le sue dita mi entrano dentro. Mi solleva appena tirandomi da lì, con la mano a gancio. Mi sfugge un sospiro, apro la bocca, dolore, mentre mi tiene alzata per la fica.   
Mi mordo un labbro mentre mi tiene così sollevata per non gridare, il Mio Signore mi fa male e questo mi piace.  

Ci sono altre dita ora e non sono le sue, altre che mi toccano dietro. D'istinto mi stringo, mi oppongo. Vorrei chiedere cosa stia succedendo ma non posso, devo stare zitta.

Lui se ne accorge, con un brusco strattone mi solleva ancora.  

_Che fai? Smettila!_  

Il dito che spingeva mi _buca_ ed entra tutto dentro poi seguito subito da un altro. Senza esitazione e poi un terzo. Quella mano sconosciuta che si _introduce_, che esce appena e poi più forte rientra.  
Mi cominciano a tremare le gambe, so che sto quasi per venire.  

_Non devi venire!_  Mi urla il Mio Signore, strattonandomi i capelli.  
_Non venire!_     
_Ti punirò come non ho mai fatto se succederà_  

Quell'ordine riesce a fermarmi. Lui, sa farmi fare sempre quello che vuole.    

_Sei brava davvero, brava... Bene, non venire ora..._  Mi dice allentando la presa sui capelli.  
_Ti piace troppo che ti si apra il culo, ti piace che ti si sfondi la fica. Vero?_  Mi chiede.  

Non posso rispondere, ma non c'è bisogno perché ha ragione, Lui lo sa, Lui mi conosce, Lui lo sente.  

Questa mano che non conosco, non è grande come quella di Lui, ha le dita lunghe. Le sento molto a fondo. Si muovono e assaporo il come, sa come fare.  
Le immagino dentro di me, che toccano il morbido, che lisciano l'interno.  
Tutti e due mi penetrano, alternati, un colpo per uno. Li sento sbattere per allargarmi, infilare forte e uscire lenti, si alternano.   

Un colpo davanti, un colpo dietro.  
Un colpo che batte, forte, dentro e davanti dove sento di più, dove grondo bagnata.  
L'altro dietro, più ovattato e che riempe, è paralizzante.  
 
A un colpo dietro più forte mi scivola una gamba.  

_Reggiti! Guarda che ti lego altrimenti, se non fai la brava Bimba!_  

Mi stringo a più non posso le caviglie e allargo le gambe al massimo.  
Quelle mani mi scopano così forte adesso. Non riuscirò a non venire se continuano e il Mio Signore sarà deluso e mi punirà severamente.  
Poi però escono graziandomi, prima quelle di Lui dalla fica, poi anche le altre scivolano fuori.  
Mentre lui mi parla sento qualcosa che si allaccia, delle fibbie, come una cintura.  

_Sei davvero una brava bambina, ora vieni sul tappeto._  

Prendendomi per mano mi fa alzare e sedere sul tappeto. Delle mani mi spingono in basso, mettendomi su un fianco.  
Una mano mi solleva la testa, sento qualcosa sulle labbra. È Lui e me lo spinge in bocca.   
Apro le labbra e lo prendo. Non è lunghissimo ma è grosso, largo e mi riempie la bocca.  
Lo tira fuori e rientra tenendomi per la testa.   

Mentre mi scopa la bocca, mi sento alzare una gamba e qualcosa di duro che si appoggia.  
Con le mani mi tira e lo sento appoggiato dietro. Con la pelle tesa e tirata so che non ci sarà nessuna resistenza.  
Ormai _larga_, con poco me lo spinge dentro. Lo sento penetrare con facilità, piano scivola verso l'interno fino in fondo.    

_Apriti cagna!_  

Una voce di donna, non me l'aspettavo. Mi ritraggo e stringo ma lei che era leggermente uscita rapida me lo affonda dentro.  

_Apri questo culo..._ Mi incita.  
_Fai la brava bimba come dice Lui, apriti bene!.  
Poi più gentilmente: _Fai la cagna docile, su..._    

Entra ed esce. Non lo fa forte, ma lentamente per farmi sentire in tutta la lunghezza.    
Mi tiene una gamba alzata e con una mano mi stuzzica il clitoride. Non ce la faccio quasi più e la punizione minacciata si fa sempre più vicina.  

Il Mio Signore viene, il mio Lui nella mia bocca, lo bevo.  
Esce e mi solleva, appoggiandomi la testa sul suo sesso bagnato. Tenendomi poi per i polsi e stringendo dice alla donna:  

_Fottila bene ora, fottile il culo e fai venire la mia bambina._  

La sento montarmi più forte, spingere a colpi secchi. 
Così mi abbandono a quegli affondi e a quell'oggetto, che mi dilata e perfora.  

Poi, finalmente l'orgasmo. [m]
