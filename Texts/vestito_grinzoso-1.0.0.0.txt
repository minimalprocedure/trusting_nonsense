[title: era solo un vestito grinzoso]

Triste quella mano che si alza
triste il braccio che la muove.

Il vestito grinzoso, smesso da ieri
appoggio alle spalle, copro il capo.

A volte non posso che piangere di me
le poche lacrime a forza spinte indietro. [m]
