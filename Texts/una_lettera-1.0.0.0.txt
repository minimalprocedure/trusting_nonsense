[title: una lettera]

Sul tavolo ho lasciato una lettera per te
è scritta con la penna e l'inchiostro.

C'è scritto dei molti giorni, di quelle ore,
quei minuti eterni passati nel vissuto. [m]
