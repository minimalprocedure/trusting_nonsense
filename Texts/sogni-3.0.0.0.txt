[title: sogni]

Non ho dormito bene stanotte, mi sono svegliato varie volte con una certa ansia e mi sono riaddormentato. Non so cosa possa aver sognato.

Sa cosa sogno spesso? 
Sogno ambienti con strade tortuose, città, paesi di pietra, boschi; tutto sempre con strade che si incrociano in alte salite e discese, strade che passano accanto ad altre magari più elevate in cui per arrivarci si deve fare un giro complicato. Sono quasi sempre salite e discese, il paesaggio è altalenante come fosse in alta collina.  

Generalmente ho qualcuno con me e spesso sono armato ma niente armi da fuoco, solo archi o bastoni e qualche volta armi bianche.  
Ci sono sempre molti cancelli e porte, ma soprattutto cancelli da aprire. Passo negli edifici, che sono all'interno un dedalo di stanze piccole con una stanza enorme. La mia è generalmente una fuga, qualcuno mi bracca e mi devo difendere continuamente. Quando non è così, cammino in mezzo alla gente che popola questi luoghi come invisibile, non ho interazioni se non con chi è con me dall'inizio. A volte chiedo qualcosa ma una volta avuta la risposta ritorno invisibile.  

I paesi che incontro in questi viaggi, sono in pietra scura con archi, vie strette sempre in pietra, muri altissimi. La sensazione che le case si chiudano sopra. Nei miei sogni pervade l'ineluttabile, il senso della lotta senza speranza. Cammino con una spada di Damocle sulla testa.  

La mia è ricerca, come ricerca pura... Un anelito della ricerca. Non so cosa stia cercando e ci trascino chi è con me.  

Da più giovane sognavo sempre la morte. Nei sogni morivo continuamente.
Uno ricorrente, credo di averlo fatto per mesi e mesi era questo.  

Una piazza, una piazzetta che conosco bene. È la piazzetta sotto una scalinata accanto a dove viveva mia nonna paterna in paese che si chiama Montescudaio.  
Era come il gioco dei quattro cantoni. Ero nascosto in un angolo e il mio scopo era arrivare in diagonale all'angolo opposto e un cecchino con un fucile era in cima alla scalinata.   
Non sono mai riuscito ad arrivare dall'altra parte, morivo più o meno a metà. Mi sparava e morivo. Ricordo di averle provate tutte, linea retta, correre e camminare, un po' qua e un po' là, giri complicati e semplici. Morivo e basta, un colpo e morto.

Nei sogni in cui muoio, sento la morte e non mi sveglio. Sento l'agonia del cervello che si spenge e che si accorge di morire. Arrivo al vuoto.  
Forse è solo come mi immagino la morte ed è così per quello. Mi dicono che non si possa sognare la propria morte, il cervello si rifiuta ma non so, a me pare proprio di sentirla. Non mi sveglio e rimango nel vuoto senza sentire più niente. Forse la difesa del mio cervello è quella di non farmi ricordare e di bloccarsi in un limbo in cui sparisce anche il ricordo.  [m]













