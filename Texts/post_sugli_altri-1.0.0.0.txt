[title: i post sugli altri]

[anonimo]
Perché fare post sugli altri e sul loro modo di fare? Serve? A cosa serve? Non capisco.

[m]
Caro anonimo, la risposta semplice sarebbe: faccio quello che mi pare.
Quella complessa invece è che scrivo quello che mi pare anche analizzando le abitudini dei frequentatori dei social network. Vede, questa banalità sudaticcia da Bacio della nota fabbrica di cioccolato oggi miseramente di proprietà di un'altra fabbrica di cioccolato meno romantica, a volte mi irrita.  
Come del resto mi irritano tante cose: la falsità, l'invidia e l'analfabetismo per esempio. A cosa serve mi chiede? Non serve a niente certamente, a che dovrebbe servire? Continueranno imperterriti al: Prego come sta? Io bene e lei? Buongiorno a te, a me e all'universo.

In ogni caso, non pubblico su nessuna persona specifica e mi limito a osservare la ripetitività. Prendo una piattaforma sociale per quello che è, una collezione di piccole immagini di 64x64 pixel o poco più e un rutilante insieme di fotografie prese qua è là spesso senza un senso compiuto. Frasi sgrammaticate e traduzioni di traduzioni e traduzioni, banalità immense prese ad arché della vita.  

Ho visto nel tempo per esempio Dante, la Divina Commedia ha presente? Citata in traduzione inglese da un tumblero italiano, lei non gli avrebbe rotto le gambe appena sotto il ginocchio?

Vedo articolate trame, complotti amorosi in cui lui ama lei e ci prova con l'altra e anonimi che mettono in guardia gli uni e gli altri. Giustappunto lei, mi dica perché in anonimo? Non avrebbe potuto chiedere senza tema di svelarsi? Vede, non si fida delle persone al contrario di quanto faccio io che pubblico quello che mi sento.

Amo le persone che il viso me lo mettono davanti, oggi una foto domani dal vero magari.
Non si può chiedere fiducia quando non si è disposti a darla e il primo passo sempre noi lo dovremmo fare. 

Quindi ecco qua, se non si potesse scrivere di quello che ci accade intorno e di quello che si nota o ci colpisce non ci sarebbe sicuramente molta letteratura molto più importante della mia. Saremmo tutti felici e contenti a prenderci caffè e cappuccini amoreggiando con la nostra ultima conquista virtuale.

Virtualmente.

Servo suo m.
