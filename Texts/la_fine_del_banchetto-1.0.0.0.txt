[title: la fine del banchetto]

Il banchetto è tutto aperto in bellezza  
molti musici suonano e i giullari balzano.  

Commensali bevono, mangiano e ammiccano  
donne ubriache tanto lasciano intravedere.  

Leoni del momento, animali da preda e lupi  
ma sciacalli di un dopo, putrefatto di ossa.  [m]
