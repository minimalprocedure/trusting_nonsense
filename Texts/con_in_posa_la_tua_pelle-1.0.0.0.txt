[title: con in posa la tua pelle]

Ti fotograferò, un giorno
con in posa la tua pelle.

Le tue curve, esposte e nude
gravide di luce e madri di ombre.

Scruterò il momento, quello giusto
unico e adatto a renderti immortale. [m]
