[title: trasmutazione]

(per Edy...)

Muovo lasciando lontano l'impossibile
in una strada, in una casa vuota,
con il viso, le mani, il corpo.

Penso che lontana oltre ogni voglia,
è lì nei mondi nascosti, oltre di me.

Nella notte il desiderio, rinascere
cosi tanto diverso e molto uguale.

Per presentarsi di nuovo a lei
come unica amante identica. [m:2000]
