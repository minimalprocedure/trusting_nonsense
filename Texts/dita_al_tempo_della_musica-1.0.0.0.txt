[title: le dita al tempo della musica]

Donna, sai adesso che c'è spazio
batto le dita al tempo della musica.

Lettere che formano parole e ritmo
coro di chi canta e racconta in rima

non le storie tue o mie, né degli altri
ma uomini e donne seduti, sulla barca nera. [m]
