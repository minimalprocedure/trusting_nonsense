[title: viaggio]

Esponi i fianchi alzati, gambe in ginocchio  
lisci le gambe velate, agganci la calza.  
 
Isolato dalla musica  
oggi voglio ammirarti dal basso.  
 
Non così poi molto evidente  
ho voglia di partire per un lungo viaggio. [m:engine]  
