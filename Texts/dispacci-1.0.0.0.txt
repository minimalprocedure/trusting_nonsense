[title: i dispacci]

Darò a quel fiume che porta i messaggi
dispacci a sigillo con faccende da fare.

Aperti e letti per l'impegno con cura
l'effetto all'acqua dovrai rimandare.

Così che io sappia dell'ordine attuato
da spianar la strada per quella venuta. [m]
