[title: asincronia]

Rinasco quando il resto muore  
asincrono nel passaggio accanto.  

Sempre piano con chi corre e poi   
così veloce con chi si muove lento.  

È questo lungo ruotare in cerchio  
dove le mani sono solo sfiorate. [m]  
