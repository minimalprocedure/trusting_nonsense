[title: sonata a tre]

**[anonimo]**  

squinzuglio?

**[m]**  

Essendo in tema di neologismi, giustappunto anche inseriti mi pare nell'ultimo Devoto-Oli, in tal _guisa_ talune volte mi cimento. Lo _squinzuglio_ o _squinziglio_ è quell'atteggiamento di aggradata sufficienza che fa l'uomo un gradino superiore alla bestia ma nel contempo appena inferiore al cosiddetto, sempre in tema di _fluenza_ linguistica, _tal dei tali_. Per capirsi, si avvicina al _l'uomo che non deve chiedere mai_ di _Denim Musk_ memoria. Acciordunque oltre che sulla _quarta corda_ lo vedrei più proprio al _nodo scorsoio_ adatto allo _slogamento dell'osso_ come definito dall'emerito Sig. Duff.  

Quindi lasci la domanda _inposa_ o _supposa_ e si goda il Bach con un tocco di ironia sì, ma pur sempre lasciva.  

Le confesso però che per il _tocco_ più che al Bach, mi rivolgerei a un Corelli.  

L'immagini sinor appese, nove sono.  

Servo suo, m.
