[title: lancio i dadi]

Troppe cose da sapere    
troppe quelle da guardare.  

Così tante sono da incontrare  
che c'è da perdersi tra di loro.  

Lancio i dadi a fortuna memoria  
ne esce un numero, è di due più tre.  

(avanzo e leggo il biglietto che dice cosa fare) [m]
