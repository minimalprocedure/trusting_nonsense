[title: il banchetto dei mostri]

Ti hanno lavato e pulito e adesso  
tu sei vestita e bella, per la sala.  
  
Piena di latte, dentro al tuo corpo  
verserai accucciata coppa per coppa.  

Al banchetto dei mostri, quelli ridenti  
ti presenti perfetta e a seno nudo. [m]  
