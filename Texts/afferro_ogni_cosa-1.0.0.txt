[title: afferro ogni cosa]  

Afferro ogni cosa e ogni cosa è mia  
dibatto e agito e scuoto.  

L'albero anciente dei forti rami  
al vento opposti, fieri.  

Dalle radici che sale l'umida vita  
nel fondo nascosto della terra.  

Immobile il mio tronco    
fisso aspetta un tuo ritorno.    

Per ripararti dalla pioggia. [m]  
