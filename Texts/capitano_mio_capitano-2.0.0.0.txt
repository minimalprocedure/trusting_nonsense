[title: capitano mio capitano!]

Caro mio sconosciuto, navigatore astuto di mari in tempesta, ora che il porno scarseggia su queste onde che si fa?
Il remo si tira in barca mentre il capitano affonda con la nave? Ci aspetta il giro di chiglia magari doppio?

Niente gassa d'amante amico mio ora che i nodi sono tutti sciolti. 
Ci resta forse solo il mozzo dalla chiappa di pesca?
La figlia del caraibico governatore per fargli la festa?

Domande e domande e domande, ora che lo stoppin s'arresta.  

Orza la banda e strizza la randa! [m]
