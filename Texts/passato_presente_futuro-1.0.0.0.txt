[title: passato, presente e futuro]

Potrei amarti, per poco o per molto
nel tempo potrei averti già amato.

Conosciuti nei giorni a venire
ci siamo scambiati segni e sorrisi.

Come allora in quel futuro che attende
vissuti da sempre per conoscerci ancora. [m]
