[title: i neuroni a cerchio]

**[anonimo]**  

Bellissimo “il ratto delle sabine” -> “il topo delle tope”  

**[m]**  

Buongiorno Signora.  

È stato indubbiamente un lampo di genio, uno dei quei momenti in cui tutti i neuroni in fila indiana si sono chiusi a cerchio e come l’O di Giotto, hanno stupito il mondo. Cotanta e tale perfezione allusiva so, ahimé! Che sarà rara.

Il _gatto delle gatte_ o il _cane delle cagne_, per quanto quest’ultima sia almeno in certi ambienti semanticamente corretta, non hanno la stessa potenza evocativa.

Immagini lei tutte queste _tope_, alcune brade e altre ben curate e altre ancora ignude che si fuggon dallo topo. Tutte allegre e sorridenti che fanno finta di inciampare. Immagini, il labbro arricciato quasi volesse proferir parola.

Che visione Signora mia, che visione!

m. 
