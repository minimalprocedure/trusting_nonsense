[title: sai a volte]

[sai, a volte mi prende la tristezza senza motivo]

Sai, a volte quando la tristezza ti prende
è quel senso di impotenza di fronte alle cose del mondo,
una leggera stretta, quella che chiude le speranze.

Sai quando, l'aria intorno tremula,
quando anche in inverno l'aria si fa spessa e pesante
come una giornata di pioggia estiva che ribolle.

Sai le lacrime, che quasi ti parlano del tutto,
che asciughi, ma che ritornano, che qualcuno ti dice
ti fanno gli occhi belli.

Sai, quella voglia di sdraiarti,
poggiarti con calma a terra, per guardar sparire gli orizzonti. [m]
