[title: le occasioni mancate]

Una voce è quella che ascolti
bocche che parlano e dicono.

Nell'insieme dei visi coperti
che ti si muovono intorno.

Insegui il racconto dei suoni
ti giri di scatto e lo vedi partire. [m]
