[title: indispensabili]

Nessuno è realmente indispensabile, nessuno non è sostituibile.
Siamo piccoli incidenti di percorso,
se non noi, qualcun altro.

Il valore della indispensabilità è una sovrastruttura del pensiero che si protegge dal terrore di rimanere da solo, in un abbandono inevitabile.

Il nostro essere, le nostre azioni,
continuamente ci agitiamo in un egoismo trascendente la nostra fisicità.

Egoismo è ritenersi indispensabili e la caduta è grande nella sua consapevolezza. [m]
