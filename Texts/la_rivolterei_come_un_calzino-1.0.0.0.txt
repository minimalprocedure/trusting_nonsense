[title: la rivolterei come un calzino]

La rivolterei come un calzino, di baci.

Stamani mi sono alzato al solito, mi alzo alle sette, doccia e le solite cose gia dette. Ma da qualche tempo mi sveglio presto, non la guardo l'ora ma è buio fuori. Mi giro e mi rigiro e soppeso fermenti non lattici. Per la verità vado a letto pure un po' tardi, lavoro e saltello qui e la per la rete, a volte col cellulare a volte piratanto una wifi vagante.

Muse - Origin of Symmetry

Mi circonda un alone leggero, non direi di santità però, più di. insomma di. non vorrei mi spuntassero corna e coda adesso.

Oggi è una giornata normale, ieri era male per un verso e bene per un altro, nel senso mi sono mancate moltissimo cose, ma ci siamo divertiti dal cliente furbo ma non troppo. C'è pure caso che  nel pomeriggio si debbba tornare, vedremo, e mi mancheranno le cose di cui sopra, ma fortunatamente.
c'è il cellulare, dio benedica la nokia, che mi permette di iniettarmi la rete un po' dovunque (anche 3 che mi fa pagare poco però), e i ragazzi di Opera per aver fatto il browser che uso(sul cell. sui pc Firefox, l'internet explorer no, vi prego meglio una martellata su un piede).

Parlando di piedi piccoli ho invece le mani grandi? non lo so mica, forse. diciamo che sono due, ma spesso paiono di più se ispirate. odio gli anelli, o meglio ho paura degli anelli, mi viene subito una visione di dita strappate e sangue dappertutto. no, no, odio gli anelli. Meglio cieco.

Sono cervellotico, analitico, nei miei istinti razionali. Analizzo, pondero, valuto e poi mi lascio andare e faccio dei casini terribili. Ho tradito donne e altre no, una con sua cugina, una con la sorella, una l'ho tradita talmente tanto che mi sono innamorato perdutamente di lei.

Sono inquieto, mi comprimo ed espando, esploro meandri. A volte sono depresso ed ho cercato di suicidarmi due volte. Per fortuna o no, sono sempre qui a tediare il mondo spargendo minimalistiche pallosità scritte.

Ho dipinto, scolpito e fotografato, ho modellato corpi per adattarli al mio. Non credo in un dio, l'ho anche cercato in certi periodi, ma non si è fatto proprio trovare, quindi ho smesso e basta. Mi piacciono le religioni antiche. forse si è notato ho il nome di un dio.

Scolpire le menti con le parole.

Continuo ad ascoltare i Muse ma sto pensando di cambiare, sono turbato e mi andrebbe di correre e roteare fino ad annullare il mondo in una spirale distorta.

Oggi mi sono vestito come al solito, ho al solito la barba più o meno incolta, che "palle" la barba. me la faccio una volta alla settimana, tanto la società è mia (con altri due amici) e comunque sono sempre "il solito stronzo" tanto vale tenermi la barba trasandata, capelli lunghi e magliette, camicie e maglioni rigorosamente in disordine. Dovrei pure avere una decina di vestiti o seri. Messi? boh, mica mi ricordo.

Quando facevo l'ispettrice per la P. (notare che ispettrice non è sbagliato, ero l'unica ispettrice d'italia con l'ingombro, se qualcuno se lo chiede. non facevo il trans), ci andavo vestito bene, rigorosamente grigio scuro e dolcevita nera e il mio taglio carré del periodo (alla valentina di crepax, senza la frangetta, davanti lunghi). Bei tempi. Ero anche parecchio più magro, ma avevo trent'anni.

A Santa Teresa di Gallura un anno ho portato tutte le mattine per un mese un paio di levi's per andare al mare rigorosamente senza mutante, completamente bucati ;) ma in quel periodo ero più nudo sempre che vestito, quindi tutto bene. Li ho venduti poi, così come erano per 100.000 lire, camminavano da soli ormai, non ho più saputo niente del tipo che li comprò.

Ho vissuto in Sardegna per parecchi anni, ne avevo otto quando sono arrivato e diciassette quando sono scappato.

I Muse cantano ancora e la Sardegna è un'altra storia. [m:2008]
