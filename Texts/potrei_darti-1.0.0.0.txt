[title: per F.]

Potrei darti mondi che neanche tu immagini,  
potrei coprirti d'amore da soffocarti.  

Tu lo sai questo e io lo so,  
per questo te ne devi andare. [m:1990]  
