[title: la camera oscura]

Quando vivevo a Perugia nella mia mansarda, allestii una camera oscura nell'antibagno di casa.
Iniziai con un ingranditore a lenti, poi ne comprai un altro a diffusione e adatto anche per il colore che peraltro non ho mai fatto. Questo nuovo lo usavo per le carte multigrade che avevano bisogno di essere impressionate con colori diversi.  
Le carte sono di gradazioni diverse e cambiano nella resa, da risultati più morbidi con intervalli tonali molto estesi a quelli duri con pochi cambi di tono. Ci sono carte a gradazione singola che vanno da zero a cinque o anche sette per alcuni produttori e le multigrade che in base a come vengono esposte variano.  
Ho usato molto delle carte speciali il cui fondo non era bianco ma avana con risultati molto belli.

Nel mio percorso fotografico ho usato la solita mia tecnica della tabula rasa. Comincio da zero o comunque il più da zero possibile.  
Mi piace capire il comportamento intimo delle cose, in ogni cosa. Ho costruito molte camere stenopeiche e anche degli obiettivi rudimentali con cui ho fatto delle fotografie che forse potrebbe anche aver già visto.  
Ho preparato le gelatine sensibili e sperimentato con molti materiali.  
In un periodo ho utilizzato molta pellicola lith, che è una pellicola speciale sensibile solo al verde nelle frequenze più alte e al blu. È una pellicola molto dura. Esponevo più fogli a morbidezze variabili: dalla prima sfuocata all'ultima nitida. In genere erano quattro o cinque fogli che poi montavo con degli spessori in mezzo per un effetto stranamente tridimensionale.

Sviluppare le pellicole e la carta è un processo direi sensuale. La carta soprattutto. La pellicola che deve essere inserita nella tank nel buio completo, a meno di pellicole ortocromatiche che si possono usare sotto la luce rossa fioca, è un processo sensoriale. Toccare solo lungo i bordi per montarla nella spirale, inserire la spirale nella tank, avvitare. Metri di pellicola che sfiori con i polpastrelli come una bella donna.  

La carta però è come farla venire. Dopo averla impressionata e inondata di luce, la poggi nel bagno di sviluppo. La vedi crescere mentre si forma, con dei leggeri colpetti sulla bacinella smuovi poco il liquido per rinfrescare la reazione chimica. A volte la devi toccare nei punti giusti dove vuoi che venga meglio, risalti. Il calore delle dita innalza la reazione chimica, la tiri. Si deve tenerla d'occhio perché se inizia piano poi progredisce velocemente. Viene ed è pronta.  
Va messa nel bagno di arresto, subito e appena venuta. Altrimenti ti morirà sotto gli occhi.  

Il fissaggio, elimina l'argento ancora reattivo. Poi va lavata, lavata con cura e con amore. Non va sbattuta, la gelatina è gonfia, turgida e delicata. Piena di liquido.  
Va lavata delicatamente per qualche minuto. La si prende e si adagia ad asciugare.

Mi sono divertito a sperimentare diversi bagni di sviluppo con ricette diverse per effetti diversi. Usavo la ricetta di Man Ray per le solarizzazioni.  
I bagni di sviluppo pronti non sono adatti alla solarizzazione perché troppo veloci, ci vogliono bagni al solo metolo. Se si espone alla luce la carta in un bagno moderno la si uccide. Con un bagno lento, si può notare l'inversione che avanza e fermarla quando si vuole e tutte le volte che si vuole. Sta per venire e la fermi, poi ripeti e la fermi, poi ripeti e sboccia.

Ho una fotografia che è in divenire da anni, lei cambia. Ho usato una solarizzazione parziale e un fissaggio a zone. Nella carta sono rimaste tracce di alogenuro d'argento che cambiano col tempo, si sta metallizzando da sola. Come se fosse immobilizzata in un orgasmo che si libera molto lentamente. Morirà di questo processo, lei morirà prima o poi. La fotografia è destinata a distruggersi. Non ho più nemmeno i negativi, li ho regalati. Non potrà essere più riprodotta dall'originale.  

Per molti anni non ho più fotografato, perché la fotografia è un rapporto intimo e per me lo è anche con la persona con cui sto. Non so perché ma più di ogni altra cosa la fotografia è legata alla persona con cui ho rapporto. È indissolubile l'ispirazione e la voglia con la mia donna. La mia donna è sempre musa per me, è punto di inizio e di riferimento.  

Ho ripreso da poco a fare qualche fotografia, in digitale anche se mi piace poco. Mi toglie tutto quello che ho detto prima.
Mi sono costretto a farlo, perché non è giusto che rinunci a quello che sono. Lo faccio con una certa svogliatezza, perché è come se fosse fine a se stesso.   
Lo faccio per me ma questo non mi completa. [m]



 




