[title: alla musica che ascolto]

Non ho niente da leggere e mi è difficile trovare da leggere.  
Con un gesto annoiato scaccio una mosca fastidiosa, lei che ronza.  

La sensazione delle cose vecchie, rifugio della tristezza  
orrori conosciuti e compresi, rassicuranti e controllabili.  

Nella stanza, in fondo la porta è chiusa  
la guardo, che mi serve per decidere. [m]  
