[title: La mia tazza nera nel suo tè verde.]

Agitando le ali verso le idee naviganti, lontane  
le mani e la voglia sono due cose strettamente correlate, pensate.  
 
Cose per il lavoro del tempo
percepire addosso solo il tuo pensiero.  
 
Un tutt'uno sfumato e mai guardato  
nel perdere i momenti e i significati delle cose. [m:engine]  
