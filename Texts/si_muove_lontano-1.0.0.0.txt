[title: si muove lontano]

Si muove lontano, in fondo 
dove lo sguardo non arriva.

Nei momenti stanchi la testa è pesante.
Ti asciughi una lacrima sola.

È quella che si perde,
che ti sfugge senza sosta.

Piangi e vorresti 
che tutto ciò non esistesse. [m:1999]
