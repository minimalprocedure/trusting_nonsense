[title: non so più cosa pensare]

Io non lo so più, non so più ascoltare le parole, non so più leggere tra le righe.
Ho perso la capacità di vedere oltre, di capire, di prevenire.
Sono perduto in un turbine di cose, di realtà e di fatti immaginari.

Ascolto e i suoni vengono da che parte?
Leggo le storie ma non capisco a chi appartengono.
Mi serve la corazza nuova, quella vecchia è troppo ammaccata.

Non so più cosa pensare. [m:2000]
