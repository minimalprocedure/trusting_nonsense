[title: come palle di gomma]

[come palle di gomma tra una parete e l'altra]

Siamo soli, muovendoci a scatti,
fermi ripartiamo a fermi di nuovo.

Al video mancano fotogrammi,
quei ricordi frammentati da anziani signori.

A piedi nudi sul selciato
col passo del __grande male__. [m]
