[title: bovarismo e speluncazioni]

**[anonimo ben conosciuto]**  
Io & il bovarismo. Non a caso amo il personaggio di Madame Bovary.  

**[m]**  
Quindi il bovarismo non è la speluncazione del bove?   

**[anonimo ben conosciuto]**  
Definire “speluncazione”.  

**[m]**  
L'etimologia fa risalire la parola speluncazione all'azione dello speluncare. _Spe_ si pensa che origini da _specŭs_, _specūs_ ovvero antro o grotta, ma anche (fig) canale, profondità, apertura; mentre _luncazione_ probabilmente dopo modificazioni medievali da _lumectum_, _lumecti_: cespuglio.

Il significato perciò si potrebbe leggere come: cercare l'antro o il canale, l'apertura se vuole, nel cespuglio. C'è da chiedersi piuttosto come mai del bove e l'analisi qui potrebbe approfondirsi.   
Si parla del bove ovvero il toro castrato? Ci si riferisce al _bovinator_ latino e quindi al tergiversatore o per dirla più moderna al caca-cazzo?   
Osando quindi una lettura più vicina a noi, il tutto potrebbe diventare: esplorare nel cespuglio l’apertura del caca-cazzo.  
Riducendo, a questo punto se ne può vedere anche la luce. 

Servo suo. [m]
