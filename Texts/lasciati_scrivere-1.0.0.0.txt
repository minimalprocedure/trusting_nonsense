[title: lasciati scrivere]

Donna lascia che ti scriva sulla schiena
saranno racconti di poveri amori infranti.

Segni indelebili delle cose che vanno,
volano via, per lasciar le bocche amare.

Poi nuda balla e vai, porta al mondo
i dolori degli altri come fossero tuoi. [m]
