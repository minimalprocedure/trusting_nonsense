[title: il seno ornato]

Mia Cara, 

stasera ti voglio infiocchettata di rosso.  

Poggia un nastro alla destra e l'altro alla sinistra   
ad ogni capezzolo poi legali a formare un fiocco.  

Vieni poi, che con quel seno ornato  
tu sei di già un pacchetto pronto. [m]
