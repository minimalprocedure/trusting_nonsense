[title: quello che manca]

Una proposta indecente,
un parlottare d'umido e sporco.

L'arrivo di solo due righe
nelle poche parole che smuovono.

Anche solo un disegno a mano
del pensare cosa faresti. [m]
