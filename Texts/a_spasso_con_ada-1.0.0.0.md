[title: a spasso con Ada]

A spasso con Ada
---

Un pomeriggio di Aprile ed ho appena finito di leggere un bellissimo articolo visionario su una macchina fantastica che ci potrebbe aiutare a produrre musica e grafica, a risolvere problemi matematici come le equazioni di Bernoulli.
Che visione romantica di un possibile progresso e che, chissà dove potrebbe portarci.
Adesso sono qui, pomeriggio primaverile del 1843 per incontrare la meravigliosa autrice di queste previsioni, Augusta Ada Byron King, Contessa Lovelace per una passeggiata tranquilla, in questo parco assolato.
Spero che dopo, possa presentarmi Babbage.

Confesso che mi sarebbe davvero piaciuto poter incontrare una delle figure più particolari, forse, del panorama matematico della metà dell’800, sicuramente una figura anche un po’ strana, che con Charles Babbage, ha gettato le fondamenta dei moderni computer, loro e la ‘Macchina Analitica’. Ma lasciamo l’introduzione ‘poetica’ per cominciare a raccogliere i pezzi.


Un linguaggio per un mondo complesso
---

Nei ‘favolosi’ anni ‘60/70, il Dipartimento della Difesa degli Stati Uniti d’America (DoD) stava usando circa 2.000 linguaggi di programmazione diversi, molti dei quali sviluppati per scopi particolari, per la loro, ovviamente mission-critical, programmazione. La complessità dei sistemi era quindi arrivata a un livello di saturazione tale da convincere le amministrazioni a trovare una soluzione. Nel 1975 fu istituito l’HOLWG (Department of Defense High-Order Language Working Group) per risolvere questa grave crisi del software.
Quello che fu deciso dall’HOLWG, fu che c’era il bisogno di creare un linguaggio unificato da usare in qualsiasi contesto, dalla programmazione di sistema, all’intelligenza artificiale, al sofware generico e soprattutto doveva fornire facilitazioni riguardo sistemi real-time ed embedded. Un linguaggio quindi adatto per lo sviluppo di sofware per pilotare qualsiasi computer, dagli orologi agli aeroplani (solo per esempio il Boeing 777).

Piuttosto che crearne uno dal nulla e per stimolare la produzione, istituirono una gara, (lascio sorrisini e polemiche, per una serata in pizzeria.) si unirono molti team di progetto, caratterizzati tutti da un colore, il team giallo, quello verde, blu e così via. Le conclusioni furono che alla fine tutti arrivarono a strutture di linguaggio simil-pascal e da delphista convinto, ne sono oltremodo felice.
La vittoria arrivò per il team verde, il CII Honeywell-Bull francese, che dopo varie scelte e vicissitudini battezzò il nuovo linguaggio Ada, in onore di Augusta Ada Byron, figlia del famoso poeta, e assistente del matematico Charles Babbage, inventore della Macchina Analitica. Lady Ada è considerata, quasi unanimemente il primo programmatore della storia.

Nel 1979, la prima bozza di documentazione fu presentata e la standardizzazione ANSI arrivò nel 1983, con il nome di ‘Ada 83’ il linguaggio è stato controllato per anni interamente dal DoD con il divieto di produrre compilatori senza espressa autorizzazione. Questo stato di cose comunque terminò, fortunatamente, nel 1987 con la standardizzazione ISO, da quell’anno si stima che siano stati prodotti circa 200 compilatori validanti. Ada, comunque detiene anche un primato, che forse pochi conoscono, nella revisione del 95, con il nome ‘Ada 95’ è stato il primo linguaggio object-oriented standardizzato.

Spero a questo punto di aver suscitato sufficiente curiosità per poter continuare, con la speranza di riuscire ad affascinare qualcuno almeno quanto lo sono stato io all’allora incontro :)


Gli ambienti di sviluppo
---

Come detto prima, il numero dei compilatori prodotti per Ada è stato grande, e quindi non è poi tanto difficile trovare una implementazione per il nostro sistema preferito, si va da ambienti costosissimi a implementazioni opensource, e di queste una in particolare merita attenzione, lo GNAT. Lo Gnu Ada Translator è un compilatore sviluppato in origine alla New York University, ma oggi mantenuto da una società, AdaCore Technologies, che ne cura il continuo sviluppo, e che mantiene, per ovvie ragioni di licenza originale, una versione di libero utilizzo. La versione che potete scaricare liberamente è un completo ambiente di sviluppo Ada95 validante, pronto per poter essere usato professionalmente.
Esistono altre due implementazioni, molto importanti, l’ObjectAda di Aonix e il JanusAda di RR Software, Aonix, tra l’altro, permette di poter scaricare il suo compilatore senza costi per uso didattico.

Lo GNAT, vi arriverà dopo parecchi Mb di download, con programmi ovviamente a linea di comando, quindi vi dovreste procurare un buon editor, e qui libertà assoluta. mai interferire nel consiglio di un editor a un programmatore, pena punizioni corporali.
In ogni caso, se lo accettate, quello che uso per la maggior parte (a parte il Jedit e scite) si chiama jGRASP, ed è appositamente stato progettato in origine per Ada, anche se oggi supporta altri linguaggi. Realizzato in java, quindi procuratevi il JSDK di SUN.

Frammenti di un linguaggio di programmazione
---

Dopo tutto questo parlare vediamo un po’ le caratteristiche importanti del linguaggio, che potrebbero farlo preferire ad altri per lo sviluppo.
Uno degli scopi principali del disegno originale era la mantenibilità del codice, e forse per questo la somiglianza con il pascal, quindi in genere la sintassi è molto pulita, e il type checking molto stretto, Ada è sicuramente uno dei linguaggi di programmazione più stretto che io conosca, il che può irritare un po’ all’inizio, specialmente per chi proviene da strumenti più permissivi.
Questo è molto importante per prevenire errori di assegnamento in fase di compilazione ma anche a runtime, una cosa molto importante di Ada.
Dovete pensare allo scopo del suo progetto e vedrete come certe sue caratteristiche diventino naturali, la gestione delle eccezioni, che oggi è sulla bocca di tutti, è inevitabile in Ada e non si ha scelta, è il compilatore che la fa automaticamente (certo possiamo anche regolarla).
Un’altra caratteristica, importante, la computazione concorrente è supportata da linguaggio, nessuna funzione esterna o artifici vari, task a gogo senza compromessi! :)

Altre cose carine, ve le lascio per altri eventuali articoli, dipende dall’interesse.

Cominciamo con il solito, stravisto e abusato:

~~~ ada
01  with Ada.Text_IO;
02     use Ada.Text_IO;
03       
04  procedure Hello is   
05     begin
06        Put_Line(“Hello World from Ada Program”);
07     end Hello;
~~~

Questo è il famoso Hello World in Ada (scrivetelo salvatelo come .adb. e compilate).

Le numerazioni sono solo per riferimento per spiegare quello che succede. Innanzitutto i pascalisti cominceranno a riconoscere qualcosa, ma comunque come si può vedere chiunque con un minimo di esperienza potrebbe essere in grado di comprendere che cosa sia accaduto. Il linguaggio supporta, come molti altri, la programmazione modulare e anzi la estende a livelli molto interessanti che vedremo.

Guardate come in ogni caso si deve comunque dire che cosa si vuole fare:

~~~ ada
with Ada.Text_IO; use Ada.Text_IO;
~~~
       
bisogna essere prolissi, difetto? personalmente non credo, anzi credo che contribuisca a limitare gli errori, in ogni caso in altri posti permette di essere concisi, sicuri e produttivi allo stesso tempo.
Questo blocco invece definisce una funzione che in questo caso è la principale e che dovrebbe avere lo stesso nome del file salvato, ma non necessariamente:

~~~ ada
procedure Hello is   
    begin
   
    end Hello;
~~~            
           
Per ultimo il metodo Put_Line:

~~~ ada
Put_Line(“Hello World from Ada Program”);
~~~        

che scrive una stringa nella console, come si vede la modalità per definire costanti stringhe è di racchiuderle tra virgolette doppie (i singoli caratteri in virgolette).
Se volete vedere la dichiarazione del metodo Put_Line e avete installato lo GNAT, dovete cercare il file a-textio.ads nella cartella:
$gnat_root$/lib/gcc-lib/pentium-mingw32msv/2.8.1/adainclude/
e verso la fine troverete:

~~~ ada
procedure Put_Line(Item : in String);
~~~

se noterete bene, vedrete anche un’altra Put_Line, con parametri diversi, ebbene sì, Ada supporta l’overloading delle funzioni e degli operatori.

Il file che dovreste aver aperto, contiene il package Ada.Text_IO (le linee con ‘—’ sono commenti) dichiarato in questo modo:

~~~ ada
package Ada.Text_IO is
    pragma Elaborate_Body (Text_IO); — direttiva del compilatore
   
    type File_Type is limited private;
    type File_Mode is (In_File, Out_File, Append_File);
    .
    .
    .
    ————————————-
    — String Input-Output —
    ————————————-

    procedure Get (File : in File_Type; Item : out String);
    procedure Get (Item : out String);
    procedure Put (File : in File_Type; Item : in String);
    procedure Put (Item : in String);

    procedure Get_Line
      (File : in File_Type;
       Item : out String;
       Last : out Natural);

    procedure Get_Line
      (Item : out String;
       Last : out Natural);

    procedure Put_Line
      (File : in File_Type;
       Item : in String);

    procedure Put_Line
      (Item : in String);
    .
    .
    .
private
    LM : constant := Character’Pos (ASCII.LF);
    — Used as line mark

    PM : constant := Character’Pos (ASCII.FF);
    — Used as page mark, except at end of file where it is implied
    .
    .
    .
end Ada.Text_IO;
~~~
    
Evidente che non è tutto qui, ho solo preso delle parti per continuare a stuzzicare la curiosità.
Questo è un package, con sezioni private, e questo anche in Ada83, quindi già allora aveva delle caratteristiche dei moderni linguaggi a oggetti.
Se si analizza la quarta riga dell’esempio, se ne noterà una che:

~~~ ada
type File_Type is limited private;
~~~        
       
questo significa che File_Type è un tipo che non permette assegnamento (limited), una sorta di costante; e come effetto collaterale avrà che nella chiamata, per esempio, a una Put_Line, il parametro sarà passato per riferimento. Sempre guardando nel package, nelle dichiarazioni di metodi si può vedere come i parametri siano contrassegnati dalle parole chiave in e out, di facile intuizione, questi infatti sono specificatori dell’uso dei parametri ma esiste anche un’altra modalità la in out.
Un’ultima cosa per concludere questa breve introduzione e visibile ancora nel package mostrato, riguarda le ultime righe dove si usa l’attributo Pos che in quel caso ritorna la posizione del carattere di LF (line-feed) della stringa (LF e FF sono dichiarate package ASCII)

Per adesso, direi di terminare, in fondo era solo una rudimentale introduzione a un linguaggio, spesso considerato, a torto, troppo complesso e forse non poi così conosciuto. Se volete questa introduzione potrebbe essere solo l’inizio, magari di un vero e proprio corso.


Verso il tramonto
---

Una magnifica passeggiata, in compagnia di una bellissima e intelligente ragazza, sono contento di averla potuta incontrare, e sembra che anche Babbage mi potrà ricevere domani.
Per stasera il mio piccolo appartamento sembrerà più grande.

(*)

REF.
http://www.cs.yale.edu/homes/tap/Files/ada-bio.html
 

Strumenti
http://www.usafa.af.mil/dfcs/bios/mcc_html/adagide.html
http://www.eng.auburn.edu/grasp/

Compilatori
http://www.adacore.com
http://www.rrsoftware.com
http://www.aonix.com

(*) Augusta Ada Byron King Lovelace 1815-1852

