[title: il morso sulla nuca]

L'animale selvaggio annusa l'aria  
aspira a fondo l'ormone vacuo.  

Distante è la femmina pronta  
che disperde quei vapori al vento.  

Dovrà correre, combattere e ferire  
meritarsi il premio da mordere. [m]  
