[title: amore e morte]

Amore e morte, indissolubili momenti.  
Nella caduta dell'uno nell'altra e nella sublimazione dell'altra nell'uno si concentra il _sentire._  
Quella passione inumana nei gesti ma profondamente nostra nella voglia spasmodica di annullamento, di dimenticanza.  
La _dimenticanza_ del reale, della materialità scomoda e della dimensione di un universo che percepiamo nella sua grandezza ma che al contempo ci rende consapevoli della nostra nullità.  

Siamo arroganti e piccoli animali senzienti, capiamo e non vogliamo capire.
Affoghiamo in un timore coraggioso, in acque torbide e spesse. Inaliamo quei fumi densi che non ci fanno pensare.  

La ragione, è il _nonsense_ che lega l'animale al pensiero eletto.  
Che lo si voglia o no, noi moriamo continuamente camuffando il processo in molti modi. [m]  

