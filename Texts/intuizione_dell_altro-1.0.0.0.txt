[title: l’intuizione dell’altro]

Volere il bene dell’altro non è mai stupido, come il dedicarsi o l’appartenersi.   
È una questione di compenetrazione e complicità, il donarsi. Solo le cose forzate prima o poi si frantumano per quanto a lungo si riesca a farle durare e non è questione di amore o affetto. Si può amare qualcuno e forzatamente conviverci o unirsi, lo si ama anche perdutamente ma mancherà sempre la fusione più profonda, quella che fa di due corpi uno solo.  

L’intuizione dell’altro, che è una cosa meravigliosa e oltrepassa lontananza e tempo. [m]  
