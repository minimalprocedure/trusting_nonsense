[title: delle cose passate]

Quando rincorro le cose passate
le seguo a mani aperte, agitando le braccia.

Le chiamo sorde ai suoni, secche alle lacrime
mi girano intorno ridendo, spingendo.

Barcollo cercando nel mucchio qualche viso
nel mosso confuso di quelle membra.

Vengo innalzato verso l'alto
dove mi lascio finalmente cadere. [m]
