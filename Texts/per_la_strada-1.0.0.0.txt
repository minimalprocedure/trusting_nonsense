[title: per la strada]

Quando cammino, è li per la strada  
che guardo fuggire le cose intorno.  

Le seguo e le osservo, loro scappano  
passano e poi vanno e così si perdono.  

Alcune lasciano pochi piccoli ricordi  
altre si dimenticano, sono mai vissute. [m]  
