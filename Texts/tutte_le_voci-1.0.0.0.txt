[title: tutte le voci]

Sono nelle voci che ascolti
nel tono, nell'aria emessa.  

Flutti di racconti di esseri
animali volanti e visi piangenti.

Nelle voci che ascolti, le parole
sensi smembrati e distrutti, persi. [m]
