[title: macinato di carne, ovvero il tumblero medio]

Nei libri prendendo frasi a caso
le monti e cambi facendole tue.

Dei sapori rifatti e poco caldi
ti fregi vantando mente eccelsa.

Macinato di carni, molli e grigie
nella flatulenza del fiato perso. [m]
