[title: Fe.]  

Ti provocherei ogni ora del giorno, per farti bagnare.  
Ti coccolerei poi dopo, per farti riposare.  

Ti racconterei tutte le storie alla sera, per farti addormentare.   
Ti abbraccerei, tutte le volte che sei triste.  

Ti tenderei la mano per afferrarti e portarti a me. [m]  
