[title: al parco]

Ci sono i padri e anche le madri  
nei loro cuori i bambini ridenti  

guardano giocare, seduti in disparte  
alcuni piangono, si corre ad asciugare.  

Qualcuno parla, una mamma è triste  
è il suo bambino che manca al gioco.   

Vicino, i ragazzi tra le mani e forse  
padri e madri di un tempo che rinnova. [m]  
