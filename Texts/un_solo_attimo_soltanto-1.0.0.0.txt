[title: un piccolo attimo soltanto]

Vorrei vederla per un momento,
un piccolo attimo soltanto,

anche intravista o incrociata
sfiorando le mani su una strada.

Girandomi al suo viso:
- Mi scusi, non volevo, ma sono qui per baciarla. - [m]
