[title: passandoti a prendere]  

Racconto delle molte tristezze  
quelle fasi vissute e passate.  

Di un bimbo bellissimo quasi morto  
e dell'essere da soli nel mucchio.  

Sai, a volte che ci penso, partirei  
verso lontano e passandoti a prendere. [m]  
