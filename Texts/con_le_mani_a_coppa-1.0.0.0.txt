[title: con le mani a coppa]

Se mi chiudessi tutti gli occhi
con le mani appoggiate a coppa

guarderei il mondo tra le dita
attraverso tagli di luce intravista.

Sarebbe sia chiaro e un po' scuro
a tratti nascosto e solo immaginato. [m]
