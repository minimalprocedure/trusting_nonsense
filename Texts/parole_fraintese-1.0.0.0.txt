[title: parole fraintese, occhi altrove]

È davvero tutta polvere?  
Terra secca e dopo sfatta?  

Di un grande amore poi arido  
parole fraintese, occhi altrove?  

Quei passi sempre più tristi  
di una vita d'insieme e vuota. [m]  
