[title: diventa il mio libro]

Non dirmi mai come sono  
fammelo vedere davanti.  

Raccontami le mie lacrime  
fai storie dei miei dolori.  

Riempi fogli di tutto quel voler morire  
che ogni tanto ho, senza farci caso.  

Diventa il mio libro,  
quello che non finisce mai. [m]  
