[title: la farfalla]

[anomimo]
Che brutta fine fanno le farfalle, povere.

[m]
È vero, il lepidottero può vivere poco alcune però oltre l'anno.

La farfalla nella mia testa è però splendida, lei vivrà a lungo perché ogni volta tornerà nuova. Lei è come l’amore che può rinascere e rinascere ancora per ogni giorno, ogni 24 ore se lo si vuole. La farfalla è simbolo di rinascita continua a fenice di sé stessa.

La mia farfalla è bella, bella da rimanere incantati come fosse un momento fisso nel tempo, quel suo volare e il battere leggero e pulsante delle ali. 
Il suo poggiarsi sul dito della mia mano, quel suo guardarmi fisso negli occhi.

La mia farfalla è delicata e forte, è meravigliosa come poche cose al mondo.

Lei rinascerà continuamente come il rinnovarsi della mia meraviglia ad averla intorno.

Questa è la mia farfalla. [m]
