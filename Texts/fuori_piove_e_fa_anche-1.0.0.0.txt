[title: fuori piove e fa anche]

Fuori piove e fa anche abbastanza freddo.

Mi piacerebbe passare la serata accanto a un grande camino acceso, ascoltare musica tranquilla con qualche vecchio amico.
Mi piacerebbe guardarti, mentre parli, leggermente arrossata dal calore e dal vino, che ti sposti i capelli con una mano.
Guardarti, mentre qualche “amico marpione” ti fa un poco di filo, pensando che non me ne accorga.

Ti vedo, guardarmi ogni tanto, velocemente, mentre parli.
Ti vengo a prendere, poggiandoti le mani sui fianchi.
Ti bacio, annullando tutto l’intorno.

Dissolvenza e non c’è più nessuno.

Solo noi, avvolti nel piumone davanti al fuoco. [m]
