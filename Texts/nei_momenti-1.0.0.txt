[title: nei momenti]

Nei momenti,
quando tornano i ricordi.
Quando a volte vorresti partire.

Mancano i sogni di ogni volta
per la storia che si ripete. [m]
