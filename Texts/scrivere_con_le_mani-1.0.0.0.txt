[title: scrivere con le mani]

Scrivere con le mani
con le dita per seguire le parole
descritte alla pelle come seta antica.

Con penello e inchiostro
a dipingere il tuo seno
di segni che raccontano,

di linee unite a significare
dell'amore provato e delle storie,
dei pensieri donati.

(Scrivo su di te, 
 delle cose che voglio e della tua bellezza, 
 per leggere e rileggere ancora) [m:1996]
