[title: il tuo viso sopra di me]  

Vorrei guardare il tuo viso, mentre sei sopra di me.  
Mentre mi togli le mani dai tuoi fianchi.    
Mentre mi dici di portarle dietro la testa.  

Vorrei osservare il tuo corpo nudo, mentre sei sopra di me.  
Tu che ti muovi, tu che mi usi.  
Tu che ti dai piacere, sopra di me. [m]
