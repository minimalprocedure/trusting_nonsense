[title: un mondo tutto pieno]

Sai, c'è un mondo tutto pieno   
in quello che potrei forse dirti.  

Completo dei molti modi da non dire  
così pregno, di tanti modi da non fare.  

Sono ospiti accolti nel greve pensiero  
slegati e in cerca di uno stretto nodo. [m]  

