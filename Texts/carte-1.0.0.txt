[title: carte]

Le carte che si girano  
posate in fronte mi dicono di te.  

Nei loro intrecci mistici  
di Mago e di Bagatto.  

Mi insegnano la via  
e di come prenderla.  

Scusami,  
ma io a volte, muoio a poco a poco. [m]
