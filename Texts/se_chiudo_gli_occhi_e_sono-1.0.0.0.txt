[title: se chiudo gli occhi e sono]

Se chiudo gli occhi e sono senza meta,
e non cammino per non alzare polveri.

Guardo da lontano, senza fiato,
senza respiro alcuno né lacrime agli occhi.

Mi volto per partire, nel ritorno.

La nebbia cala, umida al viso
nel respiro denso mi rifugge il calore. [m]
