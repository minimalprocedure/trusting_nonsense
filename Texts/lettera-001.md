[title: lettera]

Donna,

in questo giorno che volge al termine, in queste giornate più corte dell'inverno, voglio scriverle.  
Le mie dita così artritiche della parola scritta, si muovono incerte in un uso così dimenticato della penna. La carta mi pare così ruvida, il pennino che scava.  

Sa, una volta scrivevo molto e a lungo, ma oggi tutto è così troppo veloce.  
Manca l'attesa, lo sbirciare il postino dalla finestra. Il suo controllare nella borsa, il suo salire e scendere dalla bicicletta. Il suo spingerla per arrivare quassù, in cima alla salita.  
Ho pensato molto le confesso, al nostro incontro. Così fugace, il caffè, io amaro e lei? Sa che non ricordo, perduto come ero nei suoi occhi. Che figura da ragazzino che ho fatto, nelle mie poche parole così balbettate.  
La stretta di mano, il nostro _buongiorno come sta?_.  Lei è molto più bella così che _in vitro_, le sue fotografie non le rendono giustizia.  

Il tempo è passato in un attimo, è stato molto o poco? Non le saprei dire, nella mia testa solo confusione e parole.  
Chi di noi poi ha scelto il tavolo? Forse, penso che il cameriere abbia intuito la situazione, dandoci quello nell'angolo. Forse ci ha favoriti, forse...  
Sarà stato l'imbarazzo velato, quel darsi solo la mano, il desiderio di abbracciarsi o baciarsi subito.  

Le buone maniere, che ci vengono insegnate fin da piccoli. Inculcate a suon di sgridate sonore. Quelle ci fanno dare la mano invece di saltarci addosso.  
Siamo grandi, siamo adulti e dovremmo comportarci in maniera adeguata. Lei invece, non vorrebbe correre almeno una volta su un bagnasciuga pieno di gente, nuda? Non vorrebbe far l'amore su una piazza affollata? Bloccare l'ascensore dell'ufficio per potersi guardare fissi?  
Siamo grandi, siamo adulti. Sono tutte parole che ripetono tutti.  

Vorrei vivere in un sogno immenso, dove ognuno dorme e si sveglia e dove tutto è possibile. Correre a braccia aperte, a occhi chiusi scontrarsi e cadere ridendo.  

In questo mondo così buio di fumo e nero, lei riluce.  

Uomo.


