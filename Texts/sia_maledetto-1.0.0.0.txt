[title: sia maledetto]

Sia maledetto ogni centimetro di me
tutte le parti, dalla testa ai piedi.

Le tante voci, tutti quanti i gesti
urlatemi voi nelle orecchie, che io senta.
                                             
Spingete a grossi colpi, tutti insieme
lasciate che possa soltanto parlarmi. [m]
