[title: il mio lusso]

Il mio lusso è il tuo bagnato  
il tuo umido fiorire filante.  

L'odore dolciastro delle serate  
e del mischiarsi degli umori. [m]  
