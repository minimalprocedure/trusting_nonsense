[title: l'invito]

Accompagnami, scendo di sotto  
le scale portano nell'oblio.  

Là dove tutto si dimentica  
dove a soli nell'universo, noi.  

Le dita tra le dita,   
gli occhi tra gli occhi.  

Le braccia tra le braccia  
le gambe tra le gambe.  

I pensieri tra i pensieri  
noi tra di noi, assolo del mondo. [m]  
