[title: l'attesa e l'assenza]

Il divenire dell'attesa, quel momento
quando si pensa ad un dopo probabile.

Si disegna con le dita il contorno
la mano che volge in aria, un tocco sulle labbra.

Il bacio al pensiero che si soffia via. [m]
