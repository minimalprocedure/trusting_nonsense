[title: camminare]

Camminare,
negli sguardi titanici dei mondi intravisti.

Negli elementi versati e voluti
nelle linee d'ombra mi siedo e mi lascio. [m]
