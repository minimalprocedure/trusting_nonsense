[title: M.]

Nostalgia di chi non hai vissuto
così possibile e innaturale.

Sapore del cibo mai mangiato
di dolce, salato e piccante.

Ottimo, nel tuo bel ricordo vago
del lasciar perdere senza tentare. [m]
