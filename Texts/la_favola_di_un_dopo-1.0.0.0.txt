[title: la favola di un dopo]

Nella camera, aspetti ma è tardi  
ancora non arriva, lui non è qui.  

Pronta, devota a quello che viene  
al centro di un niente e di vuoto.  

Sola, seduta e cieca al mondo   
attendi, la favola di un dopo. [m]  
