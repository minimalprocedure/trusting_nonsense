[title: muterà in ogni tempo]

**Muterà in ogni tempo**   
nella mente, nel cuore e nel pazzo  
il giullare che saltella avido  
per estorcere risa e scherno.  

Nelle corti del tempo  
il re assopito gioca con la sua regina  
ne assapora gli umori e ne liscia i capelli.  

Con mano poggiata raccoglie i suoi tremori.  

(Il giullare stanco si allontana senza riso né pianto,  
abbandona la stanza vuota, coi reali assopiti) [m]  
