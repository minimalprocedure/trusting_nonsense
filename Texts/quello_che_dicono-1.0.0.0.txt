[title: quello che dicono]

Raccontano di me, cose nascoste
miscugli e frammenti presi in giro.

Mi fanno dire cose non dette
pensieri non pensati, sguardi non dati.

Lasciano nel mucchio delle altre facce
come mele nel sacco, la mia testa bacata. [m]
