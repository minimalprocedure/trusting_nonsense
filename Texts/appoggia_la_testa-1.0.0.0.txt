[title: appoggia la testa]

Fammi essere la tua stanza
le quattro pareti che ti raccolgono,

la sera, quando stanca ti lasci andare
avvolta nelle lenzuola chiare,

nei tuoi sonni agitati e quieti.

Appoggia la testa su di me
come un tuo cuscino. [m]
