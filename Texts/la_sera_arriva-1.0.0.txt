[title: la sera arriva]

la sera arriva,
e nei pensieri che seguo,
nella musica dei canti...

quello che vedo,
quelle labbra che ho visto...

Lascio un bacio volare,
dove i mostri,
ormai non albergano...

un sorriso,
mi sfugge mentre cammino...