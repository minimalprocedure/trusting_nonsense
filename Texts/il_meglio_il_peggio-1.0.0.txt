[title: il meglio il peggio]

Il meglio, il peggio, il medio, l'alluce e l'anulare.

La sveglia suona, al solito fastidiosa, sarei rimasto volentieri a letto oggi, ma tant'è che mi alzo lo stesso.

Si fa colazione.
Si finisce la colazione.
Si lava il corpo e la mente
Si porta fuori il cane.
Si prende il treno.

Si pensa.

Gestire le persone è complicato, formalmente si commettono sempre errori, "la cosa giusta" è sempre in agguato, ma come un esperto agente segreto non si trova mai. La vita è complicata o forse talmente semplice che non ci si aspetta e: - Cristo!!! non può essere così semplice! -, poi si passa alla teoria del complotto.

Oggi poi, viviamo due vite insieme, reale e virtuale, affetti da personalità multiple si incrociano relazioni e amicizie i cui unici figli sono matrici numeriche o trasformazioni di Fourier.

Come navigatore esperto, la mia età me lo permette, sono talmente "antico" da avere ancora un modem a 9600 baud a casa, un coso grosso come una scatola da scarpe numero 46, da essere promosso Ammiraglio di Fregata, cazzeggio, se mi passate il termine, a varie riprese e da anni su chat, forum o come si chiamano adesso community. Ho visto passare protocolli "comunicazionali" di più o meno tutti i generi, alcuni anche da me ideati, e il brodo riscaldato di idee vecchie rimesse a nuovo, lo si fa per tutto perché non qui, tanto le generazioni cambiano e gli adolescenti prepuberali si sa, rifiutano la storia per principio conflittuale con i padri.

La storia si ripete, quindi, ieri irc, che molti confondevano e probabilmente confondono ancora con mirc, che altro non è che un semplice client per il protocollo irc, poi ICQ, oggi chattiamo con MSN Messenger, Yahoo messenger, Google talk e a seguire (ce ne sono parecchi ancora, e parecchi ancora arriveranno). Una sfilza di figurine nella nostra finestrella in alto a destra o in basso a destra, contatti avuti o rubati, vivi o morti (perlopiù morti) residui dei passaggi virtuali di esseri che si stenta a volte a identificare come persone ( non vi dico la mia lista, dato che uso un client multiprotocollo di avatar che mi ritrovo. collezionati nel tempo, cadaveri virtuali per lo più).

La ricerca è affannosa, tutti cercano tutti, si instaurano amicizie e ci si scambia tre parole, ma la maggior parte del tempo poi si passa a litigare.

La lingua, di cui scarseggia la padronanza, è già complicata quando gli occhi si vedono, quando chi hai davanti ti parla mentre mangia un panino sbrodolante di salse, quando il corpo che vedi o che tocchi irradia significati, da risultare incomprensibile in poche righe di chat o sms o messaggetto scambiato. Il tempo passa per spiegarsi a vicenda che non si avrebbe voluto dire quello o quell'altro.

TVB, TVBUS, MaKekAzZo, faKOff... non ho parole, mi manca il sentimento per esprimermi, mi verrebbero i crampi alle mani, ma forse ho solo una artrite deformante in stadio avanzato e certe acrobazie tastieristiche non mi riescono proprio.

Amici, ogni community promette amici, mi sforzo di essere amico di una pecetta di 32x32, ma ci metterei la mano sul fuoco? oddio, virtuale magari sì.

Il mio intento è stimolare ed essere stimolato, perché la mia mente ha bisogno di cibo ed è perennemente affamata, quindi ringrazio chiunque mi stimoli, amico o non amico o solo piacevole conoscenza. Gli altri non so...

Purché il suo italiano sia almeno decente. [m:2010]

PS: Il titolo ha un significato e per chi lo svela, ricchi premi.
