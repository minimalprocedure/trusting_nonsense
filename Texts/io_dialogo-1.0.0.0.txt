[title: io dialogo]

**[anonimo]**  
io sono difficile e creo problemi.  
io sono solare.  
io ho avuto un periodaccio e l'ho risolto, da sola.  
io sono bella.  

**[m]**  
Io ieri in treno col telefono avevo scritto pareccchi "io...", ma all'invio si è perso tutto.  
Io ero incazzato.  
Io sono allo studio ora con una tastiera usabile.  
Io sono difficile ma cerco di non creare problemi agli altri.  
Io sono assoluto e di principio.  
Io non ho mai picchiato nessuno e nessuno ha mai picchiato me.  
Io sono alto 1,81 ed ero atletico.  
Io mi tocco il naso con la lingua.  
Io ho il cervello sempre in movimento.  
Io ero bello, da girarsi e dire 'che fico' (specialmente quando giravo in moto).  
Io non sono bello adesso, ma chi se ne frega.  
Io ho letto molto, leggo molto ma a periodi no.  
Io sono intelligente e altezzoso.  
Io me la tiro.  
Io sono sensuale, quando voglio.  
Io il sesso lo faccio se merita.  
Io non sono un uomo medio.  

**[anonimo]**  
io non mi innamoro da secoli.  
io voglio solo delle braccia che mi stringano.  
io non ho molto tempo, ecco perchè voglio tutto.  
io voglio rispetto.  
e basta ;-)  

**[m]**  
Io voglio rispetto.  
Io sono titanico.  
Io vivo una volta al giorno.  
Io amo distruttivamente quando amo.  
Io bivacco nella vita.  
Io mi ricordo cosa hai scritto se vuoi te lo ricordo.  

**[anonimo]**  
oh, adesso ricordo.  
ma ci sono troppi IO per miei gusti, ora.  
io non sono egocentrica ;)  
ma sono fiera e passionale. e sai cosa? me ne vanto.  
basta parlare di me.  

**[m]**  
Basta parlare di te.  
vuoi parlare?  

**[anonimo]**  
si  
