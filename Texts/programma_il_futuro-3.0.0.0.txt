[title: programma il futuro]

Mi è arrivata la newletter di Programma il Futuro: Newsletter ottobre 2016  
http://programmailfuturo.it/index.php?option=com_acymailing&ctrl=archive&task=view&mailid=18&key=4c825b89a005825ca1b63efdcf8e5361&subid=4919-360ce270b855df0ebd8ba0badadd3376&tmpl=component&acm=4919_18


Vediamo cosa c'è di nuovo. 

Si è conclusa felicemente la settimana del codice e siamo tutti contenti.

Da lunedi 7 a venerdì 15 novembre il concorso Bebras si terrà, è gratis ma ci si potrà iscrivere fino al primo novembre.
(Ricordo agli insegnanti che volessero partecipare che per iscrivere i ragazzi minorenni occorre una autorizzazione dei genitori)

La novità del 2016 è che Programma il Futuro consiglia di seguire il corso: Principi dell'informatica di Code.org.
Dice che il corso è indirizzato alla Scuola Secondaria di SECONDO grado.
SECONDO grado ovviamente ce lo urla (per chi fosse a digiuno di netiquette, scrivere in maiuscolo significa URLARE).
Dice che questo corso negli Stati Uniti fornisce crediti universitari, in Italia si stanno organizzando.
Guardando al volo, mio figlio in prima media potrebbe farlo. Penso che gli farò prendere questi crediti un po' prima, come dire: impara l'arte e mettila da parte.
(nella lezione 7 della Unit 4 si dice: Public Key Crypto. Questa mania di scorciare le parole ha portato Application ad App e Cryptography a Crypto. Si è produttivi solo se si risparmia tempo anche nel parlare e scrivere. Io abolirei le vocali già che ci siamo.)

Finalmente abbiamo il braccialetto dell'amicizia che ne sentivamo la mancanza. Manda messaggi segreti alle amiche (non è sessismo, parla proprio di: amiche) e insegna i primi rudimenti della programmazione alle ragazzine (idem cui sopra). Due programmatrici (idem cui sopra) lo hanno realizzato. Sul sito si dice che è Open Source, forse schemi e codici sono davvero Open, infatti non li ho trovati da nessuna parte. Più che Open direi Volatile.

TIM, mecenate e fondatore di Programma il Futuro promuove il pensiero computazionale a Milano il 18/10, Bologna 20/10, Catania 5/12, Roma 6/12. Per alcune date si consiglia la macchina del tempo.

Finalmente in Libano si è scelto di insegnare il coding ai rifugiati siriani. Probabilmente ne sentivano la mancanza. Da notare che non verrà usato Scratch, ma il corso sarà affrontato in javascript. Non sia mai che questi arabi imparino la nostra miglior tecnologia.

In Canada si discute se introdurre o no il coding nelle scuole e siamo tutti contenti.
Ai contadini del New Mexico un progetto insegnerà il coding, lo farà la "migliore startup dell'anno" con un progetto chiamato: Cultivating Coders. Dopo interramento e concimazione spunteranno Coders come i funghi.

Negli Stati Uniti il numero delle scuole di coding registra una crescita: 74%. L'immagine di Newsweek da dove è presa la notizia ci propone un codice che linguaggio di programmazione non è. Il "Cascading Style Sheets" o CSS è un sistema dichiarativo per definire il formato, lo stile grafico e tipografico di una pagina web.
Nello stesso articolo si mischiano linguaggi di programmazione e framework, ma insomma "famo a capisse".
