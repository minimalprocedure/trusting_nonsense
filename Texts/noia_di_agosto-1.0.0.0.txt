[title: noia d'agosto]

La noia, si dipana in flutti che vanno e vengono
librandosi in onde di tortuosa voluttà.

La noia, compagna di un ozio lascivo
che ci fa assaporare quello sforzo sudato

di una mano che rovista tra le gambe. [m]
