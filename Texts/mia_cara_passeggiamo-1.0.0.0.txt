[title: mia cara, passeggiamo?]

Passeggiamo sul ciglio del fiume, vuole?  
Chissà dove porterà, verso laggiù in fondo.  

Chissà da dove viene, dove ingrossa l'acqua  
dove, partorito dalla montagna e figlio ingrato  

scappa veloce e irrequieto poi casca più volte.  
Dove accoglie gli altri, come facce incontrate. [m]  


