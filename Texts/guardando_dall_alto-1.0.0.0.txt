[title: guardando dall'alto]

Lei guarda chi tarda, scruta orizzonti lontani
aspettando dalla curva del mondo, il moto.

Un segno lontano, faticoso, avvicinarsi
dentro strade impervie, buca dopo buca.

Arriverà, sporco e stanco, il punto lontano
a piedi e senza cavallo, nero e non azzurro. [m]
