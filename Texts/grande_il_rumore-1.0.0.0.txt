[title: grande è il rumore]

Col bastone in mano, batto, colpisco
il tamburo enorme tutto a due mani.

Grande è il rumore che perfora il capo
sale e innalza oltre il cielo nero.

Nella notte dei demoni che danzano,
nella morte dei corpi, arsi dal fuoco. [m]

