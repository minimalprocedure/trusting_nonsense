[title: al tempo di una canzone]

Quando mi vestivo la sera
nei lacci di cuoio per il ballo.

Nella mischia della gente
nel mezzo una donna mai vista.

Il bacio della pista durato
tutto il tempo di una canzone. [m]
