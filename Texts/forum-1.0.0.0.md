[title: forum]

Questa cosa del _presentarsi_ nei _forum_, _gruppi di discussione_, _chat_, _canali_, _bbs_ e tutta la seguente _riscoperta della ruota_, mi ha fatto sempre l'effetto da riunione da _alcolisti anonimi_ del venerdì sera.  

Viene poi da chiedersi perché si fa il venerdì sera?  

<< ZuZZer3ll0n3, icché tu lo sai perché? >>  
<< Boh!?! Che di dico... >>  

Sarà perché gli anglosassoni vengono pagati a settimana il venerdì e così gli impediscono di sbronzarsi di birra?

Comunque, sarà per quel che sarà, la sensazione è quella e indi per cui non mi presenterò.

<< Guarda che essere subito antipatico è presentarsi. >>  
<< Uhm! Mi sa che hai ragione! >>  
<< Ecco bravo, l'hai capita. >>  
<< È sempre la faccenda dei neuroni in fila indiana... >>  
<< In fila indiana? >>  
<< Si e quello dietro mette i piedi nelle orme di chi è avanti. >>  
<< Perché? >>  
<< Così sembrano di meno! >>  
<< Ah! Non mi pare una bella mossa. >>  
<< Dici? >>  
<< Dico. >>  
<< Sarà... >>  
<< Certo. >>  
<< Certo che? >>  
<< Parlare con te è inutile... >>  
<< Sai che ti dico? Fanculo! >>  
<< ... ma fanculo a te! >>  
<< Vabbè ciao. >>  
<< Ciao. >>  
<< A venerdì? >>  
<< Ok! A venerdì... >>  
<< Mi raccomando non bere! >>  
<< Figurati! Solo aranciata... >>  
<< Però non farla fermentare fuori dal frigo. >>   

[m]


