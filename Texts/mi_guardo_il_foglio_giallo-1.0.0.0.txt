[title: mi guardo il foglio giallo]

Mi guardo il foglio giallo
ne leggo le parole, seguo segni,
ne mimo con la mano il suo andante.

Di quei due piccoli visi,
che assommano ricordi,
che ammiro adulto per un sogno d'amore.

Ricordi, così appena avuti, come sempre stati,
e mi ritrovo nella sera a pensare a lei,
signora incontrata vissuta da sempre.

Mi siedo, mi giro e la vedo, 
lì mi guarda, lì mi sorride,
e mi avvicino nel sogno
un po' triste del tempo perduto.

"Vieni con me, e fermiamo il tempo"
e le mie mani tese a lei.
"Vieni con me, per costruire un mondo"
 
"A te donate, parole e pensieri, 
per avvolgerti di momenti" [m]