[title: seduzione]

Oggi è il 21 giugno e l'estate è già arrivata, pare alle quattro di stamani.  
Al solo pensarci fa più caldo, non trovate?

Cosa accomuna tutte le estati che dio mette in terra per tutti quanti? Un aiutino?  
Ve lo dico subito così evitiamo di perdere tempo: la seduzione estiva.  

Si, la seduzione da spiaggia è quella cosa unta e bisunta di olio di cocco che più o meno tutti sfoggiano impanati di sabbia. Quei corpi secchi e un po' tirati color terra di Siena bruciata delle signore bene di mezz'età, quelle che a volte sfoggiano anche lo zigomo a balcone col labbro proteso e decisamente a pattìno. Quel labbro roseo di burro di cacao dalla protezione mille.  
Poi c'è anche il lui rifatto, di soldo e di fisico, il palestrato cinquantenne dall'auto sportiva e il portafoglio pieno, quello con l'orologio a cipolla di Cannara sfoggiato al polso. Di questi confesso che non ho mai capito come mai sotto l'orologio a ombrellone riescano a essere abbronzati. Sarà mistero della fede.  

Li vedi, da un lettino blu all'altro lanciarsi sguardi ammiccanti colmi di promesse. Li vedi sotto il sole a schioppo dal passo lento sul bagnasciuga passare e ripassare e ripassare e ripassare e ripassare e ripassare, finché finalmente la matrona mattone non si alza per un meritato bagno.  
Col passo leggermente ondulante da cervello bollito nel brodo, lei prima si bagnerà i piedi poi si passerà lentamente le mani umide sulle gambe così con noncuranza, indugiando nell'interno coscia. Questo accenderà lo sguardo volpino di lui che con una rapida occhiata valuterà la più o meno abbondanza di pelo sotto la mutanda.  

La lenta entrata in acqua di lei, quella caduta scivolata nel brodo primordiale col mezzo tuffo. La risalita dall'acqua al ginocchio e la mano che sposta il capello indietro. Lui dal bicipite tirato, col tuffo virile e schizzo inevitabile.

<< Mi scusi! >>  
<< Si figuri! Ero accaldata... >>  

Il ghiaccio è rotto.

La seduzione, questa antica arte del gioco delle parti che oltrepassa la mera vista e sconfina negli inconsci profondi. Si nasconde nel gesto impercettibile, nella parola precisa e nel significato di uno sguardo. Non ha tempo o ne vive al di fuori ed è nell'atto estenuante della sua preparazione, un balletto di passi con il mondo fuori così pieno di voci che non si sentono più. Ci si gira intorno fissandosi, mentre si aggiungono piccoli nodi a quei lacci sempre più stretti.  
Nel tirare e rilasciare dove il pescatore ha il pesce all'amo ma anche il pesce potrà tirarlo in acqua.

La sedurrò, nonostante il tutto  
che lei sia ancora libera o meno.  

Senza toccarla e senza porgerle mani  
la inviterò gentile in questo bieco gioco.  

Saremo ambedue perdenti e vincitori  
che il tempo non conta, lui non esiste. [m]  
