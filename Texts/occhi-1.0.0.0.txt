[title: occhi]

Sono occhi che guardano occhi,
occhi che distolgono lo sguardo.

Occhi che si bagnano ad una musica
occhi asciugati da fazzoletti bianchi.

Occhi che si dicono anche l'immenso
occhi, che non possono più vedere. [m]
