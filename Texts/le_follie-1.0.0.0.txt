[title: le follie]

Le follie.

Nel circo arcano stanno gli animali dei sogni  
ruotano al galoppo saltando corde intrecciate.  

Gli acrobati, si lanciano impavidi e senza la rete  
da un trapezio all'altro, cantando forte alla luna.  

Il pagliaccio, inciampa ridendo col suo fiore a spruzzo  
per quei tanti bambini ridenti che si danno gomitate.  

Chi si spara col cannone, imbracciando mazzi di fiori  
petali bianchi che ricadono, come neve al sole.  

Le follie della funambola sulla corda in equilibrio  
scoperta lassù in cima e da guardare un po' sotto.  

Alla fine, è il signore col cilindro dal vestito di rosso:  

<< Signori e Signore, bambini e ragazzi, lo spettacolo è finito >> [m]  
