[title: sei il mio strumento]

Mi porterò le bacchette, per suonare
pizzicando sul seno indurito dal tocco.

Da cassa armonica alle tue labbra, 
il roseo organo vibrante di musiche.

Il fiato e il respiro accompagneranno
quel battere del fianco incalzante. [m]
