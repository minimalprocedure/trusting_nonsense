[title: da quando non ero]

Talvolta vorrei potermi sedere  
così comodo, piano piano iniziare.  

Partire da quando ancora non ero   
dirti mia bella, delle cose vissute.    

Annoiarti, fino a farti scappare   
fino a che non rimanga niente. [m]
