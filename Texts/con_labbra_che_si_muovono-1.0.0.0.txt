[title: con labbra che si muovono]

Le coppie dei miei racconti
si amano, parlano e guardano.

Gli amori intensi si abbracciano
come legati di corde annodate.

I visi si raccontano con gli occhi
hanno parole da dire con le labbra. [m]
