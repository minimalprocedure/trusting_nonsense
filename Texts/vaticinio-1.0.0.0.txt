[title: vaticinio]  

Pezzetti di legno, su pezzetti  
ceneri e ossa lanciati alti.  

Quelli che cadono nel cerchio  
dicono del futuro che viene.  

Dicono del passato già andato  
e del presente che se ne va. [m]  
