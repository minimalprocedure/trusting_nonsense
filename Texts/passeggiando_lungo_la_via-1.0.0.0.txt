[title: passeggiando lungo la via]

Passeggiando lungo la via,
nel freddo del tiepido invernale
verso un posto che non si conosce,

si cerca così poco.

Un calore represso, 
tutto quello che si può dare. [m]