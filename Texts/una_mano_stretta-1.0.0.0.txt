[title: una mano stretta]

Una mano stretta dove la vita nasce
nel sonno di un abbraccio notturno.

Un lungo bacio angoscioso e legato
da catene al crepuscolo. [m:1985]
