[title: la musica è finita, alzate le mani]

Scrivere poesia al tempo di una canzone   
le mani veloci, la mente aperta e pronta.   

Solo pochi minuti di un mondo annientato  
dove esisti solo tu, poi le tue parole, te.  

Il tempo passa e scrivi veloce, gli occhi  
da cui una lacrima scende, al terminare.  

La musica è finita, alzate le mani. [m]
