[title: si vorrebbe fosse l'ultima]

Le scende una lacrima rigando la guancia
scivola dall'occhio, di lato.

Con un gesto la raccoglie con un dito
portandoselo alla bocca toccando le labbra.

Sperando sempre che sia l'ultima. [m]
