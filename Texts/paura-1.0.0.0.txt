[title: paura.1.0.0]

Paura, 
di chi non vedo.

Paura, 
di chi non sento.

Paura, 
nella ricerca dei pezzi non miei o tuoi.

Paura, 
nelle maschere che mi mettono davanti.

Paura, 
nelle verità che non mi si crede.

Paura, 
negli amori per troppa fretta perduti.

Paura, 
di vedere e di forza nel sentire.

Paura, 
di fuggire e di restare.

Voglia,
di sogni non comuni.

Voglia, 
di esplodere di sensi.

Voglia, 
di costruire con le mani. [m]
