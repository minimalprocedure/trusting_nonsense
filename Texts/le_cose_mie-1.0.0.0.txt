[title: le cose mie]

Nello spingerti verso il basso
nell'ascesa del desiderio.

Ti sposto le gambe una all'altra
stringendo i capelli alla tua nuca.

Dilato e scavo con tutte le dita
le parti di te, che sono mie. [m]
