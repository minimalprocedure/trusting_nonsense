[title: il filo sottile]

La lingua batte, giusto in mezzo.
Quel piccolo raccordo che unisce una cosa all'altro
il filo sottile da seguire con la punta umida. [m]

