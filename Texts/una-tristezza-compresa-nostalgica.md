[title: Una tristezza, compresa e nostalgica.]

Porgi i polsi a far che si possa legarli,  
mi siedo, chiudo gli occhi.  
 
Tu sai quando ti parlo  
nell'atto del posarti nel basso.  
 
I tuoi occhi lucidi non solo  
con le mani rudi che passano una all'altra, carezzando. [m:engine]  
