[title: parole al vento]

Parole al vento.

Trascinati nei giochi di altri, nelle loro stupide scaramucce, non sapendo neanche chi sono, accusati di cose mai fatte né mai pensate. E’ triste a volte aprirsi verso chi non ti crederà, qualunque cosa si possa dire o fare.
Ci si sente stanchi a raccontare sempre le stesse cose, cercare di convincere, ma perché poi?

È il periodo delle scuse, mi chiedono tutti scusa ultimamente, chi per una cosa chi per un’altra. Sono talmente tante scuse da suonare strane. Vi siete messi tutti d’accordo? Deve essere una congiura lungo tutto il territorio nazionale con breve escursione all’estero. Magari sono stato davvero bravo quest’anno e gli altri tutti cattivi. Babbo Natale non mi ha portato niente però.

Stamattina sono solo, solo nella mente e solo nel corpo.
Vorrei poter rimanere solo per il tempo a venire,
ma so che tanto qualcuno, busserà alla porta per chiedermi ancora scusa, sarà il Natale...

Aprirò, lo guarderò e dirò: « non preoccuparti, non ce n’è bisogno, non sono così importante. »

Poi richiuderò la porta. [m:2000]

[intanto ascolto Wrong Way Up di Brian Eno e John Cale]
