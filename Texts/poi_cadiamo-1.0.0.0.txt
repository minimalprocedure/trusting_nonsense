[title: poi cadiamo]

Chiama, gioca e balla,
corri, salta e rotola,

corrimi incontro, ti corro incontro
ci sfioriamo nel passaggio con le mani,

poi cadiamo tra di noi lontani, ridendo. [m]