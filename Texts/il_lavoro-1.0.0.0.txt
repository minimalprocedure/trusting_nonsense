[title: il lavoro]

Ma che lavoro faranno i tumbleri?  
A prima vista gente che ha tempo da perdere, ma alla seconda?  

Analizziamo.

Certo hanno a disposizione un computer o solo anche un telefono sufficientemente moderno.
Quelli col telefono potrebbero più o meno essere dovunque, quindi difficile da inquadrare a parte che hanno tempo da perdere.

Quelli col computer.

Per primo direi gli informatici a vari livelli:

1. SEO, ovvero gente che di mestiere fa fuffa, hanno tempo da perdere.  
2. Grafici, quelli si credono artisti e quindi aspettando l'ispirazione cazzeggiano.  
3. Sistemisti, tra un firewall e un ping, rebloggano.  
4. Programmatori. Quelli sono strani, non si sa mai cosa facciano e come e quando.  
5. Gli altri...  

Impiegati del Governo:

1. Nei vari uffici statali si sa che si lavora fitto, quindi rimane molto tempo per il reblog.  
2. Maestre e maestri, durante la ricreazione chi lo sa cosa fanno?  
3. Bidelli e bidelle, frustrati dal lavoro si rifanno almeno l'occhio.  
4. Altri...  

Commercialisti e avvocati:

1. Nell'intervallo tra un F24 e l'altro non ci sta bene qualche chiappa? Negate se vi riesce.  
2. Avvocati, che aspettano la convocazione fuori dall'aula. Ripassano l'arringa.  
3. Segretari e segretarie di questi sopra, tra un caffè e l'altro.  

Commercianti:

1. Quando il cliente scarseggia e il teppistello di turno non si ruba qualcosa.  

Farmacisti e medici:

1. I farmacisti andrebbero messi tra i commercianti ormai, ma tant'è lasciamoli qui. Loro lo spazio lo trovano tra un antibiotico e un antipiretico passando per un analgesico (che con l'anal non ha niente a che fare ma ispira... e reblogga).  
2. Medici, quelli tanto si sa che lo danno e la danno in ogni corsia, pare allenti la tensione.  
3. Infermieri e infermiere a turno con il punto 2.  

Casalinghi e casalinghe:

1. La Soap non tira più e tira di più Tumblr, quindi rebloggano tra il pranzo e i figli a scuola, la cena e i compiti dei figli per la scuola.  

Studenti vari:

1. Loro vanno a lezione...  

Vari ed eventuali.  

Io che faccio? [m]

