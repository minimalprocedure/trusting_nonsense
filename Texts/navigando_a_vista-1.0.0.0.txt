[title: navigando a vista]

Navigando a vista, nell'acqua calma
al buio nell'umida foschia notturna.

Sono i remi che battono sull'acqua
entrando e uscendo, in avanti e poi

all'indietro, come una spinta in cerchio
vaghiamo su percorsi uguali e già fatti. [m] 
