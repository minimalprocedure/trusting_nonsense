[title: Il cuore è una pompa, una stupida pompa che batte anche se sei morto.]

<< Non si ama col cuore, si ama col cervello. >> [m]
