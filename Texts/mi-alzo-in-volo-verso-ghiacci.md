[title: Mi alzo in volo verso i ghiacci,.]

Dalle realtà vicine, sconosciute  
tu sopra di me e dopo sotto.  
 
Lasci alla mia bocca di sentire il tuo umore,  
dipinto a pennello, a strisce bianche e rosse.  
 
Non indossi intimo perché desideri essermi  
dissolta nella rigidità di quell'intorno. [m:engine]  
