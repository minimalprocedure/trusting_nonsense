[title: immaginando]

Muoversi intorno a cose mai viste
solo immaginandole nella loro forma.

Toccare e sfiorare, sentirne l'odore
del caffè che lei beve alla mattina.

Disteso su un letto con lenzuola viola
fresco al passaggio e caldo di lei. [m]
