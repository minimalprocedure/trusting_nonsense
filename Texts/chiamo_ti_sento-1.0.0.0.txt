[title: chiamo, ti sento]

Chiamo, ti sento e immagino.

Vedo la tua mano, ti vedo inumidire.

Ascolto così il fremito
tuo insieme al mio. [m]
