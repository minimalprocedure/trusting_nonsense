[title: il caso delle cose]

Mi lascio andare alle cose portate
come polvere nel cosmo infinito.

Il caso improbabile di quello che serve
nel dirsi quanto ognuno di noi sia.

Nuoto nel vuoto a larghe bracciate
per raggiungerti, mentre galleggi. [m]
