[title: cadere]

Le mani, palmo a palmo muoviamo
a parete di noi stessi.

L'occhio nell'occhio, fisso
segue, lontano e si avvicina.

I corpi nudi che si toccano
sotto l'indosso dei vestiti.

Il pensiero che,
a poco a poco, cade nell'amore. [m]
