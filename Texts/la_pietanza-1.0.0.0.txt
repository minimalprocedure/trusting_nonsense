[title: la pietanza]

Mia bella,
fammi perdere il senno tra le tue braccia.

Dimentico della materia delle cose
fammi conoscere solo un tuo contatto.

Sono pietanza cotta a puntino
pronta da tagliare e mangiare. [m]
