[title: tre pensieri]

Voglio per stasera di un oggi ma ormai domani, mandarle tre pensieri:

1) uno che faccia così semplice incontro  
2) l'altro che sia poco più di un bacio   
3) l'ultimo che forse, sarebbe meglio non dire. [m]
