[title: l'albero mosso dal vento]

Basterebbe certe volte solo sporgere la testa, guardare di poco oltre i soliti vetri. Quelli che andrebbero lavati, quelli che sfumano ogni oggetto oltre di loro. Osservare quell'albero mosso dal vento prima di alzarsi, prima di appoggiare la fronte alla finestra. Prima che sopravvenga il buio della sera e tutto sparisca.  
Prima di tornare a sedersi.  

Si, talvolta basterebbe davvero poco. [m]
