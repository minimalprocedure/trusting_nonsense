[title: il metodo]

Ho scoperto che gli insegnanti di mio figlio hanno un metodo e che io, essendo limitato, non lo comprendo appieno.

Il non correggere bene i compiti o insegnare che le latifoglie non hanno foglie caduche (quindi suppongo che le conifere le abbiano), che il Big Bang è successo circa circa 1,3 miliardi di anni prima delle statistiche ufficiali (compresa datazione della Terra e delle prime forme di vita anticipate di mezzo miliardo di anni) o che l'uva si coltiva insieme al grano in alta montagna (dall'uva fanno direttamente la granita al succo d'uva o il sorbetto di vino, mentre il pane lo fanno già congelato così dura di più) o che cinque centesimi di euro sono 0,5 €, è un metodo di insegnamento.
Anche il mettere tre virgole in una frase di ottanta parole di cui due usate per un inciso di due vocaboli e metterne due soltanto in una frase di centodiciassette parole per una risposta ufficiale è sicuramente un metodo.

Le mie povere maestre delle elementari, questo metodo non lo avevano e sono sicuro che molti maestri non lo abbiano neanche oggi e quindi: - Maestri di tutto il mondo unitevi nel nuovo metodo. - [m:2013]

PS: Si sa anche, che i vertebrati hanno lo scheletro e gli invertebrati no (infatti gli insetti o i crostacei, per esempio, non lo hanno lo scheletro esterno, è solo una corazza posticcia che si costruiscono da soli altrimenti gli rompono tutti le scatole chiamandoli: - Mollicci! -).
Dovrei dirlo al mio vecchio professore di zoologia che anche lui, non aveva un metodo.
