[title: ti condurrò oltre la soglia]

Il mio cammino è verso il cancello
verso il ferro rosso di ruggine.

Nella borsa in una mano porto regali
nell'altra la tua per condurti.

Senza chiavi si aprirà per passare
lasciando le anime fuori, oltre la soglia. [m]
