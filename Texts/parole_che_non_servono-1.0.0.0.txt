[title: le parole non servono a niente]

Mi urlo nelle orecchie, suoni vacui
sussurri delle cose che penso ora.

Forte da far capire senza dubbio
da fugare le paure di perdere.

Non capirò mai, non imparerò mai
quello che accade ogni volta. [m]
