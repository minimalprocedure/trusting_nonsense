[title: i mondi si intrecciano]

I mondi si intrecciano in spirali a noi note collegamenti segreti a cui nessun sospetto cala.  
Si guardano i volti assorti, quelli che si incontrano per strada e si immagina a di loro, un legame. [m]
