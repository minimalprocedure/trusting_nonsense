[title: il bosco e l’albero]

Venga, quell'albero va bene.  

Tolga la camicia, tolga il reggiseno, sfili la gonna mentre la guardo.  
La benderò, perché non voglio che lei veda. Ascolti intorno soltanto.  
Si appoggi al tronco che è ruvido. 
Appoggi la schiena alla corteccia e lasci le braccia senza coprirsi.  
Le passerò dietro e legherò i polsi insieme.  

Davanti a lei poi, le sposterò le caviglie di lato al tronco.  
Legate strette per avere le gambe appena aperte.  
Ascolti i rumori.   
Ascolti i passi di chi le gira attorno.  
Ascolti la voce che le parla di lei.

Lei è così bella, il senso che supera i sensi.  
Ascolti, sono i rumori del bosco intorno.  

Senta l'intagliare di un piccolo ramo.  
Ascolti il sibilo dell'agitarlo nell'aria.  
Ascolti e senta, lo passo sul seno.  
Da sotto lo alzo e lo lascio discendere.  

Ti chiedo: Sei mia?  
Ti chiedo: Quanto?  
Ti chiedo: Come?  
Ti chiedo: Cosa sei?  
Ti chiedo: Chi sei?  

Un piccolo colpo sul capezzolo.  
È così erto e duro che si spinge in fuori, come il _mio_ a guardarla.  
Li tocco con le dita.  

Sono duri.  
Sono pieni.  
Sono turgidi.  
Sono esposti.  

Ti chiedo: Sei mia?  
Ti chiedo: Quanto?  
Ti chiedo: Come?  
Ti chiedo: Cosa sei?  
Ti chiedo: Chi sei?  

Ascolti, ascolti i rumori e senta.  

Senta il rumore del taglio delle calze.  
Senta la lama scorrere, senta il freddo dell'acciaio che le sfiora la pancia.    
Taglio e arrivo al mezzo, al mezzo dove finisce il ventre.  
Senta la lama che taglia la calza, senta la lama che le scivola tra le labbra.  
È fredda e dura, sfiora la pelle e taglia ancora oltre.  
Elastica la calza tira e scopre.  

Ti chiedo: Sei mia?  
Ti chiedo: Quanto?  
Ti chiedo: Come?  
Ti chiedo: Cosa sei?  
Ti chiedo: Chi sei?  

Senta ascolti i rumori.  
Senta il sibilo del nerbo nell'aria.  
Le passa vicino quasi a colpire, al seno e alla pancia, al ventre.  
Ascolti il sibilo, immagini il dolore che potrebbe darle.  

Un colpo, solo uno appoggiato sul ventre.  
Poi le scivolo il legno tra le gambe.  
Ascolti il suo rumore e senta il suo tocco.  

Ti chiedo: Sei mia?  
Ti chiedo: Quanto?  
Ti chiedo: Come?  
Ti chiedo: Cosa sei?  
Ti chiedo: Chi sei?  

Lei è bagnata?  
Lo sento con la mano, lo raccolgo e porto le dita alla bocca.  
L'umore del suo ventre, che mi passo sulle labbra.  
La bacio, ma solo un bacio.  

Ti chiedo: Sei mia?  
Ti chiedo: Quanto?  
Ti chiedo: Come?  
Ti chiedo: Cosa sei?  
Ti chiedo: Chi sei?  

Le dico: Adesso venga, con le mie dita dentro.  
Le dico: Adesso venga e me lo faccia sentire.  

Poi ti chiedo: Sei mia?  
Poi ti chiedo: Quanto?  
Poi ti chiedo: Come?  
Poi ti chiedo: Cosa sei?  
Poi ti chiedo: Chi sei? [m]  
