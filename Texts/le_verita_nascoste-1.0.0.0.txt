[title: le verità nascoste]

**[anon]**  

Sento il bisogno di sciacquare occhi e cervello con candeggina e ammoniaca...  

**[m]**  

Supponendo che la candeggina che chiameremo sistema A, mentre sistema B per l'ammoniaca, siano non interagenti e compresi nei loro spazi di Hilbert, ne andremo a esaminare i loro prodotti tensiorali. Nel caso in cui tali stati siano separabili ne potremmo considerare le loro basi osservabili adeguandone opportunamente i coefficienti complessi e concludendo che se lo stato non è separabile dai precedenti esso è entagled.
Questo formalismo delle matrici di intensità può solo portare a una inequivocabile conclusione, A e B non vanno usati insieme ma anche da soli non è il caso, considerando una probabile contaminazione della superficie di curvatura dell'occhio e della ineluttabile incompatibilità dei principi di realtà e località, lasciando alla completezza tutta una serie di variabili nascoste che è bene non sapere. "Non chiedere quello che non vuoi sapere" racchiude tutto il senso di ciò e a volte vivere nel dubbio è meglio della scomoda verità.
