[title: tutto è calmo]

Tu che mi dici cosa fare
mentre io ne insegno a te.

Gli oggetti che muoviamo
con calma appoggiati sopra.

Scivolano tranquilli su noi
in un gioco, poi finito ridendo. [m] 
