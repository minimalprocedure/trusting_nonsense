[title: il calore]

C'è sole nel campo degli olivi
lo vedo e forse non fa tanto freddo.

Oltre da me, il calore che fugge
piuttosto del mio corpo scalda l'inverno.

Calore ingrato, che facile mi lasci
le mani gelate, le membra e il dentro. [m]
