[title: una donna]

Una donna che si veste con cura  
con mani esperte.  

Che si liscia e pettina.  
Che si alza e cammina.  

Attraverso un palcoscenico vuoto  
spogliandosi al suono del silenzio,  

aspettando un applauso dal fondo  
che non arriva oltre il sipario.  

Un passo, un bottone a passo  
un soffio, vola nel vento una calza.  

Stretta al collo soffoca  
un sibilo su quel corpo nudo.  

Chiuso, un ostrica che apre e chiude  
un sacrificio consumato.  

Con dita abili e veloci, sfiorate da piacere  
finito in un attimo, perso, scivolato lungo in basso,  

gambe tornite e coppe vogliose  
come seni erti, pulsanti e rosei.  

Con calma il vestito sorride di nuovo vivo  
non più cadavere in terra.  

Dal fondo, passi nebbiosi  
si perdono rattrappiti dal freddo,  

dalle quinte qualcuno spenge le luci. [m]  
