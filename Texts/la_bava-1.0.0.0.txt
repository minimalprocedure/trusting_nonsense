[title: la bava]

Di ritorno dall'università, entrai in casa.  
La zuccheriera dimenticata e il cane.   
Lo zucchero sciolto di bava e il tavolo basso completamente glassato.  
Un non meglio identificato liquido semidenso che riempiva la zuccheriera. [m]  

