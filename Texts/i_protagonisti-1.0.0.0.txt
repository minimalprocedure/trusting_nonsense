[title: i protagonisti]

Mancano solo i protagonisti della storia.  
C'è tutto, la casa, la camera e il letto.  

Ci sono tante cose, tutte belle in fila  
in ordine accanto come ricetta per i dolci.  

C'è pure l'odore del prima e quello dopo  
e le lenzuola sporche son cadute di lato. [m]  
