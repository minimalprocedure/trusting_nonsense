[title: lo magistro ovvero la beffa a mezza rima]

Lasciam sopito lo fallo altero  
erto paletto c'ha nulla piega.  

Sfrontato strumento c'lo si porge  
a chiappa bassa e'n su li, ginocchi.  

In piedi è'l viro et omo vero  
col nero anfibio a man lo regge. [m]  
