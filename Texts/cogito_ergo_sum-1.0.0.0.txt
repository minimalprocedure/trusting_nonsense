[title: cogito ergo sum]

Io non scherzo mai, anche se può sembrare.
Io scherzo sempre, anche se può non sembrare.
Io non sono mai ubriaco, anche se può sembrare.
Io posso sembrare innamorato anche se non lo sono.
Io posso non sembrare innamorato, anche se lo sono.

Io ho difficoltà di relazione.

Io sono quello che puoi odiare a morte.
Io sono da perdere, che è meglio.

Io non sono come molti altri.
Io non sono solo, sono isolato.

Io penso, ma questo non mi serve a niente. [m]
