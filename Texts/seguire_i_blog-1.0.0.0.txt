
[title: A volte seguo qualcuno un po' così: a rava. Qualcosa mi interessa sui loro blog, qualcosa mi piace, qualcosa mi fa ben sperare nel marasma della inutilità.]

Segugio del nascosto e del non mostrato
cerco cordicelle inesistenti tra le cose.

Unioni di frammenti di un discorso ampio
anagrammi di lettere da poter riordinare.

A volte ne escono frasi molto lunghe
e altre, piccoli pensieri sconnessi. [m]
