[title: un collage]

Prendo giornali e con calma ritaglio  
mescolo tutto, a riffa nascono storie  

narrano di luoghi e di molte persone  
di così tanti incontri e di abbandoni.  

A una a una le incollo sulla carta  
lo sai com'è, disegnano quella vita.  

Un collage da lasciare, insieme ad altri  
anch'esso sarà poi chiuso nel cassetto. [m]  
