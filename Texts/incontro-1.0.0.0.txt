[title: arrivo, incontro, bacio, partenza]

**L'arrivo**  

  Il rumore, il fumo, si ferma e frena  
  scende con un piccolo saltello, la predella alta.  

  Si china alla caviglia dolorante,  
  qualcuno porge una valigia.  

  Si scosta i capelli, dona un sorriso.  

**L'incontro**  

  Aspetta l'abbandono, di chi non arriva  
  non verrà.  

  Chi si avvicina le porge un aiuto,  
  ha visto una lacrima scendere, asciugata presto.  
 
**Il bacio**  

  Il cameriere che consiglia il vino  
  tra piccole risa e occhi abbassati.  

  Avvicinarsi per la carezza che nasconde,  
  le labbra umide e tristi, segnate di rosso.  

  Nella magia del tocco, quello sfiorare turgido  
  che si lascia portare di sopra, nella stanza sfatta.  
  
**La partenza**  

  Le parole che si dicono arrivederci  
  quelle mani che si svelano l'addio.  

  Il rumore, il fumo, si ferma e frena  
  le porge la valigia, accenna un saluto.  

  Il posto, che è appena libero,  
  mentre lontano una fisarmonica suona.  

Un soldo al musicante, per le belle note. [m]
