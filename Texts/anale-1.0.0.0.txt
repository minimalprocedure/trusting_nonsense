[title: anale]

Questa notte vorrei tornare indietro nel tempo  
con tutti i nodi da stringere, uno dopo l'altro.  

Con le gambe aperte, che non può chiudere  
per ogni tocco là in mezzo, sia piano che forte.  

Con il tutto adatto per addolcire la carne  
sentendo il morbido dentro, prima di entrare. [m]  
