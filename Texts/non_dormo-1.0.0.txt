[title: non dormo]

Correre e non fare in tempo
una chiamata e non so di chi.
Come vorrei che fosse tua.

Non dormo e le tue parole
mi girano nella testa.

Non dormo e mi giro.

Non dormo e sono stanco
come se mi avessi posseduto per ore. [m]
