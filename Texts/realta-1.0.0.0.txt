[title: realtà]

La realtà, va presa per quella che è.  
La si guarda scivolare davanti, come un film.  
Poi si pensa ad altro.  [m]
