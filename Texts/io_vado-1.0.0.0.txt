[title: io vado]

Io vado
ti lascio le mie mani,
ti regalo quanto mi è più caro.

Ti lascio un pensiero, un mio pensiero di te.

Non usarlo, lascialo per quando sarai sola,
per scaldarti un po' in un inverno troppo lungo. [m]
