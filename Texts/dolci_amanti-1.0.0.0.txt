[title: i dolci amanti]

In disparte nella mia vecchiezza   
colgo così, gli attimi scambiati.   

Quei pezzi di pelle mostrati  
e le parole e risposte dei due.  

Nelle tenere passioni, così tanto arse  
che li chiamano poi, l'uno verso l'altra. [m]  

#noircharlotteagain, #instabileatrofia
