[title: competizione]

Non voglio essere in competizione con nessuno, io non competo mai. Mi ritiro se devo competere per avere qualcosa o qualcuno. Sono estremamente esclusivo, non c'entra la gelosia ma solo un forte senso di possesso delle cose o persone a cui tengo. [m]  

