[title: il vedo non vedo]

Quanta saggezza gira nei social  
e quanto tra le righe, si deve scorgere.    

Quante parole sono incastrate a forza   
nel _vedo non vedo_ o nel _la do, oppure no_.    

Nel miele del discount che cola a fiotti   
più di barbabietola melassa, che di bava d'ape. [m]  
