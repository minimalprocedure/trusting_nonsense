[title: il piacere inaspettato]

La intreccerò con un filo di lana
per legarla stretta e immobile.

Avrà aperta solo quella parte 
quella di cui ha paura, che un po' teme.

L'abbandono assurdo al lento ampliare
che la riempie di stupore accolto. [m]
