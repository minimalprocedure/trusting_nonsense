[title: piovono parole]

Apro l'ombrello grande
per ripararmi dalle parole.

Piovono dal cielo portate dal vento
impregnandomi i vestiti. [m]

[A volte le parole che non ci sono sono molto più grandi e importanti di quelle dette. 
La consapevolezza della vicinanza non è mai equiparabile a qualunque discorso si possa mai fare.] [m]
