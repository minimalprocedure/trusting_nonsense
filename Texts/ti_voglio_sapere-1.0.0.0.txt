[title: ti voglio sapere]

Ti voglio sapere, mentre ti aspetti le cose
l'ansia addensata di quel forte desiderio.

La scossa frizzante che percorre il nervo
che sale e che scende senza alcuna pace.

La pancia, che si tende convulsa dentro
il filo umido di pelle che freme e trema. [m]
