[title: altrimenti rimani un imbianchino che lavora male]

Per me l'arte è _concettuale_ e _ragionata_, non mi si può tirare uno schizzo nel muro se non mi dici il _perché_.
