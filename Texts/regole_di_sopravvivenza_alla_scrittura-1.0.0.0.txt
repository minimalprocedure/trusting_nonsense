[title: regole di sopravvivenza alla scrittura]

Non buttare quello che scrivi  
non perdere i momenti, i significati delle parole.  

Carica di senso ogni segno  
scrivi con la carta e con l'inchiostro.  

Poi aspetta che si asciughi. [m]  
