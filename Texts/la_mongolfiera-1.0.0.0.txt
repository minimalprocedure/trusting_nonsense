[title: la mongolfiera]

Traffico di corde, allungo e tiro  
la cesta è pronta, la zavorra anche.  

Il tempo inclemente, il vento alza  
le nuvole all'orizzonte vanno veloci.  

So che così mi perderò in cielo  
a lungo senza meta trascinato  

fino alla fine del mondo. [m]
