[title: dei lacci che ti legano i polsi]

Dal bianco al nero, armoniche languide  
sei piena di sbaffi di poesie mancate.  

Veniamo a noi ed al buon giorno  
dove ancheggi e fletti, avanti indietro.  

Io su in alto che ti vedevo salire  
sporca di fango e degli schizzi. [m]  
