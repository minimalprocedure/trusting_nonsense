[title: i colori mischiati]

I colori che disegno con la mano
si confondono mentre ci passo attraverso

piccole gocce, d'acqua tremolante. [m]
