[title: ti disegnerò il corpo]

Quando vorrei disegnarti il corpo
prendo sempre penna e calamaio.

Nell'intingere la punta nel nero
poi lento il grattare la parola.

La goccia che a volte cade, macchia
e ci tiro quelle linee che ti avvolgono. [m]
