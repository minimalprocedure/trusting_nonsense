[title: ho toccato e perso]

Nella strada della gente che passa
spingendo a lato nello stretto spazio.

Una mano tra tante, quelle della festa
quelle dita che per troppo poco unite

si sfuggono presto dal calore del tocco
anonime e perse al viso, che poi si cerca. [m]
