[title: dai piedi alle labbra]

Ti sfoglio pezzo a pezzo
come un libro antico, rilegato.

Ti leggo dai piedi alle labbra
indugiando sui fianchi tondi.

Lascio per sempre l'ultima pagina
la fine che non voglio trovare. [m]
