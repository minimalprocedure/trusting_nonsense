[title: carissimi]

Cari maschietti che mi seguite.
Mettiamola in chiaro, generalmente non vi seguo e vi prego di capirmi. Ho un certo tropismo per l'essere femminile in tutte le sue manifestazioni e quindi mentre sono disposto a sorbirmi caffè, gatti, cornetti e cappuccini da loro non lo sono con voi. Anche riguardo a culi e tette, fiche e cazzi e smielamenti vari preferisco loro pur rimanendo le stesse immagini che vorticano e non più girano sul tumblr.

Per voi è ben più ardua questione, se scrivete qualcosa di interessante potrebbe anche essere che una capatina la faccia, ma se già nell'aulico testo cominciassi a trovare parole e frasi come: cappella e scappellare, orifizio più impercettibile, ingroppare e alluvione di sperma... Siete alla frutta, pera mela o cocomero fate voi.

Insomma sta roba la scrivo pure io e se mi permettete, nella media ponderata della minchiata porno sono un po' meglio di voi. Tanto vale che mi legga la mia.

Per il resto seguitemi pure. [m]
