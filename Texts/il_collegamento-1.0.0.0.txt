[title: il collegamento]

**[anonimo]**  
Qual è il collegamento fra il bel viso e l'episodio della pecora bollita?


**[m]**    
Lo sa, caro il mio _anonimo_ affezionato, che è una domanda interessante?  
Quale collegamento tra viso e pecora, fosse tra viso e chiappa sarebbe più facile ma così diventa complicato.

Intanto fermo subito le polemiche: ancora parli di chiappe?  
A mia discolpa vi dico che lo so, sono tediante nell'argomento, ma vi giuro che non ci posso fare niente: viso e chiappe sono i miei punti di attrazione in una donna. Non le tette, di cui in genere apprezzo le piccole dimensioni o il _pelo_, oggi sempre più scarso; no, prima il viso e poi le chiappe. Un bel viso è fondamentale e poi delle belle chiappe attaccate a una schiena. 

Però stiamo divagando vero?  

Torniamo alla pecora collegata al viso. Ovviamente non è correlato alle gentili signore nerborute della piccola storia. No, si parla di un viso così, apparso fugacemente nei ricordi del tempo che fu; un ricordo leggermente nostalgico di una terra dove ho abitato per qualche anno. Il raccontino, è un fatto reale che ho scritto per un altro viso qualche tempo fa.
Come vede sono pieno di visi che sono come fantasmi o presenze che mi accompagnano sempre, ricordi riposti in ordine nella mia borsa. Io trovo i ricordi importanti, come il nuovo che viene, senza rancori irrisolti o melanconiche insoddisfazioni.

Come ho detto poco fa, la mia bestia ha la porta aperta e può uscire quando vuole, perché non ha desideri mancati e il futuro sarà solo nuove scoperte.  

Un viso e una pecora bollita, perché no?  
È il flusso dei pensieri che mi ricorda che sono vivo, nonostante allora volessi morire. [m]
