[title: con la lingua bagnata]

Nel porgere la tua voglia segreta
lo scopri con le mani e lo tendi.

Lo vedo muovere appena
prima di spingermi dentro

con la mia lingua bagnata, 
quella con cui mi tocco il naso. [m]
