[title: le sere che sei via]

Ti racconto, quando tutte le sere sei via  
quello che non so dire quando ti vedo.  

Impossibile carezza sul tuo corpo vero  
abbraccio l'aria dove non sei mai passata. [m]  
