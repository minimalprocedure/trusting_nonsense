[title: sulle punte delle dita]

Niente tempo per aspettare
nessun ritardo ancora, di troppo.

Scosta ogni cosa per farti avere.
Dai colpi, presa

mentre ti appoggi sulle punte. [m]
