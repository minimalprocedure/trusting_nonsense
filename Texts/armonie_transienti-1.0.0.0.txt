[title: armonie transienti]

Rumori casuali, transienti  
tra onde bianche e onde rosa.  

Mescolo suoni a creare armonie  
aleatorie mescolanze cromatiche  
e di stocastiche passioni. [m]  
