#!/bin/bash

VERSION=$(git log -1 --pretty=%B)

git checkout --orphan temp_branch
git add -A
git commit -am "$VERSION"
git branch -D master
git branch -m master
git push --set-upstream -f origin master
