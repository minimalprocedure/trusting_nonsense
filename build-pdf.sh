#!/bin/bash

ebook-convert \
./build/trusting_nonsense.epub \
./build/trusting_nonsense-temp.pdf \
--transform-html-rules ./build-pdf-html-rules.txt \
--pdf-default-font-size 14 \
--pdf-mono-font-size  11 \
--pdf-mono-family "Iosevka Extended" \
--pdf-sans-family "Iosevka Aile" \
--pdf-serif-family "Iosevka Etoile" \
--pdf-standard-font sans \
--toc-title Indice \
--pdf-footer-template "<footer style=\"justify-content: space-between; font-size: 12px; border-top: 1px solid\"><div>_TITLE_</div><div>pag: _PAGENUM_/_TOTAL_PAGES_</div></footer>"

cpdf -squeeze ./build/trusting_nonsense-temp.pdf -o ./build/trusting_nonsense.pdf

rm ./build/trusting_nonsense-temp.pdf
